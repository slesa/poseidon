Feature: BonList verarbeitet neuen Bon

Scenario: neuer Bon trifft in der BonListe ein ohne AllBons ohne AutoFill
	Given Alle Bons anzeigen ist aus
	And Bons automatisch hinzufügen ist aus
	When der Bon verarbeitet wurde
	Then wird der Bon in der BonListe angezeigt

Scenario: neuer Bon trifft in der BonListe ein mit AllBons ohne AutoFill
	Given Alle Bons anzeigen ist an
	And Bons automatisch hinzufügen ist aus
	When der Bon verarbeitet wurde
	Then wird der Bon in der BonListe angezeigt

Scenario: neuer Bon trifft in der BonListe ein ohne AllBons mit AutoFill
	Given Alle Bons anzeigen ist aus
	And Bons automatisch hinzufügen ist an
	When der Bon verarbeitet wurde
	Then wird der Bon nicht in der BonListe angezeigt

Scenario: neuer Bon trifft in der BonListe ein mit AllBons mit AutoFill
	Given Alle Bons anzeigen ist an
	And Bons automatisch hinzufügen ist an
	When der Bon verarbeitet wurde
	Then wird der Bon in der BonListe angezeigt
