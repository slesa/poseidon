# This is a sample .feature file
# Squish feature files use the Gherkin language for describing features, a short example
# is given below. You can find a more extensive introduction to the Gherkin format at
# https://cucumber.io/docs/gherkin/reference/
Feature: BonList View

    Some textual description of the business value of this feature goes
    here. The text is free-form.

    The description can span multiple paragraphs.

    Scenario: Wenn ein Bon verarbeitet wird

        Given Alle Bons in der Bonliste anzeigen ist true
          And ein Bon in die Datenbank geschrieben wird
         When Server und Client gestartet sind
         Then soll der Bon angezeigt werden

    Scenario: Wenn ein zweiter Bon verarbeitet wird

        Given Alle Bons in der Bonliste anzeigen ist true
          And ein Bon in die Datenbank geschrieben wird
          And ein zweiter Bon in die Datenbank geschrieben wird
         When Server und Client gestartet sind
         Then sollen beide Bons angezeigt werden
