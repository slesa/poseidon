# -*- coding: utf-8 -*-

# A quick introduction to implementing scripts for BDD tests:
#
# This file contains snippets of script code to be executed as the .feature
# file is processed. See the section 'Behaviour Driven Testing' in the 'API
# Reference Manual' chapter of the Squish manual for a comprehensive reference.
#
# The decorators Given/When/Then/Step can be used to associate a script snippet
# with a pattern which is matched against the steps being executed. Optional
# table/multi-line string arguments of the step are passed via a mandatory
# 'context' parameter:
#
#   @When("I enter the text")
#   def whenTextEntered(context):
#      <code here>
#
# The pattern is a plain string without the leading keyword, but a couple of
# placeholders including |any|, |word| and |integer| are supported which can be
# used to extract arbitrary, alphanumeric and integer values resp. from the
# pattern; the extracted values are passed as additional arguments:
#
#   @Then("I get |integer| different names")
#   def namesReceived(context, numNames):
#      <code here>
#
# Instead of using a string with placeholders, a regular expression can be
# specified. In that case, make sure to set the (optional) 'regexp' argument
# to True.

import names

@Step("Alle Bons in der Bonliste anzeigen ist true")
def Alle_Bons_in_der_Bonliste_zeigen_true(context):
    pass

@Step("ein Bon in die Datenbank geschrieben wird")
def Ein_Bon_in_die_DB_schreiben(context):
    pass

@Step("ein zweiter Bon in die Datenbank geschrieben wird")
def Ein_zweiten_Bon_in_die_DB_schreiben(context):
    pass

@Step("Server und Client gestartet sind")
def Server_und_client_starten(context):
    startApplication("BonMonitorServer.Asp")
    startApplication("BonClient")
    #tartApplication("/home/jpreiss/work/gitlab/Poseidon/bin/deploy/desktop/")
    #rtApplication("/home/jpreiss/work/42/bonviewer/src/BonMonitorServer.Asp/bin/Debug/net5.0/BonMonitorServer.Asp.dll")
    pass

@Step("soll der Bon angezeigt werden")
def soll_der_Bon_angezeigt_werden(context):
    pass

@Step("sollen beide Bons angezeigt werden")
def soll_der_Bon_angezeigt_werden(context):
    pass
