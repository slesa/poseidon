cmake_minimum_required(VERSION 3.16)
project(lib)

add_subdirectory(spectest)
add_subdirectory(basics)
add_subdirectory(basics.specs)
