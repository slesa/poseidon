#pragma once

#ifdef Q_OS_WIN32
#include <windows.h>
#define _sleep Sleep
#else
#include <unistd.h>
#define _sleep usleep
#endif
