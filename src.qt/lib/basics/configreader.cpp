#include "configreader.h"

namespace Basics {

ConfigReader::ConfigReader(const QString& fileName, QObject *parent)
    : QSettings(fileName, QSettings::IniFormat, parent)
{
}

QVariant ConfigReader::value(const QString& section, const QString& key, const QVariant& def)
{
    return QSettings::value(section+'/'+key, def);
}
void ConfigReader::setValue(const QVariant& value, const QString& section, const QString& key)
{
    QSettings::setValue(section+'/'+key, value);
}

}
