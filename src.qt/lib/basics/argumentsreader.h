#ifndef ARGUMENTREADER_H
#define ARGUMENTREADER_H
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QList>
#include <QObject>

namespace Basics {

    class ArgumentSupplier
    {
    public:
        virtual QStringList arguments() const = 0;
    };

    class DefaultArgumentSupplier: public ArgumentSupplier
    {
    public:
        virtual QStringList arguments() const;
    };

    class SettingsReader : public QObject
    {
//        Q_OBJECT
    public:
        virtual QList<QCommandLineOption> options() const = 0;
        virtual QString readArguments(QCommandLineParser& parser) = 0;
    };

    enum ArgumentsTarget { Gui, Console, Test };

    class ArgumentsReader: public QObject
    {
        Q_OBJECT
    public:
        ArgumentsReader(ArgumentsTarget target=Console, QObject* parent=nullptr);
        void setNoConsoleOutput(ArgumentsTarget target=Gui) { _target = target; }
        void readArguments(SettingsReader& reader, const ArgumentSupplier& argSupplier = DefaultArgumentSupplier());
    signals:
        void helpText(QString text);
    private:
        void outputMessage(const QString& msg);
        ArgumentsTarget _target;
    };
}

using namespace Basics;

#endif // ARGUMENTREADER_H
