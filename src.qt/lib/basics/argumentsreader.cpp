#include "argumentsreader.h"

namespace Basics {

ArgumentsReader::ArgumentsReader(ArgumentsTarget target, QObject* parent)
    : QObject(parent)
    , _target(target)
{
}

void ArgumentsReader::readArguments(SettingsReader& reader, const ArgumentSupplier& argSupplier)
{
    QCommandLineParser parser;
    auto helpOption = parser.addHelpOption();
    parser.addOptions(reader.options());
    parser.setSingleDashWordOptionMode(QCommandLineParser::ParseAsLongOptions);

    auto arguments = argSupplier.arguments();
    if( !parser.parse(arguments)) {
        auto msg = parser.helpText() + '\n' + parser.errorText();
        outputMessage(msg);
        if( _target==Console ) exit(1);
    }
    if(parser.isSet(helpOption)) {
        auto msg = parser.helpText();
        outputMessage(msg);
        if( _target==Console ) exit(0);
    }
    auto result = reader.readArguments(parser);
    if( !result.isEmpty() ) {
        auto msg = result + '\n' + parser.helpText();
        outputMessage(msg);
        if( _target==Console ) exit(2);
    }
}

void ArgumentsReader::outputMessage(const QString& msg)
{
    if (_target==Gui)
        emit helpText(msg);
    if (_target==Console)
        qWarning() << msg;
}

QStringList DefaultArgumentSupplier::arguments() const
{
    return QCoreApplication::arguments();
}

}
