#ifndef CONFIGREADER_H
#define CONFIGREADER_H

#include <QSettings>

namespace Basics {

    class ConfigReader : public QSettings
    {
    public:
        explicit ConfigReader(const QString& fileName, QObject *parent = nullptr);
        QVariant value(const QString& key)
        {
            return QSettings::value(key);
        }
        QVariant value(const QString& section, const QString& key, const QVariant& def=QVariant());
        void setValue(const QVariant& value, const QString& section, const QString& key);
    private:
        Q_DISABLE_COPY(ConfigReader)
    };
}

using namespace Basics;

#endif // CONFIGREADER_H
