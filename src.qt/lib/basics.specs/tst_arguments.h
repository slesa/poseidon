#ifndef TST_ARGUMENTS_H
#define TST_ARGUMENTS_H
#include "basics/argumentsreader.h"
#include <QObject>

class QSignalSpy;

class When_setting_help_as_gui_argument : public QObject
{
    Q_OBJECT
private slots:
    void init();
    void cleanup();
    void it_should_send_signal();
private:
    ArgumentsReader _sut;
    QSignalSpy* _spy;
};

class When_setting_help_as_console_argument : public QObject
{
    Q_OBJECT
private slots:
    void init();
    void cleanup();
    void it_should_send_no_signal();
private:
    ArgumentsReader _sut;
    QSignalSpy* _spy;
};

class When_setting_unknown_argument : public QObject
{
    Q_OBJECT
private slots:
    void init();
    void cleanup();
    void it_should_send_signal();
private:
    ArgumentsReader _sut;
    QSignalSpy* _spy;
};

class When_reading_arguments_with_port_below_0 : public QObject
{
    Q_OBJECT
private slots:
    void init();
    void it_should_not_read_port();
    void it_should_send_msg();
    void it_should_send_also_hint();
    void onHelpText(QString msg) { _msg = msg; }
private:
    ArgumentsReader _sut;
    QString _msg;
    int _port;
};

class When_reading_arguments : public QObject
{
    Q_OBJECT
private slots:
    void init();
    //void initTestCase();
    //void cleanupTestCase();
    void it_should_read_host();
    void it_should_read_port();
private:
    ArgumentsReader _sut;
    QString _host;
    int _port;
};

#endif // TST_ARGUMENTS_H
