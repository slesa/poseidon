#ifndef TST_CONFIGS_H
#define TST_CONFIGS_H
#include "basics/configreader.h"
#include <QObject>

class When_reading_config : public QObject {
    Q_OBJECT

private slots:
    void initTestCase();
    void cleanupTestCase();
    void it_should_leave_organization_empty();
    void it_should_leave_pplication_empty();

private:
    ConfigReader* _sut;
};


class When_reading_config_file : public QObject {
    Q_OBJECT

private slots:
    void initTestCase();
    void cleanupTestCase();
    void soll_ein_string_gelesen_werden();
    void soll_ein_int_gelesen_werden();
    void soll_ein_bool_gelesen_werden();

#ifndef Q_OS_WIN
    void soll_gross_klein_in_der_Section_untterschieden_werden();
    void soll_gross_klein_im_Key_untterschieden_werden();
#endif
    void soll_eine_Gruppe_gelesen_werden();
    void sollen_die_Werte_einer_Gruppe_gelesen_werden();

private:
    ConfigReader* _sut;
};

#endif // TST_CONFIGS_H
