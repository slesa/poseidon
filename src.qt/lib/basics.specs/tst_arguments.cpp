#include "tst_arguments.h"
#include <QtTest>
#include <QList>
#include <QCommandLineOption>
#include "spectest/testcollector.h"


class TestSettings
{
public:
    TestSettings() {
        host = "localhost";
        port = 4711;
    }
    QString host;
    int port;
};

class TestSettingsReader: public SettingsReader
{
public:
    TestSettingsReader()
        : hostOption("host", ("The network adress or name of the bonmonitor server"), settings.host)
        , portOption("port", ("The network port of the bonmonitor server"), QString::number(settings.port))
    {
    }
    QList<QCommandLineOption> options() const
    {
        return QList<QCommandLineOption>() << hostOption << portOption;
    }
    QString readArguments(QCommandLineParser& parser)
    {
        if(parser.isSet(hostOption))
            settings.host = parser.value(hostOption);
        if(parser.isSet(portOption)) {
            auto ok = true;
            auto port = parser.value(portOption).toInt(&ok);
            if( !ok || port<1 || port>65535 ) {
                auto msg = "No valid port given. It should be a number between 1 and 65535";
                return msg;
            }
            settings.port = port;
        }
        return QString();
    }
    TestSettings settings;
private:
    QCommandLineOption hostOption;
    QCommandLineOption portOption;
};

class TestArgumentSupplier: public ArgumentSupplier
{
public:
    TestArgumentSupplier(QStringList arguments)
        : _arguments(QStringList() << "TestApp" << arguments)
    {
    }
    TestArgumentSupplier()
        : _arguments(QStringList() << "TestApp")
    {
    }
    virtual QStringList arguments() const override
    {
        return _arguments;
    }
private:
    QStringList _arguments;
};


void When_setting_help_as_gui_argument::init()
{
    _sut.setNoConsoleOutput(Gui);
    _spy = new QSignalSpy( &_sut, SIGNAL(helpText(QString)) );
}
void When_setting_help_as_gui_argument::cleanup()
{
    delete _spy;
}
void When_setting_help_as_gui_argument::it_should_send_signal()
{
    TestSettingsReader sr;
    TestArgumentSupplier as(QStringList()<<"-help");
    _sut.readArguments(sr, as);
    auto signalCount = _spy->count();
    QCOMPARE( signalCount, 1 );
}
ADD_TEST(Arguments_Gui, When_setting_help_as_gui_argument)


void When_setting_help_as_console_argument::init()
{
    _sut.setNoConsoleOutput(Test);
    _spy = new QSignalSpy( &_sut, SIGNAL(helpText(QString)) );
}
void When_setting_help_as_console_argument::cleanup()
{
    delete _spy;
}
void When_setting_help_as_console_argument::it_should_send_no_signal()
{
    TestSettingsReader sr;
    TestArgumentSupplier as(QStringList()<<"-help");
    _sut.readArguments(sr, as);
    auto signalCount = _spy->count();
    QCOMPARE( signalCount, 0 );
}
ADD_TEST(Arguments_Console, When_setting_help_as_console_argument)


void When_setting_unknown_argument::init()
{
    _sut.setNoConsoleOutput(Gui);
    _spy = new QSignalSpy( &_sut, SIGNAL(helpText(QString)) );
}
void When_setting_unknown_argument::cleanup()
{
    delete _spy;
}
void When_setting_unknown_argument::it_should_send_signal()
{
    TestSettingsReader sr;
    TestArgumentSupplier as(QStringList()<<"-unknown");
    _sut.readArguments(sr, as);
    auto signalCount = _spy->count();
    QCOMPARE( signalCount, 1 );
}
ADD_TEST(Arguments_Unknown_Args, When_setting_unknown_argument)


void When_reading_arguments_with_port_below_0::init()
{
    TestSettingsReader sr;
    TestArgumentSupplier as(QStringList()<<"-port=-3");
    connect(&_sut, &ArgumentsReader::helpText, this, &When_reading_arguments_with_port_below_0::onHelpText);
    _sut.setNoConsoleOutput(Gui);
    _sut.readArguments(sr, as);
    _port = sr.settings.port;
}
void When_reading_arguments_with_port_below_0::it_should_not_read_port()
{
    QCOMPARE( _port, 4711 );
}
void When_reading_arguments_with_port_below_0::it_should_send_msg()
{
    QVERIFY( _msg.contains("No valid port given"));
}
void When_reading_arguments_with_port_below_0::it_should_send_also_hint()
{
    QVERIFY( _msg.contains("The network adress"));
}
ADD_TEST(Arguments_Specific_Arg, When_reading_arguments_with_port_below_0)


void When_reading_arguments::init()
{
    TestSettingsReader sr;
    TestArgumentSupplier as(QStringList()<<"-host=alter-schwede.de" << "-port=313");
    _sut.readArguments(sr, as);
    _host = sr.settings.host;
    _port = sr.settings.port;
}
void When_reading_arguments::it_should_read_host()
{
    QCOMPARE( _host, "alter-schwede.de" );
}
void When_reading_arguments::it_should_read_port()
{
    QCOMPARE( _port, 313 );
}
ADD_TEST(Arguments_Read, When_reading_arguments)


#include "tst_arguments.moc"
