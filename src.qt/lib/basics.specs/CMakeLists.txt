cmake_minimum_required(VERSION 3.16)
set(PROJECT_NAME basicsSpecs)
project(${PROJECT_NAME})

include(../CMakeLists.inc)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOMOC ON)

find_package(Qt6 COMPONENTS Core Test)

# file(GLOB SOURCES ${PROJECT_SOURCE_DIR}/*.cpp)
# file(GLOB HEADERS ${PROJECT_SOURCE_DIR}/*.h)
add_executable(${PROJECT_NAME}  # ${SOURCES} ${HEADERS})
  tst_arguments.h tst_arguments.cpp
  tst_configs.h tst_configs.cpp
)

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/inifile.ini
    DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
add_custom_command(
  TARGET ${PROJECT_NAME} POST_BUILD
  COMMAND ${CMAKE_COMMAND} -E copy 
    ${CMAKE_CURRENT_SOURCE_DIR}/inifile.ini 
    ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

target_link_libraries(${PROJECT_NAME} PRIVATE basics spectest Qt::Test)

enable_testing()
