#include "tst_configs.h"
#include <QtTest>
#include "basics/configreader.h"
#include "spectest/testcollector.h"


void When_reading_config::initTestCase() {
    _sut = new ConfigReader("inifile.ini");
}
void When_reading_config::cleanupTestCase() {
    delete _sut;
}
void When_reading_config::it_should_leave_organization_empty() {
    QCOMPARE(_sut->organizationName(), QString());
}
void When_reading_config::it_should_leave_pplication_empty() {
    QCOMPARE(_sut->applicationName(), QString());
}
ADD_TEST(ReadConfig, When_reading_config)



void When_reading_config_file::initTestCase() {
    _sut = new ConfigReader("inifile.ini");
}
void When_reading_config_file::cleanupTestCase() {
    delete _sut;
}
void When_reading_config_file::soll_ein_string_gelesen_werden() {
    auto logFile = _sut->value("settings", "logfile").toString();
    QCOMPARE(logFile, "var/log/inifile.log");
}
void When_reading_config_file::soll_ein_int_gelesen_werden() {
    auto terminal = _sut->value("settings", "terminal", 2).toInt();
    QCOMPARE(terminal, 1);
}
void When_reading_config_file::soll_ein_bool_gelesen_werden() {
    auto showGui = _sut->value("settings", "showgui", false).toBool();
    QCOMPARE(showGui, true);
}
#ifndef Q_OS_WIN
void When_reading_config_file::soll_gross_klein_in_der_Section_untterschieden_werden() {
    auto terminal = _sut->value("Settings", "terminal", 2).toInt();
    QCOMPARE(terminal, 2);
}
void When_reading_config_file::soll_gross_klein_im_Key_untterschieden_werden() {
    auto terminal = _sut->value("settings", "Terminal", 2).toInt();
    QCOMPARE(terminal, 2);
}
#endif
void When_reading_config_file::soll_eine_Gruppe_gelesen_werden() {
    _sut->beginGroup("devices");
    auto keys = _sut->allKeys();
    _sut->endGroup();
    QCOMPARE(keys.count(), 3);
}
void When_reading_config_file::sollen_die_Werte_einer_Gruppe_gelesen_werden() {
    QMap<QString, QString> map;
    _sut->beginGroup("devices");
    auto keys = _sut->allKeys();
    for(auto key: keys)
        map.insert(key, _sut->value(key).toString());
    _sut->endGroup();
    QCOMPARE(map["posserial"], "etc/dallas.ini");
    QCOMPARE(map["possocket"], "etc/lsr.ini");
    QCOMPARE(map["posopos"], "etc/fiscal.ini");
}
ADD_TEST(ReadConfigFile, When_reading_config_file)


#include "tst_configs.moc"
