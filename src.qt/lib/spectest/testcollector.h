#pragma once
#include <map>
#include <memory>
#include <QtTest>
#include <QObject>

namespace SpecTests {
    // Taken from https://stackoverflow.com/questions/12194256/qt-how-to-organize-unit-test-with-more-than-one-class

    #define ADD_TEST(name, className) static SpecTests::UnitTestClass<className> name(#className);

    typedef std::map<std::string, std::shared_ptr<QObject> > TestList;
    inline TestList& GetTestList()
    {
        static TestList list;
        return list;
    }

    inline int RunAllTests(int argc, char **argv) {
        int result = 0;
        for (const auto&i:GetTestList()) {
            result += QTest::qExec(i.second.get(), argc, argv);
        }
        return result;
    }

    template <class T>
    class UnitTestClass {
    public:
        explicit UnitTestClass(const std::string& pTestName) {
            auto& testList = SpecTests::GetTestList();
            if (0==testList.count(pTestName)) {
                testList.insert(std::make_pair(pTestName, std::make_shared<T>()));
            }
        }
    };
}

using namespace SpecTests;
