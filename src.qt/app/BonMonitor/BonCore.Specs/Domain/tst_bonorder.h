#ifndef TST_BONORDER_H
#define TST_BONORDER_H
#include "BonCore/Domain/bonorder.h"
#include <QObject>

class When_creating_bonorder : public QObject {
    Q_OBJECT

private slots:
    void init();
    void cleanup();
    void it_should_set_id_zero();
    void it_should_set_archived_false();
    void it_should_set_bonid_zero();
    void it_should_set_entryguid_empty();
    void it_should_set_orderedtime_empty();
    void it_should_set_waiter_zero();
    void it_should_set_waitername_empty();
    void it_should_set_plu_zero();
    void it_should_set_article_empty();
    void it_should_set_count_zero();
    void it_should_set_ishint_false();
    void it_should_set_family_zero();
    void it_should_set_familyname_empty();
    void it_should_set_group_zero();
    void it_should_set_groupname_empty();
    void it_should_set_round_zero();
    void it_should_set_coursecontrol_zero();
    void it_should_set_monitors_empty();

private:
    BonOrder* _sut;
};

#endif // TST_BON_H
