#include "tst_bon.h"
#include <QtTest>
#include "spectest/testcollector.h"


void When_creating_bon::init() {
    _sut = new Bon();
}
void When_creating_bon::cleanup() {
    delete _sut;
}
void When_creating_bon::it_should_set_id_zero() {
    QCOMPARE(_sut->id(), 0);
}
void When_creating_bon::it_should_set_archived_false() {
    QCOMPARE(_sut->isArchived(), false);
}
void When_creating_bon::it_should_set_table_zero() {
    QCOMPARE(_sut->table(), 0);
}
void When_creating_bon::it_should_set_party_zero() {
    QCOMPARE(_sut->party(), 0);
}
void When_creating_bon::it_should_set_tableguid_empty() {
    QCOMPARE(_sut->tableGuid(), QString());
}
void When_creating_bon::it_should_set_creationtime_empty() {
    QCOMPARE(_sut->creationTime(), QDateTime());
}
void When_creating_bon::it_should_set_waiter_zero() {
    QCOMPARE(_sut->waiter(), 0);
}
void When_creating_bon::it_should_set_waiername_empty() {
    QCOMPARE(_sut->waiterName(), QString());
}
ADD_TEST(CreateBon, When_creating_bon)


#include "tst_bon.moc"
