#include "tst_bonorder.h"
#include <QtTest>
#include "spectest/testcollector.h"


void When_creating_bonorder::init() {
    _sut = new BonOrder();
}
void When_creating_bonorder::cleanup() {
    delete _sut;
}
void When_creating_bonorder::it_should_set_id_zero() {
    QCOMPARE(_sut->id(), 0);
}
void When_creating_bonorder::it_should_set_archived_false() {
    QCOMPARE(_sut->isArchived(), false);
}
void When_creating_bonorder::it_should_set_bonid_zero() {
    QCOMPARE(_sut->bonId(), 0);
}
void When_creating_bonorder::it_should_set_entryguid_empty() {
    QCOMPARE(_sut->entryGuid(), QString());
}
void When_creating_bonorder::it_should_set_orderedtime_empty() {
    QCOMPARE(_sut->orderedTime(), QDateTime());
}
void When_creating_bonorder::it_should_set_waiter_zero() {
    QCOMPARE(_sut->waiter(), 0);
}
void When_creating_bonorder::it_should_set_waitername_empty() {
    QCOMPARE(_sut->waiterName(), QString());
}
void When_creating_bonorder::it_should_set_plu_zero() {
    QCOMPARE(_sut->plu(), 0);
}
void When_creating_bonorder::it_should_set_article_empty() {
    QCOMPARE(_sut->article(), QString());
}
void When_creating_bonorder::it_should_set_count_zero() {
    QCOMPARE(_sut->count(), 0);
}
void When_creating_bonorder::it_should_set_ishint_false() {
    QCOMPARE(_sut->isHint(), false);
}
void When_creating_bonorder::it_should_set_family_zero() {
    QCOMPARE(_sut->family(), 0);
}
void When_creating_bonorder::it_should_set_familyname_empty() {
    QCOMPARE(_sut->familyName(), QString());
}
void When_creating_bonorder::it_should_set_group_zero() {
    QCOMPARE(_sut->familyGroup(), 0);
}
void When_creating_bonorder::it_should_set_groupname_empty() {
    QCOMPARE(_sut->familyGroupName(), QString());
}
void When_creating_bonorder::it_should_set_round_zero() {
    QCOMPARE(_sut->round(), 0);
}
void When_creating_bonorder::it_should_set_coursecontrol_zero() {
    QCOMPARE(_sut->courseControl(), 0);
}
void When_creating_bonorder::it_should_set_monitors_empty() {
    QCOMPARE(_sut->bonMonitors().count(), 0);
}
ADD_TEST(CreateBonOrder, When_creating_bonorder)


#include "tst_bonorder.moc"
