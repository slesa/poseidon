#ifndef TST_BON_H
#define TST_BON_H
#include "BonCore/Domain/bon.h"
#include <QObject>

class When_creating_bon : public QObject {
    Q_OBJECT

private slots:
    void init();
    void cleanup();
    void it_should_set_id_zero();
    void it_should_set_archived_false();
    void it_should_set_table_zero();
    void it_should_set_party_zero();
    void it_should_set_tableguid_empty();
    void it_should_set_creationtime_empty();
    void it_should_set_waiter_zero();
    void it_should_set_waiername_empty();

private:
    Bon* _sut;
};

#endif // TST_BON_H
