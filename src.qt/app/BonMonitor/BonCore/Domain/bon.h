#ifndef BON_H
#define BON_H
#include "bonorder.h"
#include <QObject>
#include <QDateTime>

namespace BonCore::Domain {

    class Bon : public QObject
    {
        /* Q_OBJECT
        Q_PROPERTY(quint64 id READ id WRITE setId)
        Q_PROPERTY(bool isArchived READ isArchived WRITE setIsArchived)
        Q_PROPERTY(int table READ table WRITE setTable)
        Q_PROPERTY(int party READ party WRITE setParty)
        Q_PROPERTY(QString tableGuid READ tableGuid WRITE setTableGuid)
        Q_PROPERTY(QDateTime creationTime READ creationTime WRITE setCreationTime)
        Q_PROPERTY(int waiter READ waiter WRITE setWaiter)
        Q_PROPERTY(QString waiterName READ waiterName WRITE setWaiterName)*/

    public:
        explicit Bon(QObject *parent = nullptr);
        quint64 id() const { return _id; }
        void setId(quint64 id) { _id = id; }
        bool isArchived() const { return _isArchived; }
        void setIsArchived(bool flag) { _isArchived = flag; }
        int table() const { return _table; }
        void setTable(int table) { _table = table; }
        int party() const { return _party; }
        void setParty(int party) { _party = party; }
        QString tableGuid() const { return _tableGuid; }
        void setTableGuid(QString value) { _tableGuid = value; }
        const QDateTime creationTime() const { return _creationTime; }
        void setCreationTime(const QDateTime& value) { _creationTime = value; }
        int waiter() const { return _waiter; }
        void setWaiter(int waiter) { _waiter = waiter; }
        const QString waiterName() const { return _waiterName; }
        void setWaiterName(QString waiter) { _waiterName = waiter; }
        int center() const { return _center; }
        void setCenter(int center) { _center = center; }
        const QString centerName() const { return _centerName; }
        void setCenterName(QString center) { _centerName = center; }

        QList<BonOrder*> orders;

    private:
        quint64 _id;
        bool _isArchived;
        int _table;
        int _party;
        QString _tableGuid;
        QDateTime _creationTime;
        int _waiter;
        QString _waiterName;
        int _center;
        QString _centerName;
    };

}

using namespace BonCore::Domain;

#endif // BON_H
