#ifndef BONEVENT_H
#define BONEVENT_H
#include <QObject>
#include <QString>

enum BonEventType
{
    None = 0,
    VoidOrder = 1,
    SplitOrder = 2,
    GangAbruf = 3,
    Nachbuchung = 4,
    TransferTable = 5, // Tischwechsel
    VipGuest = 6,

    MoveToMonitor = 10,

    // BonStart = 20, gibt es nicht. Ein Bon wird gestartet, wenn ein Gang gestartet wird, der zu ihm gehört
    CourseStart = 20,
    // BonDone = 21, gibt es nicht. Ein Bon ist beendet, wenn alle seine Gänge beendet worden sind. Aktion selbst ist ein Makro
    CourseDone = 21,

    ItemsDone = 30,
    ItemsUndo = 31,

    BonDone = 40,
    BonDrop = 41,

    BonActivate = 60, // Aus der  Bon- in die Workliste
    BonBack = 61, // Aus der Work- zurück in die Bonliste
    BonFinish = 62,

    OpenOrCloseCourse = 80,
    SwapTwoCells = 81,
    MoveCells = 82,

    OrderArchived = 90,
    BonArchived = 91,
};

class BonEvent // : public QObject
{
    // Q_OBJECT
public:
    explicit BonEvent(BonEventType type); //, QObject *parent = nullptr);
    explicit BonEvent(quint64 id); //, QObject *parent = nullptr);
    quint64 id() const { return _id; }
    void setId(quint64 id) { _id = id; }
    bool isArchived() const { return _isArchived; }
    void setIsArchived(bool flag) { _isArchived = flag; }
    BonEventType type() { return _type; }
    void setType(BonEventType type) { _type = type; }
    int sourceMonitor() { return _sourceMonitor; }
    void setSourceMonitor(int value) { _sourceMonitor = value; }
    int  bonId() { return _bonId; }
    void setBonId(int id) { _bonId = id; }
    QString payload() { return _payload; }
    void setPayload(QString value) { _payload = value; }

private:
    int _id;
    bool _isArchived;
    BonEventType _type;
    int _sourceMonitor;
    int _bonId;
    QString _payload;
};

#endif // BONEVENT_H
