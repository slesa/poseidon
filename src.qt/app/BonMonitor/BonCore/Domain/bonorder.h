#ifndef BONORDER_H
#define BONORDER_H
#include <QDateTime>
#include <QObject>

namespace BonCore::Domain {

    class BonOrder : public QObject
    {
        /* Q_OBJECT
        Q_PROPERTY(int id READ id WRITE setId)
        Q_PROPERTY(bool isArchived READ isArchived WRITE setIsArchived)
        Q_PROPERTY(int bonId READ bonId WRITE setBonId)
        Q_PROPERTY(QString entryGuid READ entryGuid WRITE setEntryGuid)
        Q_PROPERTY(QDateTime orderedTime READ orderedTime WRITE setOrderedTime)
        Q_PROPERTY(int waiter READ waiter WRITE setWaiter)
        Q_PROPERTY(QString waiterName READ waiterName WRITE setWaiterName)
        Q_PROPERTY(int plu READ plu WRITE setPlu)
        Q_PROPERTY(QString article READ article WRITE setArticle)
        Q_PROPERTY(int count READ count WRITE setCount)
        Q_PROPERTY(bool isHint READ isHint WRITE setIsHint)
        Q_PROPERTY(int family READ family WRITE setFamily)
        Q_PROPERTY(QString familyName READ familyName WRITE setFamilyName)
        Q_PROPERTY(int familyGroup READ familyGroup WRITE setFamilyGroup)
        Q_PROPERTY(QString familyGroupName READ familyGroupName WRITE setFamilyGroupName)
        Q_PROPERTY(int round READ round WRITE setRound)
        Q_PROPERTY(int courseControl READ courseControl WRITE setCourseControl)
        Q_PROPERTY(QStringList bonMonitors READ bonMonitors WRITE setBonMonitors) */

    public:
        explicit BonOrder(QObject *parent = nullptr);
        int id() const { return _id; }
        void setId(int id) { _id = id; }
        bool isArchived() const { return _isArchived; }
        void setIsArchived(bool flag) { _isArchived = flag; }
        int bonId() const { return _bonId; }
        void setBonId(int bonid) { _bonId = bonid; }
        QString entryGuid() const { return _entryGuid; }
        void setEntryGuid(const QString& value) { _entryGuid = value; }
        const QDateTime orderedTime() const { return _orderedTime; }
        void setOrderedTime(const QDateTime& value) { _orderedTime = value; }
        int waiter() const { return _waiter; }
        void setWaiter(int waiter) { _waiter = waiter; }
        const QString waiterName() const { return _waiterName; }
        void setWaiterName(const QString& waiter) { _waiterName = waiter; }
        int plu() const { return _plu; }
        void setPlu(int plu) { _plu = plu; }
        QString article() const { return _article; }
        void setArticle(const QString& article) { _article = article; }
        int count() const { return _count; }
        void setCount(int count) { _count = count; }
        bool isHint() const { return _isHint; }
        void setIsHint(int flag) { _isHint = flag; }
        int family() const { return _family; }
        void setFamily(int family) { _family = family; }
        const QString familyName() const { return _familyName; }
        void setFamilyName(const QString& family) { _familyName = family; }
        int familyGroup() const { return _familyGroup; }
        void setFamilyGroup(int familyGroup) { _familyGroup = familyGroup; }
        const QString familyGroupName() const { return _familyGroupName; }
        void setFamilyGroupName(const QString& familyGroup) { _familyGroupName = familyGroup; }
        int round() const { return _round; }
        void setRound(int round) { _round = round; }
        int courseControl() const { return _courseControl; }
        void setCourseControl(int value) { _courseControl = value; }
        QStringList bonMonitors() const { return _bonMonitors; }
        void setBonMonitors(const QStringList& monitors) { _bonMonitors = monitors; }

    private:
        int _id;
        bool _isArchived;
        int _bonId;
        QString _entryGuid;
        QDateTime _orderedTime;
        int _waiter;
        QString _waiterName;
        int _plu;
        QString _article;
        int _count;
        bool _isHint;
        int _family;
        QString _familyName;
        int _familyGroup;
        QString _familyGroupName;
        int _round;
        int _courseControl;
        QStringList _bonMonitors;
    };

}

using namespace BonCore::Domain;

#endif // BONORDER_H
