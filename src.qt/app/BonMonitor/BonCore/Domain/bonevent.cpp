#include "bonevent.h"

BonEvent::BonEvent(BonEventType type)
    : _id(0)
    , _isArchived(false)
    , _type(type)
    , _sourceMonitor(0)
    , _bonId(0)
    // , _payload()
{
}

BonEvent::BonEvent(quint64 id)
    //: QObject(parent)
    : _id(id)
    , _isArchived(false)
    , _type(BonEventType::None)
    , _sourceMonitor(0)
    , _bonId(0)
    // , _payload()
{
}
