#include "bonorder.h"

namespace BonCore::Domain {

BonOrder::BonOrder(QObject *parent)
    : QObject(parent)
    , _id(0)
    , _isArchived(false)
    , _bonId(0)
    //, _entryGuid()
    //, _orderedTime()
    , _waiter(0)
    //, waiterName()
    , _plu(0)
    //, _article()
    , _count(0)
    , _isHint(false)
    , _family(0)
    //, _familyName()
    , _familyGroup(0)
    //, _familyGroupName()
    , _round(0)
    , _courseControl(0)
    //, _bonMonitors()
{
}

}
