#ifndef PAYLOADS_H
#define PAYLOADS_H
#include <QString>
#include <QUuid>
#include <QDateTime>
#include <QList>

class QJsonDocument;
class QJsonObject;

namespace BonCore::Logic {

    class Payload
    {
    public:
        static bool getJsonObject(const QString& payload, QJsonObject& jdoc);
    };

    struct VoidPayload
    {
        VoidPayload();
        QUuid tableGuid;
        QUuid orderGuid;
        uint table;
        uint party;
        uint count;
        int plu;
        QString article;
        QString reason;
        QDateTime voidTime;
        static VoidPayload fromPayload(const QString& payload);
    };

    struct SplitPayload
    {
        SplitPayload();
        QUuid sourceTableGuid;
        QUuid sourceOrderGuid;
        QUuid destTableGuid;
        QUuid destOrderGuid;
        uint table;
        uint party;
        uint count;
        int plu;
        QString article;
        static SplitPayload fromPayload(const QString& payload);
    };

    struct TransferOrderload
    {
        TransferOrderload();
        QUuid fromOrderGuid;
        QUuid toOrderGuid;
        static TransferOrderload fromPayload(const QJsonObject& jdoc);
    };

    struct TransferPayload
    {
        TransferPayload();
        QUuid fromTableGuid;
        uint fromTable;
        uint fromParty;
        QUuid toTableGuid;
        uint toTable;
        uint toParty;
        QList<TransferOrderload> orders;
        static TransferPayload fromPayload(const QString& payload);
    };

    struct CallCoursePayload
    {
        CallCoursePayload();
        QUuid tableGuid;
        QDateTime callTime;
        int courseId;
        uint table;
        uint party;
        static CallCoursePayload fromPayload(const QString& payload);
    };

    struct FollowUpPayload
    {
        FollowUpPayload();
        //int bonId;
        uint table;
        uint party;
        int courseId;
        uint count;
        int plu;
        QString article;
        QUuid orderGuid;
        static FollowUpPayload fromPayload(const QString& payload);
    };

    struct VipGuestPayload
    {
        VipGuestPayload();
        QUuid tableGuid;
        int bonId;
        int vipGuest;
        static VipGuestPayload fromPayload(const QString& payload);
    };

}

using namespace BonCore::Logic;

#endif // PAYLOADS_H
