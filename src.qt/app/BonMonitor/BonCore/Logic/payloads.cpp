#include "payloads.h"
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

namespace BonCore::Logic {


VoidPayload::VoidPayload()
    : tableGuid()
    , orderGuid()
    , table(0)
    , party(0)
    , count(0)
    , plu(0)
    , article()
    , reason()
    , voidTime()
{}

VoidPayload VoidPayload::fromPayload(const QString& payload)
{
    VoidPayload args;
    QJsonObject jdoc;
    if( Payload::getJsonObject(payload, jdoc) )
    {
        args.tableGuid = QUuid::fromString(jdoc["TableGuid"].toString());
        args.orderGuid = QUuid::fromString(jdoc["OrderGuid"].toString());
        args.table = jdoc["Table"].toInt();
        args.party = jdoc["Party"].toInt();
        args.count = jdoc["Count"].toInt();
        args.plu = jdoc["Plu"].toInt();
        args.article = jdoc["Article"].toString();
        args.reason = jdoc["Reason"].toString();
        args.voidTime = QDateTime::fromString(jdoc["VoidTime"].toString());
    }
    return args;
}

SplitPayload::SplitPayload()
    : sourceTableGuid()
    , sourceOrderGuid()
    , destTableGuid()
    , destOrderGuid()
    , table(0)
    , party(0)
    , count(0)
    , plu(0)
    , article()
{}

SplitPayload SplitPayload::fromPayload(const QString& payload)
{
    SplitPayload args;
    QJsonObject jdoc;
    if( Payload::getJsonObject(payload, jdoc) )
    {
        args.sourceTableGuid = QUuid::fromString(jdoc["SourceTableGuid"].toString());
        args.sourceOrderGuid = QUuid::fromString(jdoc["SourceOrderGuid"].toString());
        args.destTableGuid = QUuid::fromString(jdoc["DestinationTableGuid"].toString());
        args.destOrderGuid = QUuid::fromString(jdoc["DestinationOrderGuid"].toString());
        args.table = jdoc["Table"].toInt();
        args.party = jdoc["Party"].toInt();
        args.count = jdoc["Count"].toInt();
        args.plu = jdoc["Plu"].toInt();
        args.article = jdoc["Article"].toString();
    }
    return args;
}

TransferOrderload::TransferOrderload()
    : fromOrderGuid()
    , toOrderGuid()
{}

TransferOrderload TransferOrderload::fromPayload(const QJsonObject& jdoc)
{
    TransferOrderload args;
    args.fromOrderGuid = QUuid::fromString(jdoc["FromOrderGuid"].toString());
    args.toOrderGuid = QUuid::fromString(jdoc["ToOrderGuid"].toString());
    return args;
}

TransferPayload::TransferPayload()
    : fromTableGuid()
    , fromTable(0)
    , fromParty(0)
    , toTableGuid()
    , toTable(0)
    , toParty(0)
{}

TransferPayload TransferPayload::fromPayload(const QString& payload)
{
    TransferPayload args;
    QJsonObject jdoc;
    if( Payload::getJsonObject(payload, jdoc) )
    {
        args.fromTableGuid = QUuid::fromString(jdoc["FromTableGuid"].toString());
        args.fromTable = jdoc["FromTable"].toInt();
        args.fromParty = jdoc["FromParty"].toInt();
        args.toTableGuid = QUuid::fromString(jdoc["ToTableGuid"].toString());
        args.toTable = jdoc["ToTable"].toInt();
        args.toParty = jdoc["ToParty"].toInt();
        auto orders = jdoc["Orders"].toArray();
        for(auto idx=0; idx<orders.size(); ++idx)
        {
            auto orderObj = orders[idx].toObject();
            auto order = TransferOrderload::fromPayload(orderObj);
            args.orders.append(order);
        }
    }
    return args;
}

CallCoursePayload::CallCoursePayload()
    : tableGuid()
    , callTime()
    , courseId(0)
    , table(0)
    , party(0)
{}

CallCoursePayload CallCoursePayload::fromPayload(const QString& payload)
{
    CallCoursePayload args;
    QJsonObject jdoc;
    if( Payload::getJsonObject(payload, jdoc) )
    {
        args.tableGuid = QUuid::fromString(jdoc["TableGuid"].toString());
        args.table = jdoc["Table"].toInt();
        args.party = jdoc["Party"].toInt();
        args.courseId = jdoc["CourseId"].toInt();
        args.callTime = QDateTime::fromString(jdoc["AbrufTime"].toString());
    }
    return args;
}

FollowUpPayload::FollowUpPayload()
    //: bonId(0)
    : table(0)
    , party(0)
    , courseId(0)
    , count(0)
    , plu(0)
    , article()
    , orderGuid()
{}

FollowUpPayload FollowUpPayload::fromPayload(const QString& payload)
{
    FollowUpPayload args;
    QJsonObject jdoc;
    if( Payload::getJsonObject(payload, jdoc) )
    {
        args.table = jdoc["Table"].toInt();
        args.party = jdoc["Party"].toInt();
        args.courseId = jdoc["CourseId"].toInt();
        args.count = jdoc["Count"].toInt();
        args.plu = jdoc["Plu"].toInt();
        args.article = jdoc["Article"].toString();
        args.orderGuid = QUuid::fromString(jdoc["OrderGuid"].toString());
    }
    return args;
}

VipGuestPayload::VipGuestPayload()
    : tableGuid()
    , bonId(0)
    , vipGuest(0)
{}

VipGuestPayload VipGuestPayload::fromPayload(const QString& payload)
{
    VipGuestPayload args;
    QJsonObject jdoc;
    if( Payload::getJsonObject(payload, jdoc) )
    {
        args.tableGuid = QUuid::fromString(jdoc["TableGuid"].toString());
        args.bonId = jdoc["BonId"].toInt();
        args.vipGuest = jdoc["VipGuest"].toInt();
    }
    return args;
}

bool Payload::getJsonObject(const QString& payload, QJsonObject& jdoc)
{
    auto jsonDoc = QJsonDocument::fromJson(payload.toUtf8());
    // auto jsonDoc = QJsonDocument::fromBinaryData(data);
    if( jsonDoc.isNull()) {
        qDebug() << "JSonDoc is null";
        return false;
    }
    if( jsonDoc.isEmpty() ) {
        qDebug() << "JSonDoc is empty";
        return false;
    }
    if( !jsonDoc.isObject() ) {
        qDebug() << "JSonDoc is no object";
        return false;
    }
    jdoc = jsonDoc.object();
    return true;
}

}
