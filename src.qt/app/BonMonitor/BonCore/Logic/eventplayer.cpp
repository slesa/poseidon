#include "eventplayer.h"
#include <QJsonDocument>

namespace BonCore::Logic {

void EventPlayer::onGotEvent(BonEvent* event) {
    switch(event->type()) {
        // --- External Events ---------------------
        case VoidOrder:
            handleVoidOrder(event->bonId(), event->payload());
            break;
        case SplitOrder:
            handleSplitOrder(event->bonId(), event->payload());
            break;
        case TransferTable:
            handleTransfer(event->bonId(), event->payload());
            break;
        case GangAbruf:
            handleCallCourse(event->bonId(), event->payload());
            break;
        case Nachbuchung:
            handleFollowUp(event->bonId(), event->payload());
            break;
        case VipGuest:
            handleVipGuest(event->bonId(), event->payload());
            break;
        case MoveToMonitor:
        case CourseStart:
        case CourseDone:
        case ItemsDone:
        case ItemsUndo:
        case BonDone:
        case BonDrop:
        case BonActivate:
        case BonBack:
        case BonFinish:
        case OpenOrCloseCourse:
        case SwapTwoCells:
        case MoveCells:
        case OrderArchived:
        case BonArchived:
        case None:
            break;
    }
}

void EventPlayer::handleVoidOrder(int bonId, const QString& payload)
{
    auto args = VoidPayload::fromPayload(payload);
    emit voidOrder(bonId, args);
}

void EventPlayer::handleSplitOrder(int bonId, const QString& payload)
{
    auto args = SplitPayload::fromPayload(payload);
    emit splitOrder(bonId, args);
}

void EventPlayer::handleTransfer(int bonId, const QString& payload)
{
    auto args = TransferPayload::fromPayload(payload);
    emit transfer(bonId, args);
}

void EventPlayer::handleCallCourse(int bonId, const QString& payload)
{
    auto args = CallCoursePayload::fromPayload(payload);
    emit callCourse(bonId, args);
}

void EventPlayer::handleFollowUp(int bonId, const QString& payload)
{
    auto args = FollowUpPayload::fromPayload(payload);
    emit followUp(bonId, args);
}

void EventPlayer::handleVipGuest(int bonId, const QString& payload)
{
    auto args = VipGuestPayload::fromPayload(payload);
    emit vipGuest(bonId, args);
}

}
