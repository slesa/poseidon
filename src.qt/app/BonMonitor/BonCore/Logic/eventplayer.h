#ifndef BONMONITOR_EVENTPLAYER_H
#define BONMONITOR_EVENTPLAYER_H
#include <QObject>
#include "BonCore/Domain/bonevent.h"
#include "payloads.h"

class QJsonDocument;

namespace BonCore::Logic {

    class EventPlayer : public QObject {
       Q_OBJECT

    public:
        void onGotEvent(BonEvent *event);

    signals:
        void voidOrder(int bonId, BonCore::Logic::VoidPayload args);
        void splitOrder(int bonId, BonCore::Logic::SplitPayload args);
        void transfer(int bonId, BonCore::Logic::TransferPayload args);
        void callCourse(int bonId, BonCore::Logic::CallCoursePayload args);
        void followUp(int bonId, BonCore::Logic::FollowUpPayload args);
        void vipGuest(int bonId, BonCore::Logic::VipGuestPayload args);

    private:
        void handleVoidOrder(int bonId, const QString& payload);
        void handleSplitOrder(int bonId, const QString& payload);
        void handleTransfer(int bonId, const QString& payload);
        void handleCallCourse(int bonId, const QString& payload);
        void handleFollowUp(int bonId, const QString& payload);
        void handleVipGuest(int bonId, const QString& payload);
    };
}

using namespace BonCore::Logic;

#endif //BONMONITOR_EVENTPLAYER_H
