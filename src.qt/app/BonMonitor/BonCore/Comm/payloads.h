#ifndef PAYLOADS_H
#define PAYLOADS_H
#include <QDateTime>
#include <QUuid>

class QJsonObject;

class DoneItem
{
public:
    DoneItem(QUuid orderGuid, int plu, QString article, uint doneCount, bool isHint=false);
    QUuid orderGuid() const { return _orderGuid; }
    int plu() const { return _plu; }
    QString article() const { return _article; }
    uint doneCount() const { return _doneCount; }
    bool isHint() const { return _isHint; }
    QJsonObject toJson() const;
private:
    QUuid _orderGuid;
    int _plu;
    QString _article;
    uint _doneCount;
    bool _isHint;
};

class BonEventPayload
{
public:
    explicit BonEventPayload(const QDateTime& timeStamp);
    QDateTime timeStamp() const { return _timeStamp; }
    QJsonObject toJson() const;
private:
    QDateTime _timeStamp;
};

class BonDoneEventPayload : BonEventPayload
{
public:
    BonDoneEventPayload(const QDateTime& timeStamp, QList<DoneItem> doneItems);
    QList<DoneItem> doneItems() const { return _doneItems; }
    QJsonObject toJson() const;
private:
    QList<DoneItem> _doneItems;
};

class CourseEventPayload : BonEventPayload
{
public:
    CourseEventPayload(const QDateTime& timeStamp, int courseId);
    int courseId() const { return _courseId; }
    QJsonObject toJson() const;
private:
    int _courseId;
};

class CourseDoneEventPayload : CourseEventPayload
{
public:
    CourseDoneEventPayload(const QDateTime& timeStamp, int courseId, QList<DoneItem> doneItems);
    QList<DoneItem> doneItems() const { return _doneItems; }
    QJsonObject toJson() const;
private:
    QList<DoneItem> _doneItems;
};

class ItemsDoUndoHintPayload
{
public:
    explicit ItemsDoUndoHintPayload(uint count, int plu, const QString& article, QUuid orderGuid);
    uint count() const { return _count; }
    int plu() const { return _plu; }
    QString article() const { return _article; }
    QUuid orderGuid() const { return _orderGuid; }
    QJsonObject toJson() const;
private:
    uint _count;
    int _plu;
    QString _article;
    QUuid _orderGuid;
};

class ItemsDoUndoEventPayload : CourseEventPayload
{
public:
    explicit ItemsDoUndoEventPayload(const QDateTime& timeStamp, int courseId, QUuid orderGuid, uint count, int plu, const QString& article, QList<ItemsDoUndoHintPayload> hints);
    QUuid orderGuid() const { return _orderGuid; }
    uint count() const { return _count; }
    int plu() const { return _plu; }
    QString article() const  { return _article; }
    QList<ItemsDoUndoHintPayload> hints() const { return _hints; }
    QJsonObject toJson() const;
private:
    QUuid _orderGuid;
    uint _count;
    int _plu;
    QString _article;
    QList<ItemsDoUndoHintPayload> _hints;
};

class PrintPositionPayload
{
public:
    PrintPositionPayload(int bonId, int courseId, QUuid orderGuid, uint count, int plu, bool single);
    int bonId() const { return _bonId; }
    int courseId() const { return _courseId; }
    QUuid orderGuid() const { return _orderGuid; }
    uint count() const { return _count; }
    int plu() const { return _plu; }
    bool single() const { return _single; }
    QJsonObject toJson() const;
private:
    int _bonId;
    int _courseId;
    QUuid _orderGuid;
    uint _count;
    int _plu;
    bool _single;
};

class MoveToMonitorPayload
{
public:
    explicit MoveToMonitorPayload(int destMonitor, int orderId=-1);
    int destMonitor() const { return _destMonitor; }
    int orderId() const { return _orderId; }
    QDateTime timestamp() const { return _timeStamp; }
    QJsonObject toJson() const;
private:
    int _destMonitor;
    int _orderId;
    QDateTime _timeStamp;
};

class OpenOrCloseCoursePayload : CourseEventPayload
{
public:
    OpenOrCloseCoursePayload(bool doOpen, const QDateTime& timeStamp, int courseId);
    bool doOpen() const { return _doOpen; }
    QJsonObject toJson() const;
private:
    bool _doOpen;
};

class SwapTwoCellsPayload
{
public:
    explicit SwapTwoCellsPayload(int selectedColumn, int selectedRow, int selectedBonId, int targetColumn, int targetRow, int targetBonId);
    int selectedBonId() const { return _selectedBonId; }
    int selectedColumn() const { return _selectedColumn; }
    int selectedRow() const { return _selectedRow; }
    int targetBonId() const { return _targetBonId; }
    int targetColumn() const { return _targetColumn; }
    int targetRow() const { return _targetRow; }
    QJsonObject toJson() const;
private:
    int _selectedBonId;
    int _selectedColumn;
    int _selectedRow;
    int _targetBonId;
    int _targetColumn;
    int _targetRow;
};

class MoveCellPayload
{
public:
    explicit MoveCellPayload(int selectedBonId, int targetColumn, int targetRow);
    int selectedBonId() const { return _selectedBonId; }
    int targetColumn() const { return _targetColumn; }
    int targetRow() const { return _targetRow; }
    QJsonObject toJson() const;
private:
    int _selectedBonId;
    int _targetColumn;
    int _targetRow;
};

#endif //PAYLOADS_H
