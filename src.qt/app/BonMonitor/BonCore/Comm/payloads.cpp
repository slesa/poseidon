#include "payloads.h"
#include <QJsonObject>
#include <QJsonArray>

DoneItem::DoneItem(QUuid orderGuid, int plu, QString article, uint doneCount, bool isHint)
    : _orderGuid(orderGuid)
    , _plu(plu)
    , _article(article)
    , _doneCount(doneCount)
    , _isHint(isHint) {
}
QJsonObject DoneItem::toJson() const {
    QJsonObject json;
    json.insert("orderGuid", orderGuid().toString());
    json.insert("plu", plu());
    json.insert("article", article());
    json.insert("doneCount", (int)doneCount());
    json.insert("isHint", isHint());
    return json;
}

BonEventPayload::BonEventPayload(const QDateTime& timeStamp)
    : _timeStamp(timeStamp) {
}
QJsonObject BonEventPayload::toJson() const {
    QJsonObject json;
    json.insert("timeStamp", timeStamp().toString(Qt::DateFormat::ISODateWithMs));
    return json;
}

BonDoneEventPayload::BonDoneEventPayload(const QDateTime& timeStamp, QList<DoneItem> doneItems)
    : BonEventPayload(timeStamp)
    , _doneItems(std::move(doneItems)) {
}
QJsonObject BonDoneEventPayload::toJson() const {
    auto json = BonEventPayload::toJson();
    QJsonArray array;
    for (auto & doneItem : doneItems())
        array.append(doneItem.toJson());
    json.insert("doneItems", array);
    return json;
}

CourseEventPayload::CourseEventPayload(const QDateTime& timeStamp, int courseId)
    : BonEventPayload(timeStamp)
    , _courseId(courseId) {
}
QJsonObject CourseEventPayload::toJson() const {
    auto json = BonEventPayload::toJson();
    json.insert("courseId", courseId());
    return json;
}

CourseDoneEventPayload::CourseDoneEventPayload(const QDateTime& timeStamp, int courseId, QList<DoneItem> doneItems)
        : CourseEventPayload(timeStamp, courseId)
        , _doneItems(std::move(doneItems)) {
}
QJsonObject CourseDoneEventPayload::toJson() const {
    auto json = CourseEventPayload::toJson();
    QJsonArray array;
    for (auto & doneItem : doneItems())
        array.append(doneItem.toJson());
    json.insert("doneItems", array);
    return json;
}

ItemsDoUndoHintPayload::ItemsDoUndoHintPayload(uint count, int plu, const QString& article, QUuid orderGuid)
    : _count(count)
    , _plu(plu)
    , _article(article)
    , _orderGuid(orderGuid) {
}
QJsonObject ItemsDoUndoHintPayload::toJson() const {
    QJsonObject json;
    json.insert("count", (int)count());
    json.insert("plu", plu());
    json.insert("article", article());
    json.insert("orderGuid", orderGuid().toString());
    return json;
}

ItemsDoUndoEventPayload::ItemsDoUndoEventPayload(const QDateTime& timeStamp, int courseId, QUuid orderGuid, uint count, int plu, const QString& article, QList<ItemsDoUndoHintPayload> hints)
    : CourseEventPayload(timeStamp, courseId)
    , _orderGuid(orderGuid)
    , _count(count)
    , _plu(plu)
    , _article(article)
    , _hints(std::move(hints)) {
}
QJsonObject ItemsDoUndoEventPayload::toJson() const {
    auto json = CourseEventPayload::toJson();
    json.insert("count", (int)count());
    json.insert("plu", plu());
    json.insert("article", article());
    json.insert("orderGuid", orderGuid().toString());
    QJsonArray array;
    for (auto & hint : hints())
        array.append(hint.toJson());
    json.insert("hints", array);
    return json;
}

PrintPositionPayload::PrintPositionPayload(int bonId, int courseId, QUuid orderGuid, uint count, int plu, bool single)
    : _bonId(bonId)
    , _courseId(courseId)
    , _orderGuid(orderGuid)
    , _count(count)
    , _plu(plu)
    , _single(single) {
}

QJsonObject PrintPositionPayload::toJson() const {
    QJsonObject json;
    json.insert("bonId", bonId());
    json.insert("courseId", courseId());
    json.insert("orderGuid", orderGuid().toString());
    json.insert("count", (int)count());
    json.insert("plu", plu());
    json.insert("single", single());
    return json;
}

MoveToMonitorPayload::MoveToMonitorPayload(int destMonitor, int orderId)
    : _destMonitor(destMonitor)
    , _orderId(orderId)
    , _timeStamp(QDateTime::currentDateTime()) {
}

QJsonObject MoveToMonitorPayload::toJson() const {
    QJsonObject json;
    json.insert("destinationMonitor", destMonitor());
    json.insert("timeStamp", timestamp().toString(Qt::DateFormat::ISODateWithMs));
    if(orderId()>=0)
        json.insert("orderId", orderId());
    return json;
}

OpenOrCloseCoursePayload::OpenOrCloseCoursePayload(bool doOpen, const QDateTime& timeStamp, int courseId)
    : CourseEventPayload(timeStamp, courseId)
    , _doOpen(doOpen) {
}
QJsonObject OpenOrCloseCoursePayload::toJson() const {
    auto json = CourseEventPayload::toJson();
    json.insert("doOpen", doOpen());
    return json;
}

SwapTwoCellsPayload::SwapTwoCellsPayload(int selectedColumn, int selectedRow, int selectedBonId, int targetColumn, int targetRow, int targetBonId)
        : _selectedBonId(selectedBonId)
        , _selectedColumn(selectedColumn)
        , _selectedRow(selectedRow)
        , _targetBonId(selectedBonId)
        , _targetColumn(targetColumn)
        , _targetRow(targetRow) {
}
QJsonObject SwapTwoCellsPayload::toJson() const {
    QJsonObject json;
    json.insert("selectedBonId", selectedBonId());
    json.insert("selectedColumn", selectedColumn());
    json.insert("selectedRow", selectedRow());
    json.insert("targetBonId", targetBonId());
    json.insert("targetColumn", targetColumn());
    json.insert("targetRow", targetRow());
    return json;
}

MoveCellPayload::MoveCellPayload(int selectedBonId, int targetColumn, int targetRow)
    : _selectedBonId(selectedBonId)
    , _targetColumn(targetColumn)
    , _targetRow(targetRow) {
}
QJsonObject MoveCellPayload::toJson() const {
    QJsonObject json;
    json.insert("selectedBonId", selectedBonId());
    json.insert("targetColumn", targetColumn());
    json.insert("targetRow", targetRow());
    return json;
}
