#include "restcommandhandler.h"
#include "BonCore/Comm/payloads.h"
#include <QNetworkReply>
#include <QJsonDocument>
#include <QDebug>

RestCommandHandler::RestCommandHandler(Settings settings, QObject *parent)
    : RestBase(settings, parent)
{
}

#pragma region External Commands
void RestCommandHandler::printBon(int thisMonitor, int bonId)
{
    qDebug() << "Printing bon on REST server " << _settings.host();
    auto url = QStringLiteral("printbon/%1/%2").arg(thisMonitor).arg(bonId);
    sendBonCommand(url);
}

void RestCommandHandler::printCourse(int thisMonitor, int bonId, int courseId)
{
    qDebug() << "Printing course on REST server " << _settings.host();
    auto url = QStringLiteral("printbon/%1/%2/%3").arg(thisMonitor).arg(bonId).arg(courseId);
    sendBonCommand(url);
}

void RestCommandHandler::printPosition(int thisMonitor, PrintPositionPayload payload)
{
    qDebug() << "Printing position on REST server " << _settings.host();
    auto url = QStringLiteral("printpos/%1").arg(thisMonitor);
    sendBonCommand(url, QJsonDocument(payload.toJson()).toJson());
}

void RestCommandHandler::callGuestPager(int thisMonitor, int bonId, int pager)
{
    qDebug() << "Printing guest pager on REST server " << _settings.host();
    auto url = QStringLiteral("guestpager/%1/%2").arg(thisMonitor).arg(pager);
    sendBonCommand(url);
}

void RestCommandHandler::callWaiterPager(int thisMonitor, int bonId, int waiter, int pager)
{
    qDebug() << "Printing waiter pager on REST server " << _settings.host();
    auto url = QStringLiteral("waiterpager/%1/%2/%3").arg(thisMonitor).arg(waiter).arg(pager);
    sendBonCommand(url);
}
#pragma endregion }

#pragma region Order Events {
void RestCommandHandler::orderArchived(int thisMonitor, int bonId, int orderId)
{
    return deleteBonOrder(BonEventType::OrderArchived, thisMonitor, bonId, orderId);
}
#pragma endregion }

#pragma region Bon Events {
void RestCommandHandler::bonActivate(int thisMonitor, int bonId, const BonEventPayload& payload)
{
    sendBonEvent(BonEventType::BonActivate, thisMonitor, payload.toJson(), bonId);
}

void RestCommandHandler::bonBack(int thisMonitor, int bonId, const BonEventPayload& payload)
{
    sendBonEvent(BonEventType::BonBack, thisMonitor, payload.toJson(), bonId);
}

void RestCommandHandler::bonDrop(int thisMonitor, int bonId, const BonDoneEventPayload& payload)
{
    sendBonEvent(BonEventType::BonDrop, thisMonitor, payload.toJson(), bonId);
}

void RestCommandHandler::bonDone(int thisMonitor, int bonId, const BonDoneEventPayload& payload)
{
    sendBonEvent(BonEventType::BonDone, thisMonitor, payload.toJson(), bonId);
}

void RestCommandHandler::bonFinish(int thisMonitor, int bonId, const BonDoneEventPayload& payload)
{
    sendBonEvent(BonEventType::BonFinish, thisMonitor, payload.toJson(), bonId);
}

void RestCommandHandler::bonArchived(int thisMonitor, int bonId)
{
    return deleteBonOrder(BonEventType::BonArchived, thisMonitor, bonId);
}
#pragma endregion }

#pragma region Course Events {
void RestCommandHandler::startCourse(int thisMonitor, int bonId, CourseEventPayload payload)
{
    sendBonEvent(BonEventType::CourseStart, thisMonitor, payload.toJson(), bonId);
}

void RestCommandHandler::courseDone(int thisMonitor, int bonId, CourseDoneEventPayload payload)
{
    sendBonEvent(BonEventType::CourseDone, thisMonitor, payload.toJson(), bonId);
}
#pragma endregion }

#pragma region Item Events {
void RestCommandHandler::itemsDone(int thisMonitor, int bonId, ItemsDoUndoEventPayload payload)
{
    sendBonEvent(BonEventType::ItemsDone, thisMonitor, payload.toJson(), bonId);
}

void RestCommandHandler::itemsUndo(int thisMonitor, int bonId, ItemsDoUndoEventPayload payload)
{
    sendBonEvent(BonEventType::ItemsUndo, thisMonitor, payload.toJson(), bonId);
}
#pragma endregion }

#pragma region Mirror Movement {
void RestCommandHandler::openOrCloseCourse(int thisMonitor, int bonId, OpenOrCloseCoursePayload payload)
{
    sendBonEvent(BonEventType::OpenOrCloseCourse, thisMonitor, payload.toJson(), bonId);
}

void RestCommandHandler::swapTwoCells(int thisMonitor, SwapTwoCellsPayload payload)
{
    sendBonEvent(BonEventType::SwapTwoCells, thisMonitor, payload.toJson(), payload.selectedBonId());
}

void RestCommandHandler::moveCells(int thisMonitor, MoveCellPayload payload)
{
    sendBonEvent(BonEventType::MoveCells, thisMonitor, payload.toJson(), payload.selectedBonId());
}
#pragma endregion }

void RestCommandHandler::moveToMonitor(int thisMonitor, int bonId, MoveToMonitorPayload payload)
{
    sendBonEvent(BonEventType::MoveToMonitor, thisMonitor, payload.toJson(), bonId);
}

void RestCommandHandler::deleteBonOrder(BonEventType type, int fromMonitor, int bonId, int orderId)
{
//    connect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleBonData(QNetworkReply*)));

    auto func = QStringLiteral("orders/%1").arg(bonId);
    if(orderId>=0)
        func += QStringLiteral("/%1").arg(orderId);
    auto url = getEndPoint(func);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    _restClient.deleteResource(request);
}

void RestCommandHandler::sendBonEvent(BonEventType type, int fromMonitor, QJsonObject payload, int bonId)
{
    auto url = QStringLiteral("events");
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    auto body = createBonEvent(type, fromMonitor, QJsonDocument(payload).toJson(), bonId);
    _restClient.put(request, QJsonDocument(body).toJson());
}

void RestCommandHandler::sendBonCommand(const QString& url, const QByteArray& body)
{
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    _restClient.post(request, body);
}

QJsonObject RestCommandHandler::createBonEvent(BonEventType type, int fromMonitor, const QString& payload, int bonId)
{
    QJsonObject body;
    body.insert("type", type);
    body.insert("sourceMonitor", fromMonitor);
    body.insert("bonId", bonId);
    body.insert("payload", payload);
    return body;
}
