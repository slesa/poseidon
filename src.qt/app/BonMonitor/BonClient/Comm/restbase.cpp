#include "restbase.h"

RestBase::RestBase(Settings settings, QObject *parent)
    : QObject(parent)
    , _settings(settings)
    , _restClient(this)
{
}

QUrl RestBase::getEndPoint(QString func)
{
    QUrl url;
    url.setScheme("http");
    url.setHost(_settings.host());
    url.setPort(_settings.port());
    url.setPath(QString("/bm/%1").arg(func));
    return url;
}
