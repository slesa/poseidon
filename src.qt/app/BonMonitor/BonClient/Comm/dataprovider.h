#ifndef DATAPROVIDER_H
#define DATAPROVIDER_H
#include <QtStateMachine/QStateMachine>

class RestDataProvider;

class DataProvider : public QStateMachine
{
    Q_OBJECT
public:
    DataProvider(int delay, RestDataProvider* provider, QObject* parent=nullptr);
    void start();

private slots:
    void onServerVersion(QString);
    void askServerVersion();
    void askBons();
    void askEvents();

private:
    int _delay;
    bool _firstRound;
    RestDataProvider* _provider;
};

#endif // DATAPROVIDER_H
