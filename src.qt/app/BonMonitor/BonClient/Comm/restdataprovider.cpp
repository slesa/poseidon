#include "restdataprovider.h"
#include <QNetworkReply>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QQmlEngine>

RestDataProvider* RestDataProvider::_instance = nullptr;

RestDataProvider::RestDataProvider(Settings settings, QObject *parent)
    : RestBase(settings, parent)
    , _lastBonId(0L)
    , _lastEventId(0L)
{
//    _version = "4711";
}

void RestDataProvider::setInstance(RestDataProvider* instance) {
    _instance = instance;
}

QObject* RestDataProvider::getInstance(QQmlEngine* engine, QJSEngine* scriptEngine) {
    auto provider = _instance;
    QQmlEngine::setObjectOwnership(provider, QQmlEngine::CppOwnership);
    return provider;
}

void RestDataProvider::getServerVersion() {
    auto url = getEndPoint("version");

    // qDebug() << "Requesting version";
    connect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleVersionData(QNetworkReply*)));
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    _restClient.get(request);
}

void RestDataProvider::getBons() {
    auto func = QStringLiteral("bons");
    if(_lastBonId>0)
        func += QStringLiteral("/%1").arg(_lastBonId);
    auto url = getEndPoint(func);

    // qDebug() << "Requesting bons at " << func;
    connect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleBonData(QNetworkReply*)));
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    _restClient.get(request);
}

void RestDataProvider::getEvents() {
    auto func = QStringLiteral("events");
    if(_lastEventId>0)
        func += QStringLiteral("/%1").arg(_lastEventId);
    auto url = getEndPoint(func);

    // qDebug() << "Requesting events at " << func;
    connect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleEventData(QNetworkReply*)));
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    _restClient.get(request);
}

void RestDataProvider::handleVersionData(QNetworkReply* reply) {

    disconnect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleVersionData(QNetworkReply*)));
    if( reply->error()!=QNetworkReply::NoError ) {
        qDebug() << "Could not get server version: " << reply->errorString();
        emit noConnection();
        return;
    }
    auto data = reply->readAll();
    auto version = QString::fromUtf8(data);
    // qDebug() << "Server version is " << version;
    reply->deleteLater();

    _serverVersion = version;
    emit serverVersionChanged(_serverVersion);
}

void RestDataProvider::handleBonData(QNetworkReply* reply) {

    disconnect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleBonData(QNetworkReply*)));

    if( reply->error()!=QNetworkReply::NoError ) {
        qDebug() << "Could not get bons: " << reply->errorString();
        emit noConnection();
        return;
    }

    QJsonDocument jsonDoc;
    if( !getJsonDocument(reply, jsonDoc)) {
        emit bonsHandled();
        return;
    }

    auto lastId = jsonDoc["lastId"].toInteger();
    // qDebug() << "LastId is " << lastId;
    _lastBonId = lastId;

    auto bons = jsonDoc["bons"].toArray();
    emit bonsHandled();
    for(auto idx=0; idx<bons.size(); ++idx)
    {
        auto jsonBon = bons[idx].toObject();
        auto bon = new Bon();
        bon->setId( jsonBon["id"].toInt() );
        bon->setIsArchived( jsonBon["isArchived"].toBool() );
        bon->setTable( jsonBon["table"].toInt() );
        bon->setParty( jsonBon["party"].toInt() );
        bon->setTableGuid( jsonBon["tableGuid"].toString() );
        bon->setCreationTime( jsonBon["creationDateTime"].toVariant().toDateTime() );
        bon->setWaiter( jsonBon["waiter"].toInt() );
        bon->setWaiterName( jsonBon["waiterName"].toString() );
        bon->setCenter( jsonBon["costCenter"].toInt() );
        bon->setCenterName( jsonBon["centerName"].toString() );

        qDebug() << "Bon " << bon->id() << (bon->isArchived()?"A ":"  ") << "T" << bon->table() << " P" << bon->party()
                 // << " G" << bon->tableGuid() << " "
                 // << bon->creationTime()
                 << " W" << bon->waiter() << " " << bon->waiterName();

        auto ordersJson = jsonBon["orders"];
        if( ordersJson.isNull()) {
            qDebug() << "Orders is null";
            continue;
        }
        if( ordersJson.isObject() ) {
            qDebug() << "Orders is object";
            return;
        }
        auto pup = ordersJson.toVariant();
        if( !ordersJson.isArray() ) {
            qDebug() << "orders is no array";
            return;
        }
        auto orders = ordersJson.toArray();
        for(auto idx=0; idx<orders.size(); ++idx)
        {
            auto jsonOrder = orders[idx].toObject();
            auto order = new BonOrder(bon);
            order->setId( jsonOrder["id"].toInt() );
            order->setIsArchived( jsonOrder["isArchived"].toBool() );
            order->setBonId( jsonOrder["bonId"].toInt() );
            order->setEntryGuid( jsonOrder["entryGuid"].toString() );
            order->setOrderedTime( jsonOrder["orderedDateTime"].toVariant().toDateTime() );
            order->setWaiter( jsonOrder["waiter"].toInt() );
            order->setWaiterName( jsonOrder["waiterName"].toString() );
            order->setPlu( jsonOrder["plu"].toInt() );
            order->setArticle( jsonOrder["article"].toString() );
            order->setCount( jsonOrder["count"].toInt() );
            order->setRound( jsonOrder["round"].toInt() );
            order->setCourseControl( jsonOrder["courseControl"].toInt() );
            order->setIsHint( jsonOrder["isHint"].toBool());
            order->setFamily( jsonOrder["family"].toInt() );
            order->setFamilyName( jsonOrder["familyName"].toString() );
            order->setFamilyGroup( jsonOrder["familyGroup"].toInt() );
            order->setFamilyGroupName( jsonOrder["familyGroupName"].toString() );
            order->setBonMonitors( jsonOrder["bonMonitors"].toString().replace(';', ',').split(',') );
            //qDebug() << "Order " << order->id() << (order->isArchived()?"A ":"  ") << "B" << order->bonId() << " G" << order->entryGuid() << " " << order->orderedTime() << " "
            //        << " W" << order->waiter() << " " << order->waiterName();
            //qDebug() << "  " << order->bonMonitors() << " " << order->plu() << " " << order->article() << " X " << order->count() << "(" << order->round() << "/" << order->courseControl() << ")";
            //qDebug() << "  " << order->family() << " " << order->familyName() << " " << order->familyGroup() << " " << order->familyGroupName();
            bon->orders.append(order);
        }
        emit gotBon(bon);
        //qDebug() << Qt::endl;
    }
}

void RestDataProvider::handleEventData(QNetworkReply* reply)
{
    disconnect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleEventData(QNetworkReply*)));

    if( reply->error()!=QNetworkReply::NoError ) {
        qDebug() << "Could not get events: " << reply->errorString();
        emit noConnection();
        return;
    }

    QJsonDocument jsonDoc;
    if( !getJsonDocument(reply, jsonDoc)) {
        emit eventsHandled();
        return;
    }

    auto lastId = jsonDoc["lastId"].toInteger();
    // qDebug() << "LastId is " << lastId;
    _lastEventId = lastId;

    auto events = jsonDoc["events"].toArray();
    emit eventsHandled();
    for(auto idx=0; idx<events.size(); ++idx)
    {
        auto jsonEvent = events[idx].toObject();
        auto id = jsonEvent["id"].toInt();
        auto event = new BonEvent(id);
        event->setIsArchived( jsonEvent["isArchived"].toBool() );
        event->setType( (BonEventType) jsonEvent["type"].toInt() );
        event->setSourceMonitor( jsonEvent["sourceMonitor"].toInt() );
        event->setBonId( jsonEvent["bonId"].toInt() );
        event->setPayload( jsonEvent["payload"].toString() );

        //qDebug() << "Event " << event->id() << (event->isArchived()?"A ":"  ") << "B" << event->bonId() << " M" << event->sourceMonitor() << " " << event->payload();
        emit gotEvent(event);
        //qDebug() << Qt::endl;
    }
}

bool RestDataProvider::getJsonDocument(QNetworkReply* reply, QJsonDocument& jsonDoc)
{
    auto data = reply->readAll();
    //auto restData = QString::fromUtf8(data);
    //qDebug() << "REST data " << restData;
    jsonDoc = QJsonDocument::fromJson(data);
    // auto jsonDoc = QJsonDocument::fromBinaryData(data);
    if( jsonDoc.isNull()) {
        qDebug() << "JSonDoc is null";
        return false;
    }
    if( jsonDoc.isEmpty() ) {
        qDebug() << "JSonDoc is empty";
        return false;
    }
    if( !jsonDoc.isObject() ) {
        qDebug() << "JSonDoc is no object";
        return false;
    }
    return true;
}

