#include "dataprovider.h"
#include "restdataprovider.h"
#include <QtStateMachine/QStateMachine>
#include <QTimer>
#include <QDebug>

DataProvider::DataProvider(int delay, RestDataProvider* provider, QObject* parent)
    : QStateMachine(parent)
    , _delay(delay)
    , _firstRound(true)
    , _provider(provider)
{
}

void DataProvider::start()
{
    auto initState = new QState(this);
    connect(initState, &QAbstractState::entered, this, &DataProvider::askServerVersion);
    auto bonsState = new QState(this);
    connect(bonsState, &QAbstractState::entered, this, &DataProvider::askBons);
    auto eventsState = new QState(this);
    connect(eventsState, &QAbstractState::entered, this, &DataProvider::askEvents);
    auto waitState = new QState(this);
    auto timer = new QTimer(waitState);
    timer->setInterval(_delay);
    timer->setSingleShot(true);
    QObject::connect(waitState, &QAbstractState::entered, timer, QOverload<>::of(&QTimer::start));

    waitState->addTransition(timer, &QTimer::timeout, initState);
    initState->addTransition(_provider, &RestDataProvider::serverVersionChanged, bonsState);
    initState->addTransition(_provider, &RestDataProvider::noConnection, waitState);
    bonsState->addTransition(_provider, &RestDataProvider::bonsHandled, eventsState);
    bonsState->addTransition(_provider, &RestDataProvider::noConnection, waitState);
    eventsState->addTransition(_provider, &RestDataProvider::eventsHandled, waitState); //bonsState);
    eventsState->addTransition(_provider, &RestDataProvider::noConnection, waitState);
    setInitialState(initState);

    connect(_provider, &RestDataProvider::serverVersionChanged, this, &DataProvider::onServerVersion);
    connect(this, &QStateMachine::stopped, this, &QObject::deleteLater);
    QStateMachine::start();
}

void DataProvider::onServerVersion(QString version)
{
    if( _firstRound )
        qInfo() << "Talking to server version " << version;
    _firstRound = false;
}

void DataProvider::askServerVersion()
{
    if( _firstRound )
        qDebug() << "Asking server version";
    _provider->getServerVersion();
}

void DataProvider::askBons()
{
    if( _firstRound )
        qDebug() << "Asking new bons";
    _provider->getBons();
}

void DataProvider::askEvents()
{
    if( _firstRound )
        qDebug() << "Asking new events";
    _provider->getEvents();
}
