#ifndef RESTDATAPROVIDER_H
#define RESTDATAPROVIDER_H
#include "restbase.h"
#include "BonCore/Domain/bon.h"
#include "BonCore/Domain/bonevent.h"
#include <QObject>

class QQmlEngine;
class QJSEngine;

class RestDataProvider : public RestBase
{
    Q_OBJECT
    Q_CLASSINFO("Version", "1.0.0")
    Q_PROPERTY(QString version READ version NOTIFY versionChanged)
    Q_PROPERTY(QString serverVersion READ serverVersion NOTIFY serverVersionChanged)

public:
    explicit RestDataProvider(Settings settings, QObject *parent = nullptr);
    static void setInstance(RestDataProvider* instance);
    static QObject* getInstance(QQmlEngine* engine, QJSEngine* scriptEngine);

    void getServerVersion();
    void getBons();
    void getEvents();


    QString version() const { return _version; }
    QString serverVersion() const { return _serverVersion; }

signals:
    void noConnection();
    void versionChanged(QString);
    void serverVersionChanged(QString);
    void gotBon(Bon*);
    void bonsHandled();
    void gotEvent(BonEvent*);
    void eventsHandled();


private slots:
    void handleVersionData(QNetworkReply* reply);
    void handleBonData(QNetworkReply* reply);
    void handleEventData(QNetworkReply* reply);

private:
    bool getJsonDocument(QNetworkReply* reply, QJsonDocument& jsonDoc);

private:
    static RestDataProvider* _instance;
    QString _version;
    QString _serverVersion;
    long _lastBonId;
    long _lastEventId;
};

#endif // RESTDATAPROVIDER_H
