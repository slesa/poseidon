#ifndef RESTCOMMANDHANDLER_H
#define RESTCOMMANDHANDLER_H
#include "restbase.h"
#include "BonCore/Domain/bonevent.h"
#include <QJsonObject>
#include <QObject>

class BonEventPayload;
class BonDoneEventPayload;
class CourseEventPayload;
class CourseDoneEventPayload;
class ItemsDoUndoEventPayload;
class PrintPositionPayload;
class MoveToMonitorPayload;
class OpenOrCloseCoursePayload;
class SwapTwoCellsPayload;
class MoveCellPayload;

class RestCommandHandler: public RestBase
{
public:
    explicit RestCommandHandler(Settings settings, QObject *parent = nullptr);

#pragma region External Commands
    void printBon(int thisMonitor, int bonId);
    void printCourse(int thisMonitor, int bonId, int courseId);
    void printPosition(int thisMonitor, PrintPositionPayload payload);
    void callGuestPager(int thisMonitor, int bonId, int pager);
    void callWaiterPager(int thisMonitor, int bonId, int waiter, int pager);
#pragma endregion }

#pragma region Order Events {
    void orderArchived(int thisMonitor, int bonId, int orderId);
#pragma endregion }

#pragma region Bon Events {
    void bonActivate(int thisMonitor, int bonId, const BonEventPayload& payload);
    void bonBack(int thisMonitor, int bonId, const BonEventPayload& payload);
    void bonDrop(int thisMonitor, int bonId, const BonDoneEventPayload& payload);
    void bonDone(int thisMonitor, int bonId, const BonDoneEventPayload& payload);
    void bonFinish(int thisMonitor, int bonId, const BonDoneEventPayload& payload);
    void bonArchived(int thisMonitor, int bonId);
#pragma endregion }

#pragma region Course Events {
    void startCourse(int thisMonitor, int bonId, CourseEventPayload payload);
    void courseDone(int thisMonitor, int bonId, CourseDoneEventPayload payload);
#pragma endregion Course Events

#pragma region Item Events {
    void itemsDone(int thisMonitor, int bonId, ItemsDoUndoEventPayload payload);
    void itemsUndo(int thisMonitor, int bonId, ItemsDoUndoEventPayload payload);
#pragma endregion }

#pragma region Mirror Movement {
    void openOrCloseCourse(int thisMonitor, int bonId, OpenOrCloseCoursePayload payload);
    void swapTwoCells(int thisMonitor, SwapTwoCellsPayload payload);
    void moveCells(int thisMonitor, MoveCellPayload payload);
#pragma endregion }

    void moveToMonitor(int thisMonitor, int bonId, MoveToMonitorPayload payload);

private:
    void deleteBonOrder(BonEventType type, int fromMonitor, int bonId, int orderId=0);
    void sendBonEvent(BonEventType type, int fromMonitor, QJsonObject payload, int bonId=-1);
    QJsonObject createBonEvent(BonEventType type, int fromMonitor, const QString& payload, int bonId=-1);
    void sendBonCommand(const QString& url, const QByteArray& body=QByteArray());
};

#endif // RESTCOMMANDHANDLER_H
