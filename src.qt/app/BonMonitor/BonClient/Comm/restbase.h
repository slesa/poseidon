#ifndef RESTBASE_H
#define RESTBASE_H
#include "Settings/settings.h"
#include <QNetworkAccessManager>
#include <QObject>

class RestBase: public QObject
{
    Q_OBJECT
public:
    explicit RestBase(Settings settings, QObject *parent = nullptr);

protected:
    QUrl getEndPoint(QString func);

protected:
    Settings _settings;
    QNetworkAccessManager _restClient;
};

#endif // RESTBASE_H
