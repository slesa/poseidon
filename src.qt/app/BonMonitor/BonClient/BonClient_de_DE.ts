<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutForm.ui</name>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="6"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="35"/>
        <source>Bon Monitor v0.0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="40"/>
        <source>(c) 42 GmbH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="45"/>
        <source>See what&apos;s going on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="50"/>
        <source>in your kitchen</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppSettingsReader</name>
    <message>
        <location filename="Settings/configure.cpp" line="13"/>
        <source>The network address or name of the bonmonitor server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="14"/>
        <source>The network port of the bonmonitor server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="15"/>
        <source>The polling delay in milliseconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="16"/>
        <source>The number of this bonmonitor client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="17"/>
        <source>The name of this bonmonitor client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="35"/>
        <source>No valid port given. It should be a number between 1 and 65535</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="44"/>
        <source>No valid poll delay given</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="53"/>
        <source>No valid monitor id given</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ArticleList</name>
    <message>
        <location filename="Views/ArticleList.qml" line="15"/>
        <source>Articles</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BonList</name>
    <message>
        <location filename="Views/BonList.qml" line="15"/>
        <source>Bonlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/BonList.qml" line="62"/>
        <source>Bon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/BonList.qml" line="67"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/BonList.qml" line="72"/>
        <source>Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/BonList.qml" line="74"/>
        <source>Party</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/BonList.qml" line="78"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/BonList.qml" line="80"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CenterView</name>
    <message>
        <location filename="Views/CenterView.qml" line="18"/>
        <source>Bondetails</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsForm.ui</name>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="9"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="31"/>
        <source>Poll &amp;delay:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="40"/>
        <source>Monitor &amp;name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="47"/>
        <source>Monitor &amp;id:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="56"/>
        <source>BonMonitor &amp;port:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="65"/>
        <source>BonMonitor &amp;host:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="68"/>
        <source>IP address of the BonMonitor server</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="Views/main.qml" line="16"/>
        <source>Bonmonitor Client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/main.qml" line="50"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/main.qml" line="58"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
