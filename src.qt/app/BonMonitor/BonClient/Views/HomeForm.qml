import QtQuick
import QtQuick.Layouts

Item {

    id: root
    anchors.fill: parent
    property string title: 'Home Sweet Home'

    //RowLayout {
        //anchors.fill: parent
        LeftView {
            id: leftView
            anchors { left: parent.left; top: parent.top; bottom: parent.bottom }
        }

        RightView {
            id: rightView
            anchors { right: parent.right; top: parent.top; bottom: parent.bottom }
        }

        CenterView {
            anchors { left: leftView.right; right: rightView.left; top: parent.top; bottom: parent.bottom }
        }
    //}
}
