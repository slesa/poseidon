import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

// https://stackoverflow.com/questions/21974272/how-to-make-visible-both-icon-and-text-on-qml-toolbutton
Rectangle {
    id: centerView
    color: 'red'

    RowLayout {
        id: header
        anchors { top: parent.top; left: parent.left; right: parent.right }
        height: 60
        Text {
            //anchors.left: parent.left
            //anchors.leftMargin: 5
            anchors.horizontalCenter: parent.Center
            text: qsTr('Bondetails')
            font.pixelSize: 24
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        /* ToolBar {
            height: 60
            RowLayout {
                height: 60
                Image {
                    id: multiSelect
                    source: "/res/multiselect.png"
                    fillMode: Image.PreserveAspectFit
                    width: 32
                    height: 32
                }

                Image {
                    id: bonBack
                    source: "/res/back.png"
                    fillMode: Image.PreserveAspectFit
                    width: 32
                    height: 32
                }
            }
        } */

        Text {
            //anchors.right: parent.right
            //anchors.rightMargin: 5
            text: "42" //bons.count
            font.pixelSize: 26
            //horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

    }

}
