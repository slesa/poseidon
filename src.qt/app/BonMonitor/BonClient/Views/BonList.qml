import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Rectangle {
    id: root
    color: 'green'

    Rectangle {
        id: header
        anchors { top: parent.top; left: parent.left; right: parent.right }
        height: 58
        Text {
            anchors { left: parent.left; leftMargin: 5; verticalCenter: parent.verticalCenter }
            text: qsTr('Bonlist')
            font.pixelSize: 26
            font.bold: true
            // horizontalAlignment: parent.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        Text {
            anchors { right: parent.right; rightMargin: 5; verticalCenter: parent.verticalCenter }
            text: bons.count
            font.pixelSize: 24
            //horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    Component {
        id: bonDelegate
        Item {
            id: bonItem
            //property bool hovered: false
            anchors { left: parent.left }
            width: parent.width
            height: 90
        MouseArea {
            id: mouseArea
            anchors.fill: parent
            hoverEnabled: true
            //OnEntered: { isHovered = true; } //bonArea.hoverColor = 'Blue' }
            //OnExited: { isHovered = false; } //bonArea.hoverColor = 'Red' }
            onClicked: { console.debug('OnClicked') }
            onHoveredChanged: highlightBar.color = containsMouse ? 'Red' : 'transparent' //bonItem.hovered = hovered
        }

        RowLayout {
            anchors.fill: parent
            spacing: 5
            Rectangle {
                id: highlightBar
                width: 10
                height: parent.height
                color: 'transparent'
            }

            Column {
                anchors { leftMargin: 10 }
                spacing: 3
                Row {
                    Text { text: qsTr("Bon"); width: bonItem.width/4; font.pixelSize: 14; font.bold: true }
                    Text { text: bonId; width: bonItem.width/4; font.pixelSize: 14; font.bold: true }
                    Text { text: waiterName; width: bonItem.width/2; font.pixelSize: 14; font.bold: true }
                }
                Row {
                    Text { text: qsTr("Center"); width: bonItem.width/4; font.bold: true }
                    Text { text: center; width: bonItem.width/4; font.bold: true }
                    Text { text: centerName; width: bonItem.width/2; font.bold: true }
                }
                Row {
                    Text { text: qsTr("Table"); width: bonItem.width/4 }
                    Text { text: table; width: bonItem.width/4 }
                    Text { text: qsTr("Party"); width: bonItem.width/4 }
                    Text { text: party; width: bonItem.width/4 }
                }
                Row {
                    Text { text: qsTr("Date"); width: bonItem.width/4 }
                    Text { text: creationDate.toLocaleDateString(Locale.ShortFormat); width: bonItem.width/4 }
                    Text { text: qsTr("Time"); width: bonItem.width/4 }
                    Text { text: creationTime.toLocaleTimeString(Locale.ShortFormat); width: bonItem.width/4 }
                }
            } }
            /*
            Text { text: table }
            Text { text: party }
            Text { text: waiter }
            Text { text: '('+orderCount+')' }
            // font.pixelSize: 24
*/
        }
    }

    ScrollView {
        anchors { top: header.bottom; bottom: parent.bottom; left: parent.left; right: parent.right }
        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff

        ListView {
            id: bons
            anchors { topMargin: 10; bottomMargin: 10 }
            //anchors.fill: parent
            spacing: 5
            clip: true
            model: bonsModel
            focus: true
            highlight: Rectangle {
                color: 'cyan'
                width: root.width
            }
            delegate: bonDelegate /*Rectangle {
                color: 'orange'
                required property int index
                width: root.width
                height: 40
                Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: 'Bon ' + (index+1)
                }
            }*/
        }
    }

}
