import QtQuick 2.0

Rectangle {
    id: root
    color: 'brown'
    width: 340

    BonList {
        id: bons
        anchors {
                top: root.top; topMargin: 20
                bottom: preorders.top; bottomMargin: 20
                left: parent.left; leftMargin: 20
                right: parent.right; rightMargin: 20 }
        height: root.height*2/3.0
    }

    PreorderList {
        id: preorders
        anchors {
                bottom: root.bottom; bottomMargin: 20
                left: parent.left; leftMargin: 20
                right: parent.right; rightMargin: 20 }
        height: root.height/3.0
    }
}
