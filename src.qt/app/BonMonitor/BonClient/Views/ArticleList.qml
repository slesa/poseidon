import QtQuick
import QtQuick.Controls

Rectangle {
    id: root
    color: "green"

    Rectangle {
        id: header
        anchors { top: parent.top; left: parent.left; right: parent.right }
        height: 60
        Text {
            anchors.left: parent.left
            anchors.leftMargin: 5
            text: qsTr('Articles')
            font.pixelSize: 24
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        Text {
            anchors.right: parent.right
            anchors.rightMargin: 5
            text: articles.count
            font.pixelSize: 24
            //horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    Component {
        id: articleDelegate
        Row {
            id: bonItem
            spacing: 3
            width: parent.width
            height: 30

            Text { text: plu; width: 40; horizontalAlignment: Text.AlignRight }
            Text { text: article; width: bonItem.width-90; font.bold: true }
            Text { text: count; width: 40; horizontalAlignment: Text.AlignRight }
        }
    }

    ScrollView {
        anchors { top: header.bottom; bottom: parent.bottom; left: parent.left; right: parent.right }
        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff

        ListView {
            id: articles
            anchors.fill: parent
            clip: false
            model: articlesModel
            delegate: articleDelegate /* Rectangle {
                color: "orange"
                width: root.width
                height: 40
                Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    text: 'Article ' + (index+1)
                }
            } */
            spacing: 5
        }
    }
}
