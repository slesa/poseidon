import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Item {
    id: root
    anchors.fill: parent
    property string title: 'Home Sweet Home'

    SwipeView {
        id: view
        currentIndex: 1
        anchors.fill: parent
        // anchors { top: parent.top; bottom: indicator.top; left: parent.left; right: parent.right }

        LeftView {
            id: leftView
        }
        CenterView {
            id: centerView
        }

        RightView {
            id: rightView
        }
    }
    PageIndicator {
        id: indicator

        count: view.count
        currentIndex: view.currentIndex

        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
