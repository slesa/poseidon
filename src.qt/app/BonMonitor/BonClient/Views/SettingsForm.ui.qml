import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import Qt.labs.settings 1.0

Page {
    id: root
    // anchors.fill: parent
    title: qsTr("Settings")

    Settings {
        category: "General"
        fileName: "etc/settings.ini"
        property alias host: textHost.text
        property alias port: spinPort.value
        property alias pollDelay: spinPollDelay.value
        property alias monitorId: spinMonitorId.value
        property alias monitorName: textMonitorName.text
    }

    GridLayout {
        id: grid
        //height: parent.height-filler.height
        anchors { top: parent.top; left: parent.left; right: parent.right; margins: 20 }
        //width: parent.width
        //anchors.fill: parent
        columns: 2
        //rowSpan: 10

        FormControl {
            label.text: qsTr("Poll &delay:")
            control: SpinBox {
                id: spinPollDelay
                editable: true
                from: 1
                to: 5000
            }
        }
        FormControl {
            label.text: qsTr("Monitor &name:")
            control: TextField {
                id: textMonitorName
                placeholderText: qsTr("Monitor " + spinMonitorId.value)
            }
        }
        FormControl {
            label.text: qsTr("Monitor &id:")
            control: SpinBox {
                id: spinMonitorId
                editable: true
                from: 1
                to: 1000
            }
        }
        FormControl {
            label.text: qsTr("BonMonitor &port:")
            control: SpinBox {
                id: spinPort
                editable: true
                from: 1
                to: 65535
            }
        }
        FormControl {
            label.text: qsTr("BonMonitor &host:")
            control: TextField {
                id: textHost
                placeholderText: qsTr("IP address of the BonMonitor server")
            }
        }
    }
}
