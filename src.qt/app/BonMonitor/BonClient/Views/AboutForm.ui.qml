import QtQuick
import QtQuick.Controls

Page {
    //anchors.fill: parent
    title: qsTr("About...")

    Row {
        id: grid
        anchors.fill: parent
        //columns: 2
        spacing: 10

        Rectangle {
            color: 'transparent'
            width: grid.width / 2
            height: parent.height
            Image {
                id: image
                anchors.centerIn: parent
                width: parent.width - 20
                source: 'qrc:/res/bonmonitor.jpg'
                fillMode: Image.PreserveAspectFit
            }
        }
        Rectangle {
            color: 'transparent'
            width: grid.width / 2
            height: parent.height
            Column {
                anchors.centerIn: parent
                spacing: 10
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Bon Monitor v0.0.1")
                    font.pixelSize: 12
                }
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("(c) 42 GmbH")
                    font.pixelSize: 12
                }
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("See what's going on")
                    font.pixelSize: 12
                }
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("in your kitchen")
                    font.pixelSize: 12
                }
            }
        }
    }
}
