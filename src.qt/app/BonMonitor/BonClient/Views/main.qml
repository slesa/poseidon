import QtQuick
import QtQuick.Controls
import QtQuick.VirtualKeyboard

// https://stackoverflow.com/questions/37494752/qt-how-to-create-a-library-from-an-existing-project
// https://doc.qt.io/qt-6/scalability.html
// https://github.com/brexis/qml-bootstrap
// https://dev.qgroundcontrol.com/master/en/index.html
// https://docs.qgroundcontrol.com/master/en/
// https://resources.qt.io/videos/qtws16-a-simple-way-to-create-a-qml-ui-for-any-screen-size-or-resolution-chris-cortopassi-ics : 10.03
ApplicationWindow {
    id: window
    width: 900
    height: 600
    visible: true
    title: qsTr("Bonmonitor Client")

    header: ToolBar {
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            text: stackView.currentItem.title ?? "Bon Monitor"
            anchors.centerIn: parent
        }
    }


    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height

        Column {
            anchors.fill: parent

            ItemDelegate {
                text: qsTr("Settings")
                width: parent.width
                onClicked: {
                    stackView.push("SettingsForm.ui.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("About")
                width: parent.width
                onClicked: {
                    stackView.push("AboutForm.ui.qml")
                    drawer.close()
                }
            }
        }
    }

    Loader {
        id: pageLoader
        // anchors.fill: parent
        source: Screen.orientation!==Qt.LandscapeOrientation || window.width<800
            ? "qrc:/Views/HomeSwipe.qml"
            : "qrc:/Views/HomeForm.qml"
    }
    Component.onCompleted: {
        console.debug("Orientation: "+Screen.orientation)
        console.debug("Width: "+window.width)
    }

    StackView {
        id: stackView
        initialItem: pageLoader
        anchors.fill: parent
    }

    InputPanel {
        id: inputPanel
        z: 99
        x: 0
        y: window.height
        width: window.width

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: window.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }
}
