import QtQuick
import QtQuick.Controls

Rectangle {
    id: root
    color: 'blue'

    Rectangle {
        id: header
        anchors { top: parent.top; left: parent.left; right: parent.right }
        height: 30
        Text {
            anchors.fill: parent
            text: orders.count + ' preorders'
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    ScrollView {
        anchors { top: header.bottom; bottom: parent.bottom; left: parent.left; right: parent.right }
        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff

        ListView {
            id: orders
            anchors.fill: parent
            clip: true
            model: 5
            delegate: Rectangle {
                color: 'violet'
                required property int index
                width: root.width
                height: 40
                Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: 'Order ' + (index+1)
                }
            }
            spacing: 5
        }
    }
}
