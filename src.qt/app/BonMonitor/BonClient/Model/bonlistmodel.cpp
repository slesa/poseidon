#include "bonlistmodel.h"

BonListModel::BonListModel(EventPlayer* player, QObject* parent)
    : QAbstractListModel(parent)
{
    registerMe(player);
}

void BonListModel::onGotBon(Bon* bon)
{
    auto bonItem = new BonListItem(bon->id(), bon->table(), bon->party(), this);
    bonItem->setTableGuid(bon->tableGuid());
    bonItem->setCreationTime(bon->creationTime());
    bonItem->setWaiter(bon->waiter());
    bonItem->setWaiterName(bon->waiterName());
    bonItem->setCenter(bon->center());
    bonItem->setCenterName(bon->centerName());

    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    bons.append(bonItem);
    endInsertRows();
}

int BonListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    auto result = bons.size();
    return result;
}

QVariant BonListModel::data(const QModelIndex &index, int role) const
{
    auto& current = bons.at(index.row());
    switch(role)
    {
    case TableRole:
        return QVariant::fromValue(current->table());
    case PartyRole:
        return QVariant::fromValue(current->party());
    case WaiterRole:
        return QVariant::fromValue(current->waiter());
    case WaiterNameRole:
        return QVariant::fromValue(current->waiterName());
    case CenterRole:
        return QVariant::fromValue(current->center());
    case CenterNameRole:
        return QVariant::fromValue(current->centerName());
    case CreationDateRole:
        return QVariant::fromValue(current->creationTime().date());
    case CreationTimeRole:
        return QVariant::fromValue(current->creationTime().time());
    case OrderCountRole:
        return QVariant::fromValue(current->orders.count());
    case IdRole:
        return QVariant::fromValue(current->bonId());
    }
    return QVariant();
}

QHash<int, QByteArray> BonListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[IdRole] = "bonId";
    roles[TableRole] = "table";
    roles[PartyRole] = "party";
    roles[WaiterRole] = "waiter";
    roles[WaiterNameRole] = "waiterName";
    roles[CenterRole] = "center";
    roles[CenterNameRole] = "centerName";
    roles[CreationDateRole] = "creationDate";
    roles[CreationTimeRole] = "creationTime";
    roles[OrderCountRole] = "orderCount";

    return roles;
}

void BonListModel::onVoidOrder(int bonId, BonCore::Logic::VoidPayload args)
{
}

void BonListModel::onSplitOrder(int bonId, BonCore::Logic::SplitPayload args)
{
}

void BonListModel::onTransfer(int bonId, BonCore::Logic::TransferPayload args)
{
}

void BonListModel::onCallCourse(int bonId, BonCore::Logic::CallCoursePayload args)
{
}

void BonListModel::onFollowUp(int bonId, BonCore::Logic::FollowUpPayload args)
{
}

void BonListModel::onVipGuest(int bonId, BonCore::Logic::VipGuestPayload args)
{
}

void BonListModel::registerMe(EventPlayer* player)
{
    connect(player, &EventPlayer::voidOrder, this, &BonListModel::onVoidOrder);
    connect(player, &EventPlayer::splitOrder, this, &BonListModel::onSplitOrder);
    connect(player, &EventPlayer::transfer, this, &BonListModel::onTransfer);
    connect(player, &EventPlayer::callCourse, this, &BonListModel::onCallCourse);
    connect(player, &EventPlayer::followUp, this, &BonListModel::onFollowUp);
    connect(player, &EventPlayer::vipGuest, this, &BonListModel::onVipGuest);
}
