#include "articlesmodel.h"
#include <QDebug>

ArticlesModel::ArticlesModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

void ArticlesModel::onGotBon(Bon* bon)
{
    foreach(auto order, bon->orders) {
        auto item = new ArticlesItem(order->count(), order->plu(), order->article());
        item->setFamily(order->family());
        item->setFamilyName(order->familyName());
        item->setGroup(order->familyGroup());
        item->setGroupName(order->familyGroupName());
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        articles.append(item);
        endInsertRows();
    }
}

int ArticlesModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    auto result = articles.size();
    // qDebug() << "Got " << result << " articles";
    return result;
}

QVariant ArticlesModel::data(const QModelIndex &index, int role) const
{
    auto& current = articles.at(index.row());
    //qDebug() << "Data " << current->plu() << " of " << articles.size();
    switch(role)
    {
    case CountRole:
        return QVariant::fromValue(current->count());
    case PluRole:
        return QVariant::fromValue(current->plu());
    case ArticleRole:
        return QVariant::fromValue(current->article());
    case FamilyRole:
        return QVariant::fromValue(current->family());
    case FamilyNameRole:
        return QVariant::fromValue(current->familyName());
    case GroupRole:
        return QVariant::fromValue(current->group());
    case GroupNameRole:
        return QVariant::fromValue(current->groupName());
    }
    return QVariant();
}

QHash<int, QByteArray> ArticlesModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[CountRole] = "count";
    roles[PluRole] = "plu";
    roles[ArticleRole] = "article";
    roles[FamilyRole] = "family";
    roles[FamilyNameRole] = "familyName";
    roles[GroupRole] = "group";
    roles[GroupNameRole] = "groupName";

    return roles;
}
