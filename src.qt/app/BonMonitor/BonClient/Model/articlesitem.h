#ifndef ARTICLESITEM_H
#define ARTICLESITEM_H
#include <QObject>


class ArticlesItem : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int count READ count WRITE setCount)
    Q_PROPERTY(int plu READ plu WRITE setPlu)
    Q_PROPERTY(QString article READ article WRITE setArticle)
    Q_PROPERTY(int family READ family WRITE setFamily)
    Q_PROPERTY(QString familyName READ familyName WRITE setFamilyName)
    Q_PROPERTY(int group READ group WRITE setGroup)
    Q_PROPERTY(QString groupName READ groupName WRITE setGroupName)

public:
    explicit ArticlesItem(int count, int plu, QString article, QObject *parent = nullptr);
    int count() const { return _count; }
    void setCount(int count) { _count = count; }
    int plu() const { return _plu; }
    void setPlu(int plu) { _plu = plu; }
    QString article() const { return _article; }
    void setArticle(QString value) { _article = value; }
    int family() const { return _family; }
    void setFamily(int value) { _family = value; }
    const QString familyName() const { return _familyName; }
    void setFamilyName(QString value) { _familyName = value; }
    int group() const { return _group; }
    void setGroup(int value) { _group = value; }
    const QString groupName() const { return _groupName; }
    void setGroupName(QString value) { _groupName = value; }

private:
    int _count;
    int _plu;
    QString _article;
    int _family;
    QString _familyName;
    int _group;
    QString _groupName;
};


#endif // ARTICLESITEM_H
