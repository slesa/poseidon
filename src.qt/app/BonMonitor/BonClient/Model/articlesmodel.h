#ifndef ARTICLESMODEL_H
#define ARTICLESMODEL_H
#include <QAbstractListModel>
#include <QModelIndex>
#include <QObject>
#include "articlesitem.h"
#include "BonCore/Domain/bon.h"


class ArticlesModel: public QAbstractListModel
{
    Q_OBJECT
public:
    enum ArticlesRoles {
        CountRole = Qt::UserRole + 1,
        PluRole,
        ArticleRole,
        FamilyRole,
        FamilyNameRole,
        GroupRole,
        GroupNameRole,
    };

    explicit ArticlesModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent=QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;

public slots:
    void onGotBon(Bon* bon);

protected:
    QHash<int, QByteArray> roleNames() const;
    QList<ArticlesItem*> articles;
};

#endif // ARTICLESMODEL_H
