#ifndef BONLISTMODEL_H
#define BONLISTMODEL_H
#include <QAbstractListModel>
#include <QModelIndex>
#include <QObject>
#include "bonlistitem.h"
#include "BonCore/Domain/bon.h"
#include "BonCore/Logic/eventplayer.h"

class BonListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum BonRoles {
        IdRole = Qt::UserRole + 1,
        TableRole,
        PartyRole, //= Qt::UserRole + 2,
        WaiterRole, //= Qt::UserRole + 3,
        WaiterNameRole, //= Qt::UserRole + 4,
        CenterRole, //= Qt::UserRole + 5,
        CenterNameRole, //= Qt::UserRole + 6,
        CreationDateRole, //= Qt::UserRole + 7,
        CreationTimeRole, //= Qt::UserRole + 8,
        OrderCountRole, //= Qt::UserRole + 9,
    };

    explicit BonListModel(EventPlayer* player, QObject* parent=nullptr);
    int rowCount(const QModelIndex &parent=QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;

//public slots:
    void onGotBon(Bon* bon);
    void onVoidOrder(int bonId, BonCore::Logic::VoidPayload args);
    void onSplitOrder(int bonId, BonCore::Logic::SplitPayload args);
    void onTransfer(int bonId, BonCore::Logic::TransferPayload args);
    void onCallCourse(int bonId, BonCore::Logic::CallCoursePayload args);
    void onFollowUp(int bonId, BonCore::Logic::FollowUpPayload args);
    void onVipGuest(int bonId, BonCore::Logic::VipGuestPayload args);

protected:
    QHash<int, QByteArray> roleNames() const;
    QList<BonListItem*> bons;
private:
    void registerMe(EventPlayer* player);
};

#endif // BONLISTMODEL_H
