#ifndef BONLISTITEM_H
#define BONLISTITEM_H
#include <QObject>
#include "BonCore/Domain/bon.h"

class BonListItem : public QObject
{
    Q_OBJECT

    Q_PROPERTY(quint64 bonId READ bonId WRITE setBonId)
    Q_PROPERTY(int table READ table WRITE setTable)
    Q_PROPERTY(int party READ party WRITE setParty)
    Q_PROPERTY(QString tableGuid READ tableGuid WRITE setTableGuid)
    Q_PROPERTY(QDateTime creationTime READ creationTime WRITE setCreationTime)
    Q_PROPERTY(int waiter READ waiter WRITE setWaiter)
    Q_PROPERTY(QString waiterName READ waiterName WRITE setWaiterName)
    Q_PROPERTY(int center READ center WRITE setCenter)
    Q_PROPERTY(QString centerName READ centerName WRITE setCenterName)

public:
    explicit BonListItem(quint64 id, int table, int party, QObject *parent = nullptr);
    quint64 bonId() const { return _bonId; }
    void setBonId(quint64 id) { _bonId = id; }
    int table() const { return _table; }
    void setTable(int table) { _table = table; }
    int party() const { return _party; }
    void setParty(int party) { _party = party; }
    QString tableGuid() const { return _tableGuid; }
    void setTableGuid(QString value) { _tableGuid = value; }
    const QDateTime creationTime() const { return _creationTime; }
    void setCreationTime(const QDateTime& value) { _creationTime = value; }
    int waiter() const { return _waiter; }
    void setWaiter(int waiter) { _waiter = waiter; }
    const QString waiterName() const { return _waiterName; }
    void setWaiterName(QString waiter) { _waiterName = waiter; }
    int center() const { return _center; }
    void setCenter(int center) { _center = center; }
    const QString centerName() const { return _centerName; }
    void setCenterName(QString center) { _centerName = center; }
    

    QList<BonOrder*> orders;
private:
    quint64 _bonId;
    int _table;
    int _party;
    QString _tableGuid;
    QDateTime _creationTime;
    int _waiter;
    QString _waiterName;
    int _center;
    QString _centerName;
};

#endif // BONLISTITEM_H
