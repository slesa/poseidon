#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QLocale>
#include <QTranslator>
#include <QQmlContext>
#include <QtQuickControls2/QQuickStyle>

#include "Model/articlesmodel.h"
#include "Model/bonlistmodel.h"
#include "BonCore/Logic/eventplayer.h"
// #include <QtQuick>

#include "Settings/configure.h"
#include "Comm/restdataprovider.h"
#include "Comm/dataprovider.h"

// https://www.qt.io/blog/introduction-to-the-qml-cmake-api

int main(int argc, char *argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));
    QQuickStyle::setStyle("Fusion");

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    Configure configure;
    auto settings = configure.configure();

    auto rest = new RestDataProvider(settings, &app);
    RestDataProvider::setInstance(rest);
    qmlRegisterSingletonType<RestDataProvider>("DataProvider", 1, 0, "RestDataProvider", RestDataProvider::getInstance);

    qDebug() << "Starting data provider...";
    auto dataProvider = new DataProvider(settings.pollDelay(), rest);
    dataProvider->start();
    // QThreadPool takes ownership and deletes 'hello' automatically
    // QThreadPool::globalInstance()->start(dataProvider);
    qDebug() << "Starting engine...";
    QQmlApplicationEngine engine;
    auto context = engine.rootContext();
    context->setContextProperty("DataProvider", rest);


    qDebug() << "Creating models...";

    auto* player = new EventPlayer;
    QObject::connect(rest, &RestDataProvider::gotEvent, player, &EventPlayer::onGotEvent);

    auto* bonListModel = new BonListModel(player);
    context->setContextProperty("bonsModel", bonListModel);
    QObject::connect(rest, &RestDataProvider::gotBon, bonListModel, &BonListModel::onGotBon);

    auto* articlesModel = new ArticlesModel();
    QObject::connect(rest, SIGNAL(gotBon(Bon*)), articlesModel, SLOT(onGotBon(Bon*)));
    context->setContextProperty("articlesModel", articlesModel);


    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "BonClient_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            app.installTranslator(&translator);
            break;
        }
    }

#ifdef Q_OS_WINDOWS
//    engine.addImportPath("c:/work/gitlab/poseidon/bin/deploy/desktop/qml");
    engine.addImportPath("./plugins/virtualkeyboard");
#endif

    const QUrl url(QStringLiteral("qrc:/Views/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    qDebug() << "App and running";
    auto result = app.exec();
    qDebug() << "Shutting down";
    return result;
}
