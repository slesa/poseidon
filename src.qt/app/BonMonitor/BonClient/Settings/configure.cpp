#include "configure.h"
#include "basics/argumentsreader.h"
#include "basics/configreader.h"
#include <QCommandLineParser>
#include <QObject>


class AppSettingsReader: public SettingsReader
{
    Q_OBJECT
public:
    AppSettingsReader(Settings& settings)
        : hostOption("host", tr("The network address or name of the bonmonitor server"), settings.host())
        , portOption("port", tr("The network port of the bonmonitor server"), QString::number(settings.port()))
        , pollOption("poll", tr("The polling delay in milliseconds"), QString::number(settings.port()))
        , idOption("id", tr("The number of this bonmonitor client"), QString::number(settings.monitorId()))
        , nameOption("name", tr("The name of this bonmonitor client"), settings.monitorName())
        , _settings(settings)
    {
    }
    QList<QCommandLineOption> options() const
    {
        return QList<QCommandLineOption>()
                << hostOption << portOption << pollOption
                << idOption << nameOption;
    }
    QString readArguments(QCommandLineParser& parser)
    {
        if(parser.isSet(hostOption))
            _settings.setHost(parser.value(hostOption));
        if(parser.isSet(portOption)) {
            auto ok = true;
            auto port = parser.value(portOption).toInt(&ok);
            if( !ok || port<1 || port>65535 ) {
                auto msg = tr("No valid port given. It should be a number between 1 and 65535");
                return msg;
            }
            _settings.setPort(port);
        }
        if(parser.isSet(pollOption)) {
            auto ok = true;
            auto delay = parser.value(pollOption).toInt(&ok);
            if( !ok ) {
                auto msg = tr("No valid poll delay given");
                return msg;
            }
            _settings.setPollDelay(delay);
        }
        if(parser.isSet(idOption)) {
            auto ok = true;
            auto id = parser.value(idOption).toInt(&ok);
            if( !ok ) {
                auto msg = tr("No valid monitor id given");
                return msg;
            }
            _settings.setMonitorId(id);
        }
        if(parser.isSet(nameOption))
            _settings.setMonitorName(parser.value(nameOption));
        return QString();
    }
    Settings settings() { return _settings; }
private:
    QCommandLineOption hostOption;
    QCommandLineOption portOption;
    QCommandLineOption pollOption;
    QCommandLineOption idOption;
    QCommandLineOption nameOption;
    Settings _settings;
};


Settings Configure::configure()
{
    Settings settings;
    ConfigReader cr("etc/bonmonitor.ini");
    settings.setHost(cr.value("General", "host", settings.host()).toString());
    settings.setPort(cr.value("General", "port", settings.port()).toInt());
    settings.setPollDelay(cr.value("General", "poll", settings.pollDelay()).toInt());
    settings.setMonitorId(cr.value("General", "monitorId", settings.monitorId()).toInt());
    settings.setMonitorName(cr.value("General", "monitorName", settings.monitorName()).toString());

    AppSettingsReader sr(settings);
    ArgumentsReader ar;
    ar.readArguments(sr);
    return sr.settings();
}

#include "configure.moc"
