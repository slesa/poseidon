#ifndef SETTINGS_H
#define SETTINGS_H
#include <QString>

class Settings
{
public:
    Settings();

    QString host() const { return _host; }
    void setHost(const QString& host) { _host = host; }

    int port() const { return _port; }
    void setPort(int port) { _port = port; }

    int pollDelay() const { return _pollDelay; }
    void setPollDelay(int delay) { _pollDelay = delay; }

    int monitorId() const { return _monitorId; }
    void setMonitorId(int id) { _monitorId = id; }
    QString monitorName() const { return _monitorName; }
    void setMonitorName(const QString& name) { _monitorName = name; }
private:
    QString _host;
    int _port;
    int _pollDelay;
    int _monitorId;
    QString _monitorName;
};

#endif // SETTINGS_H
