#include "settings.h"

Settings::Settings()
    : _host("localhost")
    , _port(8282)
    , _pollDelay(1000)
    , _monitorId(1)
    , _monitorName("Monitor 1")
{
}
