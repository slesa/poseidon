#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QLocale>
#include <QTranslator>
#include <QQmlContext>
#include <QtQuickControls2/QQuickStyle>
#include "notificationclient.h"

#include "Settings/configure.h"
#include "Comm/restcommandhandler.h"
#include "Comm/pagerconnection.h"

/* Examples Qml" contactlist, recipebrowser
 */

int main(int argc, char *argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));
    //QQuickStyle::setStyle("Fusion");

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    Configure configure;
    auto settings = configure.configure();
    //auto settings2 = new Settings();

    auto rest = new RestCommandHandler(settings, &app);
    RestCommandHandler::setInstance(rest);
    qmlRegisterSingletonType<RestCommandHandler>("CommandHandler", 1, 0, "CommandHandler", RestCommandHandler::getInstance);

    auto conn = new PagerConnection(settings, &app);
    PagerConnection::setInstance(conn);
    qmlRegisterSingletonType<PagerConnection>("PagerConnection", 1, 0, "PagerConnection", PagerConnection::getInstance);
    QObject::connect(conn, &PagerConnection::gotMessage, [](QString msg) {
        NotificationClient().setNotification(msg);
    });

    conn->start();

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "PagerClient_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            app.installTranslator(&translator);
            break;
        }
    }

    QQmlApplicationEngine engine;
    auto context = engine.rootContext();
    //context->setContextProperty("Settings", settings2);

    const QUrl url(QStringLiteral("qrc:/Views/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
