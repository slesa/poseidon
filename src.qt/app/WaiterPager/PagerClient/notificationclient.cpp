#include "notificationclient.h"
#include <QtCore/qjniobject.h>
#include <QtCore/qcoreapplication.h>
#include <QDebug>

NotificationClient::NotificationClient(QObject *parent)
    : QObject(parent)
{
    connect(this, &NotificationClient::notificationChanged,
            this, &NotificationClient::updateAndroidNotification);
}

void NotificationClient::setNotification(const QString &notification)
{
    if (_notification == notification)
        return;

//! [notification changed signal]
    _notification = notification;
    emit notificationChanged();
//! [notification changed signal]
}

QString NotificationClient::notification() const
{
    return _notification;
}

//! [Send notification message to Java]
void NotificationClient::updateAndroidNotification()
{
#ifdef ANDROID
    QJniObject javaNotification = QJniObject::fromString(_notification);
    QJniObject::callStaticMethod<void>(
                    "de/poseidon/waiterpager/NotificationClient",
                    "notify",
                    "(Landroid/content/Context;Ljava/lang/String;)V",
                    QNativeInterface::QAndroidApplication::context(),
                    javaNotification.object<jstring>());
#else
    qDebug() << "Sending " << _notification << " as notification";
#endif
}
//! [Send notification message to Java]
