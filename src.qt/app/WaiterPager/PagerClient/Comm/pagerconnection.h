#ifndef PAGERCONNECTION_H
#define PAGERCONNECTION_H
#include "Settings/settings.h"
#include <QStateMachine>
#include <QtWebSockets/QWebSocket>
#include <QObject>

class QQmlEngine;
class QJSEngine;

class PagerConnection : public QStateMachine
{
    Q_OBJECT
    Q_CLASSINFO("Version", "1.0.0")
public:
    explicit PagerConnection(Settings settings, QObject *parent = nullptr);
    static void setInstance(PagerConnection* instance);
    static QObject* getInstance(QQmlEngine* engine, QJSEngine* scriptEngine);
    void start();

signals:
    void gotMessage(QString);

private slots:
    void stop();
    void onTryConnect();
    void onConnected();
    void onMessage(QString);

private:
    QUrl getEndPoint(QString func);

private:
    static PagerConnection* _instance;
    int _delay;
    Settings _settings;
    QWebSocket* _ws;
};

#endif // PAGERCONNECTION_H
