#ifndef RESTCOMMANDHANDLER_H
#define RESTCOMMANDHANDLER_H
#include "Settings/settings.h"
#include <QNetworkAccessManager>
#include <QObject>

class QQmlEngine;
class QJSEngine;

class RestCommandHandler : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("Version", "1.0.0")
    //QML_ELEMENT
public:
    explicit RestCommandHandler(Settings settings, QObject *parent = nullptr);
    static void setInstance(RestCommandHandler* instance);
    static QObject* getInstance(QQmlEngine* engine, QJSEngine* scriptEngine);

    Q_INVOKABLE void sendMessage(int waiter, int pager, const QString& msg);

signals:
    void networkError();
    void replyError();
    void pagerError(QString);
    void msgSent();

private slots:
    void handleSendMessage(QNetworkReply* reply);

private:
    bool getJsonDocument(QNetworkReply* reply, QJsonDocument& jsonDoc);
    QUrl getEndPoint(QString func);

protected:
    static RestCommandHandler* _instance;
    Settings _settings;
    QNetworkAccessManager _restClient;

};

#endif // RESTCOMMANDHANDLER_H
