#include "pagerconnection.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QHostInfo>
#include <QTimer>
#include <QQmlEngine>
#include <QDebug>

PagerConnection* PagerConnection::_instance = nullptr;

PagerConnection::PagerConnection(Settings settings, QObject *parent)
    : QStateMachine(parent)
    , _delay(3000)
    , _settings(settings)
    , _ws(nullptr)
{
}

void PagerConnection::setInstance(PagerConnection* instance) {
    _instance = instance;
}

QObject* PagerConnection::getInstance(QQmlEngine* engine, QJSEngine* scriptEngine) {
    auto provider = _instance;
    QQmlEngine::setObjectOwnership(provider, QQmlEngine::CppOwnership);
    return provider;
}

void PagerConnection::start()
{
    if( !_ws ) {
        _ws = new QWebSocket();
        connect(_ws, &QWebSocket::textMessageReceived, this, &PagerConnection::onMessage);
    }
    auto initState = new QState(this);
    connect(initState, &QAbstractState::entered, this, &PagerConnection::onTryConnect);
    auto connectedState = new QState(this);
    connect(connectedState, &QAbstractState::entered, this, &PagerConnection::onConnected);
    auto waitState = new QState(this);
    auto timer = new QTimer(waitState);
    timer->setInterval(_delay);
    timer->setSingleShot(true);
    QObject::connect(waitState, &QAbstractState::entered, timer, QOverload<>::of(&QTimer::start));
    waitState->addTransition(timer, &QTimer::timeout, initState);
    initState->addTransition(_ws, &QWebSocket::connected, connectedState);
    connectedState->addTransition(_ws, &QWebSocket::disconnected, initState);

    setInitialState(initState);
    connect(this, &QStateMachine::stopped, this, &PagerConnection::stop);
    QStateMachine::start();
}

void PagerConnection::onTryConnect()
{
    auto url = getEndPoint("register");
    qDebug() << "Connecting ws " <<url;
    _ws->open(url);
}

void PagerConnection::onMessage(QString msg)
{
    qDebug() << "Got message " << msg;
    emit gotMessage(msg);
    _ws->sendTextMessage("ok");
}

void PagerConnection::onConnected()
{
    QHostInfo info;
    /*qDebug() << "Host: " << info.hostName();
    qDebug() << "Local host: " << info.localHostName();
    qDebug() << "Local domain: " << info.localDomainName();
    auto addresses = info.addresses();
    foreach(auto address, addresses) {
        qDebug() << address << ":";
    }*/
    QJsonObject json;
    json.insert("hostName", info.localHostName());
    json.insert("pagerId", _settings.pagerId());
    json.insert("pagerName", _settings.pagerName());
    _ws->sendTextMessage(QJsonDocument(json).toJson());
}

void PagerConnection::stop()
{
    if(_ws ) {
        delete _ws;
        _ws = nullptr;
    }
    QObject::deleteLater();
}

QUrl PagerConnection::getEndPoint(QString func)
{
    QUrl url;
    url.setScheme("ws");
    url.setHost(_settings.host());
    url.setPort(_settings.port());
    url.setPath(QString("/bm/pager/%1").arg(func));
    return url;
}

// #include "pagerconnection.moc"
