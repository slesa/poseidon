#include "restcommandhandler.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QNetworkReply>
#include <QQmlEngine>

RestCommandHandler* RestCommandHandler::_instance = nullptr;

RestCommandHandler::RestCommandHandler(Settings settings, QObject *parent)
    : QObject(parent)
    , _settings(settings)
    , _restClient(this)
{
}

void RestCommandHandler::setInstance(RestCommandHandler* instance) {
    _instance = instance;
}

QObject* RestCommandHandler::getInstance(QQmlEngine* engine, QJSEngine* scriptEngine) {
    auto provider = _instance;
    QQmlEngine::setObjectOwnership(provider, QQmlEngine::CppOwnership);
    return provider;
}

void RestCommandHandler::sendMessage(int waiter, int pager, const QString& msg)
{
    // we are no kind of bonmonitor, so first param is 0
    //auto func = QStringLiteral("pager/0/%1/%2").arg(waiter).arg(pager);
    auto url = getEndPoint("pager/wp/");
    connect(&_restClient, &QNetworkAccessManager::finished, this, &RestCommandHandler::handleSendMessage);
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QJsonObject body;
    body.insert("waiter", waiter);
    body.insert("pager", pager);
    body.insert("msg", msg);

    _restClient.post(request, QJsonDocument(body).toJson());
}

void RestCommandHandler::handleSendMessage(QNetworkReply* reply)
{
    disconnect(&_restClient, &QNetworkAccessManager::finished, this, &RestCommandHandler::handleSendMessage);
    //disconnect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleSendMessage(QNetworkReply*)));
    auto error = reply->error();
    auto errstr = reply->errorString();
    if( reply->error()!=QNetworkReply::NoError ) {
        qDebug() << "Could not send message: " << reply->errorString();
        emit pagerError(QStringLiteral("%1\n%2:%3").arg(reply->errorString()).arg(_settings.host()).arg(_settings.port()));
//        emit networkError();
    } else {
        QJsonDocument jsonDoc;
        if( !getJsonDocument(reply, jsonDoc)) {
            qDebug() << "Could not read anser";
            // emit replyError();
            return;
        }

        auto errorCode = jsonDoc["error"].toInteger();
        if (errorCode>0 ) {
            auto errorMsg = jsonDoc["errorMsg"].toString();
            emit pagerError(errorMsg);
            return;
        }

        qDebug() << "Message sent";
        emit msgSent();
    }
    reply->deleteLater();
}

bool RestCommandHandler::getJsonDocument(QNetworkReply* reply, QJsonDocument& jsonDoc)
{
    auto data = reply->readAll();
    //auto restData = QString::fromUtf8(data);
    //qDebug() << "REST data " << restData;
    jsonDoc = QJsonDocument::fromJson(data);
    // auto jsonDoc = QJsonDocument::fromBinaryData(data);
    if( jsonDoc.isNull()) {
        qDebug() << "JSonDoc is null";
        return false;
    }
    if( jsonDoc.isEmpty() ) {
        qDebug() << "JSonDoc is empty";
        return false;
    }
    if( !jsonDoc.isObject() ) {
        qDebug() << "JSonDoc is no object";
        return false;
    }
    return true;
}

QUrl RestCommandHandler::getEndPoint(QString func)
{
    QUrl url;
    url.setScheme("http");
    url.setHost(_settings.host());
    url.setPort(_settings.port());
    url.setPath(QString("/bm/%1").arg(func));
    return url;
}
