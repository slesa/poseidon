<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutForm.ui</name>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="6"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="35"/>
        <source>Waiter Pager v0.0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="40"/>
        <source>(c) 42 GmbH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="45"/>
        <source>Call your waiters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="50"/>
        <source>from where you want</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="55"/>
        <source>to where you want them!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppSettingsReader</name>
    <message>
        <location filename="Settings/configure.cpp" line="12"/>
        <source>The network adress or name of the bonmonitor server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="13"/>
        <source>The network port of the bonmonitor server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="14"/>
        <source>The number of this pager client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="15"/>
        <source>The name of this pager client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="33"/>
        <source>No valid port given. It should be a number between 1 and 65535</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="42"/>
        <source>No valid monitor id given</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HomeForm</name>
    <message>
        <location filename="Views/HomeForm.qml" line="11"/>
        <source>Waiter Pager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/HomeForm.qml" line="46"/>
        <source>Message to send:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/HomeForm.qml" line="52"/>
        <source>To pager:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/HomeForm.qml" line="61"/>
        <source>To waiter:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/HomeForm.qml" line="72"/>
        <source>Send message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/HomeForm.qml" line="104"/>
        <source>Network error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/HomeForm.qml" line="105"/>
        <source>Error in reply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/HomeForm.qml" line="106"/>
        <source>Pager error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/HomeForm.qml" line="107"/>
        <source>Message sent</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsForm.ui</name>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="9"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="30"/>
        <source>Pager &amp;name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="37"/>
        <source>Pager &amp;id:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="46"/>
        <source>BonMonitor &amp;port:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="55"/>
        <source>BonMonitor &amp;host:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="58"/>
        <source>IP address of the BonMonitor server</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="Views/main.qml" line="10"/>
        <source>Waiter Pager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/main.qml" line="43"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/main.qml" line="51"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
