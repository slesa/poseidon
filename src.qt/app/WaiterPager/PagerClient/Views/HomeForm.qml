import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import CommandHandler
import PagerConnection

Item {
    id: root
    anchors.fill: parent

    property string title: qsTr("Waiter Pager")

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 20
        spacing: 5

        Frame {
            Layout.fillWidth: parent
/*            Rectangle {
                color: 'blue'
                anchors.fill: parent
            } */
            RowLayout {
                anchors.fill: parent
                Text {
                    id: receviedMsg
                    // PlaceholderText: qsTr("Receivied message")
                }
                Text {
                    id: receviedAt
                    // PlaceholderText: qsTr("Receivied at")
                }
            }
        }

        Frame {
            Layout.fillWidth: parent
            ColumnLayout {
                anchors.fill: parent
                GridLayout {
                    id: sendGrid
                    columns: 2

                    FormControl {
                        label.text: qsTr("Message to send:")
                        control: TextField {
                            id: msgText
                        }
                    }
                    FormControl {
                        label.text: qsTr("To pager:")
                        control: SpinBox {
                            id: pagerId
                            editable: true
                            from: 1
                            to: 1000
                        }
                    }
                    FormControl {
                        label.text: qsTr("To waiter:")
                        control: SpinBox {
                            id: waiterId
                            editable: true
                            from: 1
                            to: 1000
                        }
                    }
                }
                Button {
                    id: sendMsg
                    text: qsTr("Send message")
                    onClicked: CommandHandler.sendMessage(waiterId.value, pagerId.value, msgText.text)
                }
            }
        }
        Frame {
            id: statusLine
            visible: false
            Layout.fillWidth: parent
            /* Rectangle {
                anchors.fill: parent
                color: 'red'
            } */
            Text {
                anchors.fill: parent
                id: statusText
                text: ""
                onTextChanged: {
                    statusLine.visible = statusText.text
                    clearTimer.start()
                }
            }
            Timer {
                id: clearTimer
                interval: 3000
                repeat: false
                onTriggered: statusText.text = ""
            }
        }

        Connections {
            target: CommandHandler
            function onNetworkError() { statusText.text = qsTr("Network error") }
            function onReplyError() { statusText.text = qsTr("Error in reply") }
            function onPagerError(msg) { statusText.text = qsTr("Pager error: %1").arg(msg) }
            function onMsgSent() { statusText.text = qsTr("Message sent") }
        }
        Connections {
            target: PagerConnection
            function onGotMessage(msg) {
                receviedAt.text = Qt.formatDateTime(new Date(), Locale.ShortFormat)
                receviedMsg.text = msg
            }
        }
    }
}
