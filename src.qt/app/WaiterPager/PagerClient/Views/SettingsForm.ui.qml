import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import Qt.labs.settings 1.0

Page {
    id: root
    // anchors.fill: parent
    title: qsTr("Settings")

    Settings {
        category: "General"
        fileName: "etc/waiterpager.ini"
        property alias host: textHost.text
        property alias port: spinPort.value
        property alias pagerId: spinPagerId.value
        property alias pagerName: textPagerName.text
    }

    GridLayout {
        id: grid
        //height: parent.height-filler.height
        anchors { top: parent.top; left: parent.left; right: parent.right; margins: 20 }
        //width: parent.width
        //anchors.fill: parent
        columns: 2
        //rowSpan: 10

        FormControl {
            label.text: qsTr("Pager &name:")
            control: TextField {
                id: textPagerName
                placeholderText: qsTr("Pager " + spinPagerId.value)
            }
        }
        FormControl {
            label.text: qsTr("Pager &id:")
            control: SpinBox {
                id: spinPagerId
                editable: true
                from: 1
                to: 1000
            }
        }
        FormControl {
            label.text: qsTr("BonMonitor &port:")
            control: SpinBox {
                id: spinPort
                editable: true
                from: 1
                to: 65535
            }
        }
        FormControl {
            label.text: qsTr("BonMonitor &host:")
            control: TextField {
                id: textHost
                placeholderText: qsTr("IP address of the BonMonitor server")
            }
        }
    }
}
