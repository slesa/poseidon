#ifndef SETTINGS_H
#define SETTINGS_H
#include <QString>

class Settings
{
public:
    Settings();

    QString host() const { return _host; }
    void setHost(const QString& host) { _host = host; }

    int port() const { return _port; }
    void setPort(int port) { _port = port; }

    int pagerId() const { return _pagerId; }
    void setPagerId(int id) { _pagerId = id; }
    QString pagerName() const { return _pagerName; }
    void setPagerName(const QString& name) { _pagerName = name; }

private:
    QString _host;
    int _port;
    int _pagerId;
    QString _pagerName;
};

#endif // SETTINGS_H
