include(${CMAKE_CURRENT_LIST_DIR}/../CMakeLists.inc)

if(NOT ANDROID)
    message(STATUS "Setting out paths to deploy")
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${OUTS_DIR})
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
endif()

include_directories( 
    ${CMAKE_CURRENT_SOURCE_DIR}/.
    ${CMAKE_CURRENT_LIST_DIR}/PosClient.Core
    ${INCL_DIR}
)
