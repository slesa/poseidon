#ifndef CONFIGURE_H
#define CONFIGURE_H
#include "settings.h"

class Configure
{
public:
    static Settings configure();
};

#endif // CONFIGURE_H
