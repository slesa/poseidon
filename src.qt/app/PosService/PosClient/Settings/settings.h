#ifndef SETTINGS_H
#define SETTINGS_H
#include <QString>

class Settings
{
public:
    Settings();

    QString host() const { return _host; }
    void setHost(const QString& host) { _host = host; }

    int port() const { return _port; }
    void setPort(int port) { _port = port; }

    int terminalId() const { return _terminalId; }
    void setTerminalId(int terminal) { _terminalId = terminal; }
    int waiterId() const { return _waiterId; }
    void setWaiterId(int waiter) { _waiterId = waiter; }
    QString password() const { return _password; }
    void setPassword(const QString& password) { _password = password; }

private:
    QString _host;
    int _port;
    int _terminalId;
    int _waiterId;
    QString _password;
};

#endif // SETTINGS_H
