#include "configure.h"
#include "basics/argumentsreader.h"
#include "basics/configreader.h"
#include <QCommandLineParser>
#include <QObject>

class AppSettingsReader: public SettingsReader
{
Q_OBJECT
public:
    AppSettingsReader(Settings& settings)
            : hostOption("host", tr("The network address or name of the POS server"), settings.host())
            , portOption("port", tr("The network port of the POS server"), QString::number(settings.port()))
//            , pollOption("poll", tr("The polling delay in milliseconds"), QString::number(settings.port()))
            , termOption("terminal", tr("The terminal number of this POS client"), QString::number(settings.terminalId()))
            , waitOption("waiter", tr("The fixed waiter number of this POS client"), QString::number(settings.waiterId()))
            , _settings(settings)
    {
    }
    QList<QCommandLineOption> options() const
    {
        return QList<QCommandLineOption>()
                << hostOption << portOption //<< pollOption
                << termOption << waitOption;
    }
    QString readArguments(QCommandLineParser& parser)
    {
        if(parser.isSet(hostOption))
            _settings.setHost(parser.value(hostOption));
        if(parser.isSet(portOption)) {
            auto ok = true;
            auto port = parser.value(portOption).toInt(&ok);
            if( !ok || port<1 || port>65535 ) {
                auto msg = tr("No valid port given. It should be a number between 1 and 65535");
                return msg;
            }
            _settings.setPort(port);
        }
        /*if(parser.isSet(pollOption)) {
            auto ok = true;
            auto delay = parser.value(pollOption).toInt(&ok);
            if( !ok ) {
                auto msg = tr("No valid poll delay given");
                return msg;
            }
            _settings.setPollDelay(delay);
        }*/
        if(parser.isSet(termOption)) {
            auto ok = true;
            auto id = parser.value(termOption).toInt(&ok);
            if( !ok ) {
                auto msg = tr("No valid terminal id given");
                return msg;
            }
            _settings.setTerminalId(id);
        }
        if(parser.isSet(waitOption)) {
            auto ok = true;
            auto id = parser.value(waitOption).toInt(&ok);
            if( !ok ) {
                auto msg = tr("No valid waiter id given");
                return msg;
            }
            _settings.setWaiterId(id);
        }
        return QString();
    }
    Settings settings() { return _settings; }
private:
    QCommandLineOption hostOption;
    QCommandLineOption portOption;
    //QCommandLineOption pollOption;
    QCommandLineOption termOption;
    QCommandLineOption waitOption;
    //QCommandLineOption nameOption;
    Settings _settings;
};

Settings Configure::configure()
{
    Settings settings;
    ConfigReader cr("etc/posclient.ini");

    settings.setHost(cr.value("General", "host", settings.host()).toString());
    settings.setPort(cr.value("General", "port", settings.port()).toInt());
    //settings.setPollDelay(cr.value("General", "poll", settings.pollDelay()).toInt());
    settings.setTerminalId(cr.value("General", "terminal", settings.terminalId()).toInt());
    settings.setWaiterId(cr.value("General", "waiter", settings.waiterId()).toInt());
    settings.setPassword(cr.value("General", "password", settings.password()).toString());

    AppSettingsReader sr(settings);
    ArgumentsReader ar;
    ar.readArguments(sr);
    return sr.settings();
}

#include "configure.moc"
