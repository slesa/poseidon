#include "coredatahandler.h"
#include "Domain/article.h"
#include "Domain/family.h"
#include "Domain/familygroup.h"
#include "Domain/waiter.h"
#include "Domain/terminal.h"
#include <QNetworkReply>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QQmlEngine>

CoreDataHandler* CoreDataHandler::_instance = nullptr;

CoreDataHandler::CoreDataHandler(Settings settings, QObject *parent)
    : RestBase(settings, parent)
{
}

void CoreDataHandler::setInstance(CoreDataHandler* instance) {
    _instance = instance;
}

QObject* CoreDataHandler::getInstance(QQmlEngine* engine, QJSEngine* scriptEngine) {
    auto provider = _instance;
    QQmlEngine::setObjectOwnership(provider, QQmlEngine::CppOwnership);
    return provider;
}

void CoreDataHandler::getArticles() {
    auto func = QStringLiteral("data/articles");
    auto url = getEndPoint(func);

    // qDebug() << "Requesting bons at " << func;
    connect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleArticles(QNetworkReply*)));
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    _restClient.get(request);
}

void CoreDataHandler::handleArticles(QNetworkReply* reply) {
    disconnect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleArticles(QNetworkReply*)));

    if( reply->error()!=QNetworkReply::NoError ) {
        qDebug() << "Could not get articles: " << reply->errorString();
        emit noConnection();
        return;
    }

    QJsonDocument jsonDoc;
    if( !getJsonDocument(reply, jsonDoc)) {
        //emit eventsHandled();
        return;
    }

    auto articles = jsonDoc.array();
    for(auto idx=0; idx<articles.size(); ++idx)
    {
        auto jsonEvent = articles[idx].toObject();
        auto article = new Article();
        auto plu = jsonEvent["plu"].toInt();
        article->setPlu(plu);
        auto name = jsonEvent["name"].toString();
        article->setName(name);
        auto family = jsonEvent["family"].toInt();
        article->setFamily(family);

        //qDebug() << "Article " << article->plu() << article->name();
        emit gotArticle(article);
        //qDebug() << Qt::endl;
    }
    emit gotArticles();
}


void CoreDataHandler::getFamilies() {
    auto func = QStringLiteral("data/families");
    auto url = getEndPoint(func);

    connect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleFamilies(QNetworkReply*)));
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    _restClient.get(request);
}

void CoreDataHandler::handleFamilies(QNetworkReply* reply) {
    disconnect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleFamilies(QNetworkReply*)));

    if( reply->error()!=QNetworkReply::NoError ) {
        qDebug() << "Could not get families: " << reply->errorString();
        emit noConnection();
        return;
    }

    QJsonDocument jsonDoc;
    if( !getJsonDocument(reply, jsonDoc)) {
        //emit eventsHandled();
        return;
    }

    //auto articles = jsonDoc["data"].toArray();
    auto families = jsonDoc.array();
    for(auto idx=0; idx<families.size(); ++idx)
    {
        auto jsonEvent = families[idx].toObject();
        auto family = new Family();
        auto id = jsonEvent["id"].toInt();
        family->setId(id);
        auto name = jsonEvent["name"].toString();
        family->setName(name);
        auto group = jsonEvent["familyGroup"].toInt();
        family->setFamilyGroup(group);
        emit gotFamily(family);
    }
    emit gotFamilies();
}


void CoreDataHandler::getFamilyGroups() {
    auto func = QStringLiteral("data/famgroups");
    auto url = getEndPoint(func);

    connect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleFamilyGroups(QNetworkReply*)));
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    _restClient.get(request);
}

void CoreDataHandler::handleFamilyGroups(QNetworkReply* reply) {
    disconnect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleFamilyGroups(QNetworkReply*)));

    if( reply->error()!=QNetworkReply::NoError ) {
        qDebug() << "Could not get family groups: " << reply->errorString();
        emit noConnection();
        return;
    }

    QJsonDocument jsonDoc;
    if( !getJsonDocument(reply, jsonDoc)) {
        //emit eventsHandled();
        return;
    }

    auto groups = jsonDoc.array();
    for(auto idx=0; idx<groups.size(); ++idx)
    {
        auto jsonEvent = groups[idx].toObject();
        auto group = new FamilyGroup();
        auto id = jsonEvent["id"].toInt();
        group->setId(id);
        auto name = jsonEvent["name"].toString();
        group->setName(name);
        emit gotFamilyGroup(group);
    }
    emit gotFamilyGroups();
}

void CoreDataHandler::getWaiters() {
    auto func = QStringLiteral("data/waiters");
    auto url = getEndPoint(func);

    connect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleWaiters(QNetworkReply*)));
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    _restClient.get(request);
}

void CoreDataHandler::handleWaiters(QNetworkReply* reply) {
    disconnect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleWaiters(QNetworkReply*)));

    if( reply->error()!=QNetworkReply::NoError ) {
        qDebug() << "Could not get waiters: " << reply->errorString();
        emit noConnection();
        return;
    }

    QJsonDocument jsonDoc;
    if( !getJsonDocument(reply, jsonDoc)) {
        //emit eventsHandled();
        return;
    }

    auto waiters = jsonDoc.array();
    for(auto idx=0; idx<waiters.size(); ++idx)
    {
        auto jsonEvent = waiters[idx].toObject();
        auto id = jsonEvent["id"].toInt();
        auto name = jsonEvent["name"].toString();
        auto waiter = new Waiter(id, name);
        //waiter->setId(id);
        //waiter->setName(name);

        //qDebug() << "Waiter " << waiter->id() << waiter->name();
        emit gotWaiter(waiter);
        //qDebug() << Qt::endl;
    }
}


void CoreDataHandler::getTerminals() {
    auto func = QStringLiteral("data/terminals");
    auto url = getEndPoint(func);

    connect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleTerminals(QNetworkReply*)));
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    _restClient.get(request);
}

void CoreDataHandler::handleTerminals(QNetworkReply* reply) {
    disconnect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleTerminals(QNetworkReply*)));

    if( reply->error()!=QNetworkReply::NoError ) {
        qDebug() << "Could not get terminals: " << reply->errorString();
        emit noConnection();
        return;
    }

    QJsonDocument jsonDoc;
    if( !getJsonDocument(reply, jsonDoc)) {
        //emit eventsHandled();
        return;
    }

    auto terminals = jsonDoc.array();
    for(auto idx=0; idx<terminals.size(); ++idx)
    {
        auto jsonEvent = terminals[idx].toObject();
        auto id = jsonEvent["id"].toInt();
        auto name = jsonEvent["name"].toString();
        auto terminal = new Terminal(id, name);
        //terminal->setId(id);
        //terminal->setName(name);

        //qDebug() << "Terminal " << terminal->id() << terminal->name();
        emit gotTerminal(terminal);
        //qDebug() << Qt::endl;
    }
}


bool CoreDataHandler::getJsonDocument(QNetworkReply* reply, QJsonDocument& jsonDoc)
{
    auto data = reply->readAll();
    //auto restData = QString::fromUtf8(data);
    //qDebug() << "REST data " << restData;
    jsonDoc = QJsonDocument::fromJson(data);
    // auto jsonDoc = QJsonDocument::fromBinaryData(data);
    if( jsonDoc.isNull()) {
        qDebug() << "JSonDoc is null";
        return false;
    }
    if( jsonDoc.isEmpty() ) {
        qDebug() << "JSonDoc is empty";
        return false;
    }
    /* if( !jsonDoc.isObject() ) {
        qDebug() << "JSonDoc is no object";
        return false;
    }*/
    return true;
}
