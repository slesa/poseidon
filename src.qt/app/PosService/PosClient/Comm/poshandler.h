#ifndef POSHANDLER_H
#define POSHANDLER_H
#include "restbase.h"
#include <QUuid>

class QQmlEngine;
class QJSEngine;
namespace PosClientCore::Domain {
    class Article;
    class Terminal;
    class Waiter;
}

class PosHandler : public RestBase
{
    Q_OBJECT
    Q_CLASSINFO("Version", "1.0.0")
public:
    explicit PosHandler(Settings settings, QObject *parent = nullptr);
    static void setInstance(PosHandler* instance);
    static QObject* getInstance(QQmlEngine* engine, QJSEngine* scriptEngine);

    Q_INVOKABLE void turnon(int terminalId);
    Q_INVOKABLE void login(int waiterId, const QString& password);
    Q_INVOKABLE void logout();

signals:
    void turnedOn(QUuid sessionId, PosClientCore::Domain::Terminal* terminal);
    void loggedIn(PosClientCore::Domain::Waiter* waiter);
    void loggedOut();

private slots:
    void handleTurnOn(QNetworkReply* reply);
    void handleLogin(QNetworkReply* reply);
    void handleLogout(QNetworkReply* reply);

private:
    QString SessionId();

private:
    static PosHandler* _instance;
    QUuid _sessionId;
};

#endif // POSHANDLER_H
