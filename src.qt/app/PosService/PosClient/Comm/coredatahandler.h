#ifndef COREDATAHANDLER_H
#define COREDATAHANDLER_H
#include "restbase.h"
#include <QJsonObject>
#include <QObject>

class QQmlEngine;
class QJSEngine;
namespace PosClientCore::Domain {
    class Article;
    class Family;
    class FamilyGroup;
    class Terminal;
    class Waiter;
}

class CoreDataHandler : public RestBase
{
    Q_OBJECT
    Q_CLASSINFO("Version", "1.0.0")
public:
    explicit CoreDataHandler(Settings settings, QObject *parent = nullptr);
    static void setInstance(CoreDataHandler* instance);
    static QObject* getInstance(QQmlEngine* engine, QJSEngine* scriptEngine);

    Q_INVOKABLE void getArticles();
    Q_INVOKABLE void getFamilies();
    Q_INVOKABLE void getFamilyGroups();
    Q_INVOKABLE void getWaiters();
    Q_INVOKABLE void getTerminals();

signals:
    void noConnection();
    void gotArticle(const PosClientCore::Domain::Article*);
    void gotArticles();
    void gotFamily(const PosClientCore::Domain::Family*);
    void gotFamilies();
    void gotFamilyGroup(const PosClientCore::Domain::FamilyGroup*);
    void gotFamilyGroups();
    void gotWaiter(const PosClientCore::Domain::Waiter*);
    void gotTerminal(const PosClientCore::Domain::Terminal*);

private slots:
    void handleArticles(QNetworkReply* reply);
    void handleFamilies(QNetworkReply* reply);
    void handleFamilyGroups(QNetworkReply* reply);
    void handleWaiters(QNetworkReply* reply);
    void handleTerminals(QNetworkReply* reply);

private:
    bool getJsonDocument(QNetworkReply* reply, QJsonDocument& jsonDoc);

private:
    static CoreDataHandler* _instance;

};


#endif //COREDATAHANDLER_H
