#include "poshandler.h"
#include "Domain/article.h"
#include "Domain/waiter.h"
#include "Domain/terminal.h"
#include <QQmlEngine>
#include <QJsonObject>
#include <QJsonDocument>
#include <QNetworkReply>

PosHandler* PosHandler::_instance = nullptr;

PosHandler::PosHandler(Settings settings, QObject *parent)
    : RestBase(settings, parent)
{
}


void PosHandler::setInstance(PosHandler* instance) {
    _instance = instance;
}

QObject* PosHandler::getInstance(QQmlEngine* engine, QJSEngine* scriptEngine) {
    auto provider = _instance;
    QQmlEngine::setObjectOwnership(provider, QQmlEngine::CppOwnership);
    return provider;
}



void PosHandler::turnon(int terminalId) {
    auto func = QStringLiteral("terminal/turnon");
    auto url = getEndPoint(func);

    QJsonObject body;
    body.insert("terminal", terminalId);

    // qDebug() << "Requesting bons at " << func;
    connect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleTurnOn(QNetworkReply*)));
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    _restClient.post(request, QJsonDocument(body).toJson());
}


void PosHandler::handleTurnOn(QNetworkReply* reply) {
    disconnect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleTurnOn(QNetworkReply*)));

    if( reply->error()!=QNetworkReply::NoError ) {
        qDebug() << "Could not turnon terminal: " << reply->errorString();
        reply->deleteLater();
        //emit noConnection();
        return;
    }

    QJsonDocument jsonDoc;
    if( !getJsonDocument(reply, jsonDoc)) {
        reply->deleteLater();
        //emit eventsHandled();
        return;
    }

    //auto articles = jsonDoc["data"].toArray();
    auto result = jsonDoc.object();
    auto status = result["status"].toInt(-1);
    if(status) {
        reply->deleteLater();
        return;
    }
    _sessionId = QUuid::fromString(result["sessionId"].toString());
    auto term = result["terminal"].toObject();
    auto id = term["id"].toInt();
    auto name = term["name"].toString();
    auto terminal = new Terminal(id, name);
    emit turnedOn(_sessionId, terminal);
    reply->deleteLater();
}


void PosHandler::login(int waiterId, const QString& password) {
    auto func = QStringLiteral("terminal/setwaiter");
    auto url = getEndPoint(func);

    QJsonObject body;
    body.insert("sessionId", SessionId());
    body.insert("waiter", waiterId);
    if( !password.isEmpty() )
        body.insert("password", password);

    // qDebug() << "Requesting bons at " << func;
    connect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleLogin(QNetworkReply*)));
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    _restClient.post(request, QJsonDocument(body).toJson());
}

void PosHandler::handleLogin(QNetworkReply* reply) {
    disconnect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleLogin(QNetworkReply*)));

    if( reply->error()!=QNetworkReply::NoError ) {
        qDebug() << "Could not login waiter: " << reply->errorString();
        reply->deleteLater();
        //emit noConnection();
        return;
    }

    QJsonDocument jsonDoc;
    if( !getJsonDocument(reply, jsonDoc)) {
        reply->deleteLater();
        //emit eventsHandled();
        return;
    }

    //auto articles = jsonDoc["data"].toArray();
    auto result = jsonDoc.object();
    auto status = result["status"].toInt(-1);
    qDebug() << "Login waiter status: " << status;
    if(status) {
        reply->deleteLater();
        return;
    }
    //auto sessionId = QUuid::fromString(result["sessionId"].toString());
    auto wait = result["waiter"].toObject();
    auto id = wait["id"].toInt();
    auto name = wait["name"].toString();
    auto waiter = new Waiter(id, name);
    emit loggedIn(waiter);
    reply->deleteLater();
}


void PosHandler::logout() {
    auto func = QStringLiteral("terminal/clrwaiter");
    auto url = getEndPoint(func);

    QJsonObject body;
    body.insert("sessionId", SessionId());

    // qDebug() << "Requesting bons at " << func;
    connect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleLogout(QNetworkReply*)));
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    _restClient.post(request, QJsonDocument(body).toJson());
}

void PosHandler::handleLogout(QNetworkReply* reply) {
    disconnect(&_restClient, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleLogout(QNetworkReply*)));

    if( reply->error()!=QNetworkReply::NoError ) {
        qDebug() << "Could not logout waiter: " << reply->errorString();
        reply->deleteLater();
        //emit noConnection();
        return;
    }

    QJsonDocument jsonDoc;
    if( !getJsonDocument(reply, jsonDoc)) {
        reply->deleteLater();
        //emit eventsHandled();
        return;
    }

    //auto articles = jsonDoc["data"].toArray();
    auto result = jsonDoc.object();
    auto status = result["status"].toInt(-1);
    qDebug() << "Logout waiter status: " << status;
    if(status) {
        reply->deleteLater();
        return;
    }
    //auto sessionId = QUuid::fromString(result["sessionId"].toString());
    emit loggedOut();
    reply->deleteLater();
}


QString PosHandler::SessionId() {
    return _sessionId.toString().remove('{').remove('}');
}
