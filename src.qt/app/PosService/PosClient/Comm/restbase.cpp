#include "restbase.h"
#include <QNetworkReply>
#include <QJsonDocument>

RestBase::RestBase(Settings settings, QObject *parent)
    : QObject(parent)
    , _settings(settings)
    , _restClient(this)
{
    //_restClient.setAutoDeleteReplies(true);
}

QUrl RestBase::getEndPoint(QString func)
{
    QUrl url;
    url.setScheme("http");
    url.setHost(_settings.host());
    url.setPort(_settings.port());
    url.setPath(QString("/pos/%1").arg(func));
    return url;
}

bool RestBase::getJsonDocument(QNetworkReply* reply, QJsonDocument& jsonDoc)
{
    auto data = reply->readAll();
    //auto restData = QString::fromUtf8(data);
    //qDebug() << "REST data " << restData;
    jsonDoc = QJsonDocument::fromJson(data);
    // auto jsonDoc = QJsonDocument::fromBinaryData(data);
    if( jsonDoc.isNull()) {
        qDebug() << "JSonDoc is null";
        return false;
    }
    if( jsonDoc.isEmpty() ) {
        qDebug() << "JSonDoc is empty";
        return false;
    }
    /* if( !jsonDoc.isObject() ) {
        qDebug() << "JSonDoc is no object";
        return false;
    }*/
    return true;
}
