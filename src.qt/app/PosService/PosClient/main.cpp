#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QSortFilterProxyModel>
#include "Settings/configure.h"
#include "Settings/settings.h"
#include "Comm/coredatahandler.h"
#include "Comm/poshandler.h"
#include "Model/environment.h"
#include "Model/session.h"
#include "Model/terminallistitemmodel.h"
#include "Model/waiterlistitemmodel.h"
#include <QQmlContext>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QLocale>
#include <QTranslator>
#include <Model/articleitemmodel.h>
#include <Model/familygroupitemmodel.h>
#include <Model/familyitemmodel.h>


int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    auto settings = Configure::configure();

    auto coreData = new CoreDataHandler(settings, &app);
    CoreDataHandler::setInstance(coreData);
    qmlRegisterSingletonType<CoreDataHandler>("DataProvider", 1, 0, "DataProvider", CoreDataHandler::getInstance);

    auto posHandler = new PosHandler(settings, &app);
    PosHandler::setInstance(posHandler);
    qmlRegisterSingletonType<PosHandler>("PosHandler", 1, 0, "PosHandler", PosHandler::getInstance);

    Environment env;
    env.setTerminal(settings.terminalId());
    Session session(posHandler, coreData, settings);

    qDebug() << "Starting engine...";
    QQmlApplicationEngine engine;
    auto rootContext = engine.rootContext();
    rootContext->setContextProperty("env", QVariant::fromValue(&env));
    rootContext->setContextProperty("session", QVariant::fromValue(&session));

    qDebug() << "Creating models...";

    auto* articlesOrigin = new ArticleItemModel();
    QObject::connect(coreData, &CoreDataHandler::gotArticle, articlesOrigin, &ArticleItemModel::addArticle);
    auto* articlesModel = new ArticleProxyModel(articlesOrigin);
    QObject::connect(&session, &Session::familyChanged, articlesModel, &ArticleProxyModel::setFamily);
    rootContext->setContextProperty("articles", articlesModel);

    auto* familiesOrigin = new FamilyItemModel();
    QObject::connect(coreData, &CoreDataHandler::gotFamily, familiesOrigin, &FamilyItemModel::addFamily);
    auto* familiesModel = new FamilyProxyModel(familiesOrigin);
    QObject::connect(&session, &Session::familyGroupChanged, familiesModel, &FamilyProxyModel::setFamilyGroup);
    rootContext->setContextProperty("families", familiesModel);

    auto* famgroupsModel = new FamilyGroupItemModel();
    QObject::connect(coreData, &CoreDataHandler::gotFamilyGroup, famgroupsModel, &FamilyGroupItemModel::addFamilyGroup);
    rootContext->setContextProperty("famgroups", famgroupsModel);


    auto* terminalsModel = new TerminalListItemModel();
    QObject::connect(coreData, &CoreDataHandler::gotTerminal, terminalsModel, &TerminalListItemModel::addTerminal);
    rootContext->setContextProperty("terminals", terminalsModel);

    auto* waitersModel = new WaiterListItemModel();
    QObject::connect(coreData, &CoreDataHandler::gotWaiter, waitersModel, &WaiterListItemModel::addWaiter);
    rootContext->setContextProperty("waiters", waitersModel);



    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "PosClient_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            app.installTranslator(&translator);
            break;
        }
    }

    const QUrl url(QStringLiteral("qrc:/Views/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
