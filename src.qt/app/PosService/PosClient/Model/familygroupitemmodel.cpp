#include "familygroupitemmodel.h"

FamilyGroupItemModel::FamilyGroupItemModel(QObject *parent)
    : QAbstractListModel(parent)
{
}


void FamilyGroupItemModel::addFamilyGroup(const FamilyGroup* group) {
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    _groups << group;
    endInsertRows();
}

int FamilyGroupItemModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return _groups.count();
}

QVariant FamilyGroupItemModel::data(const QModelIndex &index, int role) const
{
    if (index.row()<0 || index.row()>_groups.count())
        return QVariant();

    const FamilyGroup* group = _groups[index.row()];
    switch(role) {
        case IdRole: return group->id();
        case NameRole: return group->name();
    }
    return QVariant();
}


QHash<int, QByteArray> FamilyGroupItemModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    return roles;
}
