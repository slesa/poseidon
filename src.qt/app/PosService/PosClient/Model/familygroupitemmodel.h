#ifndef FAMILYGROUPITEMMODEL_H
#define FAMILYGROUPITEMMODEL_H
#include <QAbstractItemModel>
#include "Domain/familygroup.h"

class FamilyGroupItemModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        IdRole = Qt::UserRole + 1,
        NameRole
    };

    explicit FamilyGroupItemModel(QObject* parent = nullptr);

    void addFamilyGroup(const FamilyGroup* group);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    QList<const FamilyGroup*> _groups;
};

#endif // FAMILYGROUPITEMMODEL_H
