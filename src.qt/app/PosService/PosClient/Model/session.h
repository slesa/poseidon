#ifndef SESSION_H
#define SESSION_H
#include "Comm/poshandler.h"
#include "Comm/coredatahandler.h"
#include "Settings/settings.h"
#include "Domain/terminal.h"
#include "Domain/waiter.h"
#include <QStateMachine>
#include <QUuid>

class Session : public QStateMachine
{
    Q_OBJECT

    Q_PROPERTY(int table MEMBER _table READ table WRITE setTable NOTIFY tableChanged)
    Q_PROPERTY(int party MEMBER _party READ party WRITE setParty NOTIFY partyChanged)
    Q_PROPERTY(QString waiterName READ waiterName NOTIFY waiterNameChanged)
    Q_PROPERTY(int count MEMBER _count READ count WRITE setCount NOTIFY countChanged)
    Q_PROPERTY(QString currentInput MEMBER _currentInput READ currentInput WRITE setCurrentInput NOTIFY currentInputChanged)

public:
    Session(PosHandler* posHandler, CoreDataHandler* dataHandler, Settings settings);
    Q_INVOKABLE void start();

    QUuid sessionId() const { return _sessionId; }
    Terminal* terminal() const { return _terminal; }
    Q_INVOKABLE void setTerminal(QUuid sessionId, Terminal* terminal);
    Waiter* waiter() const { return _waiter; }
    Q_INVOKABLE void setWaiter(Waiter* waiter);
    Q_INVOKABLE void clrWaiter() { setWaiter(nullptr); }
    Q_INVOKABLE void clrInput() { setCurrentInput(QString()); }
    Q_INVOKABLE void addInput(QChar ch);
    Q_INVOKABLE void backInput();

    Q_INVOKABLE void enterTable() { enterValue([&](int a) { setTable(a); }); }
    Q_INVOKABLE void enterParty() { enterValue([&](int a) { setParty(a); }); }
    Q_INVOKABLE void enterCount() { enterValue([&](int a) { setCount(a); }); }

    QString waiterName() const { return _waiter ? _waiter->name() : ""; }
    int table() const { return _table; }
    void setTable(int value) { _table = value; emit tableChanged(value); }
    int party() const { return _party; }
    void setParty(int value) { _party = value; emit partyChanged(value); }
    int count() const { return _count; }
    void setCount(int value) { _count = value; emit countChanged(value); }

    QString currentInput() const { return _currentInput; }
    void setCurrentInput(const QString& value) { _currentInput = value; emit currentInputChanged(value); }

    int family() const { return _family; }
    Q_INVOKABLE void setFamily(int value) { _family = value; emit familyChanged(value); }
    int familyGroup() const { return _familyGroup; }
    Q_INVOKABLE void setFamilyGroup(int value) { _familyGroup = value; emit familyGroupChanged(value); }

signals:
    void selectTerminal();
    void selectWaiter();
    void showOrders();

    void waiterNameChanged(const QString&);
    void tableChanged(int value);
    void partyChanged(int value);
    void countChanged(int value);
    void currentInputChanged(const QString&);

    void familyChanged(int value);
    void familyGroupChanged(int value);

private:
    void gotFamilyGroups();
    void gotFamilies();
    void gotArticles();
    void enterValue(const std::function <void (int)>& f);

private:
    PosHandler* _posHandler;
    CoreDataHandler* _dataHandler;
    Settings _settings;
    QUuid _sessionId;
    Terminal* _terminal;
    Waiter* _waiter;
    int _table;
    int _party;
    int _count;
    QString _currentInput;
    int _family;
    int _familyGroup;
};

#endif // SESSION_H
