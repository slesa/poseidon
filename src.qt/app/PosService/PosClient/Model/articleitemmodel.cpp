#include "articleitemmodel.h"

ArticleItemModel::ArticleItemModel(QObject *parent)
    : QAbstractListModel(parent)
{
}


void ArticleItemModel::addArticle(const Article* article) {
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    _articles << article;
    endInsertRows();
}

int ArticleItemModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return _articles.count();
}

QVariant ArticleItemModel::data(const QModelIndex &index, int role) const {
    if (index.row()<0 || index.row()>_articles.count())
        return QVariant();

    const Article* article = _articles[index.row()];
    switch(role) {
        case PluRole: return article->plu();
        case NameRole: return article->name();
        case FamilyRole: return article->family();
    }
    return QVariant();
}


QHash<int, QByteArray> ArticleItemModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[PluRole] = "plu";
    roles[NameRole] = "name";
    roles[FamilyRole] = "family";
    return roles;
}


ArticleProxyModel::ArticleProxyModel(ArticleItemModel* source, QObject* parent)
    : _family(0)
{
    setSourceModel(source);
}

bool ArticleProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    if(!_family) return true;
    QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
    int family = index.data(ArticleItemModel::Roles::FamilyRole).toInt();
    return family==_family;
}
