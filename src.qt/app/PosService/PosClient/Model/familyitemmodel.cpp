#include "familyitemmodel.h"

FamilyItemModel::FamilyItemModel(QObject *parent)
    : QAbstractListModel(parent)
{
}


void FamilyItemModel::addFamily(const Family* family) {
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    _families << family;
    endInsertRows();
}

int FamilyItemModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return _families.count();
}

QVariant FamilyItemModel::data(const QModelIndex &index, int role) const {
    if (index.row()<0 || index.row()>_families.count())
        return QVariant();

    const Family* family= _families[index.row()];
    switch(role) {
        case IdRole: return family->id();
        case NameRole: return family->name();
        case GroupRole: return family->familyGroup();
    }
    return QVariant();
}


QHash<int, QByteArray> FamilyItemModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    roles[GroupRole] = "group";
    return roles;
}


FamilyProxyModel::FamilyProxyModel(FamilyItemModel* source, QObject* parent)
    : _familyGroup(0)
{
    setSourceModel(source);
}

bool FamilyProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    if(!_familyGroup) return true;
    QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
    int group = index.data(FamilyItemModel::Roles::GroupRole).toInt();
    return group==_familyGroup;
}
