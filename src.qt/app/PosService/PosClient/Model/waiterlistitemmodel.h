#ifndef WAITERLISTITEMMODEL_H
#define WAITERLISTITEMMODEL_H
#include <QAbstractListModel>
#include "Domain/waiter.h"

class WaiterListItemModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        IdRole = Qt::UserRole + 1,
        NameRole
    };

    explicit WaiterListItemModel(QObject* parent = nullptr);
    void addWaiter(const Waiter* waiter);
    int rowCount(const QModelIndex& parent=QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    QList<const Waiter*> _waiters;
};

#endif // WAITERLISTITEMMODEL_H
