#ifndef TERMINALLISTITEMMODEL_H
#define TERMINALLISTITEMMODEL_H
#include <QAbstractListModel>
#include "Domain/terminal.h"

class TerminalListItemModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        IdRole = Qt::UserRole + 1,
        NameRole
    };

    explicit TerminalListItemModel(QObject* parent = nullptr);
    void addTerminal(const Terminal* terminal);
    int rowCount(const QModelIndex& parent=QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    QList<const Terminal*> _terminals;
};

#endif //TERMINALLISTITEMMODEL_H
