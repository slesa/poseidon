#include "waiterlistitemmodel.h"
#include <QVariant>

WaiterListItemModel::WaiterListItemModel(QObject* parent)
    : QAbstractListModel(parent)
{
}

void WaiterListItemModel::addWaiter(const Waiter* waiter) {
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    _waiters << waiter;
    endInsertRows();
}

int WaiterListItemModel::rowCount(const QModelIndex& parent) const {
    Q_UNUSED(parent)
    return _waiters.count();
}

QVariant WaiterListItemModel::data(const QModelIndex& index, int role) const {
    if (index.row()<0 || index.row()>_waiters.count())
        return QVariant();

    const Waiter* waiter = _waiters[index.row()];
    switch(role) {
        case IdRole: return waiter->id();
        case NameRole: return waiter->name();
    }
    return QVariant();
}

QHash<int, QByteArray> WaiterListItemModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    return roles;
}
