#include "session.h"

Session::Session(PosHandler* posHandler, CoreDataHandler* dataHandler, Settings settings)
    : _posHandler(posHandler)
    , _dataHandler(dataHandler)
    , _settings(settings)
    , _terminal(nullptr)
    , _waiter(nullptr)
    , _table(1)
    , _party(1)
    , _count(1)
    , _family(0)
    , _familyGroup(0)
{
    connect(posHandler, &PosHandler::turnedOn, this, &Session::setTerminal);
    connect(posHandler, &PosHandler::loggedIn, this, &Session::setWaiter);
    connect(posHandler, &PosHandler::loggedOut, this, &Session::clrWaiter);
    connect(dataHandler, &CoreDataHandler::gotFamilyGroups, this, &Session::gotFamilyGroups);
    connect(dataHandler, &CoreDataHandler::gotFamilies, this, &Session::gotFamilies);
    connect(dataHandler, &CoreDataHandler::gotArticles, this, &Session::gotArticles);
}

void Session::start() {
    _dataHandler->getFamilyGroups();
}

void Session::gotFamilyGroups() {
    _dataHandler->getFamilies();
}
void Session::gotFamilies() {
    _dataHandler->getArticles();
}
void Session::gotArticles() {
    if( !_settings.terminalId()) {
        emit selectTerminal();
        return;
    }
    _posHandler->turnon(_settings.terminalId());
}

void Session::setTerminal(QUuid sessionId, Terminal* terminal) {
    _terminal = terminal;
    if( !terminal) {
        emit selectTerminal();
        return;
    }
    if(!_settings.waiterId()) {
        emit selectWaiter();
        return;
    }
    _posHandler->login(_settings.waiterId(), _settings.password());
}

void Session::setWaiter(Waiter* waiter) {
    _waiter = waiter;
    if( !waiter )
        emit selectWaiter();
    else {
        emit waiterNameChanged(waiter->name());
        emit showOrders();
    }
}

void Session::addInput(QChar ch) {
    if (ch=='0' && _currentInput.isEmpty()) return;
    if (ch=='.' && _currentInput.contains('.')) return;
    setCurrentInput(currentInput() + ch);
}

void Session::backInput() {
    if (_currentInput.length()>0)
        setCurrentInput(currentInput().left(currentInput().length()-1));
}


void Session::enterValue(const std::function <void (int)>& f){
    bool ok;
    auto value = currentInput().toInt(&ok);
    if( !ok ) return;
    f(value);
    setCurrentInput(QString());
}
