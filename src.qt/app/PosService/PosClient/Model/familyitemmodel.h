#ifndef FAMILYITEMMODEL_H
#define FAMILYITEMMODEL_H
#include <QAbstractItemModel>
#include <QSortFilterProxyModel>
#include "Domain/family.h"

class FamilyItemModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        IdRole = Qt::UserRole + 1,
        NameRole,
        GroupRole
    };

    explicit FamilyItemModel(QObject* parent = nullptr);

    void addFamily(const Family* family);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    QList<const Family*> _families;
};


class FamilyProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit FamilyProxyModel(FamilyItemModel* source, QObject* parent = nullptr);
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

public slots:
    void setFamilyGroup(int group) {
        if(_familyGroup==group)
            _familyGroup = 0;
        else
            _familyGroup = group;
        invalidate();
    }

private:
    int _familyGroup;
};

#endif // FAMILYITEMMODEL_H
