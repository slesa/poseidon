#ifndef ARTICLEITEMMODEL_H
#define ARTICLEITEMMODEL_H
#include <QAbstractItemModel>
#include <QSortFilterProxyModel>
#include "Domain/article.h"

class ArticleItemModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        PluRole = Qt::UserRole + 1,
        NameRole,
        FamilyRole
    };

    explicit ArticleItemModel(QObject* parent = nullptr);
    void addArticle(const Article* article);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    QList<const Article*> _articles;
};

class ArticleProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit ArticleProxyModel(ArticleItemModel* source, QObject* parent = nullptr);
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

public slots:
    void setFamily(int family) {
        if(_family==family)
            _family = 0;
        else
            _family = family;
        invalidate();
    }

private:
    int _family;
};

#endif // ARTICLEITEMMODEL_H
