#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <QObject>

class Environment : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int terminal MEMBER _terminal READ terminal WRITE setTerminal NOTIFY terminalChanged)
    Q_PROPERTY(int waiter MEMBER _waiter READ waiter WRITE setWaiter NOTIFY waiterChanged)
    Q_PROPERTY(int table MEMBER _table READ table WRITE setTable NOTIFY tableChanged)
    Q_PROPERTY(int party MEMBER _party READ party WRITE setParty NOTIFY partyChanged)

public:
    explicit Environment(QObject *parent = nullptr);

    int terminal() const { return _terminal; }
    void setTerminal(int value) { _terminal = value; emit terminalChanged(value); }
    int waiter() const { return _waiter; }
    void setWaiter(int value) { _waiter = value; emit waiterChanged(value); }
    int table() const { return _table; }
    void setTable(int value) { _table = value; emit tableChanged(value); }
    int party() const { return _party; }
    void setParty(int value) { _party = value; emit partyChanged(value); }

signals:
    void terminalChanged(int value);
    void waiterChanged(int value);
    void tableChanged(int value);
    void partyChanged(int value);

private:
    int _terminal;
    int _waiter;
    int _table;
    int _party;
};

#endif // ENVIRONMENT_H
