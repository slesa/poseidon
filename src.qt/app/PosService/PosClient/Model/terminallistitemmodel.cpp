#include "terminallistitemmodel.h"

TerminalListItemModel::TerminalListItemModel(QObject* parent)
    : QAbstractListModel(parent)
{
}

void TerminalListItemModel::addTerminal(const Terminal* terminal) {
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    _terminals << terminal;
    endInsertRows();
}

int TerminalListItemModel::rowCount(const QModelIndex& parent) const {
    Q_UNUSED(parent)
    return _terminals.count();
}

QVariant TerminalListItemModel::data(const QModelIndex& index, int role) const {
    if (index.row()<0 || index.row()>_terminals.count())
        return QVariant();

    const Terminal* terminal = _terminals[index.row()];
    switch(role) {
        case IdRole: return terminal->id();
        case NameRole: return terminal->name();
    }
    return QVariant();
}

QHash<int, QByteArray> TerminalListItemModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    return roles;
}
