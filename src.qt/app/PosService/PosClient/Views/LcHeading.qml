import QtQuick
import QtQuick.Controls

Item {
    id: root
    property int margin: 5
    property string text
    property color color1
    property color color2

    LcLeftBallon {
        id: leftCorner
        anchors { margins: root.margin; left: parent.left; top: parent.top }
        width: parent.height
        height: parent.height
        color: root.color1
    }

    LcRightBallon {
        id: rightCorner
        anchors { margins: root.margin; right: parent.right; top: parent.top }
        width: parent.height
        height: parent.height
        color: root.color1
    }

    Label {
        id: label
        anchors { margins: root.margin; left: leftCorner.right; top: parent.top }
        height: parent.height
        color: root.color2
        text: root.text
        font.pixelSize: 24
        font.bold: true
        fontSizeMode: Text.Fit
        verticalAlignment: Text.AlignVCenter
    }

    Rectangle {
        anchors { margins: root.margin; left: label.right; top: parent.top; right: rightCorner.left }
        height: parent.height
        color: root.color1
    }
}
