import QtQuick
import QtQuick.Controls

Page {
    id: startScreen

    Rectangle {
        anchors.fill: parent
        color: 'black'
    }

    Item {
        id: colours
        property color orange: '#FBAE00'
        property color apricot: '#E7A552'
        property color darkPurple: '#9C9CFF'
        property color lightPurple: '#CE9CCE'
        property color dustyRed: '#CE6363'
        property color yellow: '#FFCE7B'
    }

    LcHeading {
        id: header
        anchors { left: parent.left; right: parent.right; top: parent.top }
        height: 30
        text: "Console 47"
        color1: colours.darkPurple
        color2: colours.orange
    }

    LcFooting {
        id: footer
        anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
        height: 30
        text: "Please stand by"
        color1: colours.darkPurple
        color2: colours.orange
    }


    // title: qsTr("Initialization Error")

    Image {
        anchors { margins: 10; top: header.bottom; bottom: footer.top; horizontalCenter: parent.horizontalCenter; }
        source: "file:./usr/img/startscreen.png"
        fillMode: Image.PreserveAspectFit
    }

    /*Label {
        x: 3
        y: 30
        text: qsTr("Terminal ID is not set")
        anchors.centerIn: parent
    }*/
}
