import QtQuick
import QtQuick.Controls

Item {
    id: root
    property int margin: 5
    property string text
    property color color1
    property color color2

    LcLeftBallon {
        id: leftCorner
        anchors { margins: root.margin; left: parent.left; bottom: parent.bottom }
        width: parent.height
        height: parent.height
        color: root.color1
    }

    LcRightBallon {
        id: rightCorner
        anchors { margins: root.margin; right: parent.right; bottom: parent.bottom }
        width: parent.height
        height: parent.height
        color: root.color1
    }

    Label {
        id: label
        anchors { margins: root.margin; right: rightCorner.left; bottom: parent.bottom }
        height: parent.height
        color: root.color2
        text: root.text
        font.pixelSize: 24
        font.bold: true
        fontSizeMode: Text.Fit
        verticalAlignment: Text.AlignVCenter
    }

    Rectangle {
        anchors { margins: root.margin; left: leftCorner.right; bottom: parent.bottom; right: label.left }
        height: parent.height
        color: root.color1
    }
}
