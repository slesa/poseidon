import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import PosHandler

Item {
    id: root
    Rectangle {
        anchors.fill: parent
        color: 'green'
    }

    Column {
        anchors.fill: parent

        Rectangle {
            color: 'violet'
            height: 60
            width: parent.width

            //ScrollView {
            //    height: 60
            //    width: parent.width
            //    anchors.fill: parent
            //    MouseArea { anchors.fill: parent }
                Flow {
                    clip: true
                    height: 60
                    width: parent.width
                    Repeater {
                        model: famgroups
                        Button {
                            width: 120
                            text: name
                            onClicked: session.setFamilyGroup(id)
                        }
                    }
                }
            //}
        }

        Rectangle {
            color: 'yellow'
            height: 120
            width: parent.width
            //ScrollView {
            //    height: 120
            //    width: parent.width
                Flow {
                    clip: true
                    height: 120
                    width: parent.width
                    Repeater {
                        model: families
                        Button {
                            width: 120
                            text: name
                            onClicked: session.setFamily(id)
                        }
                    }
                }
            //}
        }
        Rectangle {
            color: 'red'
            height: root.height/2
            width: parent.width
            //ScrollView {
            //    height: root.height/2
            //    width: parent.width
                Flow {
                    clip: true
                    height: root.height/2
                    width: parent.width
                    Repeater {
                        model: articles
                        delegate: Button {
                            width: 120
                            text: name
                            onClicked: session.order(plu)
                        }
                    }
                }
            //}
        }
    }
}
