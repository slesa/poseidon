import QtQuick

Item {
    id: root
    property color color

    Rectangle {
        id: rounded
        anchors.left: parent.left
        radius: height/2
        width: root.width
        height: root.height
        color: root.color
    }

    Rectangle {
        x: rounded.x+rounded.width-rounded.radius
        width: rounded.radius
        height: root.height
        color: root.color
    }
}
