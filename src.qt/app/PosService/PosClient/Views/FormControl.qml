import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

// https://stackoverflow.com/questions/45035800/qformlayout-equivalent-in-qtquick2

Item {
    id: root
    property alias label: formLabel

    Label {
        id: formLabel
        parent: root.parent
        // anchors.leftMargin: 10
        //width: root.width/3
        // Layout.fillHeight: true
        //Layout.fillWidth: true

        verticalAlignment: Qt.AlignVCenter

        MouseArea {
            anchors.fill: parent
            onClicked: root.control.forceActiveFocus()
        }
    }

    property Item control

    Row {
        id: content
        //width: root.width*2/3
        parent: formLabel.parent
        children: [control]
    }
}
