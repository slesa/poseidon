import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQml.Models
import DataProvider
import PosHandler

Page {
    id: loginScreen
    //width: 600
    //height: 400

    property bool selectionBusy: false

    title: qsTr("Select waiter")

    Rectangle {
        id: waiterSelect
        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
            topMargin: 10; bottomMargin: 10
        }
        width: loginScreen.width/2

        Component {
            id: waiterItem
            Item {
                id: item
                height: 30
                width: loginScreen.width
                Row {
                    Text { width: item.width/4; text: '<b>' + id + '</b>' }
                    Text { width: 3*item.width/4; text: '<b>' + name + '</b>' }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        waiterList.currentIndex = index
                    }
                }
            }
        }

        Component {
            id: highlightBar
            Rectangle {
                color: "lightsteelblue"
                radius: 5
                focus: true
                width: waiterList.width
                height: 30
                y: waiterList.currentItem.y
                Behavior on y { SpringAnimation { spring: 2; damping: 0.1 } }
            }
        }

        ListView {
            id: waiterList
            anchors {
                fill: parent
                topMargin: 5; bottomMargin: 5
            }
            spacing: 5
            model: DelegateModel {
                id: waitersModel
                model: waiters
                delegate: waiterItem
            }
            highlight: highlightBar
            highlightFollowsCurrentItem: false
            focus: true
            snapMode: ListView.SnapToItem
            onCurrentItemChanged: {
                // console.log("Region ", waiterSelect.width)
                // console.log("Item ", waiterItem.width)
                // console.log("List ", waiterList.width)
                selectionBusy = true
                waiterId.text = waitersModel.items.get(currentIndex).model.id
                selectionBusy = false
            }
        }
    }

    GridLayout {
        id: waiterEdit
        anchors {
            right: parent.right
            top: parent.top
            bottom: parent.bottom
            topMargin: 10; bottomMargin: 10
        }
        width: loginScreen.width/2
        columns: 1
        columnSpacing: 5
        rowSpacing: 5

        Text {
            text: qsTr("Waiter ID")
        }
        //Rectangle {
        //    color:'yellow'
            TextInput {
                id: waiterId
                //TextFieldStyle.background: 'yellow'
                Layout.fillWidth: true
                maximumLength: 10
            }
        //}
        Text {
            text: qsTr("Password")
        }
        TextInput {
            id: password
            //TextFieldStyle.background: 'yellow'
            Layout.fillWidth: true
            echoMode: TextInput.PasswordEchoOnEdit
            passwordMaskDelay: 300
        }

        Rectangle {
            Layout.fillHeight: true
        }

        Button {
            id: loginButton
            text: qsTr("Login")
            onClicked: {
                var id = parseInt(waiterId.text)
                PosHandler.login(id, password.text)
            }
        }
    }

    Component.onCompleted: {
        console.log("Selecting waiter")
        DataProvider.getWaiters()
    }
}
