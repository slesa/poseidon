import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import DataProvider
import PosHandler

Page {
    id: root
    //width: 600
    //height: 400

    property bool selectionBusy: false

    title: qsTr("Select terminal")

    Rectangle {
        id: terminalSelect
        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
            topMargin: 10; bottomMargin: 10
        }
        width: root.width/2

        Component {
            id: terminalItem
            Item {
                id: item
                height: 30
                width: root.width
                Row {
                    Text { width: item.width/4; text: '<b>' + id + '</b>' }
                    Text { width: 3*item.width/4; text: '<b>' + name + '</b>' }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        terminalList.currentIndex = index
                    }
                }
            }
        }

        Component {
            id: highlightBar
            Rectangle {
                color: "lightsteelblue"
                radius: 5
                focus: true
                width: terminalList.width
                height: 30
                y: terminalList.currentItem.y
                Behavior on y { SpringAnimation { spring: 2; damping: 0.1 } }
            }
        }

        ListView {
            id: terminalList
            anchors {
                fill: parent
                topMargin: 5; bottomMargin: 5
            }
            spacing: 5
            model: DelegateModel {
                id: terminalsModel
                model: terminals
                delegate: terminalItem
            }
            highlight: highlightBar
            highlightFollowsCurrentItem: false
            focus: true
            snapMode: ListView.SnapToItem
            onCurrentItemChanged: {
                // console.log("Region ", terminalSelect.width)
                // console.log("Item ", terminalItem.width)
                // console.log("List ", terminalList.width)
                selectionBusy = true
                terminalId.text = terminalsModel.items.get(currentIndex).model.id
                selectionBusy = false
            }
        }
    }


    GridLayout {
        id: waiterEdit
        anchors {
            right: parent.right
            top: parent.top
            bottom: parent.bottom
            topMargin: 10; bottomMargin: 10
        }
        width: root.width/2
        columns: 1
        columnSpacing: 5
        rowSpacing: 5

        Text {
            text: qsTr("Terminal Id")
        }
        //Rectangle {
        //    color:'yellow'
            TextInput {
                id: terminalId
                //TextFieldStyle.background: 'yellow'
                Layout.fillWidth: true
                maximumLength: 10
            }
        //}

        Rectangle {
            Layout.fillHeight: true
        }

        Button {
            id: selectButton
            text: qsTr("Select")
            onClicked: {
                var id = parseInt(terminalId.text)
                PosHandler.turnon(id)
            }
        }
    }


    Component.onCompleted: {
        console.log("Selecting terminal")
        DataProvider.getTerminals()
    }
}
