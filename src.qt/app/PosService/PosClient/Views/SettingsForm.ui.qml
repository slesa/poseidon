import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import Qt.labs.settings 1.0

Page {
    id: root
    // anchors.fill: parent
    title: qsTr("Settings")

    Settings {
        category: "General"
        fileName: "etc/posclient.ini"
        property alias host: textHost.text
        property alias port: spinPort.value
        //property alias pollDelay: spinPollDelay.value
        property alias terminalId: spinTerminalId.value
        //property alias monitorName: textMonitorName.text
    }

    GridLayout {
        id: grid
        //height: parent.height-filler.height
        anchors { top: parent.top; left: parent.left; right: parent.right; margins: 20 }
        //width: parent.width
        //anchors.fill: parent
        columns: 2
        //rowSpan: 10

        /*FormControl {
            label.text: qsTr("Poll &delay:")
            control: SpinBox {
                id: spinPollDelay
                editable: true
                from: 1
                to: 5000
            }
        }
        FormControl {
            label.text: qsTr("Monitor &name:")
            control: TextField {
                id: textMonitorName
                placeholderText: qsTr("Monitor " + spinMonitorId.value)
            }
        }*/
        FormControl {
            label.text: qsTr("Terminal &id:")
            control: SpinBox {
                id: spinTerminalId
                editable: true
                from: 0
                to: 1000
            }
        }
        FormControl {
            label.text: qsTr("PosService &port:")
            control: SpinBox {
                id: spinPort
                editable: true
                from: 1
                to: 65535
            }
        }
        FormControl {
            label.text: qsTr("PosService &host:")
            control: TextField {
                id: textHost
                placeholderText: qsTr("IP address of the PosService server")
            }
        }
    }
}
