import QtQuick 2.12
import QtQuick.Controls 2.5

ApplicationWindow {
    id: window
    width: 800
    height: 600
    visible: true
    title: qsTr("POS Client v0.1.3a")

    /*Image {
        id: background
        source: "file:./usr/img/startscreen.png"
        anchors.fill: parent
    }*/

    //header: ToolBar {
    //    contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            x: 3
            y: 3
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"

            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                } else {
                    drawer.open()
                }
            }
        }

        TextField  {
            text: stackView.currentItem.title ?? qsTr("POS Client")
            x: 30
            y: 3
            color: 'white'
            //anchors.centerIn: parent
        }

        Timer {
             interval: 500; running: true; repeat: true
             onTriggered: timeLabel.text = Date().toString()
        }

        TextField  {
            id: timeLabel
            // text: stackView.currentItem.title ?? qsTr("POS Client")
            x: window.width-130
            y: 3
            color: 'white'
            //anchors.centerIn: parent
        }
    //}

    Drawer {
        id: drawer
        width: window.width * 0.33
        height: window.height

        Column {
            anchors.fill: parent

            ItemDelegate {
                text: qsTr("Settings")
                width: parent.width
                onClicked: {
                    stackView.push("SettingsForm.ui.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("About...")
                width: parent.width
                onClicked: {
                    stackView.push("AboutForm.ui.qml")
                    drawer.close()
                }
            }
        }
    }

    Loader {
        id: pageLoader
        // anchors.fill: stackView
        source: "qrc:/Views/StartScreen.qml"
        //source: "qrc:/Views/OrderScreen.qml"
    }

    StackView {
        id: stackView
        initialItem: pageLoader
        anchors.fill: parent
    }

    Connections {
        target: session
        function onSelectTerminal() {
            pageLoader.source = "SelectTerminalScreen.qml"
        }
        function onSelectWaiter() {
            pageLoader.source = "SelectWaiterScreen.qml"
        }
        function onShowOrders() {
            pageLoader.source = "OrderScreen.qml"
        }
    }

    Component.onCompleted: {
        session.start()
    }
}
