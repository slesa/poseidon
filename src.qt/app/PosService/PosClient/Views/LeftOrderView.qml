import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import PosHandler

Item {
    /*Rectangle {
        anchors.fill: parent
        color: 'blue'
    }*/

    ColumnLayout {
        anchors.fill: parent
        Row {
            Button {
                id: waiterButton
                text: session.waiterName ?? qsTr("Waiter")
                onClicked: PosHandler.logout()
            }
            Button {
                id: tableButton
                text: qsTr("Table: %1").arg(session.table)
                onClicked: session.enterTable()
            }
            Button {
                id: partyButton
                text: qsTr("Party: %1").arg(session.party)
                onClicked: session.enterParty()
            }
            Button {
                id: countButton
                text: qsTr("Count: %1").arg(session.count)
                onClicked: session.enterCount()
            }
        }

        Rectangle {
            color: 'cyan'
//            height: 380
            width: parent.width
            //Layout.fillHeight: true
        }

        NumBlockView {
            width: 400
            height: 400
        }
    }
}
