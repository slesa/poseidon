import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import PosHandler

Page {
    id: root
    Row  {
        anchors.fill: parent

        LeftOrderView {
            height: parent.height
            width: parent.width * 0.5
        }

        RightOrderView {
            height: parent.height
            width: parent.width * 0.5
        }
    }
}
