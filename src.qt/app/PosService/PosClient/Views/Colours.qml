import QtQuick 2.0

Item {
    id: colours
    property color orange: '#FBAE00'
    property color violet: '#9C9CFF'
}
