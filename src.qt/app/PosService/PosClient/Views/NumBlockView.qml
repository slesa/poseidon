import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Item {

    ColumnLayout {
        RowLayout {
            TextInput {
                id: inputLine
                readOnly: true
                text: session.currentInput
                Layout.fillWidth: true
            }
            Button {
                id: buttonClr
                text: qsTr("Clr")
                onClicked: session.clrInput()
            }
        }
        RowLayout {
            Button {
                id: button7
                text: "&7"
                onClicked: session.addInput('7')
            }
            Shortcut { sequence: '7'; onActivated: session.addInput('7') }
            Button {
                id: button8
                text: "&8"
                onClicked: session.addInput('8')
            }
            Shortcut { sequence: '8'; onActivated: session.addInput('8') }
            Button {
                id: button9
                text: "&9"
                onClicked: session.addInput('9')
            }
            Shortcut { sequence: '9'; onActivated: session.addInput('9') }
            Button {
                id: buttonBack
                text: qsTr("<-")
                onClicked: session.backInput()
            }
            Shortcut { sequence: Qt.Key_Back; onActivated: session.backInput() }
        }
        RowLayout {
            Button {
                id: button4
                text: "4"
                onClicked: session.addInput('4')
            }
            Shortcut { sequence: '4'; onActivated: session.addInput('4') }
            Button {
                id: button5
                text: "5"
                onClicked: session.addInput('5')
            }
            Shortcut { sequence: '5'; onActivated: session.addInput('5') }
            Button {
                id: button6
                text: "6"
                onClicked: session.addInput('6')
            }
            Shortcut { sequence: '6'; onActivated: session.addInput('6') }
            Button {
                id: buttonP
                text: "."
                onClicked: session.addInput('.')
            }
            Shortcut { sequence: '.'; onActivated: session.addInput('.') }
        }
        RowLayout {
            Button {
                id: button1
                text: "1"
                onClicked: session.addInput('1')
            }
            Shortcut { sequence: '1'; onActivated: session.addInput('1') }
            Button {
                id: button2
                text: "2"
                onClicked: session.addInput('2')
            }
            Shortcut { sequence: '2'; onActivated: session.addInput('2') }
            Button {
                id: button3
                text: "3"
                onClicked: session.addInput('3')
            }
            Shortcut { sequence: '3'; onActivated: session.addInput('3') }
            Button {
                id: button0
                text: "0"
                onClicked: session.addInput('0')
            }
            Shortcut { sequence: '0'; onActivated: session.addInput('0') }
        }
    }
}
