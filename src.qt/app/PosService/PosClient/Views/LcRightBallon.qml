import QtQuick

Item {
    id: root
    property color color

    Rectangle {
        id: rounded
        anchors.right: parent.right
        radius: height/2
        width: root.width
        height: root.height
        color: root.color
    }

    Rectangle {
      x: rounded.x
      //y: root.y
      width: rounded.radius
      height: root.height
      color: root.color
    }
}
