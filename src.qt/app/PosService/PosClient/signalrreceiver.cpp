#include "signalrreceiver.h"
#include <iostream>
#include <future>
#include "signalrclient/hub_connection_builder.h"
#include "signalrclient/signalr_value.h"


SignalRReceiver::SignalRReceiver()
{
    _connection = new signalr::hub_connection(
                signalr::hub_connection_builder::create("http://localhost:5000/pos").build()
                );
}

SignalRReceiver::~SignalRReceiver()
{
    delete _connection;
}

void SignalRReceiver::start() {

    std::promise<void> start_task;
    _connection->on("got_waiters", [](const signalr::value& m)
    {
        std::cout << m.as_array()[0].as_string() << std::endl;
    });

    _connection->start([&start_task](std::exception_ptr exception) {
        start_task.set_value();
    });
    start_task.get_future().get();

    std::promise<void> send_task;
    std::vector<signalr::value> arr { "get_waiters" };
    signalr::value arg(arr);
    _connection->invoke("Echo", arg, [&send_task](const signalr::value& value, std::exception_ptr exception) {
        send_task.set_value();
    });

    send_task.get_future().get();

}


void SignalRReceiver::stop()
{
    std::promise<void> stop_task;
    _connection->stop([&stop_task](std::exception_ptr exception) {
        stop_task.set_value();
    });

    stop_task.get_future().get();
}




