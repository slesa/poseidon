<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutForm.ui</name>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="6"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="35"/>
        <source>POS Client v0.0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="40"/>
        <source>(c) 42 GmbH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/AboutForm.ui.qml" line="45"/>
        <source>Order at your fingertips</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppSettingsReader</name>
    <message>
        <location filename="Settings/configure.cpp" line="12"/>
        <source>The network address or name of the POS server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="13"/>
        <source>The network port of the POS server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="15"/>
        <source>The terminal number of this POS client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="16"/>
        <source>The fixed waiter number of this POS client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="34"/>
        <source>No valid port given. It should be a number between 1 and 65535</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="52"/>
        <source>No valid terminal id given</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/configure.cpp" line="61"/>
        <source>No valid waiter id given</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LeftOrderView</name>
    <message>
        <location filename="Views/LeftOrderView.qml" line="17"/>
        <source>Waiter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/LeftOrderView.qml" line="22"/>
        <source>Table: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/LeftOrderView.qml" line="27"/>
        <source>Party: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/LeftOrderView.qml" line="32"/>
        <source>Count: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NumBlockView</name>
    <message>
        <location filename="Views/NumBlockView.qml" line="17"/>
        <source>Clr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/NumBlockView.qml" line="42"/>
        <source>&lt;-</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Page1Form.ui</name>
    <message>
        <location filename="Views/Page1Form.ui.qml" line="8"/>
        <source>Page 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/Page1Form.ui.qml" line="11"/>
        <source>You are on Page 1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectTerminalScreen</name>
    <message>
        <location filename="Views/SelectTerminalScreen.qml" line="14"/>
        <source>Select terminal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SelectTerminalScreen.qml" line="100"/>
        <source>Terminal Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SelectTerminalScreen.qml" line="118"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectWaiterScreen</name>
    <message>
        <location filename="Views/SelectWaiterScreen.qml" line="15"/>
        <source>Select waiter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SelectWaiterScreen.qml" line="100"/>
        <source>Waiter ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SelectWaiterScreen.qml" line="112"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SelectWaiterScreen.qml" line="128"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsForm.ui</name>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="9"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="47"/>
        <source>Terminal &amp;id:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="56"/>
        <source>PosService &amp;port:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="65"/>
        <source>PosService &amp;host:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/SettingsForm.ui.qml" line="68"/>
        <source>IP address of the PosService server</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="Views/main.qml" line="9"/>
        <source>POS Client v0.1.3a</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/main.qml" line="37"/>
        <source>POS Client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/main.qml" line="68"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Views/main.qml" line="76"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
