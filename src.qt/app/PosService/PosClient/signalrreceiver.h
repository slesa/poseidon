#ifndef SIGNALRRECEIVER_H
#define SIGNALRRECEIVER_H
#include "signalrclient/hub_connection.h"

class SignalRReceiver
{
public:
    SignalRReceiver();
    ~SignalRReceiver();
    void start();
    void stop();
private:
    signalr::hub_connection* _connection;
};

#endif // SIGNALRRECEIVER_H
