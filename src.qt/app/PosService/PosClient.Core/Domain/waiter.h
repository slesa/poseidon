#ifndef WAITER_H
#define WAITER_H
#include <QString>

namespace PosClientCore::Domain {

    class Waiter
    {
    public:
        explicit Waiter(int id, const QString& name);
        int id() const { return _id; }
        void setId(int id) { _id = id; }
        QString name() const { return _name; }
        void setName(const QString &name) { _name = name; }
    private:
        int _id;
        QString _name;
    };
}

using namespace PosClientCore::Domain;

#endif // WAITER_H
