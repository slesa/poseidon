#include "waiter.h"

namespace PosClientCore::Domain {

    Waiter::Waiter(int id, const QString& name)
        : _id(id)
        , _name(name)
    { }

}