#include "article.h"

namespace PosClientCore::Domain {

    Article::Article()
    : _plu(0)
    , _family(0)
    , _price(0.0)
    , _isHint(false)
    , _isFreePrice(false)
    , _isFreeText(false)
    , _isWeight(false)
    , _isCutGood(false)
    , _isAreaGood(false)
    { }

}