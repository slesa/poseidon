#ifndef FAMILY_H
#define FAMILY_H
#include <QString>

namespace PosClientCore::Domain {

    class Family
    {
    public:
        Family();
        int id() const { return _id; }
        void setId(int plu) { _id = plu; }
        QString name() const { return _name; }
        void setName(const QString &name) { _name = name; }
        int familyGroup() const { return _familyGroup; }
        void setFamilyGroup(int group) { _familyGroup= group; }
    private:
        int _id;
        QString _name;
        int _familyGroup;
    };

}

using namespace PosClientCore::Domain;

#endif // FAMILY_H
