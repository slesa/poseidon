#ifndef FAMILYGROUP_H
#define FAMILYGROUP_H
#include <QString>

namespace PosClientCore::Domain {


    class FamilyGroup
    {
    public:
        FamilyGroup();
        int id() const { return _id; }
        void setId(int plu) { _id = plu; }
        QString name() const { return _name; }
        void setName(const QString &name) { _name = name; }
    private:
        int _id;
        QString _name;
    };

}

using namespace PosClientCore::Domain;

#endif // FAMILYGROUP_H
