#ifndef ARTICLE_H
#define ARTICLE_H
#include <QString>

namespace PosClientCore::Domain {

class Article {
    public:
        Article();
        int plu() const { return _plu; }
        void setPlu(int plu) { _plu = plu; }
        QString name() const { return _name; }
        void setName(const QString &name) { _name = name; }
        int family() const { return _family; }
        void setFamily(int family) { _family = family; }
        float price() const { return _price; }
        void setPrice(float price) { _price = price; }
        bool isHint() const { return _isHint; }
        void setIsHint(bool value) { _isHint = value; }
        bool isFreePrice() const { return _isFreePrice; }
        void setIsFreePrice(bool value) { _isFreePrice = value; }
        bool isFreeText() const { return _isFreeText; }
        void setIsFreeText(bool value) { _isFreeText = value; }
        bool isWeight() const { return _isWeight; }
        void setIsWeight(bool value) { _isWeight = value; }
        bool isCutGood() const { return _isCutGood; }
        void setIsCutGood(bool value) { _isCutGood = value; }
        bool isAreaGood() const { return _isAreaGood; }
        void setIsAreaGood(bool value) { _isAreaGood = value; }
        // Constraint Constraint { get; set; }
    private:
        int _plu;
        QString _name;
        int _family;
        float _price;
        bool _isHint;
        bool _isFreePrice;
        bool _isFreeText;
        bool _isWeight;
        bool _isCutGood;
        bool _isAreaGood;
    };
}

using namespace PosClientCore::Domain;

#endif //ARTICLE_H
