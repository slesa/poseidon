#ifndef EVENTPAYLOADS_H
#define EVENTPAYLOADS_H
#include "eventpayload.h"
#include "Domain/terminal.h"
#include "Domain/waiter.h"

namespace PosClientCore::Payloads {

    class TurnedOnPayload: EventPayload
    {
    public:
        TurnedOnPayload(QUuid sessionId, Terminal terminal, ErrorCode status=ErrorCode::None);
        const Terminal& terminal() const { return _terminal; }
    private:
        Terminal _terminal;
    };


    class LoggedInPayload: EventPayload
    {
    public:
        LoggedInPayload(QUuid sessionId, Waiter waiter, ErrorCode status=ErrorCode::None);
        const Waiter& waiter() const { return _waiter; }
    private:
        Waiter _waiter;
    };

}

using namespace PosClientCore::Payloads;

#endif // EVENTPAYLOADS_H
