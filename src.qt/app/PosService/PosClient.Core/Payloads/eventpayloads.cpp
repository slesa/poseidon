#include "eventpayloads.h"

namespace PosClientCore::Payloads {

TurnedOnPayload::TurnedOnPayload(QUuid sessionId, Terminal terminal, ErrorCode status)
    : EventPayload(sessionId, status)
    , _terminal(terminal)
{}


LoggedInPayload::LoggedInPayload(QUuid sessionId, Waiter waiter, ErrorCode status)
    : EventPayload(sessionId, status)
    , _waiter(waiter)
{}

}
