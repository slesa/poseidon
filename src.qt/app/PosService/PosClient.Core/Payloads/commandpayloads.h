#ifndef COMMANDPAYLOADS_H
#define COMMANDPAYLOADS_H
#include "commandpayload.h"
#include <QString>

class QJsonObject;

namespace PosClientCore::Payloads {

    class TurnOnPayload
    {
    public:
        TurnOnPayload(int terminal);
        int terminal() const { return _terminal; }
        QJsonObject toJson() const;
    private:
        int _terminal;
    };


    class LoginPayload : CommandPayload
    {
    public:
        LoginPayload(QUuid sessionId, int waiter, const QString& password=QString());
        int waiter() const { return _waiter; }
        QString password() const { return _password; }
        QJsonObject toJson() const;
    private:
        int _waiter;
        QString _password;
    };


    class OpenTablePayload : CommandPayload
    {
    public:
        OpenTablePayload(QUuid sessionId, int table, int party=0);
        int table() const { return _table; }
        int party() const { return _party; }
        QJsonObject toJson() const;
    private:
        int _table;
        int _party;
    };


    class OpenCheckoutPayload: CommandPayload
    {
    public:
        OpenCheckoutPayload(QUuid sessionId, int checkout=0);
        int checkout() const { return _checkout; }
        QJsonObject toJson() const;
    private:
        int _checkout;
    };

    class ProcessPayload : CommandPayload
    {
    public:
        ProcessPayload(QUuid sessionId, QUuid processId);
        QUuid processId() const { return _processId; }
        QJsonObject toJson() const;
    private:
        QUuid _processId;
    };
}

using namespace PosClientCore::Payloads;

#endif // COMMANDPAYLOADS_H
