#include "commandpayload.h"

namespace PosClientCore::Payloads {

const char* CommandPayload::cmdTurnOn = "turn_on";
const char* CommandPayload::cmdTurnOff = "turn_off";
const char* CommandPayload::cmdSetWaiter = "setwaiter";
const char* CommandPayload::cmdSetWaiterKey = "setwaiterkey";
const char* CommandPayload::cmdClearWaiter = "clrwaiter";
const char* CommandPayload::cmdOpenTable = "opentable";
const char* CommandPayload::cmdGetCheckouts = "getcheckouts";
const char* CommandPayload::cmdOpenCheckout = "opencheckout";
const char* CommandPayload::cmdCloseProcess = "closeprocess";
const char* CommandPayload::cmdOrder = "order";
//const char* CommandPayload::cmdOrderProgress = "order_progress";
const char* CommandPayload::cmdCommit = "commit";

CommandPayload::CommandPayload(QUuid sessionId)
    : _sessionId(sessionId)
{
}

}
