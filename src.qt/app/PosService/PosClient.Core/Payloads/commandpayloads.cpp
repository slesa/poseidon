#include "commandpayloads.h"
#include <QJsonObject>

namespace PosClientCore::Payloads {

TurnOnPayload::TurnOnPayload(int terminal)
    : _terminal(terminal)
{}

QJsonObject TurnOnPayload::toJson() const {
    QJsonObject json;
    json.insert("terminal", terminal());
    return json;
}


LoginPayload::LoginPayload(QUuid sessionId, int waiter, const QString& password)
    : CommandPayload(sessionId)
    , _waiter(waiter)
    , _password(password)
{}

QJsonObject LoginPayload::toJson() const
{
    QJsonObject json;
    json.insert("sessionId", sessionId().toString());
    json.insert("waiter", waiter());
    if(!password().isEmpty())
        json.insert("password", password());
    return json;
}


OpenTablePayload::OpenTablePayload(QUuid sessionId, int table, int party)
    : CommandPayload(sessionId)
    , _table(table)
    , _party(party)
{}


QJsonObject OpenTablePayload::toJson() const
{
    QJsonObject json;
    json.insert("sessionId", sessionId().toString());
    json.insert("table", table());
    if( party()!=0 )
        json.insert("party", party());
    return json;
}


OpenCheckoutPayload::OpenCheckoutPayload(QUuid sessionId, int checkout)
    : CommandPayload(sessionId)
    , _checkout(checkout)
{}

QJsonObject OpenCheckoutPayload::toJson() const
{
    QJsonObject json;
    json.insert("sessionId", sessionId().toString());
    if( checkout()!=0 )
        json.insert("checkoutId", checkout());
    return json;
}


ProcessPayload::ProcessPayload(QUuid sessionId, QUuid processId)
    : CommandPayload(sessionId)
    , _processId(processId)
{}

QJsonObject ProcessPayload::toJson() const {
    QJsonObject json;
    json.insert("sessionId", sessionId().toString());
    json.insert("processId", processId().toString());
    return json;
}

}
