#ifndef COMMANDPAYLOAD_H
#define COMMANDPAYLOAD_H
#include <QUuid>

class QJsonObject;

namespace PosClientCore::Payloads {

    class CommandPayload
    {
    public:
        // System
        static const char* cmdTurnOn;
        static const char* cmdTurnOff;
        static const char* cmdSetWaiter;
        static const char* cmdSetWaiterKey;
        static const char* cmdClearWaiter;
        // Processes
        static const char* cmdGetTables;
        static const char* cmdOpenTable;
        static const char* cmdGetCheckouts;
        static const char* cmdOpenCheckout;
        static const char* cmdCloseProcess;
        // Actions
        static const char* cmdOrder;
        //static const char* cmdOrderProgress;
        static const char* cmdCommit;

    public:
        CommandPayload(QUuid sessionId);
        QUuid sessionId() const { return _sessionId; }
        QJsonObject toJson() const;
    private:
        QUuid _sessionId;
    };
}

using namespace PosClientCore::Payloads;

#endif // COMMANDPAYLOAD_H
