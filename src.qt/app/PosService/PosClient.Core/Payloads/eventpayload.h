#ifndef EVENTPAYLOAD_H
#define EVENTPAYLOAD_H
#include <QUuid>

namespace PosClientCore::Payloads {

    enum ErrorCode {
        None = 0,
        NoSessionId = 1,
        Password = 2,
        Forbidden = 3,
        NotFound = 4,
        NoWaiter = 5,
        AlreadyOpen = 6,
        NoProcess = 7,
        WrongTerminal= 8,
        WrongWaiter = 9

    };

    class EventPayload
    {
    public:
        EventPayload(QUuid sessionId, ErrorCode status);
        QUuid sessionId() const { return _sessionId; }
        ErrorCode status() const { return _status; }
    private:
        QUuid _sessionId;
        ErrorCode _status;
    };

}

using namespace PosClientCore::Payloads;

#endif // EVENTPAYLOAD_H
