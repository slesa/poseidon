#include "eventpayload.h"

namespace PosClientCore::Payloads {


EventPayload::EventPayload(QUuid sessionId, ErrorCode status)
    : _sessionId(sessionId)
    , _status(status)
{}

}
