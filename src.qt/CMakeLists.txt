cmake_minimum_required(VERSION 3.16)
project(src)

add_subdirectory(lib)
add_subdirectory(app)
