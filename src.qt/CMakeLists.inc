set(CMAKE_IDENTIFIER "www.poseidon.de")

# ---- General rules ------------------------------------------------
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

#if(WIN32)
#    set (CMAKE_PREFIX_PATH "$ENV{QTDIR}")
#endif(WIN32)
#if(DEFINED $ENV{QTDIR})
#    set(QT6_DIR "$ENV{QTDIR}")
#endif()


# ---- Platform definition ------------------------------------------
set(PLATFORM "desktop")
# message(STATUS "List dir ${CMAKE_CURRENT_BINARY_DIR}")
string(FIND ${CMAKE_CURRENT_BINARY_DIR} "Android" andipos)
# message(STATUS "Debug ${andipos}")
if( andipos GREATER 0 )
    string(LENGTH ${CMAKE_CURRENT_BINARY_DIR} andilen)
    string(SUBSTRING ${CMAKE_CURRENT_BINARY_DIR} ${andipos} ${andilen}-${andipos} andistr)
    # message(STATUS "Andistr: ${andistr}")

    string(FIND ${andistr} "Clang_" clangpos)
    # message(STATUS "CLang pos: ${clangpos}")
    math(EXPR clangpos ${clangpos}+6)
    # message(STATUS "CLang pos: ${clangpos}")
    string(FIND ${andistr} "-" hyphenpos)
    # message(STATUS "Hypen pos: ${hyphenpos}")
    math(EXPR hyphenpos ${hyphenpos}-${clangpos})
    # message(STATUS "Hypen pos: ${hyphenpos}")

    string(SUBSTRING ${andistr} ${clangpos} ${hyphenpos}-${clangpos} platformstr)

    # message(STATUS "THING environment variable defined ${clangstr}")
    set(PLATFORM ${platformstr})
endif()


# ---- Pfade definition ---------------------------------------------
get_filename_component(tmp_dir ${CMAKE_CURRENT_LIST_DIR} ABSOLUTE)
file(RELATIVE_PATH POS_DIR "${CMAKE_CURRENT_SOURCE_DIR}" ${tmp_dir})
message(STATUS "POS_DIR: ${POS_DIR}")

set(ARCH_DIR ${POS_DIR}../bin/libs/${PLATFORM})
message(STATUS "ARCH_DIR: ${ARCH_DIR}")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${ARCH_DIR})
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY})
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY})

set(LIBS_DIR ${POS_DIR}../bin/libs/${PLATFORM})
message(STATUS "LIBS_DIR: ${LIBS_DIR}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${LIBS_DIR})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})

set(TEST_DIR ${POS_DIR}../bin/tests/${PLATFORM})
message(STATUS "TEST_DIR: ${TEST_DIR}")
set(OUTS_DIR ${POS_DIR}../bin/deploy/${PLATFORM})
message(STATUS "OUTS_DIR: ${OUTS_DIR}")
#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${OUTS_DIR})
#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

set(REPS_DIR ${POS_DIR}../bin/reports/${PLATFORM})
message(STATUS "REPS_DIR: ${REPS_DIR}")
set(REPORTS_OUTPUT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/${REPS_DIR})

set(INCL_DIR ${POS_DIR}lib)
message(STATUS "INCL_DIR: ${INCL_DIR}")
include_directories( ${INCL_DIR} )
link_directories( ${ARCH_DIR} )
