import Vue from 'vue'
import App from './App.vue'
// import './pos-connection'
//import VueSignalR from '@aspnet/signalr'

Vue.config.productionTip = false
    // Vue.use(PosConnection);
//Vue.use(VueSignalR, 'http://localhost:5000/pos')

export const bus = new Vue();

new Vue({
  render: h => h(App),
}).$mount('#app')
