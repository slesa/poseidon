import { HubConnectionBuilder, LogLevel } from '@aspnet/signalr'

export default {
    PosConnection.install = function(Vue, options) {
        const connection = new HubConnectionBuilder()
            .withUrl('http://localhost:5000/pos')
            .configureLogging(LogLevel.Debug)
            .build();
        connection.start()
            .onError( function(err) {
                console.error('Got connection error: ' + err)
            })
            .catch( function(err) {
                return console.error(err);
            });

    }
}
