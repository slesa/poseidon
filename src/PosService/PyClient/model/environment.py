from PySide2.QtCore import QObject, Signal, Property


class Environment(QObject):
    def __init__(self):
        QObject.__init__(self)
        self._terminal = 0

    def get_terminal(self) -> int:
        return self._terminal

    def set_terminal(self, value: int):
        self._terminal = value

    @Signal
    def terminal_changed(self):
        pass

    terminal = Property(int, get_terminal, set_terminal, notify=terminal_changed)
