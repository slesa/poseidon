from PySide2.QtCore import QAbstractListModel, Qt, QModelIndex, QObject, SIGNAL


class SignalSlotChanger(QObject):
    def __init__(self, model):
        QObject.__init__(self)
        self.model = model
        QObject.connect(self, SIGNAL("AddWaiter(Waiter)"), self.on_add_waiter)

    def add_waiter(self, waiter):
        print (f'sendding {waiter.id()} - {waiter.name()} to ui')
        self.emit(SIGNAL("AddWaiter(Waiter)"), waiter)

    def on_add_waiter(self, waiter):
        print (f'adding {waiter.id()} - {waiter.name()} to model')
        self.model.add_waiter(waiter)


class WaiterListItemModel(QAbstractListModel):
    IdRole = Qt.UserRole + 1
    NameRole = Qt.UserRole + 2

    _roles = {
        IdRole: b"id",
        NameRole: b"name"
    }

    def __init__(self, parent=None):
        super(WaiterListItemModel, self).__init__(parent)
        self._waiters = []

    def reset(self):
        self.beginResetModel()
        self._waiters = []
        self.endResetModel()

    def add_waiter(self, waiter):
        self.beginInsertRows(QModelIndex(), self.rowCount(), self.rowCount())
        self._waiters.append(waiter)
        self.endInsertRows()

    def rowCount(self, parent=QModelIndex()):
        return len(self._waiters)

    def data(self, index, role=Qt.DisplayRole):
        try:
            waiter = self._waiters[index.row()]
        except IndexError:
            return None  # QVariant()

        if role == self.IdRole:
            return waiter.id()
        if role == self.NameRole:
            return waiter.name()
        return None  # QVariant()

    def roleNames(self):
        return self._roles
