# This Python file uses the following encoding: utf-8
import sys
import os

import requests
from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine

from inputdata.waiter import Waiter
from model.waiterlistitemmodel import WaiterListItemModel
from tools.settings_reader import read_settings
from model.environment import Environment


def get_environment(cfg):
    init_env = Environment()
    init_env.terminal = cfg.terminal_id
    return init_env


def load_waiters(waiter_list):
    r = requests.get('http://localhost:5000/data/waiters')
    data = r.json()  # as its a rest api you can directly access the json object
    print(data)
    # data = json.loads(payload)
    waiter_list.reset()
    # changer = SignalSlotChanger(waiter_list)
    for waiter in data:
        w = Waiter(int(waiter['id']), waiter['name'])
        waiter_list.add_waiter(w)
        # changer.add_waiter(w)


if __name__ == "__main__":

    settings = read_settings()
    print (f'settings terminal: {settings.terminal_id} of {type(settings.terminal_id)}')

    waiters = WaiterListItemModel()
    waiters.add_waiter(Waiter(0, 'Data not loaded yet'))
    load_waiters(waiters)

    # def event_handler(event_name, payload):
    #     print(f'Event {event_name}, payload {payload}')
    #     if event_name == 'got_waiters':
    #         load_waiters(payload, waiters)

    env = get_environment(settings)
    print (f'env terminal: {env.terminal} of {type(env.terminal)}')

    app = QGuiApplication(sys.argv)
    # qmlRegisterType(WaiterListItemModel, 'CoreData', 1, 0, 'WaiterListItemModel')

    engine = QQmlApplicationEngine()
    context = engine.rootContext()
    context.setContextProperty('env', env)
    context.setContextProperty('waiters', waiters)

    engine.load(os.path.join(os.path.dirname(__file__), "main.qml"))
    # sender = signalr_start(settings.host, settings.port, on_connect, settings.terminal_id, event_handler)
    if not engine.rootObjects():
        sys.exit(-1)

    result = app.exec_()
    # signalr_stop(sender)
    sys.exit(result)
