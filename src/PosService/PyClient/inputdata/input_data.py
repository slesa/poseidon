import requests

from inputdata.terminal import Terminal
from inputdata.waiter import Waiter


class InputData:

    def __init__(self):
        pass

    def load_terminals(self):
        r = requests.get('http://localhost:5000/data/terminals')
        data = r.json()  # as its a rest api you can directly access the json object
        result = []
        for terminal in data:
            w = Terminal(int(terminal['id']), terminal['name'])
            result.append(w)
        return result

    def load_waiters(self):
        r = requests.get('http://localhost:5000/data/waiters')
        data = r.json()  # as its a rest api you can directly access the json object
        # print(data)
        # changer = SignalSlotChanger(waiter_list)
        result = []
        for waiter in data:
            w = Waiter(int(waiter['id']), waiter['name'])
            result.append(w)
        return result
