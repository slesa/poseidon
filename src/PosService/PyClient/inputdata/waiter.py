class Waiter(object):

    def __init__(self, id=0, name=''):
        self._id = id
        self._name = name

    def id(self):
        return self._id

    def name(self):
        return self._name
