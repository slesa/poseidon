from dataclasses import dataclass

from PySide2.QtCore import QSettings

company = "42 GmbH"
program = "pyclient"
groupSettings = "Settings"
entryHost = "host"
defaultHost = "localhost"
entryPort = "port"
defaultPort = 5000
entryTerminal = "terminal"
defaultTerminal = 0


@dataclass
class ClientSettings:
    host: str
    port: int
    terminal_id: int


def read_settings():
    #    reader = QSettings(company, program, QSettings.IniFormat)
    reader = QSettings('pyclient.ini', QSettings.IniFormat)

    reader.beginGroup(groupSettings)
    host = reader.value(entryHost, defaultHost)  # .toString()
    port = reader.value(entryPort, defaultPort)  # .toInt()
    terminal = int(reader.value(entryTerminal, defaultTerminal))  # .toInt()
    reader.endGroup()

    return ClientSettings(host, port, terminal)
