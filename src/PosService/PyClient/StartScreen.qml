import QtQuick 2.12
import QtQuick.Controls 2.5

Page {
    id: startScreen
    width: 600
    height: 400

    title: qsTr("Initialization Error")

    Label {
        text: qsTr("Terminal ID is not set")
        anchors.centerIn: parent
    }
}
