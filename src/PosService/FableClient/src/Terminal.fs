[<RequireQualifiedAccess>]
module Terminal

    open Elmish
    open Feliz
//    open Fable.SignalR
//    open Fable.SignalR.Elmish
    open System

    type State = {
        terminal: int
        sessionId: Guid option
        loginError: string option
    }
    
    type Action =
    | Set_terminal of int
    
    type Response =
    | Terminal_logged_in of Guid
    | Terminal_error of string
    
    type Msg =
    | TerminalLoggedIn of Guid
    | TerminalError of string
    
    let init() = 
        let state = {
            terminal = 1
            sessionId = None
            loginError = None
        }
        state, Cmd.none
        
    let update (terminalMsg: Msg) (terminalState: State) =
        match terminalMsg with
            _ -> terminalState
//        | RegisterHub hub ->
//            let cmd = Cmd.SignalR.send (Some(hub)) (Action.Set_terminal terminalState.terminal)
//            terminalState, cmd
//        | SignalRMsg event ->
//            match event with
//            | Terminal_logged_in sessionId ->
//                { terminalState with sessionId = Some sessionId }, Cmd.none
//            | Terminal_error msg ->
//                { terminalState with loginError = Some msg }, Cmd.none
    
    let renderLoginError (terminal: int) (msg: string) =
        let text = sprintf "Could not connect terminal %d to poskernel: %s" terminal msg
        Html.h1 text
    let renderLoginTry (terminal: int) =
        let text = sprintf "Connecting terminal %d to poskernel..." terminal
        Html.h1 text
    let renderConnected (terminal: int) (sessionId: Guid) =
        let text = sprintf "Terminal %d connected to poskernel in session %s" terminal (sessionId.ToString())
        Html.h1 text
        
    let render (state: State) (dispatch: Msg -> unit) =
        Html.div [
            match state.loginError, state.sessionId with
            | Some msg, None -> renderLoginError state.terminal  msg
            | None, Some sessionId -> renderConnected state.terminal sessionId
            | _, _ -> renderLoginTry state.terminal
        ]
        