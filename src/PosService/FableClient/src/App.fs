module App

open Elmish
open Elmish.React
open Fable.SignalR
open Feliz
open Fable.SignalR.Elmish

module Endpoints =
    let [<Literal>] Root = "/pos"
    
[<RequireQualifiedAccess>]
type Page =
  | Terminal
  | Counter
  | TextInput


type Action =
  | TerminalAction of Terminal.Action
  
type Response =
  | TerminalResponse of Terminal.Response
  
type State = {
  Hub: Elmish.Hub<Action,Response> option
  Terminal: Terminal.State
  Counter: Counter.State
  Input: Input.State
  CurrentPage: Page 
}

type Msg =
  | TerminalMsg of Terminal.Msg
  | CounterMsg of Counter.Msg
  | InputMsg of Input.Msg
  | SwitchPage of Page
  | RegisterHub of Elmish.Hub<Action,Response>
  | SignalRMsg of Response


let init() =
  let hubCmd =
        Cmd.SignalR.connect RegisterHub  (fun hub -> 
            hub.withUrl(Endpoints.Root)
               .withAutomaticReconnect()
               .configureLogging(LogLevel.Debug)
               
                (*.onMessage <|
                    function
                    | Response.NewCount i -> setCount i
                    | Response.RandomCharacter str -> setText str *)
        )
  let terminalState, terminalCmd = Terminal.init()
  let counterState, counterCmd = Counter.init()
  let inputState, inputCmd = Input.init()

  let initialState = {
    Hub = None
    CurrentPage = Page.Terminal
    Terminal = terminalState
    Counter = counterState
    Input = inputState }
  let initialCmd = Cmd.batch [
    hubCmd
    Cmd.map TerminalMsg terminalCmd
    Cmd.map CounterMsg counterCmd
    Cmd.map InputMsg inputCmd
  ]
  initialState, initialCmd

let update (msg: Msg) (state: State) =
    match msg with
//    | RegisterHub hub ->
//      let updatedTerminal, terminalCmd = Terminal.update (Terminal.Msg.RegisterHub (hub)) state.Terminal
//      { state with Hub = Some hub }, Cmd.none
//    | SignalRMsg (TerminalResponse terminalEvent) ->
//      let updatedTerminal, terminalCmd = Terminal.update (Terminal.Msg >> terminalEvent) state.Terminal
//      { state with Terminal = updatedTerminal }, terminalCmd
//      state, Cmd.none
    | TerminalMsg terminalMsg ->
//      let updatedTerminal, terminalCmd = Terminal.update terminalMsg state.Terminal
//      let cmd = Cmd.map TerminalMsg terminalCmd
//      { state with Terminal = updatedTerminal }, cmd
      state, Cmd.none
    | CounterMsg counterMsg ->
      let updatedCounter, counterCmd = Counter.update counterMsg state.Counter
      let cmd = Cmd.map CounterMsg counterCmd
      { state with Counter = updatedCounter }, cmd
    | InputMsg inputMsg ->
      let updatedInput, inputCmd = Input.update inputMsg state.Input
      let cmd = Cmd.map InputMsg inputCmd
      { state with Input = updatedInput}, cmd
    | SwitchPage page ->
        { state with CurrentPage = page }, Cmd.none

let divider = Html.div [ prop.style [ style.margin 10 ] ]

let render (state: State) (dispatch: Msg -> unit) =
  
  (* let counterDispatch (counterMsg: CounterMsg) : unit =
    dispatch (Msg.CounterMsg counterMsg)*)
  
  let inputDispatch (inputMsg: Input.Msg) : unit =
    dispatch (Msg.InputMsg inputMsg)

  match state.CurrentPage with
  | Page.Terminal ->
      Terminal.render state.Terminal (TerminalMsg >> dispatch)
  | Page.Counter ->
      Html.div [
        Html.button [
          prop.text "Show Text Input"
          prop.onClick (fun _ -> dispatch(SwitchPage Page.TextInput))
        ]
        divider 
        Counter.render state.Counter (CounterMsg >> dispatch)
      ]
  | Page.TextInput ->
    Html.div [
      Html.button [
        prop.text "Show Counter"
        prop.onClick (fun _ -> dispatch(SwitchPage Page.Counter))
      ]
      divider 
      Input.render state.Input inputDispatch
    ]

Program.mkProgram init update render
|> Program.withReactSynchronous "elmish-app"
|> Program.run