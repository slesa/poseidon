[<RequireQualifiedAccess>]
module Counter
    
    open Elmish
    open Elmish.React
    open Feliz

    type State = {
        Count: int
    }
    
    type Msg = 
    | Increment
    | Decrement
    | IncrementDelayed

    let init() = 
        let state = {
            Count = 0; 
        }
        state, Cmd.none

    let update (counterMsg: Msg) (counterState: State) =
        match counterMsg with
        | Increment ->
            { counterState with Count = counterState.Count + 1 }, Cmd.none
        | Decrement ->
            { counterState with Count = counterState.Count - 1 }, Cmd.none
        | IncrementDelayed ->
             let delayedIncrement = async {
                 do! Async.Sleep 1000
                 return Increment
             }
             counterState, Cmd.fromAsync delayedIncrement

    let render (state: State) (dispatch: Msg -> unit) =
        let headerText = 
            if state.Count % 2 = 0
            then "Count is even"
            else "Count is odd"
  
        let oddOrEvenText = Html.h1 headerText

        Html.div [
            prop.style [ style.padding 10 ]
            prop.children [
                Html.button [
                    prop.classes [ "button"; "is-primary" ]
                    prop.onClick (fun _ -> dispatch Increment)
                    prop.text "Increment"
                ]
                Html.button [
                    prop.classes [ "button" ]
                    prop.onClick (fun _ -> dispatch Decrement)
                    prop.text "Decrement"
                ]
                Html.button [
                    prop.classes [ "button" ]
                    prop.onClick (fun _ -> dispatch IncrementDelayed)
                    prop.text "Increment Delayed"
                ]
                Html.h1 [
                    if state.Count < 0 then prop.classes [ "shiny"; "text-large"]
                    prop.text state.Count
                ] 
                if state.Count > 0 then oddOrEvenText
            ]
        ]
