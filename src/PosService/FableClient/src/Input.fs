[<RequireQualifiedAccess>]
module Input

    open Elmish
    open Feliz
    open Validation

    type State = {
        TextInput: string
        Capitalized: bool
        NumberInput: Validated<int>
    }

    type Msg =
    | SetTextInput of string
    | SetCapitalized of bool
    | SetNumberInput of Validated<int>

    let init() = 
        let state = {
            TextInput = "(leer)"; 
            Capitalized = false; 
            NumberInput = Validated.createEmpty()
        }
        state, Cmd.none

    let update (inputMsg: Msg) (inputState: State) =
        match inputMsg with
        | SetTextInput str ->
          { inputState with TextInput = str }, Cmd.none
        | SetCapitalized flag ->
          { inputState with Capitalized = flag }, Cmd.none
        | SetNumberInput num ->
          { inputState with NumberInput = num }, Cmd.none


    let render (state: State) (dispatch: Msg -> unit) =
        
        let tryParseInt (input: string) : Validated<int> =
            try Validated.success input (int input)
            with | _ -> Validated.failure input

        let validatedTextColor validated =
            match validated.Parsed with
            | Some _ -> color.green
            | None -> color.crimson

        Html.div [
            prop.style [ style.padding 10]
            prop.children [
                Html.input [ 
                    prop.valueOrDefault state.TextInput
                    prop.onChange (SetTextInput >> dispatch) 
                ]
                Html.div [
                  Html.label [
                        prop.htmlFor "checkbox-capitalized"
                        prop.text "Capitalized"
                  ]
                  Html.input [
                    prop.style [ style.margin 5 ]
                    prop.id "checkbox-capitalized"
                    prop.type'.checkbox
                    prop.isChecked state.Capitalized
                    prop.onChange (SetCapitalized >> dispatch)
                  ]
                ]
                Html.span (
                    if state.Capitalized 
                    then state.TextInput.ToUpper()
                    else state.TextInput
                )
    
                Html.div [
                    prop.style [ style.padding 10]
                    prop.children [ 
                        Html.input [ 
                            prop.type'.number
                            prop.valueOrDefault state.NumberInput.Raw
                            prop.onChange (tryParseInt >> SetNumberInput >> dispatch) 
                        ]
                        Html.h1 [
                            prop.style [ style.color (validatedTextColor state.NumberInput) ]
                            prop.text state.NumberInput.Raw
                        ]
                    ]
                ]
            ]
        ]
