namespace Elmish

module Cmd =

    let fromAsync (operation: Async<'msg>) : Cmd<'msg> =
        let delayedCmd (dispatch: 'msg -> unit) : unit =
            let delayedDispatch = async {
                let! msg = operation
                dispatch msg
            }
            Async.StartImmediate delayedDispatch
        Cmd.ofSub delayedCmd
        
    let indefinite (timeout: int) (msg: 'Msg) =
        let command (dispatch: 'Msg -> unit) : unit =
            let workflow = async {
                while true do
                    do! Async.Sleep timeout
                    dispatch msg
            }
            Async.StartImmediate workflow
        Cmd.ofSub command