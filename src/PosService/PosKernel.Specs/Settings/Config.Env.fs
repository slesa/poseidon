﻿module Tests.Settings.Config_Env
open PosKernel.Settings.Configuration
open System
open Expecto

[<Tests>]
let setupEnvTests =

  let teardown =
      Environment.SetEnvironmentVariable("POSKERNEL_REST_LISTEN", "")
      Environment.SetEnvironmentVariable("POSKERNEL_REST_STDPORT", "")
      Environment.SetEnvironmentVariable("POSKERNEL_REST_SECPORT", "")
    
  testSequenced <| testList "Setup Rest" [
    testCase "When configuring Rest Listen via env" <| fun _ ->
      Environment.SetEnvironmentVariable("POSKERNEL_REST_LISTEN", "RestServer")
      let config = readConfigFromEnvironment createDefaultConfig
      Expect.equal config.RestConfig.Listen "RestServer" "Rest server equals"
      teardown

    testCase "When configuring Rest Std Port via env" <| fun _ ->
      Environment.SetEnvironmentVariable("POSKERNEL_REST_STDPORT", "4711")
      let config = readConfigFromEnvironment createDefaultConfig
      Expect.equal config.RestConfig.StdPort 4711 "Rest standard port equals"
      teardown

    testCase "When configuring Rest Sec Port via env" <| fun _ ->
      Environment.SetEnvironmentVariable("POSKERNEL_REST_SECPORT", "313")
      let config = readConfigFromEnvironment createDefaultConfig
      Expect.equal config.RestConfig.SecPort 313 "Rest secure port equals"
      teardown

    testCase "When configuring Rest via env" <| fun _ ->
      Environment.SetEnvironmentVariable("POSKERNEL_REST_LISTEN", "RestServer")
      Environment.SetEnvironmentVariable("POSKERNEL_REST_STDPORT", "4711")
      Environment.SetEnvironmentVariable("POSKERNEL_REST_SECPORT", "313")
      let config = readConfigFromEnvironment createDefaultConfig
      Expect.equal config.RestConfig.Listen "RestServer" "Rest server equals"
      Expect.equal config.RestConfig.StdPort 4711 "Rest standard port equals"
      Expect.equal config.RestConfig.SecPort 313 "Rest secure port equals"
      teardown
  ]
