﻿module Tests.Settings.Config_Ini
open PosKernel.Settings.Configuration
open System.Reflection
open System.IO
open System
open Expecto

[<Tests>]
let setupIniTests =

  testSequenced <| testList "Setup Rest" [
    let setup file content =
      let cwd = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
      let fn,_ = getConfigFileAndCreate cwd file
      File.WriteAllText(fn, content)
      cwd, fn, file
      
    let teardown fn =
//      File.Delete fn
      fn |> ignore

    let setupEnv file content =
      Environment.SetEnvironmentVariable("POSKERNEL_REST_LISTEN", "EnvServer")
      Environment.SetEnvironmentVariable("POSKERNEL_REST_STDPORT", "3711")
      Environment.SetEnvironmentVariable("POSKERNEL_REST_SECPORT", "213")
      setup file content

    let teardownEnv fn =
      teardown fn
      Environment.SetEnvironmentVariable("POSKERNEL_REST_LISTEN", "")
      Environment.SetEnvironmentVariable("POSKERNEL_REST_STDPORT", "")
      Environment.SetEnvironmentVariable("POSKERNEL_REST_SECPORT", "")
      
    testCase "When configuring Rest Listen via file" <| fun _ ->
      let cwd, full, file = setup "Rest Listen via file.ini" "[Rest]\nListen = RestServer"
      let config = readConfigFrom cwd createDefaultConfig file 
      Expect.equal config.RestConfig.Listen "RestServer" "Rest server equals"
      teardown full
    
    testCase "When configuring Rest StdPort via file" <| fun _ ->
      let cwd, full, file = setup "Rest StdPort via file.ini" "[Rest]\nStdPort = 4711"
      let config = readConfigFrom cwd createDefaultConfig file
      Expect.equal config.RestConfig.StdPort 4711 "Rest std port equals"
      teardown full
    
    testCase "When configuring Rest SecPort via file" <| fun _ ->
      let cwd, full, file = setup "Rest SecPort via file.ini" "[Rest]\nSecPort = 313"
      let config = readConfigFrom cwd createDefaultConfig file
      Expect.equal config.RestConfig.SecPort 313 "Rest sec port equals"
      teardown full
    
    testCase "When configuring Rest via file" <| fun _ ->
      let cwd, full, file = setup "Rest via file.ini" "[Rest]\nListen = RestServer\nStdPort = 4711\nSecPort = 313"
      let config = readConfigFrom cwd createDefaultConfig file
      Expect.equal config.RestConfig.Listen "RestServer" "Rest server equals"
      Expect.equal config.RestConfig.StdPort 4711 "Rest std port equals"
      Expect.equal config.RestConfig.SecPort 313 "Rest sec port equals"
      teardown full

    testCase "When configuring Rest via env and file" <| fun _ ->
      let cwd, full, _ = setupEnv ConfigFile "[Rest]\nListen = FileServer\nStdPort = 4711\nSecPort = 313"
      let config = configure [||] cwd
      Expect.equal config.RestConfig.Listen "FileServer" "File overwrites env"
      Expect.equal config.RestConfig.StdPort 4711 "File overwrites env"
      Expect.equal config.RestConfig.SecPort 313 "File overwrites env"
      teardownEnv full

    testCase "When configuring Rest via file and args" <| fun _ ->
      let cwd, full, _ = setup ConfigFile "[Rest]\nListen = FileServer\nStdPort = 3711\nSecPort = 213"
      let config = configure [|"--listen=ArgServer"; "--stdport=4711"; "--secport=313"|] cwd
      Expect.equal config.RestConfig.Listen "ArgServer" "Args overwrite file"
      Expect.equal config.RestConfig.StdPort 4711 "Args overwrite file"
      Expect.equal config.RestConfig.SecPort 313 "Args overwrite file"
      teardown full
  ]
