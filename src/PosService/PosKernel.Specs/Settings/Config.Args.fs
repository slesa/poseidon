﻿module Tests.Settings.Config_Args
open PosKernel.Settings.Configuration
open Expecto

[<Tests>]
let setupArgTests =
  
  let arrangeWith args =
      let config = createDefaultConfig
      readConfigFromArgs args config
      
  testList "Setup Rest" [
    testCase "When configuring Rest Listen via args" <| fun _ ->
      let args = [|"-l=RestServer"|]
      let config = arrangeWith args
      Expect.equal config.RestConfig.Listen "RestServer" "Rest server equals"

    testCase "When configuring Rest Std Port via args" <| fun _ ->
      let args = [|"-r=4711"|]
      let config = arrangeWith args
      Expect.equal config.RestConfig.StdPort 4711 "Rest standard port equals"

    testCase "When configuring Rest Sec Port via args" <| fun _ ->
      let args = [|"-s=313"|]
      let config = arrangeWith args
      Expect.equal config.RestConfig.SecPort 313 "Rest secure port equals"

    testCase "When configuring Rest via args" <| fun _ ->
      let args = [|"-l=RestServer"; "-r=4711"; "-s=313"|]
      let config = arrangeWith args
      Expect.equal config.RestConfig.Listen "RestServer" "Rest server equals"
      Expect.equal config.RestConfig.StdPort 4711 "Rest standard port equals"
      Expect.equal config.RestConfig.SecPort 313 "Rest secure port equals"
  ]
