module PosKernel.InputData.DatReader
open System
open System.IO
open System.Text

// Todo: doppelt definiert in DataHandler
type KeyValues = Map<string,string>

// --- Math ---------------------

let SwapEndian value =
    let b1 = (value >>> 0) &&& 0xff
    let b2 = (value >>> 8) &&& 0xff
    let b3 = (value >>> 16) &&& 0xff
    let b4 = (value >>> 24) &&& 0xff
    b1 <<< 24 ||| b2 <<< 16 ||| b3 <<< 8 ||| b4 //<<< 0

// --- File I/O -----------------

let CheckDataPath (path: string) =
    let di = DirectoryInfo(path)
    if not di.Exists then
        di.Create()
    path

let GetDatFile (path: string) (file: string) =
    let fn = (if not (file.EndsWith(".dat")) then file + ".dat" else file)
    Path.Combine(path, fn)


let ReadString (reader: BinaryReader) : string =
    let len = reader.ReadInt32()
    if len <= 0 then ""
    else
        let bufferLen = SwapEndian (len)
        let buffer = reader.ReadBytes(bufferLen)
        let result = Encoding.BigEndianUnicode.GetString(buffer)
        result

// --- Single Value -------------

let ReadDatValueFrom (reader: BinaryReader) =

    let rec ReadKeyValuesRec (itemCount: int) (reader: BinaryReader): KeyValues =
        if itemCount = 0 then Map.empty
        else
            let key = ReadString reader
            if (String.IsNullOrEmpty(key)) then Map.empty
            else
                let value = ReadString reader
                let keyValues = ReadKeyValuesRec (itemCount-1) reader 
                keyValues.Add(key, value)

    let count = reader.ReadInt32()
    if count <= 0 then Map.empty
    else
        let itemCount = SwapEndian count
        printfn $"Read {itemCount} key/values for item"
        let result = ReadKeyValuesRec itemCount reader
        result

// --- Value List ---------------

let rec ReadValuesFromStream (reader: BinaryReader) (fileLength: int64) : KeyValues list =
    if reader.BaseStream.Position >= fileLength then []
    else
        let newValue = ReadDatValueFrom reader
        if Map.isEmpty newValue then
            []
        else
            let result = [ newValue ] @ (ReadValuesFromStream reader fileLength)
            result

let ReadDatValueList fileName : KeyValues list =
    let data = File.ReadAllBytes(fileName)
    let fs = new MemoryStream(data)
    use reader = new BinaryReader(fs)
    ReadValuesFromStream reader (int64(data.Length))
 
let ReadDatFile (path: string) (file: string) : KeyValues list =

    let dataPath = CheckDataPath path
    let fileName = GetDatFile dataPath file
    if File.Exists(fileName) then
        ReadDatValueList fileName
    else
        []

(*
// --- Debug --------------------

let PrintValue (item: string) (value: Map<string,string>) =
    let id = value.["id"];
    printfn $"--- {item} {id} -------"
    for kv in value do
        printfn $"{kv.Key} = {kv.Value}"
 
let PrintList title item values =
    printfn $"==== {title} ========================"
    values |> List.iter (fun value -> PrintValue item value)

let DoTheMagic =
    let cwd = Directory.GetCurrentDirectory()
    printfn $"Running in {cwd}"
    
    let articles = ReadDatFile "data" "articles"
    printfn $"{articles.Length} articles loaded"
    // PrintList "Articles" "Article" articles
*)
