﻿module PosKernel.InputData.DataHandler
open PosKernel.InputData.DatReader
open Giraffe
open System.Text.Json
//open Microsoft.AspNetCore.Hosting
//open Microsoft.Extensions.Hosting

type DataList = {
    Articles: KeyValues list option
    Families: KeyValues list option
    FamilyGroups: KeyValues list option
    Currencies: KeyValues list option
    Payforms: KeyValues list option
    Waiters: KeyValues list option
    Terminals: KeyValues list option
}

let dataList : DataList = {
    Articles = Option.None
    Families = Option.None
    FamilyGroups = Option.None
    Currencies = Option.None
    Payforms = Option.None
    Waiters = Option.None
    Terminals = Option.None
}

let deliverList (list: KeyValues list option) (listName: string) =
    let values =
        match list with
        | None -> ReadDatFile "data" listName
        | Some data -> data
    let answer = JsonSerializer.Serialize values
    answer |> json
    
    
let deliverData listName =
    match listName with
    | "articles" ->
        deliverList dataList.Articles "articles" 
    | "families" ->
        deliverList dataList.Families "families" 
    | "famgroups" ->
        deliverList dataList.FamilyGroups "famgroups" 
    | "currencies" ->
        deliverList dataList.Currencies "currency" 
    | "payforms" ->
        deliverList dataList.Payforms "payform" 
    | "waiters" ->
        deliverList dataList.Waiters "waiters" 
    | "terminals" ->
        deliverList dataList.Terminals "terminals" 
    | _ -> setStatusCode 404 >=> text $"{listName} not Found"
