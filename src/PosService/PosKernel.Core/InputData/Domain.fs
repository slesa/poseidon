module PosKernel.InputData.Domain

type ConstraintEntry = {
    Plu: int
    IsHint: bool
    UsePrice: bool
    Price: decimal
}

type ConstraintLevel = {
    Id: int
    IsActive: bool
    IsCloseable: bool
    IsRecursive: bool
    IsTakeCount: bool
    Entries: ConstraintEntry list
}

type Constraint = {
    Plu: int
    Levels: ConstraintLevel list
}    

type ArticleFlags =
    | IsHint      = 0b00000001
    | IsFreePrice = 0b00000010
    | IsFreeText  = 0b00000100
    | IsWeight    = 0b00001000
    | IsCutGood   = 0b00010000
    | IsAreaGood  = 0b00100000
    
type Article = {
    Plu: int
    Name: string
    Family: int
    Price: decimal
    Flags: ArticleFlags
    Constraint: Constraint
}
type Articles = Article list

type Currency = {
    Id: int
    Name: string
}
type Currencies = Currency list

type Family = {
    Id: int
    Name: string
    FamilyGroup: int
}
type Families = Family list

type FamilyGroup = {
    Id: int
    Name: string
}
type FamilyGroups = FamilyGroup list

type Payform = {
    Id: int
    Name: string
}
type Payforms = Payform list

type VatRate = {
    Id: int
    Name: string
}
type VatRates = VatRate list

type Terminal = {
    Id: int
    Name: string
}
type Terminals = Terminal list

type WaiterRights =
    | CanCreateTable = 0b00000001
    | CanOrder       = 0b00000010
    | CanVoid        = 0b00000100
    | CanPay         = 0b00001000
    | CanSplit       = 0b00010000
    
type Waiter = {
    Id: int
    Name: string
    Keylock: string
    Rights: WaiterRights    
}
type Waiters = Waiter list