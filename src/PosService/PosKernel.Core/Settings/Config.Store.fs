module PosKernel.Settings.Config_Store

open System
open Argu
open PosKernel.Settings.Config_Args
open PosKernel.Settings.Config_Ini

type StoreType =
    | InMemory // using LiteDb's in-memory structure
    | LocalDatabase

type StoreConfig = {
    Type: StoreType
}

let createDefaultStore: StoreConfig =
    { Type = StoreType.LocalDatabase }

let string2type str config =
    match str with
    | "localdb" -> StoreType.LocalDatabase
    | "inmem" -> StoreType.InMemory
    |  _ -> config.Type
    
let readStoreFromEnvironment config =
    let stype: string = (Environment.GetEnvironmentVariable "POSKERNEL_STORE_TYPE").ToLower()
    let config: StoreConfig = {
        config with
            Type = string2type stype config
    }
    config

let readStoreFromFile (config: StoreConfig) (cfg: StoreSettings) =
    { config with
        Type =
            match cfg.Type with
            | None -> config.Type
            | Some stype -> string2type stype config
    }
    
let readStoreFromArgs (config: StoreConfig) (results: ParseResults<AppArguments>)=
    let stype = results.GetResult (Server, defaultValue = None)
    { config with Type = string2type stype.Value config }
