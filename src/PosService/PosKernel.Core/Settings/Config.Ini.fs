module PosKernel.Settings.Config_Ini
open FsConfig

type RestSettings = {
    Listen: string option
    StdPort: int option
    SecPort: int option
}

type MqttSettings = {
    Server: string option
    Port: int option
}

type StoreSettings = {
    Type: string option
}

type Settings = {
    Rest: RestSettings
    Mqtt: MqttSettings
    Store: StoreSettings
}

let readIniFile (appConfig: AppConfig) =
    match appConfig.Get<Settings>() with
    | Ok cfg -> cfg
    | Error error ->
        match error with
        | NotFound iniVar ->
            failwithf $"INI var %s{iniVar} not found"
        | BadValue (iniVar, value) ->
            failwithf $"INI var %s{iniVar} has bad value %s{value}"
        | NotSupported msg ->
            failwith msg
