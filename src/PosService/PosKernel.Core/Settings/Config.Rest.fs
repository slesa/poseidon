module PosKernel.Settings.Config_Rest
open PosKernel.Settings.Config_Args
open PosKernel.Settings.Config_Ini
open Argu
open System

[<Literal>]
let DefaultRestListen = "0.0.0.0"
[<Literal>]
let DefaultRestStdPort = 8900
[<Literal>]
let DefaultRestSecPort = 8901

type RestConfig = {
    Listen: string
    StdPort: int
    SecPort: int
}

let createDefaultRest: RestConfig =
    { Listen = DefaultRestListen
      StdPort = DefaultRestStdPort
      SecPort = DefaultRestSecPort }

let readRestFromEnvironment config =
    let listen = Environment.GetEnvironmentVariable "POSKERNEL_REST_LISTEN"
    let stdport = Environment.GetEnvironmentVariable "POSKERNEL_REST_STDPORT"
    let secport = Environment.GetEnvironmentVariable "POSKERNEL_REST_SECPORT"
    let config: RestConfig = {
        config with
            Listen =
                if String.IsNullOrWhiteSpace(listen) then config.Listen
                else listen
            StdPort =
                if String.IsNullOrWhiteSpace(stdport) then config.StdPort
                else int(stdport)
            SecPort =
                if String.IsNullOrWhiteSpace(secport) then config.SecPort
                else int(secport)
    }
    config

let readRestFromFile (config: RestConfig) (cfg: RestSettings) =
    { config with
        Listen =
            match cfg.Listen with
            | None -> config.Listen
            | Some listen -> listen
        StdPort =
            match cfg.StdPort with
            | None -> config.StdPort
            | Some port -> port
        SecPort =
            match cfg.SecPort with
            | None -> config.SecPort
            | Some port -> port }
    
let readRestFromArgs (config: RestConfig) (results: ParseResults<AppArguments>) =
    let listen = results.GetResult (Listen, defaultValue = Some(config.Listen))
    let stdport = results.GetResult (StdPort, defaultValue = Some(config.StdPort))
    let secport = results.GetResult (SecPort, defaultValue = Some(config.SecPort))
    { config with Listen = listen.Value; StdPort = stdport.Value; SecPort = secport.Value }
