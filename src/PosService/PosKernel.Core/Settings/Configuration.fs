module PosKernel.Settings.Configuration
open System.IO
open PosKernel.Settings.Config_Args
open PosKernel.Settings.Config_Ini
open PosKernel.Settings.Config_Rest
open PosKernel.Settings.Config_Mqtt
open PosKernel.Settings.Config_Store
open Argu
open FsConfig
open System
open Microsoft.Extensions.Configuration

[<Literal>]
let PathEtc = "etc"
let ConfigFile = "settings.ini"
    
type Configuration = {
    RestConfig: RestConfig
    MqttConfig: MqttConfig
    StoreConfig: StoreConfig
}

let createDefaultConfig: Configuration =
    { RestConfig = createDefaultRest
      MqttConfig = createDefaultMqtt
      StoreConfig = createDefaultStore }

let readConfigFromEnvironment config =
    { config with
        RestConfig = readRestFromEnvironment config.RestConfig
        MqttConfig = readMqttFromEnvironment config.MqttConfig
        StoreConfig = readStoreFromEnvironment config.StoreConfig }

let getConfigPathAndCreate cwd =
    let path = Path.Combine(cwd, PathEtc)
    Directory.CreateDirectory(path) |> ignore
    path

let getConfigFileAndCreate cwd fn =
    let path = getConfigPathAndCreate cwd
    let fn = Path.Combine(path, fn)
    fn, path
    
let readConfigFrom cwd config fn =
    let fn, path = getConfigFileAndCreate cwd fn
    if not (File.Exists(fn)) then
        config
    else
        let configRoot = ConfigurationBuilder().SetBasePath(path).AddIniFile(fn).Build()
        let appConfig = AppConfig(configRoot)
        let settings = readIniFile appConfig
        let result = {
            config with
                RestConfig = readRestFromFile config.RestConfig settings.Rest
                MqttConfig = readMqttFromFile config.MqttConfig settings.Mqtt
                StoreConfig = readStoreFromFile config.StoreConfig settings.Store }
        result

let readConfigFromFile cwd config =
    readConfigFrom cwd config ConfigFile

let readConfigFromArgs args config =
    let errorHandler = ProcessExiter(colorizer = function ErrorCode.HelpText -> None | _ -> Some ConsoleColor.Red)
    let parser = ArgumentParser.Create<AppArguments>(programName = "PosKernel", errorHandler = errorHandler)
    let results = parser.Parse args // [| "--log-level"; "debug" ; "--server" ; "localhost" ; "8080" |]
    // let restResult = results.GetResult (Rest)
    
    { config with
        RestConfig = readRestFromArgs config.RestConfig results 
        MqttConfig = readMqttFromArgs config.MqttConfig results
        StoreConfig = readStoreFromArgs config.StoreConfig results }

let configure args cwd: Configuration =
    createDefaultConfig
    |> readConfigFromEnvironment
    |> readConfigFromFile cwd
    |> readConfigFromArgs args
    