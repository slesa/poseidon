module PosKernel.Settings.Config_Args
open Argu

type AppArguments =
    | [<AltCommandLine("-v")>] Version

    | [<AltCommandLine("-l")>] [<EqualsAssignmentOrSpaced>] Listen of RestListen:string option
    | [<AltCommandLine("-r")>] [<EqualsAssignmentOrSpaced>] StdPort of RestStdPort:int option
    | [<AltCommandLine("-s")>] [<EqualsAssignmentOrSpaced>] SecPort of RestSecPort:int option

    | [<AltCommandLine("-h")>] [<EqualsAssignmentOrSpaced>] Server of MqttServer:string option
    | [<AltCommandLine("-p")>] [<EqualsAssignmentOrSpaced>] Port of MqttPort:int option

    | [<AltCommandLine("-d")>] [<EqualsAssignmentOrSpaced>] Type of StoreType:string option
    
    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | Version -> "Prints the version of the PosKernel."
            
            | Listen _ -> "specify an address poskernel is listening on. Localhost would restrict communication to local."
            | StdPort _ -> "specify the port for unsecure communication."
            | SecPort _ ->  "specify the port for secured communication."

            | Server _ ->  "specify the address of the MQTTL server."
            | Port _ ->  "specify the port of the MQTTL server."

(*and RestArguments =

    interface IArgParserTemplate with
        member s.Usage =
            match s with
*)