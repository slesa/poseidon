module PosKernel.Settings.Config_Mqtt
open PosKernel.Settings.Config_Args
open PosKernel.Settings.Config_Ini
open Argu
open System

[<Literal>]
let DefaultMqttServer = "localhost"
[<Literal>]
let DefaultMqttPort = 5700

type MqttConfig = {
    Server: string
    Port: int    
}

let createDefaultMqtt: MqttConfig =
    { Server = DefaultMqttServer
      Port = DefaultMqttPort }

let readMqttFromEnvironment config =
    let server = Environment.GetEnvironmentVariable "POSKERNEL_MQTT_SERVER"
    let port = Environment.GetEnvironmentVariable "POSKERNEL_MQTT_PORT"
    let config: MqttConfig = {
        config with
            Server =
                if String.IsNullOrWhiteSpace(server) then config.Server
                else server
            Port =
                if String.IsNullOrWhiteSpace(port) then config.Port
                else int(port)
    }
    config


let readMqttFromFile (config: MqttConfig) (cfg: MqttSettings) =
    { config with
        Server =
            match cfg.Server with
            | None -> config.Server
            | Some server -> server
        Port =
            match cfg.Port with
            | None -> config.Port
            | Some port -> port }
    
let readMqttFromArgs (config: MqttConfig) (results: ParseResults<AppArguments>)=
    let server = results.GetResult (Server, defaultValue = Some(config.Server))
    let port = results.GetResult (Port, defaultValue = Some(config.Port))
    { config with Server = server.Value; Port = port.Value }
