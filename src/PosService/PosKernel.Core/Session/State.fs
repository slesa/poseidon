module PosKernel.Session.State
open PosKernel.Session.State_Input
open System

   
type OpenProcess = {
    OnTerminal: int
    FromWaiter: int
}

type OpenProcesses = {
    Processes: OpenProcess list
}

let initOpenProcesses: OpenProcesses =
    { Processes = [] }
    
type OutputData = {
    OpenProcesses: OpenProcesses
}

let initOutput =
    { OpenProcesses = initOpenProcesses }

type TerminalSession = {
    SessionId: Guid
    OpenProcesses: OpenProcess list
}
    
type State = {
    InputData: InputData
    OutputData: OutputData
    Terminals: TerminalSession list
}

let initState =
    { InputData = initInput
      OutputData = initOutput
      Terminals = [] }
