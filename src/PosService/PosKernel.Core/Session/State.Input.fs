module PosKernel.Session.State_Input
open PosKernel.InputData.Domain

type InputConfig = {
    DataPath: string
}

type InputData = {
    Version: int
//    Articles: InputConfig -> Articles
//    Families: InputConfig -> Families
}

let initInput: InputData =
    { Version = 1 }
