﻿module PosKernel.Setup
open Settings.Configuration

let debugConfiguration config =
    printfn $"Rest, listen on %s{config.RestConfig.Listen}"
    printfn $"Rest, stdport %d{config.RestConfig.StdPort}"
    printfn $"Rest, secport %d{config.RestConfig.SecPort}"
    printfn $"Mqtt, server %s{config.MqttConfig.Server}"
    printfn $"Mqtt, port %d{config.MqttConfig.Port}"

let configureSelf args cwd =
    let config = configure args cwd
    debugConfiguration config
    config

