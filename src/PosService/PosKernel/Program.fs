module PosKernel.App
open System.Reflection
open PosKernel.Setup
open System.IO
open Suave
open Suave.Operators
open Suave.Filters
open Environment
open Suave.RequestErrors
open Serilog
open Suave.SerilogExtensions
    
//open PosKernel.CoreData.Classic.DatReader
[<EntryPoint>]
let main args =
    let cwd = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
    let config = configureSelf args cwd
    
    let webApp = WebApp.createUsing config.StoreConfig.Type
    let clientPath = solutionRoot </> "dist" </> "client"
    printfn "Client directory: %s" clientPath
    let webAppConfig = { defaultConfig with homeFolder = Some clientPath }
    
    let webApp =
        choose [ GET >=> path "/" >=> Files.browseFileHome "index.html"
                 Files.browseHome
                 webApp
                 WebApp.socketServer
                 NOT_FOUND "The resource you requested was not found" ]
    Log.Logger <- LoggerConfiguration().Destructure.FSharpTypes().WriteTo.Console().CreateLogger()
    startWebServer webAppConfig webApp
    0
    
    
    
    
(*    let configApp = configureApp config
    
    let contentRoot = Directory.GetCurrentDirectory()
    let webRoot     = Path.Combine(contentRoot, "WebRoot")
    Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(
            fun webHostBuilder ->
                webHostBuilder
                    .UseContentRoot(contentRoot)
                    .UseWebRoot(webRoot)
                    .Configure(Action<IApplicationBuilder> configApp)
                    .ConfigureServices(configureServices)
                    .ConfigureLogging(configureLogging)
                    |> ignore)
        .Build()
        .Run()
    0
    
*)