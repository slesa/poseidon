module PosKernel.App
open System.Reflection
open PosKernel.InputData.DataHandler
open PosKernel.Setup
open Settings.Configuration
open System
open System.IO
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection
open Giraffe

// ---------------------------------
// Models
// ---------------------------------

type Message =
    {
        Text : string
    }

// ---------------------------------
// Views
// ---------------------------------

module Views =
    open Giraffe.ViewEngine

    let layout (content: XmlNode list) =
        html [] [
            head [] [
                title []  [ encodedText "PosKernel" ]
                link [ _rel  "stylesheet"
                       _type "text/css"
                       _href "/main.css" ]
            ]
            body [] content
        ]

    let partial () =
        h1 [] [ encodedText "PosKernel" ]

    let index (model : Message) =
        [
            partial()
            p [] [ encodedText model.Text ]
        ] |> layout

// ---------------------------------
// Web app
// ---------------------------------

let indexHandler (name : string) =
    let greetings = sprintf "Hello %s, from Giraffe!" name
    let model     = { Text = greetings }
    let view      = Views.index model
    htmlView view

let webApp =
    choose [
        GET >=>
            choose [
//                route "/" >=> indexHandler "world"
                route "/" >=> htmlFile "WebRoot/index.html"
                routef "/data/%s" deliverData
                routef "/hello/%s" indexHandler
            ]
        setStatusCode 404 >=> text "Not Found" ]

// ---------------------------------
// Error handler
// ---------------------------------

let errorHandler (ex : Exception) (logger : ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")
    clearResponse >=> setStatusCode 500 >=> text ex.Message

// ---------------------------------
// Config and Main
// ---------------------------------

let configureCors (config: Configuration) (builder : CorsPolicyBuilder) =
    builder
        .WithOrigins(
            $"http://%s{config.RestConfig.Listen}:%d{config.RestConfig.StdPort}",
            $"https://%s{config.RestConfig.Listen}:%d{config.RestConfig.SecPort}")
       .AllowAnyMethod()
       .AllowAnyHeader()
       |> ignore

let configureApp (config: Configuration) (app : IApplicationBuilder) =
    let env = app.ApplicationServices.GetService<IWebHostEnvironment>()
    let configCors = configureCors config
    (match env.IsDevelopment() with
    | true  ->
        app.UseDeveloperExceptionPage()
    | false ->
        app .UseGiraffeErrorHandler(errorHandler)
            .UseHttpsRedirection())
        .UseCors(configCors)
        .UseStaticFiles()
        .UseGiraffe(webApp)

let configureServices (services : IServiceCollection) =
    services.AddCors()    |> ignore
    services.AddGiraffe() |> ignore

let configureLogging (builder : ILoggingBuilder) =
    builder.AddConsole()
           .AddDebug() |> ignore

    
    
//open PosKernel.CoreData.Classic.DatReader
[<EntryPoint>]
let main args =
    let cwd = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
    let config = configureSelf args cwd
    
    let webApp = WebApp.createUsing storageType
    let clientPath = solutionRoot </> "dist" </> "client"
    printfn "Client directory: %s" clientPath
    let webAppConfig = { defaultConfig with homeFolder = Some clientPath }
    
    let webApp =
        choose [ GET >=> path "/" >=> Files.browseFileHome "index.html"
                 Files.browseHome
                 webApp
                 WebApp.socketServer
                 NOT_FOUND "The resource you requested was not found" ]
    Log.Logger <- LoggerConfiguration().Destructure.FSharpTypes().WriteTo.Console().CreateLogger()
    startWebServer webAppConfig webApp
    0
    
    
    
    
(*    let configApp = configureApp config
    
    let contentRoot = Directory.GetCurrentDirectory()
    let webRoot     = Path.Combine(contentRoot, "WebRoot")
    Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(
            fun webHostBuilder ->
                webHostBuilder
                    .UseContentRoot(contentRoot)
                    .UseWebRoot(webRoot)
                    .Configure(Action<IApplicationBuilder> configApp)
                    .ConfigureServices(configureServices)
                    .ConfigureLogging(configureLogging)
                    |> ignore)
        .Build()
        .Run()
    0
    
*)