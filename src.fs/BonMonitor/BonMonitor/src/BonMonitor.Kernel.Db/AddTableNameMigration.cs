using FluentMigrator;

namespace BonMonitor.Kernel.Db
{
    [Migration(20220215001, "Tablename dazu")]
    public class AddTableNameMigration : Migration
    {
        public override void Up()
        {
            Alter.Table("Bons")
                .AddColumn("TableName").AsString().Nullable();
        }

        public override void Down()
        {
            Delete.Column("TableName").FromTable("Bons");
        }
    }
}