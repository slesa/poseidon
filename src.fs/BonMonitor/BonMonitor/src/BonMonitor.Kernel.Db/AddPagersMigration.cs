using FluentMigrator;

namespace BonMonitor.Kernel.Db
{
    [Migration(20211101001, "Add guest and waiter pager to bon")]
    public class AddPagersMigration : Migration
    {
        public override void Up()
        {
            Alter.Table("Bons")
                .AddColumn("GuestPager").AsInt32().Nullable()
                .AddColumn("WaiterPager").AsInt32().Nullable();
        }

        public override void Down()
        {
            Delete.Column("GuestPager").FromTable("Bons");
            Delete.Column("WaiterPager").FromTable("Bons");
        }
    }
}