using FluentMigrator;

namespace BonMonitor.Kernel.Db
{
    [Migration(20201210001, "Kostenstelle zum Bon zwecks Anzeige und Ausdruck")]
    public class AddCostCenterMigration : Migration
    {
        public override void Up()
        {
            Alter.Table("Bons")
                .AddColumn("CostCenter").AsInt64().Nullable()
                .AddColumn("CenterName").AsString().Nullable();
        }

        public override void Down()
        {
            Delete.Column("CostCenter").FromTable("Bons");
            Delete.Column("CenterName").FromTable("Bons");
        }
    }
}