using FluentMigrator;

namespace BonMonitor.Kernel.Db
{
    [Migration(20210323001, "Add memo to orders")]
    public class ChangeMemoSizeMigration : Migration
    {
        public override void Up()
        {
            Alter.Table("BonEntities")
                .AlterColumn("Memos")
                .AsString(65000)
                .Nullable();

        }

        public override void Down()
        {
            Alter.Table("BonEntities")
                .AlterColumn("Memos")
                .AsString()
                .Nullable();
        }
    }
}