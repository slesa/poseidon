using FluentMigrator;

namespace BonMonitor.Kernel.Db
{
    [Migration(20201029001, "Add denounce to events to let mirrors receive finish events")]
    public class AddDenounceMigration : Migration
    {
        public override void Up()
        {
            Alter.Table("BonEvents")
                .AddColumn("Denounce")
                .AsInt64()
                .Nullable();
        }

        public override void Down()
        {
            Delete.Column("Denounce").FromTable("BonEvents");
        }
    }
}