using System.Data;
using FluentMigrator.Builders.Create.Table;
using FluentMigrator.Builders.IfDatabase;

namespace BonMonitor.Kernel.Db
{
    internal static class FluentMigrationExtensions
    {
        public static ICreateTableColumnOptionOrWithColumnSyntax WithAutoIncrementId(
            this ICreateTableWithColumnSyntax tableWithColumnSyntax)
        {
            return tableWithColumnSyntax
                .WithColumn("Id")
                .AsInt64()
                .NotNullable()
                .PrimaryKey()
                .Identity();
        }

        // For SqLite where AutoInc leads to trouble
        public static ICreateTableColumnOptionOrWithColumnSyntax WithIdColumn(
            this ICreateTableWithColumnSyntax tableWithColumnSyntax)
        {
            return tableWithColumnSyntax
                .WithColumn("Id")
                .AsInt64()
                .NotNullable()
                .PrimaryKey();
        }

        public static ICreateTableColumnOptionOrWithColumnSyntax CreateAutoIncrementIdTable(
            this IIfDatabaseExpressionRoot migration, string name)
        {
            return migration.Create.Table(name)
                .WithAutoIncrementId();
        }

        public static ICreateTableColumnOptionOrWithColumnSyntax CreateIdTable(
            this IIfDatabaseExpressionRoot migration, string name)
        {
            return migration.Create.Table(name)
                .WithIdColumn();
        }

        public static ICreateTableColumnOptionOrWithColumnSyntax WithBonFields(
            this ICreateTableWithColumnSyntax tableWithColumnSyntax)
        {
            return tableWithColumnSyntax
                .WithColumn("IsArchived").AsBoolean().NotNullable()
                .WithColumn("Table").AsInt32().NotNullable()
                .WithColumn("Party").AsInt16().NotNullable()
                .WithColumn("TableGuid").AsGuid()//.NotNullable() Needs default value
                .WithColumn("CreationDateTime").AsDateTime2().NotNullable()
                .WithColumn("Waiter").AsInt32().NotNullable()
                .WithColumn("WaiterName").AsString();
        }

        public static ICreateTableColumnOptionOrWithColumnSyntax WithBonEntitiesFields(
            this ICreateTableWithColumnSyntax tableWithColumnSyntax)
        {
            return tableWithColumnSyntax
                .WithColumn("IsArchived").AsBoolean().NotNullable()
                .WithColumn("BonId").AsInt64()
                    .ForeignKey("Bons", "Id").OnDeleteOrUpdate(Rule.Cascade).NotNullable()
                .WithColumn("OrderedDateTime").AsDateTime2().NotNullable()
                .WithColumn("EntryGuid").AsGuid() //.NotNullable() Needs default value
                .WithColumn("Plu").AsInt64().NotNullable()
                .WithColumn("Article").AsString().Nullable()
                .WithColumn("Count").AsInt64().NotNullable()
                .WithColumn("IsHint").AsBoolean().NotNullable()
                .WithColumn("Family").AsInt64().NotNullable()
                .WithColumn("FamilyName").AsString().Nullable()
                .WithColumn("FamilyGroup").AsInt64().NotNullable()
                .WithColumn("FamilyGroupName").AsString().Nullable()
                .WithColumn("Round").AsInt16().NotNullable()
                .WithColumn("CourseControl").AsInt16().NotNullable()
                .WithColumn("BonMonitors").AsString().Nullable()
                .WithColumn("Waiter").AsInt64().NotNullable()
                .WithColumn("WaiterName").AsString().Nullable()
                .WithColumn("ImportedAtTicks").AsInt64().NotNullable();
        }

        public static ICreateTableColumnOptionOrWithColumnSyntax WithBonEventsFields(
            this ICreateTableWithColumnSyntax tableWithColumnSyntax)
        {
            return tableWithColumnSyntax
                .WithColumn("IsArchived").AsBoolean().NotNullable()
                .WithColumn("ImportedAtTicks").AsInt64().NotNullable()
                .WithColumn("Type").AsInt16().NotNullable()
                .WithColumn("SourceMonitor").AsInt32().NotNullable()
                .WithColumn("Payload").AsString(int.MaxValue).Nullable()
                .WithColumn("BonId").AsInt64().Nullable();
                    // .ForeignKey("Bons", "Id").OnDeleteOrUpdate(Rule.Cascade).NotNullable();
        }
    }
}