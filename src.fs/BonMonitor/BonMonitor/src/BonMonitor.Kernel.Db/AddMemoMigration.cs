using FluentMigrator;

namespace BonMonitor.Kernel.Db
{
    [Migration(20200723001, "Add memo to orders")]
    public class AddMemoMigration : Migration
    {
        public override void Up()
        {
            Alter.Table("BonEntities")
                .AddColumn("Memos")
                .AsString()
                .Nullable();
        }

        public override void Down()
        {
            Delete.Column("Memos").FromTable("BonEntities");
        }
    }
}