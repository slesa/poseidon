using FluentMigrator;

namespace BonMonitor.Kernel.Db
{
    [Migration(20220316001, "SplittedFrom zu Orders hinzu um erledigte Bons zu ignorieren")]
    public class AddSplittedFromMigration : Migration
    {
        public override void Up()
        {
            Alter.Table("BonEntities")
                .AddColumn("SplittedFrom")
                .AsGuid()
                .Nullable();
        }

        public override void Down()
        {
            Delete.Column("SplittedFrom").FromTable("BonEntities");
        }
    }
}