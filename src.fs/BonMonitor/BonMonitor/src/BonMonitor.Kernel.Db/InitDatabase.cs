using FluentMigrator;

namespace BonMonitor.Kernel.Db
{
    [Migration(20200710003, "Initial creation")]
    public class InitDatabase : Migration
    {
        public override void Up()
        {
            IfDatabase("SqlServer", "Postgres").CreateAutoIncrementIdTable("Bons")
                .WithBonFields();
            IfDatabase("Sqlite").CreateIdTable("Bons")
                .WithBonFields();

            IfDatabase("SqlServer", "Postgres").CreateAutoIncrementIdTable("BonEntities")
                .WithBonEntitiesFields();
            IfDatabase("Sqlite").CreateIdTable("BonEntities")
                .WithBonEntitiesFields();

            IfDatabase("SqlServer", "Postgres").CreateAutoIncrementIdTable("BonEvents")
                .WithBonEventsFields();
            IfDatabase("Sqlite").CreateIdTable("BonEvents")
                .WithBonEventsFields();

            Create.Table("BonClients")
                .WithColumn("Id").AsInt32().PrimaryKey()
                .WithColumn("Name").AsString()
                .WithColumn("Configuration").AsString(int.MaxValue);

        }

        public override void Down()
        {
            Delete.Table("Bons");
            Delete.Table("BonEntities");
            Delete.Table("BonEvents");
            Delete.Table("BonClients");
        }

    }
}