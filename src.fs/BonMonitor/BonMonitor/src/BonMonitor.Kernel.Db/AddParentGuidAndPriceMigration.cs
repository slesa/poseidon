using FluentMigrator;

namespace BonMonitor.Kernel.Db
{
    [Migration(20200728001, "Add parent guid to orders")]
    public class AddParentGuidAndPriceMigration : Migration
    {
        public override void Up()
        {
            Alter.Table("BonEntities")
                .AddColumn("ParentGuid").AsGuid().Nullable()
                .AddColumn("Price").AsInt64().Nullable();
        }

        public override void Down()
        {
            Delete.Column("ParentGuid").FromTable("BonEntities");
            Delete.Column("Price").FromTable("BonEntities");
        }
    }
}