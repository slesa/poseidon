using System;
using System.Data;
using System.Collections.Generic;
using BonMonitor.Shared;
using Dapper;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Initialization;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace BonMonitor.Kernel.Db
{
    public class SqLiteGuidTypeHandler : SqlMapper.TypeHandler<Guid>
    {
        public override void SetValue(IDbDataParameter parameter, Guid guid)
        {
            parameter.Value = guid.ToString();
        }

        public override Guid Parse(object value)
        {
            return new Guid((string)value);
        }
    }

    public class MemosTypeHandler : SqlMapper.TypeHandler<List<Domain.OrderMemo>>
    {
        public override void SetValue(IDbDataParameter parameter, List<Domain.OrderMemo> value)
        {
            parameter.Value = JsonConvert.SerializeObject(value);
            parameter.DbType = DbType.String;
        }

        public override List<Domain.OrderMemo> Parse(object value)
        {
            return JsonConvert.DeserializeObject<List<Domain.OrderMemo>>(value.ToString());
        }
    }

    public class DatabaseProvider
    {
        readonly BonMonitor.Shared.Database.DbType _dbType;
        readonly string _connection;
        // readonly string _scriptFile;

        public DatabaseProvider(BonMonitor.Shared.Database.DbType dbType, string connection)
        {
            _dbType = dbType;
            _connection = connection;
            // _scriptFile = scriptFile;
        }

        bool _alreadyPrepared;
        // bool _alreadyScripted;
        public void Start()
        {
            if (!_alreadyPrepared)
            {
                _alreadyPrepared = true; // Warning, recursion
                PrepareDatabase();
            }

            /* if (!_alreadyScripted)
            {
                _alreadyScripted = true;
                ScriptDatabase();
            } */
        }

        /* void ScriptDatabase()
        {
            if (string.IsNullOrEmpty(_scriptFile)) return;

            var cwd = Directory.GetCurrentDirectory();
            Console.Write($"Using import script file {_scriptFile} ...");
            if (!File.Exists(_scriptFile))
            {
                Console.WriteLine($"does not exist!");
                return;
            }
            var scriptContent = File.ReadAllText(_scriptFile);
            using (var db = GetConnection())
            {
                db.Execute(scriptContent);
            }
            Console.WriteLine($" done");
        } */

        void PrepareDatabase()
        {
            var serviceProvider = CreateServices();

            SqlMapper.AddTypeHandler(typeof(List<Domain.OrderMemo>), new MemosTypeHandler());

            if (_dbType == BonMonitor.Shared.Database.DbType.SQLite)
            {
                SqlMapper.AddTypeHandler(new SqLiteGuidTypeHandler());
                SqlMapper.RemoveTypeMap(typeof(Guid));
                SqlMapper.RemoveTypeMap(typeof(Guid?));

            }
            using (var scope = serviceProvider.CreateScope())
            {
                UpdateDatabase(scope.ServiceProvider);
            }
        }


        IServiceProvider CreateServices()
        {
            return new ServiceCollection()
                .AddFluentMigratorCore()
                .ConfigureRunner(CreateMigrationRunner)
#if DEBUG
                .Configure<RunnerOptions>(cfg => cfg.Profile = "Development")
#endif
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                .BuildServiceProvider(false);
        }

        void UpdateDatabase(IServiceProvider serviceProvider)
        {
            // Instantiate the runner
            var runner = serviceProvider.GetRequiredService<IMigrationRunner>();

            // Execute the migrations
            runner.MigrateUp();
        }

        void CreateMigrationRunner(IMigrationRunnerBuilder runnerBuilder)
        {
            switch (_dbType)
            {
                case BonMonitor.Shared.Database.DbType.PostGres:
                    runnerBuilder.AddPostgres92();
                    break;
                case BonMonitor.Shared.Database.DbType.SQLite:
                    runnerBuilder.AddSQLite();
                    break;
                default:
                    // case DatabaseType.MsSql:
                    runnerBuilder.AddSqlServer2016();
                    break;
            }
            runnerBuilder.WithGlobalConnectionString(_connection)
                .ScanIn(typeof(DatabaseProvider).Assembly).For.Migrations();
        }
    }
}