using FluentMigrator;

namespace BonMonitor.Kernel.Db
{
    [Migration(20211031001, "CookingTime und PreparationTime dazu")]
    public class AddCookingMigration : Migration
    {
        public override void Up()
        {
            Alter.Table("BonEntities")
                .AddColumn("CookSeconds").AsInt32().Nullable()
                .AddColumn("PrepSeconds").AsInt32().Nullable();
        }

        public override void Down()
        {
            Delete.Column("CookSeconds").FromTable("BonEntities");
            Delete.Column("PrepSeconds").FromTable("BonEntities");
        }
    }
}