using FluentMigrator;

namespace BonMonitor.Kernel.Db
{
    [Migration(20220216001, "Add index to database")]
    public class AddIndexMigration : Migration
    {
        public override void Up()
        {
            Create.Index()
                .OnTable("Bons")
                .OnColumn("Id").Unique();
            Create.Index()
                .OnTable("Bons")
                .OnColumn("IsArchived").Ascending();
            Create.Index()
                .OnTable("Bons")
                .OnColumn("TableGuid").Ascending();
            Create.Index()
                .OnTable("BonEntities")
                .OnColumn("Id").Unique();
            Create.Index()
                .OnTable("BonEntities")
                .OnColumn("IsArchived").Ascending();
            //.WithOptions()
            //.Online()
            //.NonClustered();
        }

        public override void Down()
        {
        }
    }
}