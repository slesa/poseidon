module Shared.Helpers
open System

let translatePath (path: string) =
    if path.StartsWith("~") then
        let home = System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)
        home + path.Substring(1)
    else
        path



module Route =
    let builder typeName methodName =
        sprintf "/api/%s/%s" typeName methodName

