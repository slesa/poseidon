module Shared.IniReader
open System
open System.IO
open IniFile

let loadIni content =
    Ini.Load content

let loadIniFile fn =
    let content = if File.Exists fn then File.ReadAllText fn else ""
    loadIni content

let readIniAttr (ini: Ini) (section: string) (key: string) defValue =
    let sect = ini.[section]
    if sect=null then defValue
    else
        let value = sect.[key]
        if value.IsEmpty() then defValue
        else
            value.ToString()

let readIniSection (ini: Ini) (section: string) (key: string) =
    let sect = ini.[section]
    //sect.Items |> List.ofSeq |> List.find (fun x -> x=key) |> List.map(fun x -> x.Value)
    [ if sect<>null then
        for entry in sect do
            if entry.Name=key then
                yield entry.Value.ToString() ]

let readIniStr (ini: Ini) (section: string) (key: string) defValue =
    let result = readIniAttr ini section key defValue
    result.ToString().Trim()

let readIniVal<'t> (ini: Ini) (section: string) (key: string) (defValue: 't) (convert: string -> 't) =
    let result = readIniStr ini section key ""
    if String.IsNullOrEmpty(result) then defValue
    else
        try
            convert result
        with
        | _ -> defValue