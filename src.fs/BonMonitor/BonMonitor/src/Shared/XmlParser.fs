module Shared.XmlParser
open System.Collections.Generic
open System
open System.Xml.Linq

let xn s = XName.Get s

let toBool str =
    match str with
    | "yes" -> true
    | "no" -> false
    | "ja" -> true
    | "nein" -> false
    | "1" -> true
    | "0" -> false
    | _ -> false

let readNode (doc: XElement) (nodeName: string) =
    doc.Descendants(xn nodeName) |> Seq.head

let readNodes (doc: XElement) (nodeName: string) =
    doc.Descendants(xn nodeName)


let readValue (nodes: XElement) attrName =
    try
        let node = nodes.Descendants() |> Seq.find(fun x -> x.Name.LocalName=attrName)
        Some(node.Value)
    with
    | :? KeyNotFoundException ->
        None

let readDoubleAsInt (nodes: XElement) attrName =
    match readValue nodes attrName with
    | Some x -> x |> double |> Math.Ceiling |> int
    | None -> 0

let readInt (nodes: XElement) attrName =
    match readValue nodes attrName with
    | Some x -> x |> int
    | None -> 0

let readBool (nodes: XElement) attrName =
    match readValue nodes attrName with
    | Some x -> x |> toBool
    | None -> false

let readStr (nodes: XElement) attrName =
    match readValue nodes attrName with
    | Some x -> x
    | None -> ""

let readGuid (nodes: XElement) attrName =
    match readValue nodes attrName with
    | Some x -> x |> Guid.Parse
    | None -> Guid.Empty

let readTimeSpan (nodes: XElement) attrName =
    match readValue nodes attrName with
    | Some x -> x |> TimeSpan.Parse
    | None -> TimeSpan.Zero



let parseNode (doc:XDocument) nodeName func =
    //let root = doc.Root
    doc.Descendants(xn nodeName) |> Seq.iter func

let openXml (path: string) =
    let doc = XDocument.Load path
    doc
    (*
    //doc.Descendants()
    let root = doc.Root
    let header = root.Descendants(xn "tableheader") |> Seq.iter(
        fun x -> match x.Name.LocalName with
            | "origintableguid" -> x.Value
    )

    printfn $"Header: {header.ToString()}"
    let create = root.Descendants(xn "tablecreate").Nodes() |> Seq.head
    printfn $"Create: {create.ToString()}"
    //header.
    *)