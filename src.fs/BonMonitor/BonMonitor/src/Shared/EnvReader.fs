module Shared.EnvReader
open System

let readEnvStr (key: string) defValue =
    let value = Environment.GetEnvironmentVariable(key)
    if String.IsNullOrEmpty(value) then defValue
    else value

let readEnvVal<'t> (key: string) (defValue: 't) (convert: string -> 't) =
    let result = readEnvStr key ""
    if String.IsNullOrEmpty(result) then defValue
    else
        try
            convert result
        with
        | _ -> defValue