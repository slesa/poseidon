module TextMaker.Kernel.Core.Settings

let envKey (str: string) = $"TM_{str.ToUpper()}"

[<Literal>]
let SectTextMaker = "TextMaker"
[<Literal>]
let EntryTemplatePath = "Layouts"

type TextMakerSettings = {
    TemplatePath: string
}

let defaultTextMakerSettings = {
    TemplatePath = "etc/layouts"
}
