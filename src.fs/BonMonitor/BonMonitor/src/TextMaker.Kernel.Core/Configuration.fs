module TextMaker.Kernel.Core.Configuration
open System
open TextMaker.Kernel.Core.Settings
open Shared
open Shared.Helpers
open Argu

type Arguments =
    | [<AltCommandLine("-t")>] [<EqualsAssignmentOrSpaced>] [<Unique>] TemplatePath of path:string

    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | TemplatePath _ -> "specify the path where to find layout files"

let readTextMakerEnv (settings: TextMakerSettings) =
    let path = EnvReader.readEnvStr (envKey EntryTemplatePath) settings.TemplatePath
    { settings with TemplatePath = translatePath path }

let readTextMakerIni ini (settings: TextMakerSettings) =
    let path = IniReader.readIniStr ini SectTextMaker EntryTemplatePath settings.TemplatePath
    { settings with TemplatePath = translatePath path }

let readTextMakerArgs args (settings: TextMakerSettings) =

    let readArgs (args: ParseResults<Arguments>) (settings: TextMakerSettings) =
        let path = args.GetResult (TemplatePath, defaultValue=settings.TemplatePath)
        { settings with TemplatePath = translatePath path }

    let errorHandler = ProcessExiter(colorizer = function ErrorCode.HelpText -> None | _ -> Some ConsoleColor.Red)
    let parser = ArgumentParser.Create<Arguments>(programName = "TextMaker", errorHandler = errorHandler)
    let results = parser.Parse args

    let all = results.GetAllResults() // [ Detach ; Listener ("localhost", 8080) ]
    printfn "Got arguments %A" <| results.GetAllResults()

    readArgs results settings
