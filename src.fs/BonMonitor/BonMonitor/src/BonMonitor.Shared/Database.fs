module BonMonitor.Shared.Database

type DbType =
    | MySQL = 1
    | MSSql = 2
    | SQLite = 3
    | PostGres = 4

