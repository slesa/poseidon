module BonMonitor.Shared.Domain
open System

type MemoType =
| Hint = 1
| Memo = 2
| Nachbuchung = 3
| FromSplit = 4
| Modifier = 11
| Discount = 12
| Void = 15

type OrderMemo = {
    Type: MemoType
    MemoGuid: Guid option
    Name: string
    Count: int
    Price: int64
}

type BonOrder = {
    Id: int
}

type Bon = {
    Id: int
    IsArchived: bool
    // Header
    Table: int
    Party: int
    TableGuid: Guid
    TableName: string
    // Create
    CreationDateTime: DateTime
    Waiter: int
    WaiterName: string
    GuestPager: int
    WaiterPager: int
    // Zuordnung eines Bons bei mehreren Restaurants pro BM
    CostCenter: int
    CenterName: string

    Orders: BonOrder list
}

type EntityTypes =
| Transfer = 2
| Order = 3
| Void = 4
| Split = 6
| CourseDef = 9

type TableHeader = {
    Party: int // [XmlElement("party")]
    Table: int // [XmlElement("table")]
    TableGuid: Guid // [XmlElement("origintableguid")]
    GuestPager: int // [XmlElement("pos_pager")]
    WaiterPager: int // [XmlElement("pos_wpager")]
    VipGuest: int // [XmlElement("vipguest")]
}

type TableCreate = {
    TableName: string // [XmlElement("tablename")]
    CostCenter: int // [XmlElement("center")]
    CenterName: string // [XmlElement("centername")]
    CreationDateTime: DateTime
    // CreationDate: string // [XmlElement("date")]
    // CreationTime: string // [XmlElement("time")]
    Waiter: int // [XmlElement("orgwaiter")]
    WaiterName: string // [XmlElement("orgwaitername")]
}

type TableOrder = {
    OrderId: int
    IsArchived: bool
    //long ImportedAtTicks { get; set; }
    BonId: int
        // Entry
    //OrderedDateTime: DateTime
    OrderGuid: Guid
    Count: int
    Plu: int
    Article: string
    IsHint: bool
    Family: int
    FamilyName: string
    FamilyGroup: int
    FamilyGroupName: string
    Round: int
    CourseControl: int
    BonMonitors: string
    Waiter: int
    WaiterName: string
    Price: int
    ParentGuid: Guid
    SplittedFrom: Guid option
    CookSeconds: int
    PrepSeconds: int
    Memos: OrderMemo list
}