module ServiceDaemon.Configuration
open ServiceDaemon.Settings
open ServiceDaemon.Core.Configuration
open BonMonitor.Kernel.Core.Configuration
open TextMaker.Kernel.Core.Configuration
open Shared

let readEnvConfig (settings: Settings) =
    { settings with
        ServiceSettings = readServiceEnv settings.ServiceSettings
        BonMonitorSettings = readBonMonitorEnv settings.BonMonitorSettings
        TextMakerSettings = readTextMakerEnv settings.TextMakerSettings }

let readIniConfig (settings: Settings) =
    let ini = IniReader.loadIniFile "etc/settings.ini"
    { settings with
        ServiceSettings = readServiceIni ini settings.ServiceSettings
        BonMonitorSettings = readBonMonitorIni ini settings.BonMonitorSettings
        TextMakerSettings = readTextMakerIni ini settings.TextMakerSettings }

let readArgsConfig args (settings: Settings) =
    { settings with
        ServiceSettings = readServiceArgs args settings.ServiceSettings
        BonMonitorSettings = readBonMonitorArgs args settings.BonMonitorSettings
        TextMakerSettings = readTextMakerArgs args settings.TextMakerSettings }

let readConfig args : Settings =
    defaultSettings
    |> readEnvConfig
    |> readIniConfig
    |> readArgsConfig args
