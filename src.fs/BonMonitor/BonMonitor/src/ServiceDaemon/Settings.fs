module ServiceDaemon.Settings
open ServiceDaemon.Core.Settings
open BonMonitor.Kernel.Core.Settings
open TextMaker.Kernel.Core.Settings

type Settings = {
    ServiceSettings: ServiceSettings
    BonMonitorSettings: BonMonitorSettings
    TextMakerSettings: TextMakerSettings
}

let defaultSettings: Settings = {
    ServiceSettings = defaultServiceSettings
    BonMonitorSettings = defaultBonMonitorSettings
    TextMakerSettings = defaultTextMakerSettings
}
