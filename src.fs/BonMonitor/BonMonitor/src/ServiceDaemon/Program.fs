module ServiceDaemon.Kernel
open Shared
open System
open System.Runtime.InteropServices
open NLog.Config
open NLog.FSharp
open Fable.Remoting.Server
open Fable.Remoting.Giraffe
open Saturn
open ServiceDaemon.Core.Configuration
open ServiceDaemon.Core.Settings

let getLogConfigFile =
    let template = "etc/nlog.template.config"
    let config = "etc/nlog.config"
    let fn =
        if System.IO.File.Exists config then config
        else template
    fn

let startService webApp (settings: ServiceSettings) =
    let app =
        application {
            url $"%s{settings.Host}:%d{settings.RestPort}"
            use_router webApp
            memory_cache
            use_static "public"
            use_gzip
        }
    run app

let webApp =
    Remoting.createApi ()
    |> Remoting.withRouteBuilder Route.builder
    //|> Remoting.fromValue todosApi
    |> Remoting.buildHttpHandler

open Topshelf

[<EntryPoint>]
let main argv =

    let version = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString()
    let cwd = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)
    System.IO.Directory.SetCurrentDirectory(cwd)

    let logCfg = getLogConfigFile
    NLog.LogManager.Configuration <- (NLog.Config.XmlLoggingConfiguration(logCfg) :> LoggingConfiguration)
    let log = Logger()
    log.Info $"Starting service daemon %s{version} in %s{cwd}"

    let settings = ServiceDaemon.Configuration.readConfig argv
    let isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
    if isWindows then
        let startIt hc =
            log.Info "Starting service daemon..."

            startService webApp settings.ServiceSettings
            Threading.ThreadPool.QueueUserWorkItem(fun cb ->
                //sleep (s 3)
                log.Info "Requesting stop..."
                hc |> HostControl.stop) |> ignore
            true

        let stopIt hc =
            log.Info "Service daemon stopped"
            true

//        HostFactory.New(fun x -> x.UseNLog() ) |> ignore
        Service.Default
        |> service_name "MatrixServiceDaemon"
        |> description "Dienst zum einlesen von Bons der MatrixPosKasse und bereitstellen der Daten für das Frontent BonMonitorClient"
        |> display_name "Matrix Daemon Service"
        |> add_command_line_definition "h" (fun _ -> ())
        |> add_command_line_definition "host" (fun _ -> ())
        |> add_command_line_definition "p" (fun _ -> ())
        |> add_command_line_definition "port" (fun _ -> ())
        |> add_command_line_definition "d" (fun _ -> ())
        |> add_command_line_definition "dbtype" (fun _ -> ())
        |> add_command_line_definition "c" (fun _ -> ())
        |> add_command_line_definition "connection" (fun _ -> ())
        |> add_command_line_definition "i" (fun _ -> ())
        |> add_command_line_definition "input" (fun _ -> ())
        |> add_command_line_definition "e" (fun _ -> ())
        |> add_command_line_definition "exclude" (fun _ -> ())
        |> add_command_line_definition "t" (fun _ -> ())
        |> add_command_line_definition "articletext" (fun _ -> ())
        |> with_start startIt
        |> with_stop stopIt
        // |> with_recovery (defaultService.)
        //|> use_host_builder (startService settings)
        |> run

    else
        startService webApp settings.ServiceSettings
        0

