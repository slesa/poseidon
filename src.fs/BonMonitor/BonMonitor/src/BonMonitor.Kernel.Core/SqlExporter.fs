module BonMonitor.Kernel.Core.SqlExporter
open BonMonitor.Shared.Database
open BonMonitor.Shared.Domain
open BonMonitor.Kernel.Db
open System.Data.SQLite
open Dapper
open Npgsql

let identitySelector (settings: Settings.DbSettings) =
    if settings.DbType=DbType.SQLite then "; SELECT last_insert_rowid()"
    else " SELECT SCOPE_IDENTITY()"

let getConnection(settings: Settings.DbSettings) =
    let connStr = settings.Connection
    let conn = new SQLiteConnection(connStr)
    //let conn = new NpgsqlConnection("Server=localhost;Port=5432;User id=postgres;Password=postgres;Database=bonmonitor")
    conn.Open()
    conn

let addBon (settings: Settings.DbSettings) (header: TableHeader) (create: TableCreate) (orders: TableOrder seq) (bonId: int option) =
   let sqlBon =
       "INSERT INTO Bons(IsArchived, \"Table\", Party, TableGuid, TableName, CreationDateTime, Waiter, WaiterName, CostCenter, CenterName, GuestPager, WaiterPager) " +
       "values(@IsArchived, @Table, @Party, @TableGuid, @TableName, @CreationDateTime, @Waiter, @WaiterName, @CostCenter, @CenterName, @GuestPager, @WaiterPager) " +
       identitySelector settings
   let sqlOrder =
//       "INSERT INTO BonEntities(IsArchived, ImportedAtTicks, BonId, OrderedDateTime, EntryGuid, Article, " +
       "INSERT INTO BonEntities(IsArchived, BonId, EntryGuid, Article, " +
       "Plu, \"Count\", IsHint, Family, FamilyName, FamilyGroup, FamilyGroupName, " +
       "Price, ParentGuid, " +
       "\"Round\", CourseControl, BonMonitors, Waiter, WaiterName, Memos, " +
       "CookSeconds, PrepSeconds, SplittedFrom )"+
       "values(@IsArchived, @ImportedAtTicks, @BonId, @OrderedDateTime, @EntryGuid, @Article, " +
       "@Plu, @Count, @IsHint, @Family, @FamilyName, @FamilyGroup, @FamilyGroupName, " +
       "@Price, @ParentGuid, " +
       "@Round, @CourseControl, @BonMonitors, @Waiter, @WaiterName, @Memos, " +
       "@CookSeconds, @PrepSeconds, @SplittedFrom) " +
       identitySelector settings
   use conn = getConnection(settings)
   let bonId =
        match bonId with
        | Some x -> x
        | None ->
            let x = conn.Execute(sqlBon,
               {|  IsArchived = false; Table = header.Table; Party = header.Party; TableGuid = header.TableGuid;
                   TableName = create.TableName; CreationDateTime = create.CreationDateTime; Waiter = create.Waiter; WaiterName=create.WaiterName;
                   Center = create.CostCenter; CenterName = create.CenterName;
                   GuestPager = header.GuestPager; WaiterPager = header.WaiterPager |})
            x
   orders |> Seq.iter (
       fun order -> conn.Execute(sqlOrder,
                {| IsArchived = false; BonId = bonId; EntryGuid = order.OrderGuid; Article = order.Article;
                   Plu = order.Plu; Count = order.Count; IsHint = order.IsHint; Family = order.Family; FamilyName = order.FamilyName; FamilyGroup = order.FamilyGroup; FamilyGroupName = order.FamilyGroupName
                   Price = order.Price; ParentGuid = order.ParentGuid
                   Round = order.Round; CourseControl = order.CourseControl; BonMonitors = order.BonMonitors; Waiter = order.Waiter; WaiterName = order.WaiterName; Memos = order.Memos
                   CookSeconds = order.CookSeconds; PrepSeconds = order.PrepSeconds; SplittetFrom = order.SplittedFrom |})
                    |> ignore
       )

let init (settings: Settings.DbSettings) =
    if settings.DbType<>DbType.SQLite then
        use conn = getConnection settings
        conn.Execute("CREATE DATABASE BonMonitor") |> ignore
    let db = DatabaseProvider(settings.DbType, settings.Connection)
    db.Start();