module BonMonitor.Kernel.Core.Importer

open System.IO
open BonMonitor.Kernel.Core
open BonMonitor.Kernel.Core.Settings
open NLog.FSharp

let checkPath path =
    if not (Directory.Exists path) then
        Directory.CreateDirectory(path) |> ignore
    path

let importJob settings (di: DirectoryInfo) (job: FileSystemEventArgs) =
    let doneFolder = "done"
    let errorFolder = "error"

    let moveJob fn folder =
        let newDn = checkPath (Path.Combine(di.FullName, folder))
        let newFn = Path.Combine(newDn, job.Name)
        if File.Exists(newFn) then File.Delete newFn
        File.Move(fn, newFn)
        newDn

    let fn = Path.Combine(di.FullName, job.Name)
    try
        BonImporter.importBon settings fn
        moveJob fn doneFolder |> ignore
    with
        | ex ->
            let newDn = moveJob fn errorFolder
            let oldFi = Path.GetFileNameWithoutExtension(fn)
            let errFn = Path.Combine(newDn, oldFi+".err")
            File.WriteAllText(errFn, ex.ToString())

let startWatchers (log: ILogger) (settings: BonMonitorSettings) path =

    let di = DirectoryInfo(path)
    log.Info $"Looking for bons in '{di.FullName}'"

    let xmlWatcher = new FileSystemWatcher()
    xmlWatcher.Path <- checkPath path // I'm actually using a variable here
    xmlWatcher.Filter <- "*.xml"
    //watcher.NotifyFilter <- watcher.NotifyFilter ||| NotifyFilters.LastWrite
    xmlWatcher.EnableRaisingEvents <- true
    //watcher.IncludeSubdirectories <- false
    xmlWatcher.Created.Add(importJob settings di)

    let tmpWatcher = new FileSystemWatcher()
    tmpWatcher.Path <- checkPath path // I'm actually using a variable here
    tmpWatcher.Filter <- "*.tmp"
    tmpWatcher.EnableRaisingEvents <- true
    //watcher.Changed.Add(fun _ -> printfn "changed")
    //watcher.Deleted.Add(fun _ -> printfn "deleted")
    tmpWatcher.Renamed.Add(importJob settings di)

    (xmlWatcher, tmpWatcher)

let rec startForPaths log (settings: BonMonitorSettings) paths =
    match paths with
    | [] -> []
    | [x] ->
        let xml, tmp = startWatchers log settings x
        [xml; tmp]
    | head :: tail ->
        let xml, tmp = startWatchers log settings head
        xml :: tmp :: startForPaths log settings tail

let mutable _watchers : FileSystemWatcher list = []

let startImport log (settings: BonMonitorSettings) =
    SqlExporter.init settings.Db
    System.Diagnostics.Debug.WriteLine $"Pwd {Directory.GetCurrentDirectory()}"
    _watchers <- startForPaths log settings settings.Reader.SpoolPaths

let stopImport =
    _watchers
    |> List.iter(fun watcher ->
            watcher.EnableRaisingEvents <- false
            //watcher.Created.(importBon)
            //watcher.Renamed.RemoveHandler()
            watcher.Dispose()
        )
    _watchers <- []
