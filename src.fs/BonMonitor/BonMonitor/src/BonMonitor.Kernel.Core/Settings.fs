﻿//[<RequireQualifiedAccess>]
module BonMonitor.Kernel.Core.Settings
open BonMonitor.Shared.Database
open System.Runtime.InteropServices
open Microsoft.FSharp.Core

let envKey (str: string) =
    $"BM_{str.ToUpper()}"

[<Literal>]
let SectServer = "Server"
[<Literal>]
let EntryHost = "Host"
[<Literal>]
let EntryRestPort = "RestPort"

type ServerSettings = {
    Host: string
    RestPort: int
}

let defaultServerSettings = {
    Host = "http://localhost"
    RestPort = 8282
}

let parseDbType (str: string) =
    let cmp = str.ToLower()
    match cmp with
    | "mysql" -> DbType.MySQL
    | "mssql" -> DbType.MSSql
    | "sqlite" -> DbType.SQLite
    | "postgres" -> DbType.PostGres
    | _ -> DbType.SQLite



[<Literal>]
let SectDatabase = "Database"
[<Literal>]
let EntryDbType = "Type"
[<Literal>]
let EntryConnection = "Connection"

type DbSettings = {
    DbType: DbType
    Connection: string
}

let defaultDbSettings = {
    #if DEBUG
    DbType = DbType.SQLite
    #else
    DbType = DbType.MSSql
    #endif
    Connection="Data Source = ./BonMonitor.sqlite; Version = 3;"
}

[<Literal>]
let SectInput = "InputPaths"
[<Literal>]
let EntryPath = "Path"
[<Literal>]
let SectImport = "BonImport"
[<Literal>]
let EntryExcludes = "Excludes"
[<Literal>]
let SectMapping = "Mapping"
[<Literal>]
let EntryArticleText = "ArticleText"

[<Literal>]
let DefaultWinSpoolpath =
    #if DEBUG
    "var"
    #else
    @"C:\Matrix\MatrixTransfer\BonMonitor"
    #endif
[<Literal>]
let DefaultLinSpoolpath =
    #if DEBUG
    "var"
    #else
    @"~/.bonmonitor/var/matrixtransfer/bonmonitor";
    #endif

type ReaderSettings = {
    SpoolPaths: string list
    Excludes: int list
    ArticleText: string
}

let defaultReaderSettings = {
    SpoolPaths = [if RuntimeInformation.IsOSPlatform(OSPlatform.Windows) then DefaultWinSpoolpath else DefaultLinSpoolpath]
    Excludes = []
    ArticleText = ""
}

type BonMonitorSettings = {
    Db: DbSettings
    Server: ServerSettings
    Reader: ReaderSettings
}

let defaultBonMonitorSettings = {
    Db = defaultDbSettings
    Server = defaultServerSettings
    Reader = defaultReaderSettings
}
