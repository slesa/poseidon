﻿module BonMonitor.Kernel.Core.Configuration
open BonMonitor.Kernel.Core.Settings
open BonMonitor.Shared.Database
open Argu
open System
open Shared
open Shared.Helpers

type Arguments =
    | [<AltCommandLine("-h")>] [<EqualsAssignmentOrSpaced>] [<Unique>] Host of host:string
    | [<AltCommandLine("-p")>] [<EqualsAssignmentOrSpaced>] [<Unique>] Port of port:int
    | [<AltCommandLine("-d")>] [<EqualsAssignmentOrSpaced>] [<Unique>] DbType of dbtype:DbType
    | [<AltCommandLine("-c")>] [<EqualsAssignmentOrSpaced>] [<Unique>] Connection of connection:string
    | [<AltCommandLine("-i")>] [<EqualsAssignmentOrSpaced>] [<Unique>] Input of paths:string
    //| [<AltCommandLine("-e")>] [<EqualsAssignmentOrSpaced>] [<Unique>] Exclude of excludes:int list
    | [<AltCommandLine("-t")>] [<EqualsAssignmentOrSpaced>] [<Unique>] ArticleText of text:string


    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | DbType _ -> "Type of the database <MSSQL | MySql | Postgres | SQLite>"
            | Connection _ -> "specify the connection string to access the database"
            | Host _ -> "specify the interface to listen on, e.g. http://192.168.1.1"
            | Port _ -> "Specify the port to listen on"
            | Input _ -> "Specify a list of paths to look for input files"
            //| Exclude _ -> "Specify a list of monitor IDs to exclude for this service"
            | ArticleText _ -> "Specify which text to use as article text, 'short' or 'print'"


let readBonMonitorEnv (settings: BonMonitorSettings) =

    let readDbEnv (settings: DbSettings) =
        let dbTypeStr = EnvReader.readEnvStr (envKey EntryDbType) (settings.DbType.ToString())
        let dbType = parseDbType dbTypeStr
        let connection  = EnvReader.readEnvStr (envKey EntryConnection) settings.Connection
        { settings with DbType = dbType; Connection = connection }

    let readServerEnv (settings: ServerSettings) =
        let host = EnvReader.readEnvStr (envKey EntryHost) settings.Host
        let port = EnvReader.readEnvVal (envKey EntryRestPort) settings.RestPort int
        //let port = portStr |> int
        { settings with Host = host; RestPort = port }

    let readReaderEnv (settings: ReaderSettings) =
        let path = EnvReader.readEnvStr (envKey EntryPath) ""
        let settings =
            if String.IsNullOrEmpty(path) then settings
            else { settings with SpoolPaths = [translatePath path] }
        let excludeStr: string = EnvReader.readEnvStr (envKey EntryExcludes) ""
        let settings =
            if String.IsNullOrEmpty(excludeStr) then settings
            else
                let excludes = excludeStr.Split(',')
                               |> List.ofSeq
                               |> List.map (fun x -> int x)
                match excludes with
                     | [] -> settings
                     | list -> { settings with Excludes = list }
        let articleText = EnvReader.readEnvStr (envKey EntryArticleText) settings.ArticleText
        { settings with ArticleText = articleText }

    { settings with
        Db = readDbEnv settings.Db
        Server = readServerEnv settings.Server
        Reader = readReaderEnv settings.Reader }


let readBonMonitorIni ini (settings: BonMonitorSettings) =

    let readDbIni ini (settings: Settings.DbSettings) =
        let dbTypeStr = IniReader.readIniStr ini SectDatabase EntryDbType (settings.DbType.ToString())
        let dbType = parseDbType dbTypeStr
        let connection  = IniReader.readIniStr ini SectDatabase EntryConnection settings.Connection
        { settings with DbType = dbType; Connection = connection }

    let readServerIni ini (settings: ServerSettings) =
        let host = IniReader.readIniStr ini SectServer EntryHost settings.Host
        let port = IniReader.readIniVal ini SectServer EntryRestPort settings.RestPort int
        //let port = portStr |> int
        { settings with Host = host; RestPort = port }

    let readReaderIni ini (settings: ReaderSettings) =
        let paths = IniReader.readIniSection ini SectInput EntryPath
                    |> List.map(fun x -> translatePath x)
        let settings = match paths with
                       | [] -> settings
                       | list -> { settings with SpoolPaths = list }

        let excludeStr: string = IniReader.readIniStr ini SectImport EntryExcludes ""
        let settings =
            if String.IsNullOrEmpty(excludeStr) then settings
            else
                let excludes = excludeStr.Split(',')
                               |> List.ofSeq
                               |> List.map (fun x -> int x)
                match excludes with
                      | [] -> settings
                      | list -> { settings with Excludes = list }
        let articleText = IniReader.readIniStr ini SectMapping EntryArticleText settings.ArticleText
        { settings with ArticleText = articleText }

    { settings with
        Db = readDbIni ini settings.Db
        Server = readServerIni ini settings.Server
        Reader = readReaderIni ini settings.Reader }



let readBonMonitorArgs args (settings: BonMonitorSettings) =

    let readDbArgs (args: ParseResults<Arguments>) (settings: DbSettings) =
        let dbType = args.GetResult (DbType, defaultValue=settings.DbType)
        let connection = args.GetResult (Connection, defaultValue=settings.Connection)
        { settings with DbType = dbType; Connection = connection }

    let readServerArgs (args: ParseResults<Arguments>) (settings: ServerSettings) =
        let host = args.GetResult (Host, defaultValue=settings.Host)
        let port = args.GetResult (Port, defaultValue=settings.RestPort)
        { settings with Host = host; RestPort = port }

    let readReaderArgs (args: ParseResults<Arguments>) (settings: ReaderSettings) =
        let path = args.GetResult (Input, defaultValue="")
        let settings =
            if String.IsNullOrEmpty(path) then settings
            else { settings with SpoolPaths = [translatePath path] }
        //let excludes = args.GetResult Exclude
        //let settings = match excludes with
        //               | [] -> settings
        //               | list -> { settings with Excludes = list }
        let articleText = args.GetResult (ArticleText, defaultValue=settings.ArticleText)
        { settings with ArticleText = articleText }

    let errorHandler = ProcessExiter(colorizer = function ErrorCode.HelpText -> None | _ -> Some ConsoleColor.Red)

    //let reader = EnvironmentVariableConfigurationReader() :> IConfigurationReader
    //let parser =  ArgumentParser.Create<Args>(programName = "rutta")
// pass the reader to the Parse call
    //let results = parser.Parse(argv, configurationReader=reader)

    let parser = ArgumentParser.Create<Arguments>(programName = "BonMonitor", errorHandler = errorHandler)
    let results = parser.Parse args

    let all = results.GetAllResults() // [ Detach ; Listener ("localhost", 8080) ]
    printfn "Got arguments %A" <| results.GetAllResults()

    { settings with
        Db = readDbArgs results settings.Db
        Server = readServerArgs results settings.Server
        Reader = readReaderArgs results settings.Reader }
