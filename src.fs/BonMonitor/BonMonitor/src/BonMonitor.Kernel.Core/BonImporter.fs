﻿module BonMonitor.Kernel.Core.BonImporter
open BonMonitor.Kernel.Core.Settings
open BonMonitor.Shared.Domain
open System
open System.Xml.Linq
open Shared

//type BonProvider = XmlProvider<"jobs/bon.xml">
//type BonOrderProvider = XmlProvider<"jobs/bon_order.xml">

let convertDate dateStr timeStr =
    let date = DateTime.Parse dateStr
    let time = DateTime.Parse timeStr
    DateTime(date.Year, date.Month, date.Day, time.Hour, time.Minute, time.Second)

let importBon (settings: BonMonitorSettings) (fn: string) =

    let importHeader (doc: XDocument): TableHeader =
        let node = XmlParser.readNode doc.Root "tableheader"
        { Table = XmlParser.readInt node "table"
          Party = XmlParser.readInt node "party"
          TableGuid = XmlParser.readGuid node "origintableguid"
          GuestPager = XmlParser.readInt node "pos_pager"
          WaiterPager = XmlParser.readInt node "pos_wpager"
          VipGuest = XmlParser.readInt node "vipguest"
        }
    let importCreate (doc: XDocument): TableCreate =
        let node = XmlParser.readNode doc.Root "tablecreate"
        {
            TableName = XmlParser.readStr node "tablename"
            CostCenter = XmlParser.readInt node "center"
            CenterName = XmlParser.readStr node "centername"
            CreationDateTime = convertDate (XmlParser.readStr node "date") (XmlParser.readStr node "time")
            Waiter = XmlParser.readInt node "waiter"
            WaiterName = XmlParser.readStr node "waitername"
        }

    let readArticle (settings: ReaderSettings)  (node: XElement) =
        match settings.ArticleText with
        | "print" -> XmlParser.readStr node "artprint"
        | "short" -> XmlParser.readStr node "artshort"
        | _ -> XmlParser.readStr node "article"

    let importOrder (settings: ReaderSettings) (node: XElement): TableOrder =
        let inline asGuid attr = XmlParser.readGuid node attr
        let inline asInt attr = XmlParser.readInt node attr
        let inline asStr attr = XmlParser.readStr node attr
        let inline asTimeSpan attr = XmlParser.readTimeSpan node attr
//        let inline (=>) a b c = a, b node c
        let memo = asStr "memo"
        {
            OrderId = 0
            BonId = 0
            IsArchived = false
            OrderGuid = asGuid "guid"
            Count = XmlParser.readDoubleAsInt node "count"
            Plu = asInt "plu"
            Article = readArticle settings node
            IsHint = XmlParser.readBool node "ishint"
            Family = asInt "family"
            FamilyName = asStr "familyname"
            FamilyGroup = asInt "famgroup"
            FamilyGroupName = asStr "famgrpname"
            Round = asInt "round"
            CourseControl = asInt "ccontrol"
            BonMonitors = asStr "bonmonitors"
            Waiter = asInt "waiter"
            WaiterName = asStr "waitername"
            Price = asInt "price"
            ParentGuid = asGuid "parentorderguid"
            SplittedFrom = None //asGuid node "tableguid"
            CookSeconds = (asTimeSpan "cookingtime").TotalSeconds |> int
            PrepSeconds = (asTimeSpan "preparationtime").TotalSeconds |> int
            Memos =
                if String.IsNullOrEmpty(memo) then
                    []
                else
                    [{ Type = MemoType.Memo; MemoGuid = None; Name = memo; Count = 0; Price = 0L }]
        }

    let importOrders (settings: ReaderSettings) (doc: XDocument): TableOrder seq =
        let node = XmlParser.readNode doc.Root "orders"
        let entries = XmlParser.readNodes node "tableentry"
        entries |> Seq.map (fun x -> importOrder settings x)


//    let fi = FileInfo(fn.FullPath)
//    printfn $"Found job {fi.FullName}"
    printfn $"Found job {fn}"

    let doc = XmlParser.openXml fn // fi.FullName
    //let content = File.ReadAllText fi.FullName
    //let bon = BonProvider.Parse content
    let header = importHeader doc
    let create = importCreate doc
    let orders = importOrders settings.Reader doc


    printfn $"Importing {header.Table}/{header.Party} [{header.TableGuid}]"
    printfn $"Center ({create.CostCenter}) {create.CenterName} / Waiter ({create.Waiter}) {create.WaiterName}"
    orders |> Seq.iter (fun order ->
        printfn $"{order.Count} x {order.Plu} {order.Article}")

    SqlExporter.addBon settings.Db header create orders None