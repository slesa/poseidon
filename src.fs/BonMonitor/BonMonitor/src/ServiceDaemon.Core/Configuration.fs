module ServiceDaemon.Core.Configuration
open System
open ServiceDaemon.Core.Settings
open Argu
open Shared

type Arguments =
    | [<AltCommandLine("-h")>] [<EqualsAssignmentOrSpaced>] [<Unique>] Host of host:string
    | [<AltCommandLine("-p")>] [<EqualsAssignmentOrSpaced>] [<Unique>] Port of port:int

    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | Host _ -> "specify the interface to listen on, e.g. http://192.168.1.1"
            | Port _ -> "Specify the port to listen on"

let readServiceEnv (settings: ServiceSettings) =
    let host = EnvReader.readEnvStr (envKey EntryHost) settings.Host
    let port = EnvReader.readEnvVal (envKey EntryRestPort) settings.RestPort int
    { settings with Host = host; RestPort = port }

let readServiceIni ini (settings: ServiceSettings) =
    let host = IniReader.readIniStr ini SectServer EntryHost settings.Host
    let port = IniReader.readIniVal ini SectServer EntryRestPort settings.RestPort int
    { settings with Host = host; RestPort = port }

let readServiceArgs args (settings: ServiceSettings) =

    let readArgs (args: ParseResults<Arguments>) (settings: ServiceSettings) =
        let host = args.GetResult (Host, defaultValue=settings.Host)
        let port = args.GetResult (Port, defaultValue=settings.RestPort)
        { settings with Host = host; RestPort = port }

    let errorHandler = ProcessExiter(colorizer = function ErrorCode.HelpText -> None | _ -> Some ConsoleColor.Red)
    let parser = ArgumentParser.Create<Arguments>(programName = "ServiceDaemon", errorHandler = errorHandler)
    let results = parser.Parse args

    let all = results.GetAllResults() // [ Detach ; Listener ("localhost", 8080) ]
    printfn "Got arguments %A" <| results.GetAllResults()

    readArgs results settings
