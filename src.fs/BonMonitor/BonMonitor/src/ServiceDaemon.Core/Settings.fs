module ServiceDaemon.Core.Settings

let envKey (str: string) =
    $"SD_{str.ToUpper()}"

[<Literal>]
let SectServer = "Server"
[<Literal>]
let EntryHost = "Host"
[<Literal>]
let EntryRestPort = "RestPort"

type ServiceSettings = {
    Host: string
    RestPort: int
}

let defaultServiceSettings = {
    Host = "http://localhost"
    RestPort = 8282
}
