(*
module BonMonitor.Kernel.Server

open Fable.Remoting.Server
open Fable.Remoting.Giraffe
open Saturn
open BonMonitor.Kernel.Core.Settings
open BonMonitor.Kernel.Core.Configuration
open NLog.Config
open NLog.FSharp
open System
open Shared

type Storage() =
    let todos = ResizeArray<_>()

    member __.GetTodos() = List.ofSeq todos

    member __.AddTodo(todo: Todo) =
        if Todo.isValid todo.Description then
            todos.Add todo
            Ok()
        else
            Error "Invalid todo"

let storage = Storage()

storage.AddTodo(Todo.create "Create new SAFE project")
|> ignore

storage.AddTodo(Todo.create "Write your app")
|> ignore

storage.AddTodo(Todo.create "Ship it !!!")
|> ignore

let todosApi =
    { getTodos = fun () -> async { return storage.GetTodos() }
      addTodo =
          fun todo ->
              async {
                  match storage.AddTodo todo with
                  | Ok () -> return todo
                  | Error e -> return failwith e
              } }

let webApp =
    Remoting.createApi ()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.fromValue todosApi
    |> Remoting.buildHttpHandler

let getLogConfigFile =
    let template = "etc/nlog.template.config"
    let config = "etc/nlog.config"
    let fn =
        if System.IO.File.Exists config then config
        else template
    fn

let startService settings =
    let app =
        application {
            url $"%s{settings.Server.Host}:%d{settings.Server.RestPort}"
            use_router webApp
            memory_cache
            use_static "public"
            use_gzip
        }
    run app


open Topshelf
//open Topshelf.NLogConfiguratorExtensions
//open Topshelf.FSharpApi
open System.Runtime.InteropServices
open Importer

[<EntryPoint>]
let main argv =

    let version = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString()
    let cwd = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)
    System.IO.Directory.SetCurrentDirectory(cwd)

    let logCfg = getLogConfigFile
    NLog.LogManager.Configuration <- (NLog.Config.XmlLoggingConfiguration(logCfg) :> LoggingConfiguration)
    let log = Logger()
    log.Info $"Starting BonMonitor kernel %s{version} in %s{cwd}"

    let settings = defaultSettings |> readConfig argv

    startImport log settings

    let isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
    if isWindows then
        let startIt hc =
            log.Info "Starting service..."

            startService settings
            Threading.ThreadPool.QueueUserWorkItem(fun cb ->
                //sleep (s 3)
                log.Info "Requesting stop..."
                hc |> HostControl.stop) |> ignore
            true

        let stopIt hc =
            log.Info "BonMonitor service stopped"
            true

//        HostFactory.New(fun x -> x.UseNLog() ) |> ignore
        Service.Default
        |> service_name "MatrixBonmonitorService"
        |> description "Dienst zum einlesen von Bons der MatrixPosKasse und bereitstellen der Daten für das Frontent BonMonitorClient"
        |> display_name "Matrix Bonmonitor Service"
        |> add_command_line_definition "h" (fun _ -> ())
        |> add_command_line_definition "host" (fun _ -> ())
        |> add_command_line_definition "p" (fun _ -> ())
        |> add_command_line_definition "port" (fun _ -> ())
        |> add_command_line_definition "d" (fun _ -> ())
        |> add_command_line_definition "dbtype" (fun _ -> ())
        |> add_command_line_definition "c" (fun _ -> ())
        |> add_command_line_definition "connection" (fun _ -> ())
        |> add_command_line_definition "i" (fun _ -> ())
        |> add_command_line_definition "input" (fun _ -> ())
        |> add_command_line_definition "e" (fun _ -> ())
        |> add_command_line_definition "exclude" (fun _ -> ())
        |> add_command_line_definition "t" (fun _ -> ())
        |> add_command_line_definition "articletext" (fun _ -> ())
        |> with_start startIt
        |> with_stop stopIt
        // |> with_recovery (defaultService.)
        //|> use_host_builder (startService settings)
        |> run

    else
        startService settings
        0

*)
