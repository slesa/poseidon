﻿module BonMonitor.Kernel.Importer.Tests

open BonMonitor.Kernel.Core.Importer
open BonMonitor.Kernel.Core.Settings
open BonMonitor.Shared.Database
open Expecto
open NLog.FSharp

let importer = testList "BonMonitor.Kernel.Importer" [
    let getSettings: BonMonitorSettings =
        {
            Db = {
                DbType = DbType.SQLite
                Connection = "Data Source = :Memory:"
            }
            Server = defaultServerSettings
            Reader ={
                SpoolPaths = [ "var" ]
                Excludes = []
                ArticleText = ""
            }
        }

    let createXml fn =
        let content = "<xml><root>
          <tableheader></tableheader
        </root></xml>"
        System.IO.File.WriteAllText(fn, content)

    testCase "Invalid XML file" <| fun _ ->
        let cwd = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)
        System.IO.Directory.SetCurrentDirectory(cwd)
        printfn $"Cwd: {cwd}"

        let log = Logger()
        let settings = getSettings
        startImport log settings
        createXml "var/bon.xml"
        stopImport

//        let expectedResult = Ok ()
        let xmlFn = System.IO.Path.Combine(cwd, "var/error/bon.xml")
        let result = System.IO.File.Exists(xmlFn)
        Expect.isTrue result "Bon file has been moved to error"

        let errFn = System.IO.Path.Combine(cwd, "var/error/bon.err")
        let result = System.IO.File.Exists(errFn)
        Expect.isTrue result "Error file has been created in error"
//        Expect.equal result expectedResult "Result should be ok"
//        Expect.equal result expectedResult "Result should be ok"
//        Expect.contains (storage.GetTodos()) validTodo "Storage should contain new todo"


]
