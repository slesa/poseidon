module Login

open Elmish
open Feliz


type Model =
  { Username: string
    Password: string
    LoginAttempt: Deferred<Api.LoginResult> }


type Msg =
  | UsernameChanged of string
  | PasswordChanged of string
  | Login of AsyncOperationStatus<Api.LoginResult>


let (|UserLoggedIn|_|) = function
  | Msg.Login (Finished (Api.LoginResult.LoggedIn user)) -> Some user
  | _ -> None


let init() =
  { Username = ""
    Password = ""
    LoginAttempt = HasNotStartedYet }, Cmd.none


let update (msg: Msg) (model: Model) =
  match msg with
  | UsernameChanged username ->
      { model with Username = username  }, Cmd.none

  | PasswordChanged password ->
      { model with Password = password }, Cmd.none

  | Login Started ->
      let nextModel = { model with LoginAttempt = InProgress }
      let login = async {
        let! loginResult = Api.login model.Username model.Password
        return Login (Finished loginResult)
      }

      let nextCmd = Cmd.fromAsync login
      nextModel, nextCmd

  | Login (Finished loginResult) ->
      let nextState = { model with LoginAttempt = Resolved loginResult }
      nextState, Cmd.none


let renderLoginOutcome (loginResult: Deferred<Api.LoginResult>)=
  match loginResult with
  | Resolved Api.LoginResult.UsernameOrPasswordIncorrect ->
      Html.paragraph [
        prop.style [ style.color.crimson; style.padding 10 ]
        prop.text "Username or password is incorrect"
      ]

  | Resolved (Api.LoginResult.LoggedIn user) ->
      Html.paragraph [
        prop.style [ style.color.green; style.padding 10 ]
        prop.text (sprintf "User '%s' has succesfully logged in" user.Username)
      ]

  | otherwise ->
      Html.none


let layout (children: ReactElement list) =
  Html.section [
    prop.className "hero is-fullheight"
    prop.children [
      Html.div [
        prop.className "hero-body"
        prop.children [
          Html.div [
            prop.className "container"
            prop.children [
              Html.div [
                prop.className "columns is-centered"
                prop.children [
                  Html.div [
                    prop.className "column is-6-tablet is-4-desktop is-4-widescreen"
                    prop.children children
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]
  ]


let centered (children: ReactElement list) =
  Html.div [
    prop.style [
      style.margin.auto
      style.textAlign.center
      style.width (length.percent 100)
    ]
    prop.children children
  ]


let render (model: Model) (dispatch: Msg -> unit) =
  layout [
    Html.div [
      prop.className "box"
      prop.children [

        centered [
          Html.img [
            prop.src "login.jpg" // "https://fable.io/img/fable_logo.png"
            prop.height 300
            prop.width 200
          ]
        ]

        Html.div [
          prop.className "field"
          prop.children [
            Html.label [
              prop.className "label"
              prop.text "Username"
            ]

            Html.div [
              prop.className "control has-icons-left"
              prop.children [
                Html.input [
                  prop.className "input"
                  prop.placeholder "Username"
                  prop.type'.email
                  prop.valueOrDefault model.Username
                  prop.onChange (UsernameChanged >> dispatch)
                ]

                Html.span [
                  prop.className "icon is-small is-left"
                  prop.children [
                    Html.i [ prop.className "fa fa-user" ]
                  ]
                ]
              ]
            ]
          ]
        ]

        Html.div [
          prop.className "field"
          prop.children [
            Html.label [
              prop.className "label"
              prop.text "Password"
            ]
            Html.div [
              prop.className "control has-icons-left"
              prop.children [
                Html.input [
                  prop.className "input"
                  prop.placeholder "********"
                  prop.type'.password
                  prop.valueOrDefault model.Password
                  prop.onChange (PasswordChanged >> dispatch)
                ]
                Html.span [
                  prop.className "icon is-small is-left"
                  prop.children [
                    Html.i [ prop.className "fa fa-lock" ]
                  ]
                ]
              ]
            ]
          ]
        ]

        Html.div [
          prop.className "field"
          prop.children [
            Html.button [
              prop.className [
                "button is-info is-fullwidth"
                if model.LoginAttempt = InProgress
                then "is-loading"
              ]

              prop.onClick (fun _ -> dispatch (Login Started))
              prop.text "Login"
            ]
          ]
        ]

        renderLoginOutcome model.LoginAttempt
      ]
    ]
  ]