[<RequireQualifiedAccess>]
module Home

open Elmish
open Feliz


type Model = { 
  User: Api.User
  Title: TitleView.Model
}


type Msg =
  | Logout
  | TitleMsg of TitleView.Msg


let init (user: Api.User) =
  let titleModel, titleCmd = TitleView.init()
  { User = user
    Title = titleModel
  }, Cmd.map TitleMsg titleCmd


let update (msg: Msg) (model: Model) : Model * Cmd<Msg> =
  match msg with
  | Logout -> model, Cmd.none
  | TitleMsg titleMsg ->
      let titleModel, titleCmd = TitleView.update titleMsg model.Title
      { model with Title = titleModel } , Cmd.map TitleMsg titleCmd


let centered (children: ReactElement list) =
  Html.div [
    prop.style [
      style.margin.auto
      style.textAlign.center
      style.padding 20
      style.width (length.percent 100)
    ]

    prop.children children
  ]


let render (model: Model) (dispatch: Msg -> unit) =
  Html.div [
    TitleView.view model.Title
    //centered [
      //Html.h1 [
        //Html.strong ()
      //]
    //]

    Html.button [
      prop.className "button is-info"
      prop.onClick (fun _ -> dispatch Logout)
      prop.text model.User.Username
    ]
  ]