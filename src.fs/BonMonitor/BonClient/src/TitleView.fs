module TitleView
open Elmish
open Fable.SimpleHttp
open Thoth.Json
open System


type VersionResult =
  | Error of string
  | Version of string

type Model = {
  CurrentTime: DateTime
  Version: Deferred<VersionResult>
}


type Msg =
  | Tick
  | GetVersion of AsyncOperationStatus<Result<string, string>>
  | GotVersion of VersionResult


let getversion : Async<Msg> = async {
  let endpoint = "http://localhost:8282/bm/version"
  let! (status, responseText) = Http.get endpoint

  match status with
  | 200 -> 
      match Decode.fromString Decode.string responseText with
      | Ok versionText -> return GotVersion (Version versionText)
      | _ -> return GotVersion (Error "Could not parse response")
  | _ -> 
      return GotVersion ( Error responseText)
}


let init() =
  let initialState = {
    CurrentTime = DateTime.Now
    Version = HasNotStartedYet
  }
  let initialCmd = Cmd.batch [ Cmd.ofMsg (GetVersion Started); Cmd.ofMsg Tick ]
  initialState, initialCmd


let update (msg: Msg) (model: Model) : Model * Cmd<Msg> =
  match msg with
  | Tick ->
      let nextState = { model with CurrentTime = DateTime.Now }
      let step = async {
        do! Async.Sleep 1000
        return Tick
      }
      nextState, Cmd.fromAsync step
  | GetVersion _ ->
      let nextModel = { model with Version = InProgress }
      let getVersion = async {
        let! versionResult = getversion
        return versionResult
      }

      let nextCmd = Cmd.fromAsync getVersion
      nextModel, nextCmd

  | GotVersion version ->
      let nextModel = { model with Version = Resolved (version) }
      nextModel, Cmd.none


open Feliz
open Feliz.Bulma

let titleText = "Matrix BonMonitor"

let formatTime (time: DateTime) =
  sprintf "%02d:%02d:%02d" time.Hour time.Minute time.Second

let renderError (errorMsg: string) =
  Html.div [
    Html.h1 titleText
    Html.h1 [
      prop.style [ style.color.red ]
      prop.text errorMsg
    ]
  ]

let renderVersion (version: string) =
  Html.h1 [
    // prop.style [ style.color.red ]
    prop.text (sprintf "%s v%s" titleText (version))
  ]


let getTitleText model =
  match model.Version with
   | HasNotStartedYet -> Html.none
   | InProgress -> spinner
   | Resolved (Error errorMsg) -> renderError errorMsg
   | Resolved (Version version) -> renderVersion version


let view (model: Model) = // (dispatch: Msg -> unit) =
  //Bulma.box [
    Html.div [
      getTitleText model
      Html.paragraph (formatTime model.CurrentTime)
    ]
  //]
