[<RequireQualifiedAccess>]
module App

open Elmish


[<RequireQualifiedAccess>]
type Page =
  | Login of Login.Model
  | Home of Home.Model


type Model =
  { CurrentPage: Page }


type Msg =
  | LoginMsg of Login.Msg
  | HomeMsg of Home.Msg


let init() =
  //let loginState, loginCmd = Login.init()
  //{ CurrentPage = Page.Login loginState }, Cmd.map LoginMsg loginCmd
  let accessToken = System.Guid.NewGuid().ToString()
  let homeState, homeCmd = Home.init { Username = "Harry Hirsch"; AccessToken = Api.AccessToken accessToken }
  { CurrentPage = Page.Home homeState }, Cmd.map HomeMsg homeCmd


let update (msg: Msg) (model: Model) =
  match msg, model.CurrentPage with
  | LoginMsg loginMsg, Page.Login loginState ->
    match loginMsg with
    | Login.UserLoggedIn user ->
        let homeState, homeCmd = Home.init user
        { model with CurrentPage = Page.Home homeState }, Cmd.map HomeMsg homeCmd

    | loginMsg ->
        let loginState, loginCmd = Login.update loginMsg loginState
        { model with CurrentPage = Page.Login loginState }, Cmd.map LoginMsg loginCmd

  | HomeMsg homeMsg, Page.Home homeState ->
      match homeMsg with
      | Home.Msg.Logout ->
          init()
      | homeMsg ->
          let homeState, homeCmd = Home.update homeMsg homeState
          { model with CurrentPage = Page.Home homeState }, Cmd.map HomeMsg homeCmd

  | _, _ ->
    model, Cmd.none

let render (model: Model) (dispatch: Msg -> unit) =
  match model.CurrentPage with
  | Page.Login loginState -> Login.render loginState (LoginMsg >> dispatch)
  | Page.Home homeState -> Home.render homeState (HomeMsg >> dispatch)
