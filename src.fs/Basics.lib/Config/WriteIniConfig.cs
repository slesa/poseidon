﻿using System;
using System.Globalization;
using System.Linq;
using IniFile;

namespace Basics.Config
{
    public class WriteIniConfig
    {
        readonly string _fileName;
        protected readonly Ini _ini;
        string _currentSection;
        
        protected WriteIniConfig(string fileName, IReadConfigFile configReader)
        {
            Ini.Config
                .SetPropertyPaddingDefaults(insideLeft: 1, insideRight: 1);            
            _fileName = fileName;
            var content = configReader.ReadFromFile(_fileName);
            _ini = Ini.Load(content);
        }

        protected void UseSection(string section)
        {
            _currentSection = section;
        }
        
        protected void Save(IWriteConfigFile configWriter)
        {
            if(string.IsNullOrEmpty(_fileName))
                throw new InvalidOperationException("File name must not be null or empty");
            _ini.Format(new IniFormatOptions
            {
                EnsureBlankLineBetweenSections = true,
            });            
            var content = _ini.ToString();
            configWriter.WriteToFile(_fileName, content);
        }
        
        protected void Write(string key, double value, double defaultValue)
        {
            Write(_currentSection, key, value.ToString(CultureInfo.InvariantCulture), defaultValue.ToString());
        }
        protected void Write<T>(string key, T value, T defaultValue)
        {
            Write(_currentSection, key, value?.ToString(), defaultValue.ToString());
        }
        
        protected void Write(string section, string key, double value, double defaultValue)
        {
            Write(section, key, value.ToString(CultureInfo.InvariantCulture), defaultValue.ToString());
        }
        protected void Write<T>(string section, string key, T value, T defaultValue)
        {
            Write(section, key, value?.ToString(), defaultValue.ToString());
        }

        protected void Write(string key, string value, string defaultValue)
        {
            Write(_currentSection, key, value, defaultValue);
        }
        
        protected void Write(string section, string key, string value, string defaultValue)
        {
            var sect = AddOrGetSection(section);

            if (value != null && !value.Equals(defaultValue))
            {
                sect[key] = value;
            }
            else
            {
                var prop = sect.FirstOrDefault(x => x.Name==key);
                if(prop!=null)
                    sect.Remove(prop);
            }
        }

        protected Section AddOrGetSection(string name)
        {
            var section = _ini[name];
            if (section == null)
            {
                section = new Section(name);
                _ini.Add(section);
            }

            return section;
        }
    }
}