﻿using System;
using System.Globalization;
using System.Linq;

#if NET462
using System.Windows.Media;
#endif

namespace Basics.Config
{
    public static class ConfigConverters
    {
        public static int ConvertToInt(string value)
        {
            return int.TryParse(value, out var x) ? x : 0;
        }
        
        public static double ConvertToDouble(string value)
        {
            return double.TryParse(value, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out var x) ? x : 0.0;
        }
        
        public static bool ConvertToBool(string value)
        {
            var cmp = value.Trim().ToLower(CultureInfo.InvariantCulture);
            if (cmp == "true" || cmp == "yes" || cmp == "ja" || cmp == "1")
                return true;
            if (cmp == "false" || cmp == "no" || cmp == "nein" || cmp == "0")
                return false;
            return false;
        }

#if NET462
        public static Color ConvertToColor(string paramValue)
        {
            var value = paramValue.Trim();
            int i = 0;
            byte a, r, g, b;
            a = 255;
            try
            {
                var buffer = (value.StartsWith("#")) ? value.Substring(1) : value;
                byte[] data = StringToByteArray(buffer);
                if (buffer.Length == 8)
                {
                    a = data[i++];
                }

                r = data[i++];
                g = data[i++];
                b = data[i++];
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException($"can't convert {value} to color", ex);
            }

            return Color.FromArgb(a, r, g, b);
        }
#endif    
#if csharp_version_8
        protected T ConvertToEnum<T>(string value) where T : notnull
        {
            return Enum.TryParse(value, out T x) ? x : default(T);
        }
#endif

        static byte[] StringToByteArray(string paramHex)
        {
            var hex = paramHex.Trim(); 
            return Enumerable.Range(0, hex.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                .ToArray();
        }
    }
}