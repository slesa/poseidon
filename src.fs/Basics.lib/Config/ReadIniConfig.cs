﻿using System;
using System.IO;
using IniFile;

namespace Basics.Config
{
    public class ReadIniConfig
    {
        protected Ini _ini;
        string _currentSection;
        
        protected void OpenIniFile(string fileName)
        {
            var content = File.Exists(fileName) ? File.ReadAllText(fileName) : "";
            LoadIniFile(content);
        }

        protected void LoadIniFile(string content)
        {
            _ini = Ini.Load(content);
        }

        protected void UseSection(string section)
        {
            _currentSection = section;
        }
        
        protected string Read(string key, string defaultValue = "")
        {
            return Read(_currentSection, key, defaultValue);
        }
        
        protected string Read(string section, string key, string defaultValue)
        {
            var sect = _ini[section];
            if (sect == null) return defaultValue;

            var value = sect[key];
            if (value.IsEmpty()) return defaultValue;

            return value.ToString().Trim();
        }

        protected T Read<T>(string key, T defaultValue, Func<string, T> converter)
        {
            return Read(_currentSection, key, defaultValue, converter);
        }
        
        protected T Read<T>(string section, string key, T defaultValue, Func<string, T> converter)
        {
            var sect = _ini[section];
            if (sect == null) return defaultValue;

            var value = sect[key];
            return value.IsEmpty() ? defaultValue : MayConvert(value, converter);
        }
        
        T MayConvert<T>(string value, Func<string, T> converter)
        {
            if (converter != null)
            {
                var result = converter(value.Trim());
                return result;
            }

            return default(T);
        }
        
    }
}