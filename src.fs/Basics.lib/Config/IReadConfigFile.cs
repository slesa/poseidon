﻿using System.IO;
using System.Text;

namespace Basics.Config
{
    public interface IReadConfigFile
    {
        string ReadFromFile(string fileName);
    }
    
    public class ReadConfigFile : IReadConfigFile
    {
        public string ReadFromFile(string fileName)
        {
            return File.Exists(fileName) ? File.ReadAllText(fileName, Encoding.UTF8) : string.Empty;
        }
    }
}