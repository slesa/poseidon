﻿using System.IO;
using System.Text;

namespace Basics.Config
{
    public interface IWriteConfigFile
    {
        void WriteToFile(string filename, string content);
    }
    public class ConfigFileWriter : IWriteConfigFile
    {
        public void WriteToFile(string fileName, string content)
        {
            var fi = new FileInfo(fileName);
            if (!fi.Directory.Exists)
                fi.Directory.Create();
            File.WriteAllText(fileName, content, Encoding.UTF8);
        }
    }
}