using System.Collections.Generic;

namespace PosService.Core.NetBus
{
    public interface IHandleNetCommands
    {
        bool CanHandle(string method);
        void Handle(string method, string payload);
        List<string> Topics { get; }
    }
}