﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using PosService.Core.CoreData;
using PosShare.Payloads;

namespace PosService.Core.NetBus
{
    // ReSharper disable InconsistentNaming
    public class KeylockLoginPayload
    {
        public int terminal_id { get; set; } 
        public string keylock_uuid { get; set; } 
        public string user_name { get; set; } 
        public string key_uuid { get; set; } 
        public string pos_uuid { get; set; } 
    }
    
    public class KeylockHandler : IHandleNetCommands
    {
        const string CmdLoginEvent = "login_event";
        
        readonly IProvideCoreData _dataProvider;
        readonly Environment _environment;
        
        public KeylockHandler(IProvideCoreData dataProvider, Environment environment)
        {
            _dataProvider = dataProvider;
            _environment = environment;
        }

        public bool CanHandle(string method)
        {
            return Topics.Contains(method);
        }

        public void Handle(string method, string payload)
        {
            switch (method)
            {
                case CmdLoginEvent:
                    HandleLogin(payload);
                    break;
            }
        }

        void HandleLogin(string msg)
        {
            var args = JsonConvert.DeserializeObject<KeylockLoginPayload>(msg);

            // var terminal = _environment.TerminalSessions.FindTerminal(args.terminal_id);
            // if (terminal == null)
            {
                // var error = new ErrorPayload(payload.Terminal).WithReason("Terminal is not logged in.");
                // sender.SendEvent(EventPayload.EvtTerminalError, error);
                return;
            }
/*
            var sender = terminal.Connection;
            var waiter = _dataProvider.Waiters.GetWaiters().FirstOrDefault(x => x.Name == args.user_name);
            if(waiter==null)
                waiter = _dataProvider.Waiters.GetWaiters().FirstOrDefault(x => x.Id == 2);
            if (waiter == null) return;

            terminal.Waiter = waiter;
            var payload = JsonConvert.SerializeObject(waiter);
            sender.SendEvent(EventPayload.EvtWaiterLoggedIn, payload);
            */
        }

        List<string> _topics;
        public List<string> Topics
        {
            get
            {
                return _topics ??= GetTopics().ToList();
            }
        }

        IEnumerable<string> GetTopics()
        {
            yield return CmdLoginEvent;
        }
    }
}