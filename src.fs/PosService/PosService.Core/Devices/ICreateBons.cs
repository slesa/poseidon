using System.Collections.Generic;
using PosShare.Model;

namespace PosService.Core.Devices
{
    public interface ICreateBons
    {
        void CreateOrderBons(IEnumerable<OrderedAction> orders);
    }
}