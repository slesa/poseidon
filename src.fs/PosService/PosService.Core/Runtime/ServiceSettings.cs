using System;
using PosShare;

namespace PosService.Core.Runtime
{
    public class ServiceSettings
    {
        public ServiceSettings()
        {
            RestStdServer = "127.0.0.1";
            RestStdPort = 5000;
            RestSecServer = "127.0.0.1";
            RestSecPort = 5001;
            MqttServer = "devops.42gmbh.com"; //"127.0.0.1";
            MqttPort = 30001; //1883;
            MqttUser = "fortytwo";
            MqttPass = "423235mqtt!";
        }

        public string RestStdServer { get; set; }
        public int RestStdPort { get; set; }
        public string RestSecServer { get; set; }
        public int RestSecPort { get; set; }
        public string MqttServer { get; set; }
        public int MqttPort { get; set; }
        public string MqttUser { get; set; }
        public string MqttPass { get; set; }

        public Guid? ServiceId { get; set; }
        public string ServiceName => "PosService";
    }
}