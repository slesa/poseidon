using System;
using System.Net;
using PosShare.CoreData;

namespace PosService.Core.Runtime
{
    public class TerminalSession
    {
        public static Waiter EmptyWaiter = new();
        
        public TerminalSession(IPAddress ipAddress, Terminal terminal)
        {
            SessionId = Guid.NewGuid();
            IpAddress = ipAddress;
            Terminal = terminal;
            Waiter = EmptyWaiter;
        }

        public IPAddress IpAddress { get; set; }
        public Guid SessionId { get; }
        public Terminal Terminal { get; internal set; }
        public Waiter Waiter { get; internal set; }
    }
}