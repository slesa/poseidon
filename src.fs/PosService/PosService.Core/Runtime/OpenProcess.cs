using PosShare.Model;

namespace PosService.Core.Runtime
{
    public class OpenProcess
    {
        public OpenProcess(int terminal, int waiter, Process process)
        {
            OnTerminal = terminal;
            FromWaiter = waiter;
            Process = process;
        }
        public int OnTerminal { get; set; }
        public int FromWaiter { get; set; }
        public Process Process { get; set; }
    }
    
}