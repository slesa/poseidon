using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using PosShare;
using PosShare.CoreData;
using PosShare.Model;

namespace PosService.Core.Runtime
{
    public class OpenProcesses
    {
        int _prossiveCounter = 0;

        readonly List<OpenProcess> _openProcesses = new();

        public OpenProcess FindProcess(Guid processId)
        {
            var process = _openProcesses.FirstOrDefault(x => x.Process.ProcessId == processId);
            return process;
        }

        public void SaveProcess(OpenProcess process)
        {
            if(process.Process is TableProcess tableProcess)
                SaveTable(tableProcess);
            if(process.Process is CheckoutProcess checkoutProcess)
                SaveCheckout(checkoutProcess);
        }
        
        #region Tables 
        
        public OpenProcess OpenTable(int table, int party, Terminal terminal, Waiter waiter)
        {
            var content = LoadTable(table, party, terminal, waiter);
            var ot = new OpenProcess(terminal.Id, waiter.Id, content);
            _openProcesses.Add(ot);
            return ot;
        }

        public void CloseTable(OpenProcess table)
        {
            if (table.Process.Actions.Any())
            {
                table.Process.ProgressiveId = "PE"+ ++_prossiveCounter;
                SaveTable(table.Process as TableProcess);
            }
            _openProcesses.Remove(table);
        }
        
        public OpenProcess FindTable(int table, int party)
        {
            var tables = _openProcesses.Where(x => x.Process is TableProcess);
            tables = tables.Where(x => ((TableProcess)x.Process).Table == table);
            if (party != 0)
                tables = tables.Where(x => ((TableProcess)x.Process).Party == party);
            return tables.FirstOrDefault();
        }

        TableProcess LoadTable(int table, int party, Terminal terminal, Waiter waiter)
        {
            var fn = TablesFile(table, party);
            if (!File.Exists(fn)) return CreateTable(table, party, terminal, waiter);
            var content = File.ReadAllText(fn);
            var converters = new List<JsonConverter> {new ProcessActionConverter(), new OrderAskItemConverter()};
            return JsonConvert.DeserializeObject<TableProcess>(content, converters.ToArray());
        }

        TableProcess CreateTable(int table, int party, Terminal terminal, Waiter waiter)
        {
            var created = new CreatedAction(DateTime.Now, terminal.Id, terminal.Name, waiter.Id, waiter.Name);
            var result = new TableProcess(table, party).WithProcessId(Guid.NewGuid());
            result.Created = created;
            return result;
        }
        
        void SaveTable(TableProcess tableProcess)
        {
            var fn = TablesFile(tableProcess.Table, tableProcess.Party);
            var content = JSonShare.Serialize(tableProcess);
            File.WriteAllText(fn, content);
        }

        static internal string TablesFile(int table, int party)
        {
            var dir = GetProcessesDir();
            return Path.Combine(dir, $"T{table}.{party}");
        }

        #endregion Tables 

        #region Checkouts
        
        public OpenProcess OpenCheckout(int id, Terminal terminal, Waiter waiter)
        {
            var content = LoadCheckout(id, terminal, waiter);
            var ot = new OpenProcess(terminal.Id, waiter.Id, content);
            _openProcesses.Add(ot);
            return ot;
        }

        public void CloseProcess(OpenProcess process)
        {
            if (process.Process.Actions.Any())
            {
                process.Process.ProgressiveId = "PE"+ ++_prossiveCounter;
                SaveCheckout(process.Process as CheckoutProcess);
            }
            _openProcesses.Remove(process);
        }
        
        public OpenProcess FindCheckout(int id)
        {
            var checkouts = _openProcesses.Where(x => x.Process is CheckoutProcess);
            checkouts = checkouts.Where(x => ((CheckoutProcess)x.Process).Id == id);
            return checkouts.FirstOrDefault();
        }


        CheckoutProcess LoadCheckout(int id, Terminal terminal, Waiter waiter)
        {
            var fn = CheckoutFile(id);
            if (!File.Exists(fn)) return CreateCheckout(id, terminal, waiter);
            var content = File.ReadAllText(fn);
            var converters = new List<JsonConverter> {new ProcessActionConverter(), new OrderAskItemConverter()};
            return JsonConvert.DeserializeObject<CheckoutProcess>(content, converters.ToArray());
        }

        CheckoutProcess CreateCheckout(int id, Terminal terminal, Waiter waiter)
        {
            var created = new CreatedAction(DateTime.Now, terminal.Id, terminal.Name, waiter.Id, waiter.Name);
            var result = new CheckoutProcess(id).WithProcessId(Guid.NewGuid());
            result.Created = created;
            return result;
        }
        
        void SaveCheckout(CheckoutProcess checkoutProcess)
        {
            var fn = CheckoutFile(checkoutProcess.Id);
            var content = JsonConvert.SerializeObject(checkoutProcess, Formatting.Indented);
            File.WriteAllText(fn, content);
        }

        internal static string CheckoutFile(int id)
        {
            var dir = GetProcessesDir();
            return Path.Combine(dir, $"C{id}");
        }

        #endregion Checkouts 

        static string GetProcessesDir()
        {
            var di = new DirectoryInfo(Processes.ProcessesDir);
            if (!di.Exists)
                di.Create();
            return di.FullName;
        }
    }
}