namespace PosService.Core.Settings
{
    public class RestServerSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }
}