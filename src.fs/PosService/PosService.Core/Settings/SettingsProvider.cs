using PosService.Core.Config;

namespace PosService.Core.Settings
{
    public class SettingsProvider : IProvideSettings
    {
        public SettingsProvider(IReadSettings settingsReader)
        {
            RestServerSettings = settingsReader.ReadRestSettings();
        }

        public RestServerSettings RestServerSettings { get; }
    }
}