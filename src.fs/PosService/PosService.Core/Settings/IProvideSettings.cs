namespace PosService.Core.Settings
{
    public interface IProvideSettings
    {
        RestServerSettings RestServerSettings { get; }
    }
}