using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using PosService.Core.Runtime;

namespace PosService.Core
{
    public class Environment
    {
        [JsonIgnore]
        const string FileName = "environment.cfg";
        
        [JsonIgnore]
        internal int _checkoutId; // Wegen tests internal
        public int CheckoutId => _checkoutId;

        public Environment()
        {
            ReadCurrent();
        }

        public int NextCheckoutId()
        {
            var result = ++_checkoutId;
            var buffer = JsonConvert.SerializeObject(CheckoutId);
            File.WriteAllText(FileName, buffer);
            return result;
        }

        List<TerminalSession> _terminalSessions;
        public List<TerminalSession> TerminalSessions => _terminalSessions ??= new List<TerminalSession>();
        
        OpenProcesses _openProcesses;
        public OpenProcesses OpenProcesses => _openProcesses ??= new OpenProcesses();

        void ReadCurrent()
        {
            if (!File.Exists(FileName)) return;
            var buffer = File.ReadAllText(FileName);
            var id = JsonConvert.DeserializeObject<int>(buffer);
            _checkoutId = id;
        }
    }
}