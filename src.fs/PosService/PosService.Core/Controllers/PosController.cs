﻿using Microsoft.AspNetCore.Mvc;
using PosShare.Payloads;

// ReSharper disable UnusedMember.Global

namespace PosService.Core.Controllers
{ 
    [Route("pos/[controller]")]
    public class PosController : ControllerBase
    {
        public IActionResult order(string payload) { return HandleMessage(CommandPayload.CmdOrder, payload); }
        public IActionResult order_progress(string payload) { return HandleMessage(CommandPayload.CmdOrderProgress, payload); }
        public IActionResult commit(string payload) { return HandleMessage(CommandPayload.CmdCommit, payload); }
        

        IActionResult HandleMessage(string command, string payload)
        {
            return new BadRequestResult();
        }
    }
}
