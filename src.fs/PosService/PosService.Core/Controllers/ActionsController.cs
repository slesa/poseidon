using log4net;
using Microsoft.AspNetCore.Mvc;
using PosService.Core.CoreData;
using PosService.Core.Logic;
using PosShare.Payloads;

namespace PosService.Core.Controllers
{
    [Route("pos/actions")]
    public class ActionsController : MyControllerBase
    {
        private readonly IHandleActionsCalls _actionsCalls;
        readonly IProvideCoreData _data;
        static ILog _log = LogManager.GetLogger(typeof(ActionsController));
        
        public ActionsController(IProvideCoreData dataProvider, IHandleActionsCalls actionsCalls)
        {
            _data = dataProvider;
            _actionsCalls = actionsCalls;
        }
        
        [HttpPost]
        [Route("order")]
        public IActionResult Order([FromBody] OrderCommandPayload payload)
        {
            _log.Info($"Order on process {payload.ProcessId} for {payload.SessionId}");

            var article = _data.Articles.FindArticle(payload.Plu);
            if (article == null) return Failed(payload.SessionId, ErrorCode.NotFound);

            var result = _actionsCalls.Order(article, payload);
            return GetResult(payload.SessionId, result, () => new EventPayload());
            /*return GetResult(payload.SessionId, result.Item1, () => new GotProcessesPayload
            {
                Processes = result.Item2.ToList()
            });*/
        }
        
        [HttpPost]
        [Route("commit")]
        public IActionResult Commit([FromBody] ProcessPayload payload)
        {
            _log.Info($"Commit process {payload.ProcessId} for {payload.SessionId}");
            var result = _actionsCalls.Commit(payload.SessionId, payload.ProcessId);
            return GetResult(payload.SessionId, result, () => new EventPayload());
        }

    }
}