﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PosService.Core.CoreData;
using PosShare.CoreData;

namespace PosService.Core.Controllers
{
    [Route("pos/[controller]")]
    public class DataController : ControllerBase
    {
        readonly IProvideCoreData _dataProvider;

        public DataController(IProvideCoreData dataProvider)
        {
            _dataProvider = dataProvider;
        }
        
    #region Articles
        [HttpGet]
        [Route("articles")]
        public ActionResult<IEnumerable<Article>> GetArticles()
        {
            var articles = _dataProvider.Articles.GetArticles();
            return articles;
        }
        [HttpGet]
        [Route("articles/{plu}")]
        public ActionResult<Article> GetArticle(int plu)
        {
            var article = _dataProvider.Articles.GetArticles().FirstOrDefault(x => x.Plu==plu);
            return article;
        }
    #endregion Articles

    #region Currencies
        [HttpGet]
        [Route("currencies")]
        public ActionResult<IEnumerable<Currency>> GetCurrencies()
        {
            var currencies = _dataProvider.Currencies.GetCurrencies();
            return currencies;
        }
        [HttpGet]
        [Route("currencies/{id}")]
        public ActionResult<Currency> GetCurrency(int id)
        {
            var currency = _dataProvider.Currencies.GetCurrencies().FirstOrDefault(x => x.Id==id);
            return currency;
        }
    #endregion Currencies
        
    #region Families
        [HttpGet]
        [Route("families")]
        public ActionResult<IEnumerable<Family>> GetFamilies()
        {
            var families = _dataProvider.Families.GetFamilies();
            return families;
        }
        [HttpGet("{id}")]
        [Route("families/{id}")]
        public ActionResult<Family> GetFamily(int id)
        {
            var family = _dataProvider.Families.GetFamilies().FirstOrDefault(x => x.Id==id);
            return family;
        }
    #endregion Families
    
    #region FamilyGroups
        [HttpGet]
        [Route("famgroups")]
        public ActionResult<IEnumerable<FamilyGroup>> GetFamGroups()
        {
            var groups = _dataProvider.FamilyGroups.GetFamilyGroups();
            return groups;
        }

        [HttpGet("{id}")]
        [Route("famgroups/{id}")]
        public ActionResult<FamilyGroup> GetFamGroup(int id)
        {
            var group = _dataProvider.FamilyGroups.GetFamilyGroups().FirstOrDefault(x => x.Id==id);
            return group;
        }
    #endregion FamilyGroups
    
    #region Payforms
        [HttpGet]
        [Route("payforms")]
        public ActionResult<IEnumerable<Payform>> GetPayforms()
        {
            var payforms = _dataProvider.Payforms.GetPayforms();
            return payforms;
        }
        [HttpGet("{id}")]
        [Route("payforms/{id}")]
        public ActionResult<Payform> GetPayform(int id)
        {
            var payform = _dataProvider.Payforms.GetPayforms().FirstOrDefault(x => x.Id==id);
            return payform;
        }
    #endregion Payforms

    #region Terminals
        [HttpGet]
        [Route("terminals")]
        public ActionResult<IEnumerable<Terminal>> GetTerminals()
        {
            var terminals = _dataProvider.Terminals.GetTerminals();
            return terminals;
        }

        [HttpGet("{id}")]
        [Route("terminals/{id}")]
        public ActionResult<Terminal> GetTerminal(int id)
        {
            var terminal = _dataProvider.Terminals.GetTerminals().FirstOrDefault(x => x.Id==id);
            return terminal;
        }
    #endregion Terminals
    
    #region VAT rates
        [HttpGet]
        [Route("vatrates")]
        public ActionResult<IEnumerable<VatRate>> GetVatRates()
        {
            var vatRates = _dataProvider.VatRates.GetVatRates();
            return vatRates;
        }

        [HttpGet("{id}")]
        [Route("vatrates/{id}")]
        public ActionResult<VatRate> GetVatRate(int id)
        {
            var vatRate = _dataProvider.VatRates.GetVatRates().FirstOrDefault(x => x.Id==id);
            return vatRate;
        }
    #endregion VAT rates
    
    #region Waiters
        [HttpGet]
        [Route("waiters")]
        public ActionResult<IEnumerable<Waiter>> GetWaiters()
        {
            var waiters = _dataProvider.Waiters.GetWaiters();
            return waiters;
        }

        [HttpGet("{id}")]
        [Route("waiters/{id}")]
        public ActionResult<Waiter> GetWaiter(int id)
        {
            var waiter = _dataProvider.Waiters.GetWaiters().FirstOrDefault(x => x.Id==id);
            return waiter;
        }
    #endregion Waiters
    }
}