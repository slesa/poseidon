﻿using System;
using Microsoft.AspNetCore.Mvc;
using PosShare.Payloads;

namespace PosService.Core.Controllers
{
    public class MyControllerBase : ControllerBase
    {
        protected IActionResult Success(EventPayload payload)
        {
            return new JsonResult(payload);
        }
        protected IActionResult Failed(Guid sessionId, ErrorCode status)
        {
            return new JsonResult(new EventPayload(sessionId, status));
        }

        protected IActionResult GetResult(Guid sessionId, ErrorCode error, Func<EventPayload> creator)
        {
            return error == ErrorCode.None ? Success(creator().WithSessionId(sessionId)) : Failed(sessionId, error);
        }
    }
}