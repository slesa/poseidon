﻿using System.Linq;
using log4net;
using Microsoft.AspNetCore.Mvc;
using PosService.Core.Logic;
using PosShare.Payloads;

namespace PosService.Core.Controllers
{
    [Route("pos/process")]
    public class ProcessesController : MyControllerBase
    {
        static ILog _log = LogManager.GetLogger(typeof(ProcessesController));
        readonly IHandleProcessesCalls _processesCalls;

        public ProcessesController(IHandleProcessesCalls processesCalls)
        {
            _processesCalls = processesCalls;
        }

        [HttpPost]
        [Route("tables")]
        public IActionResult GetTables([FromBody] CommandPayload payload)
        {
            _log.Info($"Get tables for {payload.SessionId}");
            var result = _processesCalls.GetTables(payload.SessionId);
            return GetResult(payload.SessionId, result.Item1, () => new GotProcessesPayload
            {
                Processes = result.Item2.ToList()
            });
        }

        [HttpPost]
        [Route("opentable")]
        public IActionResult OpenTable([FromBody] OpenTablePayload payload)
        {
            _log.Info($"Open table {payload.Table}/{payload.Party} for {payload.SessionId}");
            var result = _processesCalls.OpenTable(payload.SessionId, payload.Table, payload.Party);
            return GetResult(payload.SessionId, result.Item1, () => new ProcessOpenedPayload()
                .WithProcessGuid(result.Item2.Process.ProcessId)
                .WithTable(payload.Table).WithParty(payload.Party)
                .WithActions(result.Item2.Process.Actions));
        }

        [HttpPost]
        [Route("checkouts")]
        public IActionResult GetCheckouts([FromBody] CommandPayload payload)
        {
            _log.Info($"Get checkouts for {payload.SessionId}");
            var result = _processesCalls.GetCheckouts(payload.SessionId);
            return GetResult(payload.SessionId, result.Item1, () => new GotProcessesPayload
            {
                Processes = result.Item2.ToList()
            });
        }

        [HttpPost]
        [Route("opencheckout")]
        public IActionResult OpenCheckout([FromBody] OpenCheckoutPayload payload)
        {
            _log.Info($"Open checkout {payload.CheckoutId} for {payload.SessionId}");
            var result = _processesCalls.OpenCheckout(payload.SessionId, payload.CheckoutId);
            return GetResult(payload.SessionId, result.Item1, () => new ProcessOpenedPayload()
                .WithProcessGuid(result.Item2.Process.ProcessId)
                .WithCheckoutId(payload.CheckoutId)
                .WithActions(result.Item2.Process.Actions));
        }

        [HttpPost]
        [Route("closeprocess")]
        public IActionResult CloseProcess([FromBody] ProcessPayload payload)
        {
            _log.Info($"Close process {payload.ProcessId} for {payload.SessionId}");
            var error = _processesCalls.CloseProcess(payload.SessionId, payload.ProcessId);
            return GetResult(payload.SessionId, error, () => new EventPayload());
        }
    }
}