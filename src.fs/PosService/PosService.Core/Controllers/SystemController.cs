using System;
using log4net;
using Microsoft.AspNetCore.Mvc;
using PosService.Core.CoreData;
using PosService.Core.Logic;
using PosShare.Payloads;

namespace PosService.Core.Controllers
{
    [Route("pos/terminal")]
    public class SystemController : MyControllerBase
    {
        static ILog _log = LogManager.GetLogger(typeof(SystemController));
        readonly IProvideCoreData _data;
        readonly IHandleSystemCalls _systemCalls;

        public SystemController(IProvideCoreData dataProvider, IHandleSystemCalls systemCalls)
        {
            _data = dataProvider;
            _systemCalls = systemCalls;
        }
        
        [HttpPost]
        [Route("turnon")]
        public IActionResult TurnOn([FromBody] TurnOnPayload payload)
        {
            if (payload == null)
            {
                _log.Info($"Turning on terminal: no payload");
                return Failed(Guid.Empty, ErrorCode.NoSessionId);
            }
            _log.Info($"Turning on terminal: {payload.Terminal}");
            var term = _data.Terminals.FindTerminal(payload.Terminal);
            if (term == null) return Failed(Guid.Empty, ErrorCode.NotFound);
            var sessionId = _systemCalls.RegisterTerminal(term, HttpContext.Connection.RemoteIpAddress);
            if (sessionId == Guid.Empty) return Failed(Guid.Empty, ErrorCode.NoSessionId);
            return Success(new TurnedOnPayload {Terminal = term}.WithSessionId(sessionId));
        }

        [HttpPost]
        [Route("turnoff")]
        public IActionResult TurnOff([FromBody] CommandPayload payload)
        {
            if (payload == null)
            {
                _log.Info($"Turning off terminal: no payload");
                return Failed(Guid.Empty, ErrorCode.NoSessionId);
            }
            _log.Info($"Turning off terminal: {payload.SessionId}");
            _systemCalls.UnregisterTerminal(payload.SessionId);
            return Ok();
        }
        
        [HttpPost]
        [Route("setwaiter")] // finger weg!
        public IActionResult Login([FromBody] LoginPayload payload)
        {
            if (payload == null)
            {
                _log.Info($"Logging in waiter: no payload");
                return Failed(Guid.Empty, ErrorCode.NoSessionId);
            }
            _log.Info($"Logging in waiter: {payload.Waiter}");
            var waiter = _data.Waiters.FindWaiter(payload.Waiter);
            if (waiter == null) return Failed(payload.SessionId, ErrorCode.NotFound);
            var error = payload.Automatic
                ? _systemCalls.AutoLoginWaiter(payload.SessionId, waiter)
                : _systemCalls.LoginWaiter(payload.SessionId, waiter, payload.Password);
            return GetResult(payload.SessionId, error, () => new LoggedInPayload {Waiter = waiter});
        }

        [HttpPost]
        [Route("clrwaiter")]
        public IActionResult Logout([FromBody] CommandPayload payload)
        {
            if (payload == null)
            {
                _log.Info($"Logging out waiter: no payload");
                return Failed(Guid.Empty, ErrorCode.NoSessionId);
            }
            _log.Info($"Logging off waiter: {payload.SessionId}");
            var error = _systemCalls.LogoutWaiter(payload.SessionId);
            return GetResult(payload.SessionId, error, () => new EventPayload(){SessionId = payload.SessionId});
        }
        // public IActionResult set_waiter_by_key(string payload) { return HandleMessage(CommandPayload.CmdSetWaiterByKey, payload); }
   }
}