#if not
using System;
using System.Linq;
using Newtonsoft.Json;
using PosService.Core.CoreData;
using PosService.Core.Devices;
using PosService.Core.Runtime;
using PosShare;
using PosShare.CoreData;
using PosShare.Model;
using PosShare.Payloads;

namespace PosService.Core.Commands
{
    public class ActionCommandsHandler
    {
        readonly ICreateBons _bonCreator;
        readonly IProvideCoreData _dataProvider;
        readonly Environment _environment;

        public ActionCommandsHandler(ICreateBons bonCreator, IProvideCoreData dataProvider, Environment environment)
        {
            _bonCreator = bonCreator;
            _dataProvider = dataProvider;
            _environment = environment;
        }

        public bool CanHandle(string command)
        {
            return command == CommandPayload.CmdOrder || command == CommandPayload.CmdOrderProgress || command == CommandPayload.CmdCommit;
        }

        public void Handle(string command, string msg)
        {
            var payload = JSonShare.Deserialize<CommandPayload>(msg);
            // var session = FindSession(_environment, payload.SessionId);
            // if(session==null) return;
            TerminalSession session = null;
            switch (command)
            {
                case CommandPayload.CmdOrder:
                    Order(session, msg);
                    break;
                case CommandPayload.CmdOrderProgress:
                    OnOrderProgress(session, msg);
                    break;
                case CommandPayload.CmdCommit:
                    Commit(msg);
                    break;
            }
        }


        void OnOrderProgress(TerminalSession session, string msg)
        {
            var order = JsonConvert.DeserializeObject<OrderPayload>(msg, new OrderAskItemConverter());
            
            var process = GetProcess(order.SessionId, order.ProcessId);
            if (process == null) return;

            var callback = order.AskItems[order.CurrentCallback];
            var line = order.OrderLines[callback.ForLine];
            if (callback.Type == OrderAskType.Constraint)
            {
                var cc = callback as OrderAskConstraint;
                var choice = cc.Choices[cc.Choice];
                if (!HandleOrder(order.SessionId, line.Count, choice.Plu, order, null)) return;
            }
            
            if (++order.CurrentCallback < order.AskItems.Count)
            {
                var result = JSonShare.Serialize(order);
                // sender.SendEvent(EventPayload.EvtOrderMissing, result);
                return;
            }
            AddOrderAction(session.Waiter, order, process);
        }

        static void AddOrderAction(Waiter waiter, OrderPayload order, OpenProcess process)
        {
            var action = new OrderedAction(DateTime.Now)
                .WithPayload(order)
                .WithWaiter(waiter.Id, waiter.Name)
                .WithActionId(Guid.NewGuid());
                
            process.Process.Actions.Add(action);

            var answer = JSonShare.Serialize(action);
            // sender.SendEvent(EventPayload.EvtOrdered, answer);
        }

        void Commit(string msg)
        {
            var commit = JsonConvert.DeserializeObject<CommitPayload>(msg);
            
            var process = GetProcess(commit.SessionId, commit.ProcessId);
            if (process == null) return;

            var uncommitted = process.Process.Actions.Where(x => !x.Committed).ToList();
            if (uncommitted.Any())
            {
                _bonCreator.CreateOrderBons(uncommitted.Where(x => x is OrderedAction).Cast<OrderedAction>());
                // _bonCreator.CreateVoidBons(uncommitted.Where(x => x is VoidedAction).Cast<OrderedAction>());
                foreach (var action in uncommitted)
                    action.Committed = true;
                _environment.OpenProcesses.SaveProcess(process);
            }
            var newaction = new CommittedAction(DateTime.Now);
            newaction.Actions.AddRange(uncommitted);

            var answer = JSonShare.Serialize(newaction);
            // sender.SendEvent(EventPayload.EvtCommitted, answer);
        }


        Article GetArticle(Guid sessionId, int plu)
        {
            var article = _dataProvider.Articles.FindArticle(plu);
            if (article != null) return article;

            // var error = new ErrorPayload(sessionId).WithReason($"Article {plu} not found.");
            // sender.SendEvent(EventPayload.EvtOrderError, error);
            return null;
        }

    }
}
#endif
