using System;
using System.Linq;
using Newtonsoft.Json;
using PosService.Core.CoreData;
using PosService.Core.Runtime;
using PosShare.CoreData;
using PosShare.Payloads;

namespace PosService.Core.Commands
{
    public class TerminalCommandsHandler
    {
        readonly IProvideCoreData _dataProvider;
        readonly Environment _environment;
        
        public TerminalCommandsHandler(IProvideCoreData dataProvider, Environment environment)
        {
            _dataProvider = dataProvider;
            _environment = environment;
        }

        void SetWaiterByKey(TerminalSession terminalSession, string msg)
        {
            var args = JsonConvert.DeserializeObject<KeylockPayload>(msg);
            if (string.IsNullOrWhiteSpace(args.Key))
            {
                // ClearWaiter(terminalSession, args, sender);
                return;
            }

            var waiter = _dataProvider.Waiters.GetWaiters().FirstOrDefault(x => x.Keylock == args.Key);
            if (waiter == null)
            {
                // var error = (new WaiterErrorPayload(terminalSession.SessionId).WithReason($"Waiter with key {args.Key} not found"));
                // sender.SendEvent(EventPayload.EvtWaiterError, error);
                return;
            }
            
            // LogInWaiter(terminalSession, sender, waiter);
            Console.WriteLine($"Waiter set to {waiter.Id} by key {args.Key}");
        }

    }
}