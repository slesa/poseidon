using Basics.Config;
using PosService.Core.Settings;

namespace PosService.Core.Config
{
    public class RestSettingsReader : ReadIniConfig
    {
        public const string DefaultHost = "localhost";
        public const int DefaultPort = 8284;
        
        public RestServerSettings ReadRestSettings(string content)
        {
            var result = new RestServerSettings();
            
            LoadIniFile(content);
            result.Host = Read(Configure.ServerSection, "RestHost", DefaultHost);
            result.Port = Read(Configure.ServerSection, "RestPort", DefaultPort, ConfigConverters.ConvertToInt);

            return result;
        }
    }
}