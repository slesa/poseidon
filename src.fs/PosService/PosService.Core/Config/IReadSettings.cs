using PosService.Core.Settings;

namespace PosService.Core.Config
{
    public interface IReadSettings
    {
        RestServerSettings ReadRestSettings();
    }
}