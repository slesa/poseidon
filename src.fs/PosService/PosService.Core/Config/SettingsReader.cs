using System.IO;
using PosService.Core.Settings;

namespace PosService.Core.Config
{
    public class Configure : IReadSettings
    {
        public const string ConfigFile = "etc/settings.ini";
        public const string ServerSection = "Server";
        
        public Configure()
        {
        }

        public Configure(string content)
        {
            _content = content;
        }

        public RestServerSettings ReadRestSettings()
        {
            var reader = new RestSettingsReader();
            return reader.ReadRestSettings(Content);
        }

        string _content;
        string Content
        {
            get
            {
                if (string.IsNullOrEmpty(_content))
                {
                    _content = File.Exists(ConfigFile) ? File.ReadAllText(ConfigFile) : "";
                }
                return _content;
            }
        }
    }
}