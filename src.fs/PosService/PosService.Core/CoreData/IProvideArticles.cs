using System.Collections.Generic;
using PosShare.CoreData;

namespace PosService.Core.CoreData
{
    public interface IProvideArticles
    {
        List<Article> GetArticles();
        Article FindArticle(int plu);
    }
}