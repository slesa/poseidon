using System.Collections.Generic;
using PosShare.CoreData;
using PosShare.Model;

namespace PosService.Core.CoreData
{
    public interface IProvideTerminals
    {
        List<Terminal> GetTerminals();
        Terminal FindTerminal(int id);
    }
}