namespace PosService.Core.CoreData
{
    public interface IProvideCoreData
    {
        IProvideFamilyGroups FamilyGroups { get; }
        IProvideFamilies Families { get; }
        IProvideArticles Articles { get; }
        IProvideCurrencies Currencies { get; }
        IProvidePayforms Payforms { get; }
        IProvideVatRates VatRates { get; }
        IProvideTerminals Terminals { get; }
        IProvideWaiters Waiters { get; }
    }
}