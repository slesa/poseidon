using System.Collections.Generic;
using PosShare.CoreData;
using PosShare.Model;

namespace PosService.Core.CoreData
{
    public interface IProvideCurrencies
    {
        List<Currency> GetCurrencies();
    }
}