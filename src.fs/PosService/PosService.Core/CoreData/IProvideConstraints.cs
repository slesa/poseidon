using PosShare.CoreData;

namespace PosService.Core.CoreData
{
    public interface IProvideConstraints
    {
        Constraint GetConstraintsFor(int plu);
    }
}