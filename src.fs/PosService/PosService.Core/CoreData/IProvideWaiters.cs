using System.Collections.Generic;
using PosShare.CoreData;
using PosShare.Model;

namespace PosService.Core.CoreData
{
    public interface IProvideWaiters
    {
        List<Waiter> GetWaiters();
        Waiter FindWaiter(int id);
    }
}