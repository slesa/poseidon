using System;
using System.Collections.Generic;
using System.Linq;
using PosShare.CoreData;
using PosShare.Model;

namespace PosService.Core.CoreData.Classic
{
    public class PayformsProvider : DatFileLoader<Payform>, IProvidePayforms
    {
        List<Payform> _payforms;
        public List<Payform> GetPayforms()
        {
            return _payforms ?? (_payforms = LoadFile("payform.dat").ToList());
        }
        
        protected override TypeMapper WithTypeMapper()
        {
            var mapper = new TypeMapper()
                // .WithRedirection("FamilyGroup", "group")
                .WithConverterFor("Id", x => Convert.ToInt32(x) );
            // .WithConverterFor("FamilyGroup", x => Convert.ToInt32(x) );
            return mapper;
        }
    }
}