using System;
using System.Collections.Generic;
using System.Linq;
using PosShare.CoreData;
using PosShare.Model;

namespace PosService.Core.CoreData.Classic
{
    public class CurrenciesProvider : DatFileLoader<Currency>, IProvideCurrencies
    {
        List<Currency> _currencies;
        public List<Currency> GetCurrencies()
        {
            return _currencies ?? (_currencies = LoadFile("currency.dat").ToList());
        }
        
        protected override TypeMapper WithTypeMapper()
        {
            var mapper = new TypeMapper()
                // .WithRedirection("FamilyGroup", "group")
                .WithConverterFor("Id", x => Convert.ToInt32(x) );
                // .WithConverterFor("FamilyGroup", x => Convert.ToInt32(x) );
            return mapper;
        }
    }
}