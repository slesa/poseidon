using System;
using System.Collections.Generic;
using System.Linq;
using PosShare.CoreData;

namespace PosService.Core.CoreData.Classic
{
    public class ArticlesProvider : DatFileLoader<Article>, IProvideArticles
    {
        readonly IProvideConstraints _constraintsProvider;

        public ArticlesProvider(IProvideConstraints constraintsProvider)
        {
            _constraintsProvider = constraintsProvider;
        }

        List<Article> _articles;
        public List<Article> GetArticles()
        {
            return _articles ??= LoadArticles();
        }

        List<Article> LoadArticles()
        {
            var articles = LoadFile("articles.dat", LoadConstraint).ToList();
            return articles;
        }

        public void LoadConstraint(Article article)
        {
            var constraint = _constraintsProvider.GetConstraintsFor(article.Plu);
            article.Constraint = constraint;
        }
        
        public Article FindArticle(int plu)
        {
            var result = GetArticles().FirstOrDefault(x => x.Plu == plu);
            if (result == null) return null;
            if (result.Plu % 2 == 0) result.IsFreeText = true;
            if (result.Plu % 3 == 0) result.IsFreePrice = true;
            return result;
        }
        
        protected override TypeMapper WithTypeMapper()
        {
            var mapper = new TypeMapper()
                .WithRedirection("Plu", "id")
                .WithConverterFor("Plu", x => Convert.ToInt32(x))
                .WithConverterFor("Family", x => Convert.ToInt32(x))
                .WithConverterFor("Price", x => Convert.ToDecimal(x) / 100.0m)
                .WithConverterFor("IsHint", x => StringToBoolConverter.Convert(x))
                .WithConverterFor("IsFreePrice", x => StringToBoolConverter.Convert(x))
                .WithConverterFor("IsFreeText", x => StringToBoolConverter.Convert(x))
                .WithConverterFor("IsWeight", x => StringToBoolConverter.Convert(x))
                .WithConverterFor("IsCutGood", x => StringToBoolConverter.Convert(x))
                .WithConverterFor("IsAreaGood", x => StringToBoolConverter.Convert(x));
            return mapper;
        }
    }
}