using System;
using System.IO;
using System.Linq;
using PosShare.CoreData;

namespace PosService.Core.CoreData.Classic
{
    public class ConstraintsProvider : HeaderDatFileLoader<ConstraintLevel, ConstraintEntry>, IProvideConstraints
    {
        const string PathConstraints = "constr";
        
        public Constraint GetConstraintsFor(int plu)
        {
            Constraint result = null;
            for (var level = 1; ; level++)
            {
                var fn = base.GetFileName( GetFileNameFor(plu, level) );
                if (!File.Exists(fn)) break;
                
                if (result == null) result = new Constraint(plu);
                var contraints = LoadFile(fn).ToList();
                Header.Entries = contraints;
                result.Levels.Add(Header);
            }
            return result;
        }

        string GetFileNameFor(int plu, int level)
        {
            var fn = $"C{plu}.{level}";
            var full = Path.Combine(PathConstraints, fn);
            return full;
        }
        
        protected override TypeMapper WithHeaderMapper()
        {
            var mapper = new TypeMapper()
                .WithRedirection("IsActive", "active")
                .WithRedirection("IsCloseable", "closeable")
                .WithRedirection("IsRecursive", "recursive")
                .WithRedirection("IsTakeCount", "takecount")
                .WithConverterFor("Id", x => Convert.ToInt32(x))
                .WithConverterFor("IsActive", x => StringToBoolConverter.Convert(x))
                .WithConverterFor("IsCloseable", x => StringToBoolConverter.Convert(x))
                .WithConverterFor("IsRecursive", x => StringToBoolConverter.Convert(x))
                .WithConverterFor("IsTakeCount", x => StringToBoolConverter.Convert(x));
            return mapper;
        }
        
        protected override TypeMapper WithTypeMapper()
        {
            var mapper = new TypeMapper()
                .WithRedirection("IsHint", "constrhint")
                .WithRedirection("UsePrice", "constrpriceuse")
                .WithRedirection("Price", "constrprice")
                .WithConverterFor("Plu", x => Convert.ToInt32(x))
                .WithConverterFor("Price", x => Convert.ToDecimal(x)/100.0m)
                .WithConverterFor("IsHint", x => StringToBoolConverter.Convert(x))
                .WithConverterFor("UsePrice", x => StringToBoolConverter.Convert(x));
            return mapper;
        }
    }
}