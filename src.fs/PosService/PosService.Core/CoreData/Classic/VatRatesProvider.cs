using System;
using System.Collections.Generic;
using System.Linq;
using PosShare.CoreData;
using PosShare.Model;

namespace PosService.Core.CoreData.Classic
{
    public class VatRatesProvider : DatFileLoader<VatRate>, IProvideVatRates
    {
        List<VatRate> _vatRates;
        public List<VatRate> GetVatRates()
        {
            return _vatRates ?? (_vatRates = LoadFile("vatrate.dat").ToList());
        }
         
        protected override TypeMapper WithTypeMapper()
        {
            var mapper = new TypeMapper()
                // .WithRedirection("FamilyGroup", "group")
                .WithConverterFor("Id", x => Convert.ToInt32(x) );
            // .WithConverterFor("FamilyGroup", x => Convert.ToInt32(x) );
            return mapper;
        }
    }
}