using System;
using System.Collections.Generic;
using System.Linq;
using PosShare.CoreData;
using PosShare.Model;

namespace PosService.Core.CoreData.Classic
{
    public class FamilyGroupsProvider : DatFileLoader<FamilyGroup>, IProvideFamilyGroups
    {
        List<FamilyGroup> _familyGroups;
        public List<FamilyGroup> GetFamilyGroups()
        {
            return _familyGroups ?? (_familyGroups = LoadFile("famgroups.dat").ToList());
        }

        protected override TypeMapper WithTypeMapper()
        {
            var mapper = new TypeMapper()
                .WithConverterFor("Id", x => Convert.ToInt32(x) );
            return mapper;
        }
    }
}