using System;
using System.Collections.Generic;
using System.Linq;
using PosShare.CoreData;
using PosShare.Model;

namespace PosService.Core.CoreData.Classic
{
    public class TerminalsProvider : DatFileLoader<Terminal>, IProvideTerminals
    {
        List<Terminal> _terminals;
        public List<Terminal> GetTerminals()
        {
            return _terminals ?? (_terminals = LoadFile("terminals.dat").ToList());
        }

        public Terminal FindTerminal(int id)
        {
            var terminals = GetTerminals();
            return terminals.FirstOrDefault(x => x.Id == id);
        }

        protected override TypeMapper WithTypeMapper()
        {
            var mapper = new TypeMapper()
                // .WithRedirection("FamilyGroup", "group")
                .WithConverterFor("Id", x => Convert.ToInt32(x) );
            // .WithConverterFor("FamilyGroup", x => Convert.ToInt32(x) );
            return mapper;
        }
    }
}