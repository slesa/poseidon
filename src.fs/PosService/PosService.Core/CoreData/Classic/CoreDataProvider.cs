namespace PosService.Core.CoreData.Classic
{
    public class CoreDataProvider : IProvideCoreData
    {
        public CoreDataProvider()
        {
            Families = new FamiliesProvider();
            FamilyGroups = new FamilyGroupsProvider();
            Constraints = new ConstraintsProvider();
            Articles = new ArticlesProvider(Constraints);
            Articles.GetArticles(); // Speed up first order
            Currencies = new CurrenciesProvider();
            Payforms = new PayformsProvider();
            VatRates = new VatRatesProvider();
            Terminals = new TerminalsProvider();
            Waiters = new WaitersProvider();
        }
        public IProvideFamilyGroups FamilyGroups { get; }
        public IProvideFamilies Families { get; }
        public IProvideConstraints Constraints { get; }
        public IProvideArticles Articles { get; }
        public IProvideCurrencies Currencies { get; }
        public IProvidePayforms Payforms { get; }
        public IProvideVatRates VatRates { get; }
        public IProvideTerminals Terminals { get; }
        public IProvideWaiters Waiters { get; }
    }
}