using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace PosService.Core.CoreData.Classic
{
    public abstract class HeaderDatFileLoader<S, T> : DatFileLoader<T>
    {
        protected HeaderDatFileLoader(string dataPath="data") : base(dataPath) {}
        
        protected virtual TypeMapper WithHeaderMapper()
        {
            return new TypeMapper();
        }

        protected S Header { get; set; }
        
        protected override void ReadHeader(BinaryReader reader)
        {
            var datValue = ReadValue(reader);
            if (datValue == null) return;

            var mapper = WithHeaderMapper();
            Header = mapper.Map<S>(datValue);
        }
    }
    
    public abstract class DatFileLoader<T>
    {
        readonly string _dataPath;

        protected DatFileLoader(string dataPath="data")
        {
            _dataPath = CheckDataPath(dataPath);
        }

        string CheckDataPath(string path)
        {
            var di = new DirectoryInfo(path);
            if( !di.Exists)
                di.Create();
            return di.FullName;
        }

        protected virtual TypeMapper WithTypeMapper()
        {
            return new TypeMapper();
        }

        protected virtual void ReadHeader(BinaryReader reader)
        {
        }
        
        public IEnumerable<T> LoadFile(string fn, Action<T> intercept=null)
        {
            var fileName = GetFileName(fn);
            if (!File.Exists(fileName)) yield break;

            var mapper = WithTypeMapper();
            // var fileStream = new FileStream(fileName, FileMode.Open);
            var data = File.ReadAllBytes(fileName);
            var fileStream = new MemoryStream(data);
            using (var reader = new BinaryReader(fileStream))
            {
                ReadHeader(reader);
                while (true)
                {
                    var datValue = ReadValue(reader);
                    if (datValue == null) yield break;

                    var mappedValue = mapper.Map<T>(datValue);
                    intercept?.Invoke(mappedValue);
                    yield return mappedValue;
                }
            }            
        }

        protected string GetFileName(string fn)
        {
            return Path.Combine(_dataPath, fn);
        }

        protected Dictionary<string, string> ReadValue(BinaryReader reader)
        {
            try
            {
                var result = new Dictionary<string, string>();
                // var dict = (IDictionary<string, string>)result;
                
                // System.Diagnostics.Debug.WriteLine("New Value");
                var sizeBin = reader.ReadInt32();
                var size = Swap( sizeBin );
                for (var i = 0; i < size; i++)
                {
                    if (reader.BaseStream.Position >= reader.BaseStream.Length) break; 

                    var key = ReadString(reader);
                    if (key == "") continue;
                    var val = ReadString(reader);
                    // System.Diagnostics.Debug.WriteLine("KeyValue " + key + ":" + val);
                    result.Add(key, val);
                }
                return result;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Fatal error when reading " + ex.Message);
                return null;
            }
        }

        string ReadString(BinaryReader reader)
        {
            var keyLen = Swap( reader.ReadInt32() );
            if (keyLen == -1) return "";
            var buffer = reader.ReadBytes(keyLen);
            var str = Encoding.BigEndianUnicode.GetString(buffer);
            return str;
        }
        
        public static int Swap(int value)
        {
            var b1 = (value >> 0) & 0xff;
            var b2 = (value >> 8) & 0xff;
            var b3 = (value >> 16) & 0xff;
            var b4 = (value >> 24) & 0xff;

            return b1 << 24 | b2 << 16 | b3 << 8 | b4 << 0;
        }    
    }
    public static class StringToBoolConverter
    {
        public static bool Convert(string source, bool def=false)
        {
            if (source == null) return def;
            var cmp = source.ToLowerInvariant();
            // System.Diagnostics.Debug.WriteLine($"Convert flag {source}");
            if (cmp == "yes") return true;
            if (cmp == "no") return false;
            if (cmp == "true") return true;
            if (cmp == "false") return false;
            if (cmp == "1") return true;
            if (cmp == "0") return false;
            return def;
        }
    }

    public class TypeMapper
    {
        public delegate object Converter(string input);
        
        readonly Dictionary<string, Converter> _converters = new Dictionary<string, Converter>();
        readonly Dictionary<string, string> _redirections = new Dictionary<string, string>();

        public TypeMapper WithConverterFor(string field, Converter converter)
        {
            _converters.Add(field, converter);
            return this;
        }

        public TypeMapper WithRedirection(string source, string dest)
        {
            _redirections.Add(source, dest);
            return this;
        }
        
        public T Map<T>(Dictionary<string, string> datValue)
        {
            var entity = Activator.CreateInstance<T>();
            var properties = entity.GetType().GetProperties();
            foreach (var property in properties)
            {
                var propertyName = property.Name;
                var key = propertyName;
                if (_redirections.TryGetValue(key, out var newKey))
                    key = newKey;
                var localKey = key.ToLower(CultureInfo.InvariantCulture);
                if( !datValue.TryGetValue(localKey, out var value) )
                    continue;
                var propertyInfo = entity.GetType().GetProperty(propertyName);
                if (_converters.TryGetValue(propertyName, out var converter))
                {
                    //System.Diagnostics.Debug.WriteLine($"Converting {value} for {propertyName}");
                    // Func<TInput, TOutput> converter = (Func<TInput, TOutput>)del;
                    propertyInfo.SetValue(entity, converter(value));
                }
                else
                {
                    // System.Diagnostics.Debug.WriteLine($"Setting {value} for {propertyName}");
                    propertyInfo.SetValue(entity, value);
                }
            }
            return entity;
        }
    }
}