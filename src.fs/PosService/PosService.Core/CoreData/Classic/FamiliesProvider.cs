using System;
using System.Collections.Generic;
using System.Linq;
using PosShare.CoreData;
using PosShare.Model;

namespace PosService.Core.CoreData.Classic
{
    public class FamiliesProvider: DatFileLoader<Family>, IProvideFamilies
    {
        List<Family> _families;
        public List<Family> GetFamilies()
        {
            return _families ?? (_families = LoadFile("families.dat").ToList());
        }

        protected override TypeMapper WithTypeMapper()
        {
            var mapper = new TypeMapper()
                .WithRedirection("FamilyGroup", "group")
                .WithConverterFor("Id", x => Convert.ToInt32(x) )
                .WithConverterFor("FamilyGroup", x => Convert.ToInt32(x) );
            return mapper;
        }
    }
}