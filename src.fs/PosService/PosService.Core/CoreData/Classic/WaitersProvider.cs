using System;
using System.Collections.Generic;
using System.Linq;
using PosShare.CoreData;
using PosShare.Model;

namespace PosService.Core.CoreData.Classic
{
    public class WaitersProvider : DatFileLoader<Waiter>, IProvideWaiters
    {
        List<Waiter> _waiters;
        public List<Waiter> GetWaiters()
        {
            return _waiters ?? (_waiters = LoadFile("waiters.dat").ToList());
        }

        public Waiter FindWaiter(int id)
        {
            var waiters = GetWaiters();
            return waiters.FirstOrDefault(x => x.Id == id);
        }

        protected override TypeMapper WithTypeMapper()
        {
            var mapper = new TypeMapper()
                .WithRedirection("CanCreateTable", "createtable")
                .WithRedirection("CanOrder", "orders")
                .WithRedirection("CanVoid", "voids")
                .WithRedirection("CanPay", "pays")
                .WithRedirection("CanSplit", "splits")
                .WithConverterFor("Id", x => Convert.ToInt32(x))
                // Todo: in demo data all are set to no?
                .WithConverterFor("CanLogin", x => true )
                .WithConverterFor("CanCreateTable", x => !StringToBoolConverter.Convert(x, true) )
                .WithConverterFor("CanOrder", x => !StringToBoolConverter.Convert(x, true))
                .WithConverterFor("CanVoid", x => !StringToBoolConverter.Convert(x, true) )
                .WithConverterFor("CanPay", x => !StringToBoolConverter.Convert(x, true) )
                .WithConverterFor("CanSplit", x => !StringToBoolConverter.Convert(x, true) );
            return mapper;
        }
    }
}