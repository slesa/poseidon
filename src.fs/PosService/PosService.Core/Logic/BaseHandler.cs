using System;
using System.Linq;
using PosService.Core.Runtime;
using PosShare.Payloads;

namespace PosService.Core.Logic
{
    public abstract class ProcessBaseHandler : WaiterBaseHandler
    {
        protected OpenProcess _process;
        
        protected ProcessBaseHandler(Environment env) : base(env)
        {
        }

        protected ErrorCode EnsureProcess(Guid sessionId, Guid processId)
        {
            var errorCode = EnsureWaiter(sessionId);
            if (errorCode != ErrorCode.None) return errorCode;
            
            _process = _env.OpenProcesses.FindProcess(processId);
            if (_process == null) return ErrorCode.NoProcess;

            return ErrorCode.None;
        }
    }
    
    public abstract class WaiterBaseHandler : BaseHandler
    {
        protected WaiterBaseHandler(Environment env) : base(env)
        {
        }

        protected ErrorCode EnsureWaiter(Guid sessionId)
        {
            if (!EnsureSession(sessionId)) return ErrorCode.NoSessionId;
            if (_session.Waiter==null || _session.Waiter == TerminalSession.EmptyWaiter) return ErrorCode.NoWaiter;
            if (_session.Terminal==null /*|| _session.Terminal == TerminalSession.EmptyTerminal*/) return ErrorCode.NoTerminal;
            return ErrorCode.None;
        }
    }
    
    public abstract class BaseHandler
    {
        protected readonly Environment _env;
        protected TerminalSession _session;

        protected BaseHandler(Environment env)
        {
            _env = env;
        }

        protected bool EnsureSession(Guid sessionId)
        {
            _session = _env.TerminalSessions.FirstOrDefault(x => x.SessionId == sessionId);
            return _session!=null;
        }
    }
}