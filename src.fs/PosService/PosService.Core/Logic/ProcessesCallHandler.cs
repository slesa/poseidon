﻿using System;
using System.Collections.Generic;
using PosService.Core.Runtime;
using PosShare.Model;
using PosShare.Payloads;

namespace PosService.Core.Logic
{
    public class ProcessesCallHandler : WaiterBaseHandler, IHandleProcessesCalls
    {
        public ProcessesCallHandler(Environment env) : base(env)
        {
        }

        public Tuple<ErrorCode, IEnumerable<ProcessEntry>> GetTables(Guid sessionId)
        {
            var result = EnsureWaiter(sessionId);
            if(result!=ErrorCode.None) 
                return new Tuple<ErrorCode, IEnumerable<ProcessEntry>>(result, null);

            var processes = Processes.GetTables(_session.Waiter);
            return new Tuple<ErrorCode, IEnumerable<ProcessEntry>>(ErrorCode.None, processes);
        }

        public Tuple<ErrorCode, OpenProcess> OpenTable(Guid sessionId, int table, int party)
        {
            if(table<0 || party<0)
                return new Tuple<ErrorCode, OpenProcess>(ErrorCode.InvalidValue, null);
            var result = EnsureWaiter(sessionId);
            if(result!=ErrorCode.None) 
                return new Tuple<ErrorCode, OpenProcess>(result, null);

            var oldTable = _env.OpenProcesses.FindTable(table, party);
            if (oldTable != null)
                return new Tuple<ErrorCode, OpenProcess>(ErrorCode.AlreadyOpen, null);

            var ot = _env.OpenProcesses.OpenTable(table, party, _session.Terminal, _session.Waiter);
            return new Tuple<ErrorCode, OpenProcess>(ErrorCode.None, ot);
        }

        public Tuple<ErrorCode, IEnumerable<ProcessEntry>> GetCheckouts(Guid sessionId)
        {
            var result = EnsureWaiter(sessionId);
            if(result!=ErrorCode.None) 
                return new Tuple<ErrorCode, IEnumerable<ProcessEntry>>(result, null);

            var processes = Processes.GetCheckouts(_session.Waiter);
            return new Tuple<ErrorCode, IEnumerable<ProcessEntry>>(ErrorCode.None, processes);
        }

        public Tuple<ErrorCode, OpenProcess> OpenCheckout(Guid sessionId, int checkoutId)
        {
            if(checkoutId<0)
                return new Tuple<ErrorCode, OpenProcess>(ErrorCode.InvalidValue, null);
            var result = EnsureWaiter(sessionId);
            if(result!=ErrorCode.None) 
                return new Tuple<ErrorCode, OpenProcess>(result, null);

            if (checkoutId == 0) checkoutId = _env.NextCheckoutId();
            var oldCheckout = _env.OpenProcesses.FindCheckout(checkoutId);
            if (oldCheckout != null)
                return new Tuple<ErrorCode, OpenProcess>(ErrorCode.AlreadyOpen, null);

            var ot = _env.OpenProcesses.OpenCheckout(checkoutId, _session.Terminal, _session.Waiter);
            return new Tuple<ErrorCode, OpenProcess>(ErrorCode.None, ot);
        }

        public ErrorCode CloseProcess(Guid sessionId, Guid processId)
        {
            var result = EnsureWaiter(sessionId);
            if(result!=ErrorCode.None) 
                return result;

            var openProcess = _env.OpenProcesses.FindProcess(processId);
            if (openProcess == null)
                return ErrorCode.NoProcess;

            if (openProcess.OnTerminal != _session.Terminal.Id)
                return ErrorCode.WrongTerminal;

            if (openProcess.FromWaiter != _session.Waiter.Id)
                return ErrorCode.WrongWaiter;

            _env.OpenProcesses.CloseProcess(openProcess);
            return ErrorCode.None;
        }
    }
}