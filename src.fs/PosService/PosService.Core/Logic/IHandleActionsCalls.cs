using System;
using PosShare.CoreData;
using PosShare.Payloads;

namespace PosService.Core.Logic
{
    public interface IHandleActionsCalls
    {
        ErrorCode Order(Article article, OrderCommandPayload payload);
        ErrorCode Commit(Guid sessionId, Guid processId);
    }
}