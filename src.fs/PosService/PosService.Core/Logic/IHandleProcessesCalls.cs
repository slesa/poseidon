﻿using System;
using System.Collections.Generic;
using PosService.Core.Runtime;
using PosShare.Model;
using PosShare.Payloads;

namespace PosService.Core.Logic
{
    public interface IHandleProcessesCalls
    {
        Tuple<ErrorCode, IEnumerable<ProcessEntry>> GetTables(Guid sessionId);
        Tuple<ErrorCode, OpenProcess> OpenTable(Guid sessionId, int table, int party);
        
        Tuple<ErrorCode, IEnumerable<ProcessEntry>> GetCheckouts(Guid sessionId);
        Tuple<ErrorCode, OpenProcess> OpenCheckout(Guid sessionId, int checkoutId=0);

        ErrorCode CloseProcess(Guid sessionId, Guid processId);
    }
}