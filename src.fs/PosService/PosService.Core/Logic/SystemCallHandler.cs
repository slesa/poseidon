using System;
using System.Linq;
using System.Net;
using PosService.Core.Runtime;
using PosShare.CoreData;
using PosShare.Payloads;

namespace PosService.Core.Logic
{
    public class SystemCallHandler : BaseHandler, IHandleSystemCalls
    {
        public SystemCallHandler(Environment env) : base(env)
        {
        }

        public Guid RegisterTerminal(Terminal terminal, IPAddress ipAddress)
        {
            var oldTerminal = _env.TerminalSessions.FirstOrDefault(x => x.Terminal.Id == terminal.Id);
            if (oldTerminal != null)
            {
                if (oldTerminal.IpAddress.Equals(ipAddress)) return oldTerminal.SessionId;
                return Guid.Empty;
            }
            var sndTerminal = _env.TerminalSessions.FirstOrDefault(x => x.IpAddress == ipAddress);
            if (sndTerminal != null) return Guid.Empty;
            
            var newTerminal = new TerminalSession(ipAddress, terminal);
            _env.TerminalSessions.Add(newTerminal);
            return newTerminal.SessionId;
        }

        public void UnregisterTerminal(Guid sessionId)
        {
            var oldTerminal = _env.TerminalSessions.FirstOrDefault(x => x.SessionId == sessionId);
            if (oldTerminal == null) return;
            _env.TerminalSessions.Remove(oldTerminal);
        }

        public ErrorCode AutoLoginWaiter(Guid sessionId, Waiter waiter)
        {
            if (!base.EnsureSession(sessionId)) return ErrorCode.NoSessionId;
            // if (!waiter.CanLogin) return ErrorCode.Forbidden;
            _session.Waiter = waiter;
            return ErrorCode.None;
        }
        
        public ErrorCode LoginWaiter(Guid sessionId, Waiter waiter, string password)
        {
            if (!base.EnsureSession(sessionId)) return ErrorCode.NoSessionId;
            // if (!waiter.CanLogin) return ErrorCode.Forbidden;
            if (!string.IsNullOrEmpty(waiter.Password))
            {
                if (waiter.Password != password) return ErrorCode.Password;
            }
            _session.Waiter = waiter;
            return ErrorCode.None;
        }

        public ErrorCode LogoutWaiter(Guid sessionId)
        {
            if (!base.EnsureSession(sessionId)) return ErrorCode.NoSessionId;
            _session.Waiter = TerminalSession.EmptyWaiter;
            return ErrorCode.None;
        }
    }
}