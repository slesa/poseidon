using System;
using System.Net;
using PosShare.CoreData;
using PosShare.Payloads;

namespace PosService.Core.Logic
{
    public interface IHandleSystemCalls
    {
        Guid RegisterTerminal(Terminal terminal, IPAddress ipAddress);
        void UnregisterTerminal(Guid sessionId);
        ErrorCode AutoLoginWaiter(Guid sessionId, Waiter waiter);
        ErrorCode LoginWaiter(Guid sessionId, Waiter waiter, string password);
        ErrorCode LogoutWaiter(Guid sessionId);
    }
}