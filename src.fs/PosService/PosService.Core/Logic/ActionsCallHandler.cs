using System;
using PosService.Core.CoreData;
using PosShare.CoreData;
using PosShare.Model;
using PosShare.Payloads;

namespace PosService.Core.Logic
{
    public class ActionsCallHandler : ProcessBaseHandler, IHandleActionsCalls
    {
        readonly IProvideCoreData _data;

        public ActionsCallHandler(IProvideCoreData dataProvider, Environment env) : base(env)
        {
            _data = dataProvider;
        }
        
        public ErrorCode Order(Article article, OrderCommandPayload payload)
        {
            var errorCode = EnsureProcess(payload.SessionId, payload.ProcessId);
            if (errorCode != ErrorCode.None) return errorCode;

            var orderPayload = new OrderPayload();
            errorCode = HandleOrder(article, payload, orderPayload);
            payload.CurrentOrderPayload = orderPayload;
            if (errorCode != ErrorCode.None) return errorCode;

            if (orderPayload.HasCallbacks)
            {
                return ErrorCode.MissingData;
                /*var result = JSonShare.Serialize(order);
                // sender.SendEvent(EventPayload.EvtOrderMissing, result);
                return;*/
            }

            AddOrderAction(_session.Terminal, _session.Waiter, orderPayload);
            return ErrorCode.None;
        }

        public ErrorCode Commit(Guid sessionId, Guid processId)
        {
            var errorCode = EnsureProcess(sessionId, processId);
            if (errorCode != ErrorCode.None) return errorCode;
            return ErrorCode.Forbidden;            
        }

        void AddOrderAction(Terminal terminal, Waiter waiter, OrderPayload order)
        {
            var action = new OrderedAction(DateTime.Now)
                .WithPayload(order)
                .WithTerminal(terminal.Id, terminal.Name)
                .WithWaiter(waiter.Id, waiter.Name)
                .WithActionId(Guid.NewGuid());
            _process.Process.Actions.Add(action);
        }
        
        ErrorCode HandleOrder(Article article, OrderCommandPayload payload, OrderPayload orderProgress)
        {
            if (article == null) return ErrorCode.InvalidValue;

            var currentLine = orderProgress.OrderLines.Count; 
            var orderLine = new OrderLine()
                .WithCount(payload.Count).WithPlu(payload.Plu)
                .WithIsFreePrice(article.IsFreePrice)
                .WithIsFreeText(article.IsFreeText)
                .WithIsWeight(article.IsWeight)
                .WithIsCutGood(article.IsCutGood)
                .WithIsAreaGood(article.IsCutGood);
            
            #region Free Price
            if (!orderLine.IsFreePrice)
                orderLine = orderLine.WithPrice(article.Price);
            else    
            {
                if (payload?.Price!=null)
                    orderLine = orderLine.WithPrice(payload.Price.Value);
                else
                {
                    var freePrice = new OrderAskPrice(currentLine, article.Price);
                    orderProgress.AskItems.Add(freePrice);
                }
            }
            #endregion Free Price
            #region Free Text
            if (!orderLine.IsFreeText)
                orderLine = orderLine.WithArticle(article.Name);
            else
            {
                if (payload?.Article != null)
                    orderLine = orderLine.WithArticle(payload.Article);
                else
                {
                    var freeText = new OrderAskArticleText(currentLine, article.Name);
                    orderProgress.AskItems.Add(freeText);
                }
            }
            #endregion Free Text
            #region Weight
            if (orderLine.IsWeight)
            {
                if (payload?.Weight != null)
                    orderLine = orderLine.WithWeight(payload.Weight.Value);
                else
                {
                    var askWeight = new OrderAskWeight(currentLine);
                    orderProgress.AskItems.Add(askWeight);
                }
            }
            #endregion Weight
            #region Cut and Area good - length
            if (article.IsCutGood || article.IsAreaGood)
            {
                if (payload?.Length != null)
                    orderLine = orderLine.WithLength(payload.Length.Value);
                else
                {
                    var askLength = new OrderAskLength(currentLine);
                    orderProgress.AskItems.Add(askLength);
                }
            }
            #endregion Cut and Area good - length
            #region Area good - width
            if (article.IsAreaGood)
            {
                if (payload?.Width != null)
                    orderLine = orderLine.WithWidth(payload.Width.Value);
                else
                {
                    var askWidth = new OrderAskWidth(currentLine);
                    orderProgress.AskItems.Add(askWidth);
                }
            }
            #endregion Area good - width
            #region Constraints
            if (article.Constraint != null)
            {
                var constraint = article.Constraint;
                foreach (var level in constraint.Levels)
                {
                    // if (!level.IsActive) continue;
                    var askConstraint = new OrderAskConstraint(currentLine, article.Constraint);
                    foreach (var entry in level.Entries)
                    {
                        var choice = new ConstraintChoice
                            {Plu = entry.Plu, IsHint = entry.IsHint, Price = entry.Price, UsePrice = entry.UsePrice};
                        var art = _data.Articles.FindArticle(entry.Plu);
                        if (art != null) choice.Article = art.Name;
                        askConstraint.Choices.Add(choice);
                    }
                    orderProgress.AskItems.Add(askConstraint);
                }
            }
            #endregion Constraints
            orderProgress.OrderLines.Add(orderLine);

            return ErrorCode.None;
        }
    }
}