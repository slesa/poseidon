﻿using Machine.Specifications;
using PosService.Core.Config;
using PosService.Core.Settings;

// ReSharper disable InconsistentNaming

namespace PosService.Core.Specs.Config
{
    [Subject(typeof(RestSettingsReader))]
    internal class Wenn_RestSettings_ohne_ConfigFile_gelesen_werden
    {
        Establish context = () => _sut = new RestSettingsReader();

        Because of = () => _settings = _sut.ReadRestSettings("");

        It soll_der_Default_Host_gesetzt_sein = () => _settings.Host.ShouldEqual(RestSettingsReader.DefaultHost);
        It soll_der_Default_Port_gesetzt_sein = () => _settings.Port.ShouldEqual(RestSettingsReader.DefaultPort);
        
        static RestSettingsReader _sut;
        static RestServerSettings _settings;
    }

    
    [Subject(typeof(RestSettingsReader))]
    internal class Wenn_RestSettings_mit_ConfigFile_gelesen_werden
    {
        Establish context = () => _sut = new RestSettingsReader();

        Because of = () => _settings = _sut.ReadRestSettings(ConfigContent);

        It soll_der_Host_gelesen_sein = () => _settings.Host.ShouldEqual("192.168.1.42");
        It soll_der_Port_gelesen_sein = () => _settings.Port.ShouldEqual(43);
        
        static RestSettingsReader _sut;
        static RestServerSettings _settings;
        static string ConfigContent =
@"[Server]
  RestHost = 192.168.1.42
  RestPort = 43
";
    }
}