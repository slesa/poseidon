﻿using System;
using System.Net;
using Machine.Specifications;
using PosService.Core.CoreData;
using PosService.Core.Logic;
using PosService.Core.Runtime;
using PosService.Core.Specs.TestData;
using PosShare.CoreData;
using PosShare.Model;
using PosShare.Payloads;
// ReSharper disable InconsistentNaming

namespace PosService.Core.Specs.Logic
{
    #region Order
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_bestellt_wird_ohne_Session : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            // _env.TerminalSessions.Clear();
            _article = _data.Articles.FindArticle(100);
            _payload = new OrderCommandPayload() { SessionId = Guid.Empty, ProcessId = _process.Process.ProcessId };
        };
        Because of = () => _result = _sut.Order(_article, _payload);
        It soll_fehlende_Session_melden = () => _result.ShouldEqual(ErrorCode.NoSessionId);
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_bestellt_wird_ohne_Terminal : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _session.Terminal = null;
            _article = _data.Articles.FindArticle(100);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId };
        };
        Because of = () => _result = _sut.Order(_article, _payload);
        It soll_fehlendes_Terminal_melden = () => _result.ShouldEqual(ErrorCode.NoTerminal);
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_bestellt_wird_ohne_Kellner : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _session.Waiter = null;
            _article = _data.Articles.FindArticle(100);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId };
        };
        Because of = () => _result = _sut.Order(_article, _payload);
        It soll_fehlendes_Terminal_melden = () => _result.ShouldEqual(ErrorCode.NoWaiter);
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_bestellt_wird_ohne_Artikel : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId };
        };
        Because of = () => _result = _sut.Order(null, _payload);
        It soll_fehlenden_Wert_melden = () => _result.ShouldEqual(ErrorCode.InvalidValue);
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_bestellt_wird : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _article = _data.Articles.FindArticle(100);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId };
        };

        Because of = () => _result = _sut.Order(_article, _payload);

        It soll_keinen_Fehler_melden = () => _result.ShouldEqual(ErrorCode.None);
        It soll_den_Artikel_bestellen = () => _process.Process.Actions.Count.ShouldEqual(1);
        It soll_die_ActionId_setzen = () => ((OrderedAction)_process.Process.Actions[0]).ActionId.ShouldNotEqual(Guid.Empty);
        It soll_den_ActionType_setzen = () => ((OrderedAction)_process.Process.Actions[0]).Type.ShouldEqual(ActionType.Order);
        It soll_die_Action_auf_uncommitted_setzen = () => ((OrderedAction)_process.Process.Actions[0]).Committed.ShouldBeFalse();
        It soll_den_TimeStamp_setzen = () => ((OrderedAction)_process.Process.Actions[0]).Timestamp.AddSeconds(2).ShouldBeGreaterThanOrEqualTo(DateTime.Now);
        It soll_das_Terminal_setzen = () => ((OrderedAction)_process.Process.Actions[0]).Terminal.ShouldEqual(_session.Terminal.Id);
        It soll_den_TerminalNamen_setzen = () => ((OrderedAction)_process.Process.Actions[0]).TerminalName.ShouldEqual(_session.Terminal.Name);
        It soll_den_Waiter_setzen = () => ((OrderedAction)_process.Process.Actions[0]).Waiter.ShouldEqual(_session.Waiter.Id);
        It soll_den_WaiterNamen_setzen = () => ((OrderedAction)_process.Process.Actions[0]).WaiterName.ShouldEqual(_session.Waiter.Name);
        
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    #endregion Order
    
    
    #region Specbase
    internal class ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            _data = new TestDataProvider();
            _env = new Environment();
            _sut = new ActionsCallHandler(_data, _env);
        };

        protected static void EnsureProcess(int table=1, int party=1)
        {
            _process = _env.OpenProcesses.OpenTable(table, party, _terminal, _waiter);
        }
        
        protected static void EnsureTerminal(int withTerm=1, int withWaiter=1)
        {
            _terminal = withTerm!=0 ? _data.Terminals.FindTerminal(withTerm) : null;
            _session = new TerminalSession(IPAddress.Broadcast, _terminal);
            if (withWaiter!=0)
            {
                _waiter = _data.Waiters.FindWaiter(withWaiter);
                _session.Waiter = _waiter;
            }
            _env.TerminalSessions.Add(_session);
        }
        
        protected static ActionsCallHandler _sut;
        protected static IProvideCoreData _data;
        protected static Environment _env;
        protected static TerminalSession _session;
        protected static Terminal _terminal;
        protected static Waiter _waiter;
        protected static OpenProcess _process;
    }
    #endregion Specbase
}