﻿using System;
using System.Linq;
using System.Net;
using Machine.Specifications;
using PosService.Core.CoreData;
using PosService.Core.Logic;
using PosService.Core.Runtime;
using PosService.Core.Specs.TestData;
using PosShare.CoreData;
using PosShare.Payloads;
// ReSharper disable InconsistentNaming

namespace PosService.Core.Specs.Logic
{
    #region Free Price
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_mit_freiem_Preis_bestellt_wird : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _article = _data.Articles.FindArticle(TestArticles.FreePriceId);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId };
        };

        Because of = () => _result = _sut.Order(_article, _payload);

        It soll_fehlende_Daten_melden = () => _result.ShouldEqual(ErrorCode.MissingData);
        It soll_den_Artikel_nicht_bestellen = () => _process.Process.Actions.Count.ShouldEqual(0);
        It soll_nach_Preis_fragen_lassen = () => _payload.CurrentOrderPayload.AskItems.First().Type.ShouldEqual(OrderAskType.Price);
        
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_mit_freiem_Preis_bestellt_wird_mit_Preis : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _article = _data.Articles.FindArticle(TestArticles.FreePriceId);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId, Price = 42};
        };

        Because of = () => _result = _sut.Order(_article, _payload);

        It soll_keinen_Fehler_melden = () => _result.ShouldEqual(ErrorCode.None);
        It soll_den_Artikel_bestellen = () => _process.Process.Actions.Count.ShouldEqual(1);
        
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    #endregion Free Price
    
    #region Free Text
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_mit_freiem_Text_bestellt_wird : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _article = _data.Articles.FindArticle(TestArticles.FreeTextId);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId };
        };

        Because of = () => _result = _sut.Order(_article, _payload);

        It soll_fehlende_Daten_melden = () => _result.ShouldEqual(ErrorCode.MissingData);
        It soll_den_Artikel_nicht_bestellen = () => _process.Process.Actions.Count.ShouldEqual(0);
        It soll_nach_Text_fragen_lassen = () => _payload.CurrentOrderPayload.AskItems.First().Type.ShouldEqual(OrderAskType.Text);
        
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_mit_freiem_Text_bestellt_wird_mit_Text : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _article = _data.Articles.FindArticle(TestArticles.FreeTextId);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId, Article = "Entered Text" };
        };

        Because of = () => _result = _sut.Order(_article, _payload);

        It soll_keinen_Fehler_melden = () => _result.ShouldEqual(ErrorCode.None);
        It soll_den_Artikel_bestellen = () => _process.Process.Actions.Count.ShouldEqual(1);
        
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    #endregion Free Text
    
    #region Free Text
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_mit_Gewicht_bestellt_wird : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _article = _data.Articles.FindArticle(TestArticles.WeightId);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId };
        };

        Because of = () => _result = _sut.Order(_article, _payload);

        It soll_fehlende_Daten_melden = () => _result.ShouldEqual(ErrorCode.MissingData);
        It soll_den_Artikel_nicht_bestellen = () => _process.Process.Actions.Count.ShouldEqual(0);
        It soll_nach_Gewicht_fragen_lassen = () => _payload.CurrentOrderPayload.AskItems.First().Type.ShouldEqual(OrderAskType.Weight);
        
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_mit_Gewicht_bestellt_wird_mit_Gewicht : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _article = _data.Articles.FindArticle(TestArticles.WeightId);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId, Weight = 4.2M };
        };

        Because of = () => _result = _sut.Order(_article, _payload);

        It soll_keinen_Fehler_melden = () => _result.ShouldEqual(ErrorCode.None);
        It soll_den_Artikel_bestellen = () => _process.Process.Actions.Count.ShouldEqual(1);
        
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    #endregion Free Text
    
    #region Free Text
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_mit_Länge_bestellt_wird : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _article = _data.Articles.FindArticle(TestArticles.CutGoodId);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId };
        };

        Because of = () => _result = _sut.Order(_article, _payload);

        It soll_fehlende_Daten_melden = () => _result.ShouldEqual(ErrorCode.MissingData);
        It soll_den_Artikel_nicht_bestellen = () => _process.Process.Actions.Count.ShouldEqual(0);
        It soll_nach_Länge_fragen_lassen = () => _payload.CurrentOrderPayload.AskItems.First().Type.ShouldEqual(OrderAskType.Length);
        
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_mit_Länge_bestellt_wird_mit_Länge : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _article = _data.Articles.FindArticle(TestArticles.CutGoodId);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId, Length = 4.2M };
        };

        Because of = () => _result = _sut.Order(_article, _payload);

        It soll_keinen_Fehler_melden = () => _result.ShouldEqual(ErrorCode.None);
        It soll_den_Artikel_bestellen = () => _process.Process.Actions.Count.ShouldEqual(1);
        
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    #endregion Free Text
    
    #region Free Text
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_mit_Fläche_bestellt_wird : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _article = _data.Articles.FindArticle(TestArticles.AreaGoodId);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId };
        };

        Because of = () => _result = _sut.Order(_article, _payload);

        It soll_fehlende_Daten_melden = () => _result.ShouldEqual(ErrorCode.MissingData);
        It soll_den_Artikel_nicht_bestellen = () => _process.Process.Actions.Count.ShouldEqual(0);
        It soll_nach_Länge_fragen_lassen = () => _payload.CurrentOrderPayload.AskItems.First().Type.ShouldEqual(OrderAskType.Length);
        It soll_nach_Breite_fragen_lassen = () => _payload.CurrentOrderPayload.AskItems.Skip(1).First().Type.ShouldEqual(OrderAskType.Width);
        
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_mit_Fläche_bestellt_wird_mit_Länge_und_Breite : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _article = _data.Articles.FindArticle(TestArticles.AreaGoodId);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId, Length = 4.2M, Width = 3.12M };
        };

        Because of = () => _result = _sut.Order(_article, _payload);

        It soll_keinen_Fehler_melden = () => _result.ShouldEqual(ErrorCode.None);
        It soll_den_Artikel_bestellen = () => _process.Process.Actions.Count.ShouldEqual(1);
        
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    #endregion Free Text
    
    
    #region Constraint
    
    [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_mit_Zwang_bestellt_wird : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _article = _data.Articles.FindArticle(TestArticles.Constraint1Id);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId };
        };

        Because of = () => _result = _sut.Order(_article, _payload);

        It soll_fehlende_Daten_melden = () => _result.ShouldEqual(ErrorCode.MissingData);
        It soll_den_Artikel_nicht_bestellen = () => _process.Process.Actions.Count.ShouldEqual(0);
        It soll_nach_Zang1_fragen_lassen = () => _payload.CurrentOrderPayload.AskItems.Count.ShouldEqual(1);
        It soll_nach_Zwang_fragen_lassen = () => _payload.CurrentOrderPayload.AskItems.First().Type.ShouldEqual(OrderAskType.Constraint);
        // It soll_nach_Breite_fragen_lassen = () => _payload.CurrentOrderPayload.AskItems.Skip(1).First().Type.ShouldEqual(OrderAskType.Width);
        
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    }
    
    /* [Subject(typeof(ActionsCallHandler))]
    internal class Wenn_im_Actionshandler_ein_Artikel_mit_Fläche_bestellt_wird_mit_Länge_und_Breite : ActionsCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            EnsureProcess();
            _article = _data.Articles.FindArticle(TestArticles.AreaGoodId);
            _payload = new OrderCommandPayload() { SessionId = _session.SessionId, ProcessId = _process.Process.ProcessId, Length = 4.2M, Width = 3.12M };
        };

        Because of = () => _result = _sut.Order(_article, _payload);

        It soll_keinen_Fehler_melden = () => _result.ShouldEqual(ErrorCode.None);
        It soll_den_Artikel_bestellen = () => _process.Process.Actions.Count.ShouldEqual(1);
        
        static Article _article;
        static OrderCommandPayload _payload;
        static ErrorCode _result;
    } */
    
    #endregion Constraint
    
}
