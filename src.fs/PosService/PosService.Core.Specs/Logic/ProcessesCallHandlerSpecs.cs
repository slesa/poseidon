﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Machine.Specifications;
using PosService.Core.Logic;
using PosService.Core.Runtime;
using PosShare.CoreData;
using PosShare.Model;
using PosShare.Payloads;
// ReSharper disable InconsistentNaming

namespace PosService.Core.Specs.Logic
{
    #region Get Tables
    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_noch_keine_Tische_reserviert_sind : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal();
        Because of = () => _result = _sut.GetTables(_session.SessionId);
        It soll_keinen_Fehler_melden = () => _result.Item1.ShouldEqual(ErrorCode.None);
        It soll_eine_leere_Prozessliste_liefern = () => _result.Item2.ShouldBeEmpty();

        static Tuple<ErrorCode,IEnumerable<ProcessEntry>> _result;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_Tische_ermittelt_werden_ohne_Session : ProcessesCallHandlerSpecsBase
    {
        Because of = () => _result = _sut.GetTables(Guid.Empty);
        It soll_fehlende_Session_melden = () => _result.Item1.ShouldEqual(ErrorCode.NoSessionId);

        static Tuple<ErrorCode,IEnumerable<ProcessEntry>> _result;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_Tische_ermittelt_werden_ohne_Terminal : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal(false,false);
        Because of = () => _result = _sut.GetTables(_session.SessionId);
        It soll_fehlenden_Kellner_melden = () => _result.Item1.ShouldEqual(ErrorCode.NoWaiter);

        static Tuple<ErrorCode,IEnumerable<ProcessEntry>> _result;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_Tische_ermittelt_werden_ohne_Kellner : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal(true,false);
        Because of = () => _result = _sut.GetTables(_session.SessionId);
        It soll_fehlenden_Kellner_melden = () => _result.Item1.ShouldEqual(ErrorCode.NoWaiter);

        static Tuple<ErrorCode,IEnumerable<ProcessEntry>> _result;
    }
    #endregion Get Tables

    #region Get Checkouts
    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_noch_keine_Checkouts_reserviert_sind : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal();
        Because of = () => _result = _sut.GetCheckouts(_session.SessionId);
        It soll_keinen_Fehler_melden = () => _result.Item1.ShouldEqual(ErrorCode.None);
        It soll_eine_leere_Prozessliste_liefern = () => _result.Item2.ShouldBeEmpty();

        static Tuple<ErrorCode,IEnumerable<ProcessEntry>> _result;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_Checkouts_ermittelt_werden_ohne_Session : ProcessesCallHandlerSpecsBase
    {
        Because of = () => _result = _sut.GetCheckouts(Guid.Empty);
        It soll_fehlende_Session_melden = () => _result.Item1.ShouldEqual(ErrorCode.NoSessionId);

        static Tuple<ErrorCode,IEnumerable<ProcessEntry>> _result;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_Checkouts_ermittelt_werden_ohne_Terminal : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal(false,false);
        Because of = () => _result = _sut.GetCheckouts(_session.SessionId);
        It soll_fehlenden_Kellner_melden = () => _result.Item1.ShouldEqual(ErrorCode.NoWaiter);

        static Tuple<ErrorCode,IEnumerable<ProcessEntry>> _result;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_Checkouts_ermittelt_werden_ohne_Kellner : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal(true,false);
        Because of = () => _result = _sut.GetCheckouts(_session.SessionId);
        It soll_fehlenden_Kellner_melden = () => _result.Item1.ShouldEqual(ErrorCode.NoWaiter);

        static Tuple<ErrorCode,IEnumerable<ProcessEntry>> _result;
    }
    #endregion Get Checkouts

    #region Open Table
    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Tisch_geöfnet_wird : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal();
        Because of = () => { _result = _sut.OpenTable(_session.SessionId, 3, 13); _process = _result.Item2; }; 
        It soll_keinen_Fehler_melden = () => _result.Item1.ShouldEqual(ErrorCode.None);
        It soll_das_öffnende_Terminal_setzen = () => _process.OnTerminal.ShouldEqual(_terminal.Id);
        It soll_den_öffnenden_Kellner_setzen = () => _process.FromWaiter.ShouldEqual(_waiter.Id);
        It soll_den_Tisch_setzen = () => ((TableProcess)_process.Process).Table.ShouldEqual(3);
        It soll_die_Partei_setzen = () => ((TableProcess)_process.Process).Party.ShouldEqual(13);
        It soll_einen_leeren_Prozess_liefern = () => _process.Process.Actions.ShouldBeEmpty();
        It soll_einen_Createvorgang_anlegen = () => _process.Process.Created.ShouldNotBeNull();
        It soll_im_Createvorgang_das_Terminal_setzen = () => _process.Process.Created.Terminal.ShouldEqual(_terminal.Id);
        It soll_im_Createvorgang_den_Terminalnamen_setzen = () => _process.Process.Created.TerminalName.ShouldEqual(_terminal.Name);
        It soll_im_Createvorgang_den_Kellner_setzen = () => _process.Process.Created.Waiter.ShouldEqual(_waiter.Id);
        It soll_im_Createvorgang_den_Kellnernamen_setzen = () => _process.Process.Created.WaiterName.ShouldEqual(_waiter.Name);
        It soll_im_Createvorgang_den_Timestamp_setzen = () => _process.Process.Created.Timestamp.Date.ShouldEqual(DateTime.Today);

        static Tuple<ErrorCode, OpenProcess> _result;
        static OpenProcess _process;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Tisch_geöfnet_wird_ohne_Session : ProcessesCallHandlerSpecsBase
    {
        Because of = () => _result = _sut.OpenTable(Guid.Empty, 3, 1);
        It soll_fehlende_Session_melden = () => _result.Item1.ShouldEqual(ErrorCode.NoSessionId);
        static Tuple<ErrorCode, OpenProcess> _result;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Tisch_geöfnet_wird_ohne_Terminal : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal(false,false);
        Because of = () => _result = _sut.OpenTable(_session.SessionId, 3, 1);
        It soll_fehlenden_Kellner_melden = () => _result.Item1.ShouldEqual(ErrorCode.NoWaiter);
        static Tuple<ErrorCode, OpenProcess> _result;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Tisch_geöfnet_wird_ohne_Kellner : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal(true,false);
        Because of = () => _result = _sut.OpenTable(_session.SessionId, 3, 1);
        It soll_fehlenden_Kellner_melden = () => _result.Item1.ShouldEqual(ErrorCode.NoWaiter);
        static Tuple<ErrorCode, OpenProcess> _result;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Tisch_geöfnet_wird_mit_invalidem_Tisch : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal();
        Because of = () => { _result = _sut.OpenTable(_session.SessionId, -3, 13); }; 
        It soll_ungültigen_Tisch_melden = () => _result.Item1.ShouldEqual(ErrorCode.InvalidValue);
        static Tuple<ErrorCode, OpenProcess> _result;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Tisch_geöfnet_wird_mit_invalider_Partei : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal();
        Because of = () => { _result = _sut.OpenTable(_session.SessionId, 3, -13); }; 
        It soll_ungültigen_Tisch_melden = () => _result.Item1.ShouldEqual(ErrorCode.InvalidValue);
        static Tuple<ErrorCode, OpenProcess> _result;
    }
    #endregion Open Table

    #region Open Checkout
    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Checkout_geöfnet_wird : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal();
        Because of = () => { _result = _sut.OpenCheckout(_session.SessionId, 3); _process = _result.Item2; }; 
        It soll_keinen_Fehler_melden = () => _result.Item1.ShouldEqual(ErrorCode.None);
        It soll_das_öffnende_Terminal_setzen = () => _process.OnTerminal.ShouldEqual(_terminal.Id);
        It soll_den_öffnenden_Kellner_setzen = () => _process.FromWaiter.ShouldEqual(_waiter.Id);
        It soll_die_CheckoutId_setzen = () => ((CheckoutProcess)_process.Process).Id.ShouldEqual(3);
        It soll_einen_leeren_Prozess_liefern = () => _process.Process.Actions.ShouldBeEmpty();
        It soll_einen_Createvorgang_anlegen = () => _process.Process.Created.ShouldNotBeNull();
        It soll_im_Createvorgang_das_Terminal_setzen = () => _process.Process.Created.Terminal.ShouldEqual(_terminal.Id);
        It soll_im_Createvorgang_den_Terminalnamen_setzen = () => _process.Process.Created.TerminalName.ShouldEqual(_terminal.Name);
        It soll_im_Createvorgang_den_Kellner_setzen = () => _process.Process.Created.Waiter.ShouldEqual(_waiter.Id);
        It soll_im_Createvorgang_den_Kellnernamen_setzen = () => _process.Process.Created.WaiterName.ShouldEqual(_waiter.Name);
        It soll_im_Createvorgang_den_Timestamp_setzen = () => _process.Process.Created.Timestamp.Date.ShouldEqual(DateTime.Today);

        static Tuple<ErrorCode, OpenProcess> _result;
        static OpenProcess _process;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Checkout_ohne_Id_geöfnet_wird : ProcessesCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            _env._checkoutId = 12;
        };
        Because of = () => { _result = _sut.OpenCheckout(_session.SessionId, 0); _process = _result.Item2; }; 
        It soll_keinen_Fehler_melden = () => _result.Item1.ShouldEqual(ErrorCode.None);
        It soll_die_CheckoutId_setzen = () => ((CheckoutProcess)_process.Process).Id.ShouldEqual(13);

        static Tuple<ErrorCode, OpenProcess> _result;
        static OpenProcess _process;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Checkout_geöfnet_wird_ohne_Session : ProcessesCallHandlerSpecsBase
    {
        Because of = () => _result = _sut.OpenCheckout(Guid.Empty, 3);
        It soll_fehlende_Session_melden = () => _result.Item1.ShouldEqual(ErrorCode.NoSessionId);
        static Tuple<ErrorCode, OpenProcess> _result;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Checkout_geöfnet_wird_ohne_Terminal : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal(false,true);
        Because of = () => _result = _sut.OpenCheckout(_session.SessionId, 3);
        It soll_fehlendes_Terminal_melden = () => _result.Item1.ShouldEqual(ErrorCode.NoTerminal);
        static Tuple<ErrorCode, OpenProcess> _result;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Checkout_geöfnet_wird_ohne_Kellner : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal(true,false);
        Because of = () => _result = _sut.OpenCheckout(_session.SessionId, 3);
        It soll_fehlenden_Kellner_melden = () => _result.Item1.ShouldEqual(ErrorCode.NoWaiter);
        static Tuple<ErrorCode, OpenProcess> _result;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Checkout_geöfnet_wird_mit_invalidem_Checkout : ProcessesCallHandlerSpecsBase
    {
        Establish context = () => EnsureTerminal();
        Because of = () => { _result = _sut.OpenCheckout(_session.SessionId, -3); }; 
        It soll_ungültigen_Checkout_melden = () => _result.Item1.ShouldEqual(ErrorCode.InvalidValue);
        static Tuple<ErrorCode, OpenProcess> _result;
    }
    #endregion Open Checkout


    #region Close Process
    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Vorgang_geschlossen_wird : ProcessesCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            _process = _sut.OpenCheckout(_session.SessionId, 1).Item2;
        };
        Because of = () => { _result = _sut.CloseProcess(_session.SessionId, _process.Process.ProcessId); }; 
        It soll_keinen_Fehler_melden = () => _result.ShouldEqual(ErrorCode.None);
        It soll_den_Vorgang_schliessen = () => _env.OpenProcesses.FindCheckout(1).ShouldBeNull();
        static ErrorCode _result;
        static OpenProcess _process;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Vorgang_geschlossen_wird_ohne_Session : ProcessesCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            _process = _sut.OpenCheckout(_session.SessionId, 1).Item2;
        };
        Because of = () => _result = _sut.CloseProcess(Guid.Empty, _process.Process.ProcessId);
        It soll_fehlende_Session_melden = () => _result.ShouldEqual(ErrorCode.NoSessionId);
        static ErrorCode _result;
        static OpenProcess _process;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Vorgang_geschlossen_wird_ohne_Terminal : ProcessesCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            _process = _sut.OpenCheckout(_session.SessionId, 1).Item2;
            _env.TerminalSessions.First().Terminal = null;
        };
        Because of = () => _result = _sut.CloseProcess(_session.SessionId, _process.Process.ProcessId);
        It soll_fehlendes_Terminal_melden = () => _result.ShouldEqual(ErrorCode.NoTerminal);
        static ErrorCode _result;
        static OpenProcess _process;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Vorgang_geschlossen_wird_mit_anderem_Terminal : ProcessesCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            _process = _sut.OpenCheckout(_session.SessionId, 1).Item2;
            _env.TerminalSessions.First().Terminal = new Terminal() { Id=42 };
        };
        Because of = () => _result = _sut.CloseProcess(_session.SessionId, _process.Process.ProcessId);
        It soll_fehlendes_Terminal_melden = () => _result.ShouldEqual(ErrorCode.WrongTerminal);
        static ErrorCode _result;
        static OpenProcess _process;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Vorgang_geschlossen_wird_ohne_Kellner : ProcessesCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            _process = _sut.OpenCheckout(_session.SessionId, 1).Item2;
            _env.TerminalSessions.First().Waiter = null;
        };
        Because of = () => _result = _sut.CloseProcess(_session.SessionId, _process.Process.ProcessId);
        It soll_fehlenden_Kellner_melden = () => _result.ShouldEqual(ErrorCode.NoWaiter);
        static ErrorCode _result;
        static OpenProcess _process;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Vorgang_geschlossen_wird_mit_anderem_Kellner : ProcessesCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            _process = _sut.OpenCheckout(_session.SessionId, 1).Item2;
            _env.TerminalSessions.First().Waiter = new Waiter() { Id = 42 };
        };
        Because of = () => _result = _sut.CloseProcess(_session.SessionId, _process.Process.ProcessId);
        It soll_fehlenden_Kellner_melden = () => _result.ShouldEqual(ErrorCode.WrongWaiter);
        static ErrorCode _result;
        static OpenProcess _process;
    }

    [Subject(typeof(ProcessesCallHandler))]
    internal class Wenn_im_ProcessHandler_ein_Vorgang_geschlossen_wird_ohne_ProcessId : ProcessesCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            EnsureTerminal();
            _process = _sut.OpenCheckout(_session.SessionId, 1).Item2;
        };
        Because of = () => { _result = _sut.CloseProcess(_session.SessionId, Guid.NewGuid()); }; 
        It soll_ungültigen_Vorgang_melden = () => _result.ShouldEqual(ErrorCode.NoProcess);
        static ErrorCode _result;
        static OpenProcess _process;
    }
    #endregion Open Table

    
    #region Specbase
    internal class ProcessesCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            _env = new Environment();
            _sut = new ProcessesCallHandler(_env);
        };

        protected static void EnsureTerminal(bool withTerm=true, bool withWaiter=true)
        {
            _terminal = withTerm ? new Terminal() { Id = 1, Name = "Terminal 1" } : null;
            _session = new TerminalSession(IPAddress.Broadcast, _terminal);
            if (withWaiter)
            {
                _waiter = new Waiter() { Id = 2, Name = "Waitress 2"};
                _session.Waiter = _waiter;
            }
            _env.TerminalSessions.Add(_session);
        }
        
        protected static ProcessesCallHandler _sut;
        protected static Environment _env;
        protected static TerminalSession _session;
        protected static Terminal _terminal;
        protected static Waiter _waiter;
    }
    #endregion Specbase
}