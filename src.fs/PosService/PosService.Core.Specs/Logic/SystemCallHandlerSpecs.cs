﻿using System;
using System.Linq;
using System.Net;
using Machine.Specifications;
using PosService.Core.Logic;
using PosService.Core.Runtime;
using PosShare.CoreData;
using PosShare.Payloads;
// ReSharper disable InconsistentNaming

namespace PosService.Core.Specs.Logic
{
    #region Terminal
    
    [Subject(typeof(SystemCallHandler))]
    internal class When_registering_terminal : SystemCallHandlerSpecsBase
    {
        Because of = () => { _result = _sut.RegisterTerminal(_term, IPAddress.Any); };
        
        It should_register_terminal = () => _result.ShouldNotEqual(Guid.Empty);
        It should_add_terminalsession = () => _env.TerminalSessions.ShouldContain(x => x.SessionId == _result);
        
        static Guid _result;
    }

    
    [Subject(typeof(SystemCallHandler))]
    internal class When_registering_terminal_twice : SystemCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            _ip = IPAddress.Parse("192.168.1.1");
            _result1 = _sut.RegisterTerminal(_term, _ip);
        };
        
        Because of = () => _result2 = _sut.RegisterTerminal(_term, _ip);
        
        It should_take_old_registration = () => _result1.ShouldEqual(_result2);
        It should_add_terminalsession = () => _env.TerminalSessions.ShouldContain(x => x.SessionId == _result1);
        
        static Guid _result1;
        static Guid _result2;
        static IPAddress _ip;
    }

    
    [Subject(typeof(SystemCallHandler))]
    internal class When_registering_terminal_with_other_ip : SystemCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            _ip1 = IPAddress.Parse("192.168.1.1");
            _ip2 = IPAddress.Parse("192.168.1.42");
            _result1 = _sut.RegisterTerminal(_term, _ip1);
        };
        
        Because of = () => _result2 = _sut.RegisterTerminal(_term, _ip2);
        
        It should_fail_registration = () => _result2.ShouldEqual(Guid.Empty);
        It should_add_terminalsession1 = () => _env.TerminalSessions.ShouldContain(x => x.SessionId == _result1);
        It should_not_add_terminalsession2 = () => _env.TerminalSessions.ShouldNotContain(x => x.SessionId == _result2);
        
        static Guid _result1;
        static Guid _result2;
        static IPAddress _ip1;
        static IPAddress _ip2;
    }
    
    
    [Subject(typeof(SystemCallHandler))]
    internal class When_registering_terminal_with_other_id : SystemCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            _ip = IPAddress.Parse("192.168.1.1");
            _result1 = _sut.RegisterTerminal(_term, _ip);
            _term2 = new Terminal { Id = 42, Name = "Terminal 42" };
        };
        
        Because of = () => _result2 = _sut.RegisterTerminal(_term2, _ip);
        
        It should_fail_registration = () => _result2.ShouldEqual(Guid.Empty);
        It should_add_terminalsession1 = () => _env.TerminalSessions.ShouldContain(x => x.SessionId == _result1);
        It should_not_add_terminalsession2 = () => _env.TerminalSessions.ShouldNotContain(x => x.SessionId == _result2);
        
        static Guid _result1;
        static Guid _result2;
        static IPAddress _ip;
        static Terminal _term2;
    }
    
    
    [Subject(typeof(SystemCallHandler))]
    internal class When_unregistering_registered_terminal : SystemCallHandlerSpecsBase
    {
        Establish  context = () => { _session = _sut.RegisterTerminal(_term, IPAddress.Any); };
        Because of = () => { _sut.UnregisterTerminal(_session); };
        
        It should_remove_terminalsession = () => _env.TerminalSessions.ShouldNotContain(x => x.SessionId == _session);
        
        static Guid _session;
    }
    
    [Subject(typeof(SystemCallHandler))]
    internal class When_unregistering_unregistered_terminal : SystemCallHandlerSpecsBase
    {
        Establish  context = () => { _session = _sut.RegisterTerminal(_term, IPAddress.Any); };
        Because of = () => { _sut.UnregisterTerminal(Guid.NewGuid()); };
        
        It should_not_unregister_terminal = () => _env.TerminalSessions.Count.ShouldEqual(1);
        It should_not_remove_terminalsession = () => _env.TerminalSessions.ShouldContain(x => x.SessionId == _session);
        
        static Guid _session;
    }

    #endregion Terminal

    #region Waiter
    
    [Subject(typeof(SystemCallHandler))]
    internal class When_login_waiter_without_session : SystemCallWaiterSpecsBase
    {
        Because of = () => { _result = _sut.LoginWaiter(Guid.Empty, _waiter, "" ); };
        
        It should_not_login_waiter = () => _result.ShouldEqual(ErrorCode.NoSessionId);
        
        static ErrorCode _result;
    }

    
    [Subject(typeof(SystemCallHandler))]
    internal class When_login_waiter_with_invalid_session : SystemCallWaiterSpecsBase
    {
        Because of = () => _result = _sut.LoginWaiter(Guid.NewGuid(), _waiter, "" );
        
        It should_not_login_waiter = () => _result.ShouldEqual(ErrorCode.NoSessionId);
        
        static ErrorCode _result;
    }

    
    [Subject(typeof(SystemCallHandler))]
    internal class When_login_waiter_with_password : SystemCallWaiterSpecsBase
    {
        Establish context = () =>
        {
            _waiter.Password = "password";
            _sessionId = _sut.RegisterTerminal(_term, IPAddress.Any);
        };
        Because of = () => _result = _sut.LoginWaiter(_sessionId, _waiter, "" );
        It should_not_set_waiter = () => _env.TerminalSessions.First().Waiter.ShouldEqual(TerminalSession.EmptyWaiter);
        
        It should_not_login_waiter = () => _result.ShouldEqual(ErrorCode.Password);
        
        static ErrorCode _result;
        static Guid _sessionId;
    }

    
    [Subject(typeof(SystemCallHandler))]
    internal class When_login_waiter_with_matching_password : SystemCallWaiterSpecsBase
    {
        Establish context = () =>
        {
            _waiter.Password = "password";
            _sessionId = _sut.RegisterTerminal(_term, IPAddress.Any);
        };
        
        Because of = () => _result = _sut.LoginWaiter(_sessionId, _waiter, "password" );
        
        It should_login_waiter = () => _result.ShouldEqual(ErrorCode.None);
        It should_set_waiter = () => _env.TerminalSessions.First().Waiter.ShouldEqual(_waiter);
        
        static ErrorCode _result;
        static Guid _sessionId;
    }
    
    
    [Subject(typeof(SystemCallHandler))]
    internal class When_login_waiter : SystemCallWaiterSpecsBase
    {
        Establish context = () => _sessionId = _sut.RegisterTerminal(_term, IPAddress.Any);
        
        Because of = () => _result = _sut.LoginWaiter(_sessionId, _waiter, "" );
        
        It should_login_waiter = () => _result.ShouldEqual(ErrorCode.None);
        It should_set_waiter = () => _env.TerminalSessions.First().Waiter.ShouldEqual(_waiter);
        
        static ErrorCode _result;
        static Guid _sessionId;
    }

    
    [Subject(typeof(SystemCallHandler))]
    internal class When_autologin_waiter_without_session : SystemCallWaiterSpecsBase
    {
        Because of = () => { _result = _sut.AutoLoginWaiter(Guid.Empty, _waiter); };
        
        It should_not_login_waiter = () => _result.ShouldEqual(ErrorCode.NoSessionId);
        
        static ErrorCode _result;
    }
    
    
    [Subject(typeof(SystemCallHandler))]
    internal class When_autologin_waiter_with_invalid_session : SystemCallWaiterSpecsBase
    {
        Because of = () => _result = _sut.AutoLoginWaiter(Guid.NewGuid(), _waiter);
        
        It should_not_login_waiter = () => _result.ShouldEqual(ErrorCode.NoSessionId);
        
        static ErrorCode _result;
    }

    
    [Subject(typeof(SystemCallHandler))]
    internal class When_autologin_waiter_with_password : SystemCallWaiterSpecsBase
    {
        Establish context = () =>
        {
            _waiter.Password = "password";
            _sessionId = _sut.RegisterTerminal(_term, IPAddress.Any);
        };
        Because of = () => _result = _sut.AutoLoginWaiter(_sessionId, _waiter);
        It should_login_waiter = () => _result.ShouldEqual(ErrorCode.None);
        It should_set_waiter = () => _env.TerminalSessions.First().Waiter.ShouldEqual(_waiter);
        
        static ErrorCode _result;
        static Guid _sessionId;
    }

    
    [Subject(typeof(SystemCallHandler))]
    internal class When_logout_waiter_without_session : SystemCallWaiterSpecsBase
    {
        Because of = () => { _result = _sut.LogoutWaiter(Guid.Empty); };
        
        It should_not_logout_waiter = () => _result.ShouldEqual(ErrorCode.NoSessionId);
        
        static ErrorCode _result;
    }
    
    
    [Subject(typeof(SystemCallHandler))]
    internal class When_logout_logged_in_waiter : SystemCallWaiterSpecsBase
    {
        Establish context = () =>
        {
            _sessionId = _sut.RegisterTerminal(_term, IPAddress.Any);
            _sut.LoginWaiter(_sessionId, _waiter, "" );
        };
        
        Because of = () => _result = _sut.LogoutWaiter(_sessionId);
        
        It should_logout_waiter = () => _result.ShouldEqual(ErrorCode.None);
        It should_clear_waiter = () => _env.TerminalSessions.First().Waiter.ShouldEqual(TerminalSession.EmptyWaiter);
        
        static ErrorCode _result;
        static Guid _sessionId;
    }

    #endregion Waiter

    
    internal class SystemCallWaiterSpecsBase : SystemCallHandlerSpecsBase
    {
        Establish context = () => { _waiter = new Waiter { Id = 1, Name = "Waiter 1" }; };
        
        protected static Waiter _waiter;
    };

    internal class SystemCallHandlerSpecsBase
    {
        Establish context = () =>
        {
            _env = new Environment();
            _sut = new SystemCallHandler(_env);
            _term = new Terminal { Id = 1, Name = "Terminal 1" };
        };
        protected static Environment _env;
        protected static SystemCallHandler _sut;
        protected static Terminal _term;
    }
}