﻿using System.Collections.Generic;
using System.Linq;
using PosService.Core.CoreData;
using PosShare.CoreData;

namespace PosService.Core.Specs.TestData
{
    internal class TestFamilies : IProvideFamilies
    {
        public List<Family> GetFamilies()
        {
            return CreateFamilies().ToList();
        }

        public IEnumerable<Family> CreateFamilies()
        {
            yield return new Family() { Id = 10, Name = "Family 10", FamilyGroup = 1 };
            yield return new Family() { Id = 11, Name = "Family 11", FamilyGroup = 1 };
            yield return new Family() { Id = 12, Name = "Family 12", FamilyGroup = 1 };
            yield return new Family() { Id = 20, Name = "Family 20", FamilyGroup = 2 };
            yield return new Family() { Id = 21, Name = "Family 21", FamilyGroup = 2 };
            yield return new Family() { Id = 22, Name = "Family 22", FamilyGroup = 2 };
            yield return new Family() { Id = 30, Name = "Family 30", FamilyGroup = 3 };
            yield return new Family() { Id = 31, Name = "Family 31", FamilyGroup = 3 };
            yield return new Family() { Id = 32, Name = "Family 32", FamilyGroup = 3 };
        }
    }
}