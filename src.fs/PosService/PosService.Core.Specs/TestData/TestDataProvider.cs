﻿using PosService.Core.CoreData;

namespace PosService.Core.Specs.TestData
{
    public class TestDataProvider : IProvideCoreData
    {
        public TestDataProvider()
        {
            FamilyGroups = new TestFamilyGroups();
            Families = new TestFamilies();
            Articles = new TestArticles();
            Currencies = new TestCurrencies();
            Payforms = new TestPayforms();
            VatRates = new TestVatRates();
            Terminals = new TestTerminals();
            Waiters = new TestWaiters();
        }
        public IProvideFamilyGroups FamilyGroups { get; }
        public IProvideFamilies Families { get; }
        public IProvideArticles Articles { get; }
        public IProvideCurrencies Currencies { get; }
        public IProvidePayforms Payforms { get; }
        public IProvideVatRates VatRates { get; }
        public IProvideTerminals Terminals { get; }
        public IProvideWaiters Waiters { get; }
    }
}
