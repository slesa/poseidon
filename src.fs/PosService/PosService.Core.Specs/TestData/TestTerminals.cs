﻿using System.Collections.Generic;
using System.Linq;
using PosService.Core.CoreData;
using PosShare.CoreData;

namespace PosService.Core.Specs.TestData
{
    internal class TestTerminals : IProvideTerminals
    {
        List<Terminal> _terminals;
        List<Terminal> Terminals
        {
            get
            {
                if( _terminals==null ) _terminals = CreateTerminals().ToList();
                return _terminals;
            }
        }
        
        public List<Terminal> GetTerminals()
        {
            return Terminals;
        }

        IEnumerable<Terminal> CreateTerminals()
        {
            yield return new Terminal() { Id = 1, Name = "Terminal 1" };
            yield return new Terminal() { Id = 2, Name = "Terminal 2" };
            yield return new Terminal() { Id = 3, Name = "Terminal 3" };
        }

        public Terminal FindTerminal(int id)
        {
            return Terminals.FirstOrDefault(x => x.Id == id);
        }
    }
}