﻿using System.Collections.Generic;
using System.Linq;
using PosService.Core.CoreData;
using PosShare.CoreData;

namespace PosService.Core.Specs.TestData
{
    internal class TestWaiters : IProvideWaiters
    {
        List<Waiter> _waiters;

        List<Waiter> Waiters
        {
            get
            {
                if (_waiters == null) _waiters = CreateWaiters().ToList();
                return _waiters;
            }
        }

        IEnumerable<Waiter> CreateWaiters()
        {
            yield return new Waiter() { Id = 1, Name = "Waitress 1", CanLogin = true };
            yield return new Waiter() { Id = 2, Name = "No login", CanLogin = false };
            yield return new Waiter() { Id = 3, Name = "No all tables", CanAllTables = false };
            yield return new Waiter() { Id = 4, Name = "No orders", CanOrder = false };
        }
    
        public List<Waiter> GetWaiters()
        {
            return Waiters;
        }

        public Waiter FindWaiter(int id)
        {
            return Waiters.FirstOrDefault(x => x.Id == id);
        }
    }
}