﻿using System.Collections.Generic;
using System.Linq;
using PosService.Core.CoreData;
using PosShare.CoreData;

namespace PosService.Core.Specs.TestData
{
    internal class TestCurrencies : IProvideCurrencies
    {
        public List<Currency> GetCurrencies()
        {
            return CreateCurrencies().ToList();
        }

        public IEnumerable<Currency> CreateCurrencies()
        {
            yield return new Currency() { Id = 1, Name = "Euro" };
            yield return new Currency() { Id = 2, Name = "Dollar" };
            yield return new Currency() { Id = 3, Name = "Pfund" };
        }
    }
}