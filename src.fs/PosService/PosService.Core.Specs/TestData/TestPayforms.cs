﻿using System.Collections.Generic;
using System.Linq;
using PosService.Core.CoreData;
using PosShare.CoreData;

namespace PosService.Core.Specs.TestData
{
    internal class TestPayforms : IProvidePayforms
    {
        public List<Payform> GetPayforms()
        {
            return CreatePayforms().ToList();
        }

        IEnumerable<Payform> CreatePayforms()
        {
            yield return new Payform() { Id = 1, Name = "Bar" };
            yield return new Payform() { Id = 2, Name = "Kreditkarte" };
            yield return new Payform() { Id = 3, Name = "Hotel" };
        }
    }
}