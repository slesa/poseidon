﻿using System.Collections.Generic;
using System.Linq;
using PosService.Core.CoreData;
using PosShare.CoreData;

namespace PosService.Core.Specs.TestData
{
    internal class TestArticles : IProvideArticles
    {
        public const int HintId = 101;
        public const int FreePriceId = 102;
        public const int FreeTextId = 110;
        public const int WeightId = 111;
        public const int CutGoodId = 112;
        public const int AreaGoodId = 120;
        public const int Constraint1Id = 200;
        
        List<Article> _articles;

        List<Article> Articles
        {
            get
            {
                if(_articles==null) _articles = CreateArticles().ToList();
                return _articles;
            }
        }

        public List<Article> GetArticles()
        {
            return Articles;
        }

        public Article FindArticle(int plu)
        {
            return Articles.FirstOrDefault(x => x.Plu == plu);
        }

        public IEnumerable<Article> CreateArticles()
        {
            var ca1 = new Article { Plu = Constraint1Id, Name = "Constraint 1", Family = 20 };
            ca1.Constraint = new Constraint(ca1.Plu);
            var cl1 = new ConstraintLevel() { IsActive = true };
            ca1.Constraint.Levels.Add(cl1);
            var ce1 = new ConstraintEntry() { Plu = 100, Price = 1.42M, IsHint = false, UsePrice = true };
            cl1.Entries.Add(ce1);
            var ce2 = new ConstraintEntry() { Plu = HintId, Price = 1M, IsHint = true, UsePrice = false };
            cl1.Entries.Add(ce2);
            
            yield return new Article() { Plu = 100, Name = "Article 100", Family = 10 };
            yield return new Article() { Plu = HintId, Name = "Hint", Family = 10, IsHint = true };
            yield return new Article() { Plu = FreePriceId, Name = "Free Price", Family = 10, IsFreePrice = true };
            yield return new Article() { Plu = FreeTextId, Name = "Free Text ", Family = 11, IsFreeText = true };
            yield return new Article() { Plu = WeightId, Name = "Weight", Family = 11, IsWeight = true };
            yield return new Article() { Plu = CutGoodId, Name = "Cut Good", Family = 11, IsCutGood = true };
            yield return new Article() { Plu = AreaGoodId, Name = "Area Good", Family = 12, IsAreaGood = true };
            yield return new Article() { Plu = 121, Name = "Article 121", Family = 12 };
            yield return new Article() { Plu = 122, Name = "Article 122", Family = 12 };

            yield return ca1; 
            yield return new Article() { Plu = 201, Name = "Article 201", Family = 20 };
            yield return new Article() { Plu = 202, Name = "Article 202", Family = 20 };
            yield return new Article() { Plu = 210, Name = "Article 210", Family = 21 };
            yield return new Article() { Plu = 211, Name = "Article 211", Family = 21 };
            yield return new Article() { Plu = 212, Name = "Article 212", Family = 21 };
            yield return new Article() { Plu = 220, Name = "Article 220", Family = 22 };
            yield return new Article() { Plu = 221, Name = "Article 221", Family = 22 };
            yield return new Article() { Plu = 222, Name = "Article 222", Family = 22 };
            
            yield return new Article() { Plu = 300, Name = "Article 300", Family = 30 };
            yield return new Article() { Plu = 301, Name = "Article 301", Family = 30 };
            yield return new Article() { Plu = 302, Name = "Article 302", Family = 30 };
            yield return new Article() { Plu = 310, Name = "Article 310", Family = 31 };
            yield return new Article() { Plu = 311, Name = "Article 311", Family = 31 };
            yield return new Article() { Plu = 312, Name = "Article 312", Family = 31 };
            yield return new Article() { Plu = 320, Name = "Article 320", Family = 32 };
            yield return new Article() { Plu = 321, Name = "Article 321", Family = 32 };
            yield return new Article() { Plu = 322, Name = "Article 322", Family = 32 };
        }
    }
}