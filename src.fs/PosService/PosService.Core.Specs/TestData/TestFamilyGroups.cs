﻿using System.Collections.Generic;
using System.Linq;
using PosService.Core.CoreData;
using PosShare.CoreData;

namespace PosService.Core.Specs.TestData
{
    internal class TestFamilyGroups : IProvideFamilyGroups
    {
        public List<FamilyGroup> GetFamilyGroups()
        {
            return CreateFamilyGroups().ToList();
        }

        public IEnumerable<FamilyGroup> CreateFamilyGroups()
        {
            yield return new FamilyGroup() { Id = 1, Name = "Family group One" };
            yield return new FamilyGroup() { Id = 2, Name = "Family group Two" };
            yield return new FamilyGroup() { Id = 3, Name = "Family group Three" };
        }
    }
}