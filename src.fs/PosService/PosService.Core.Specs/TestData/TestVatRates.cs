﻿using System.Collections.Generic;
using System.Linq;
using PosService.Core.CoreData;
using PosShare.CoreData;

namespace PosService.Core.Specs.TestData
{
    internal class TestVatRates : IProvideVatRates
    {
        public List<VatRate> GetVatRates()
        {
            return CreateVatRates().ToList();
        }

        IEnumerable<VatRate> CreateVatRates()
        {
            yield return new VatRate() { Id = 1, Name = "Im Haus" };
            yield return new VatRate() { Id = 2, Name = "Außer Haus" };
            yield return new VatRate() { Id = 3, Name = "Personal" };
        }
    }
}