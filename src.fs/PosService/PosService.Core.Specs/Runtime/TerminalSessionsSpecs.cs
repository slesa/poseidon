#if not
using System;
using Machine.Specifications;
using Moq;
using PosService.Core.Runtime;
using PosShare.CoreData;
using It = Machine.Specifications.It;
// ReSharper disable InconsistentNaming

namespace PosService.Core.Specs.Runtime
{
    [Subject(typeof(TerminalSessions))]
    internal class When_registering_terminal : TerminalSessionsSpecsBase
    {
        Establish context = () => _sut = new TerminalSessions();
        
        Because of = () => _result = _sut.RegisterTerminal(_terminal);

        It should_signal_success = () => _result.ShouldBeTrue();
        It should_register_terminal = () => _sut._terminals.Count.ShouldEqual(1);
        It should_find_terminal = () => _sut.FindTerminal(_terminal.Id).Terminal.ShouldEqual(_terminal);
        
        static TerminalSessions _sut;
        static bool _result;
    }

    
    [Subject(typeof(TerminalSessions))]
    internal class When_registering_terminal_twice : TerminalSessionsSpecsBase
    {
        Establish context = () =>
        {
            _sut = new TerminalSessions();
            _sut.RegisterTerminal(_terminal);
        };
        
        Because of = () => _result = _sut.RegisterTerminal(_terminal);

        It should_signal_success = () => _result.ShouldBeTrue();
        It should_keep_registered_terminal = () => _sut._terminals.Count.ShouldEqual(1);
        It should_find_terminal = () => _sut.FindTerminal(_terminal.Id).Terminal.ShouldEqual(_terminal);
        
        static TerminalSessions _sut;
        static bool _result;
    }

    
    [Subject(typeof(TerminalSessions))]
    internal class When_registering_terminal_with_other_ip : TerminalSessionsSpecsBase
    {
        Establish context = () =>
        {
            _sut = new TerminalSessions();
            _sut.RegisterTerminal(_terminal);
        };
        
        Because of = () => _result = _sut.RegisterTerminal(_terminal);

        It should_signal_fail = () => _result.ShouldBeFalse();
        It should_keep_registered_terminal = () => _sut._terminals.Count.ShouldEqual(1);
        It should_find_terminal = () => _sut.FindTerminal(_terminal.Id).Terminal.ShouldEqual(_terminal);
        
        static TerminalSessions _sut;
        static bool _result;
    }

    
    [Subject(typeof(TerminalSessions))]
    internal class When_unregistering_not_registered_terminal : TerminalSessionsSpecsBase
    {
        Establish context = () => _sut = new TerminalSessions();
        
        Because of = () => _sut.UnregisterTerminal(Guid.NewGuid());

        It should_have_no_registered_terminal = () => _sut._terminals.ShouldBeEmpty();
        
        static TerminalSessions _sut;
    }

    
    [Subject(typeof(TerminalSessions))]
    internal class When_unregistering_terminal : TerminalSessionsSpecsBase
    {
        Establish context = () =>
        {
            _sut = new TerminalSessions();
            _sut.RegisterTerminal(_terminal);
            _session = _sut.FindTerminal(_terminal.Id);
        };
        
        Because of = () => _sut.UnregisterTerminal(_session.SessionId);

        It should_have_no_registered_terminal = () => _sut._terminals.Count.ShouldEqual(0);
        
        static TerminalSessions _sut;
        static TerminalSession _session;
    }
    
    
    internal class TerminalSessionsSpecsBase
    {
        Establish context = () =>
        {
            _terminal = new Terminal() {Id = 1, Name = "Restaurant"};
        };

        protected static Terminal _terminal;
    }
}
#endif
