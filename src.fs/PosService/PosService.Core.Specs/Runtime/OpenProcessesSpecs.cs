#if not
using System;
using System.IO;
using Machine.Specifications;
using PosService.Core.Runtime;
using PosShare.CoreData;
using PosShare.Model;
using It = Machine.Specifications.It;
// ReSharper disable InconsistentNaming

namespace PosService.Core.Specs.Runtime
{
    [Subject(typeof(OpenProcesses))]
    internal class When_creating_openprocesses_and_search_process : OpenProcessesSpecsBase
    {
        Establish context = () => _sut = new OpenProcesses();
        
        Because of = () => _result = _sut.FindProcess(Guid.NewGuid());

        It should_not_create_directory = () => Directory.Exists(ProcessesDir).ShouldBeFalse();
        It should_not_find_process = () => _result.ShouldBeNull();

        static OpenProcesses _sut;
        static OpenProcess _result;
    }
    
    #region Tables
    
    [Subject(typeof(OpenProcesses))]
    internal class When_getting_table_file_name : OpenProcessesSpecsBase
    {
        Because of = () => _result = OpenProcesses.TablesFile(TableId, PartyId);

        It should_return_correct_name = () => _result.ShouldEqual(GetTableFilename(TableId, PartyId));
        It should_create_directory = () => Directory.Exists(ProcessesDir).ShouldBeTrue();
        
        static string _result;
    }

    
    [Subject(typeof(OpenProcesses))]
    internal class When_opening_table_with_openprocesses : OpenProcessesSpecsBase
    {
        Establish context = () => _sut = new OpenProcesses();
        
        Because of = () => _result = _sut.OpenTable(TableId, PartyId, Terminal, Waiter);

        It should_create_directory = () => Directory.Exists(ProcessesDir).ShouldBeTrue();
        It should_not_create_file = () => new DirectoryInfo(ProcessesDir).GetFiles().ShouldBeEmpty();
        It should_create_process = () => _result.ShouldNotBeNull();
        It should_find_process = () => _sut.FindProcess(_result.Process.ProcessId).ShouldEqual(_result);
        It should_find_table = () => _sut.FindTable(TableId, PartyId).ShouldEqual(_result);
        It should_set_processid = () => _result.Process.ProcessId.ShouldNotEqual(Guid.Empty);
        It should_not_set_progressiveid = () => _result.Process.ProgressiveId.ShouldBeNull();
        It should_set_terminal = () => _result.OnTerminal.ShouldEqual(Terminal.Id);
        It should_set_waiter = () => _result.FromWaiter.ShouldEqual(Waiter.Id);
        It should_set_table = () => ((TableProcess)_result.Process).Table.ShouldEqual(TableId);
        It should_set_party = () => ((TableProcess)_result.Process).Party.ShouldEqual(PartyId);
        It should_set_created = () => _result.Process.Created.ShouldNotBeNull();
        It should_set_creators_terminal = () => _result.Process.Created.Terminal.ShouldEqual(Terminal.Id);
        It should_set_creators_terminal_name = () => _result.Process.Created.TerminalName.ShouldEqual(Terminal.Name);
        It should_set_creators_waiter = () => _result.Process.Created.Waiter.ShouldEqual(Waiter.Id);
        It should_set_creators_waiter_name = () => _result.Process.Created.WaiterName.ShouldEqual(Waiter.Name);

        static OpenProcesses _sut;
        static OpenProcess _result;
    }
    
    
    [Subject(typeof(OpenProcesses))]
    internal class When_closing_empty_table_with_openprocesses : OpenProcessesSpecsBase
    {
        Establish context = () =>
        {
            _sut = new OpenProcesses();
            _table = _sut.OpenTable(TableId, PartyId, Terminal, Waiter);
        };

        Because of = () => _sut.CloseTable(_table);

        It should_not_create_file = () => new DirectoryInfo(ProcessesDir).GetFiles().ShouldBeEmpty();

        static OpenProcesses _sut;
        static OpenProcess _table;
    }
    
    
    [Subject(typeof(OpenProcesses))]
    internal class When_closing_filled_table_with_openprocesses : OpenProcessesSpecsBase
    {
        Establish context = () =>
        {
            _sut = new OpenProcesses();
            _table = _sut.OpenTable(TableId, PartyId, Terminal, Waiter);
            _table.Process.Actions.Add(new OrderedAction(DateTime.Now));
            _process = _table.Process;
        };

        Because of = () => _sut.CloseTable(_table);

        It should_create_file = () => new DirectoryInfo(ProcessesDir).GetFiles().ShouldNotBeEmpty();
        It should_set_progressive_counter = () => _process.ProgressiveId.ShouldEqual("PE1");

        static OpenProcesses _sut;
        static OpenProcess _table;
        static Process _process;
    }
    
    #endregion Tables
    
    #region Checkouts
    
    [Subject(typeof(OpenProcesses))]
    internal class When_getting_checkout_file_name : OpenProcessesSpecsBase
    {
        Because of = () => _result = OpenProcesses.CheckoutFile(CheckoutId);

        It should_return_correct_name = () => _result.ShouldEqual(GetCheckoutFilename(CheckoutId));
        It should_not_create_directory = () => Directory.Exists(ProcessesDir).ShouldBeTrue();
        
        static string _result;
    }

    
    [Subject(typeof(OpenProcesses))]
    internal class When_opening_checkout_with_openprocesses : OpenProcessesSpecsBase
    {
        Establish context = () => _sut = new OpenProcesses();
        
        Because of = () => _result = _sut.OpenCheckout(CheckoutId, Terminal, Waiter);

        It should_create_directory = () => Directory.Exists(ProcessesDir).ShouldBeTrue();
        It should_create_process = () => _result.ShouldNotBeNull();
        It should_find_process = () => _sut.FindProcess(_result.Process.ProcessId).ShouldEqual(_result);
        It should_find_checkout = () => _sut.FindCheckout(CheckoutId).ShouldEqual(_result);
        It should_set_processid = () => _result.Process.ProcessId.ShouldNotEqual(Guid.Empty);
        It should_not_set_progressiveid = () => _result.Process.ProgressiveId.ShouldBeNull();
        It should_set_terminal_in_process = () => _result.OnTerminal.ShouldEqual(Terminal.Id);
        It should_set_waiter_in_process = () => _result.FromWaiter.ShouldEqual(Waiter.Id);
        It should_set_checkoutid_in_process = () => ((CheckoutProcess)_result.Process).Id.ShouldEqual(CheckoutId);
        It should_set_created = () => _result.Process.Created.ShouldNotBeNull();
        It should_set_creators_terminal = () => _result.Process.Created.Terminal.ShouldEqual(Terminal.Id);
        It should_set_creators_terminal_name = () => _result.Process.Created.TerminalName.ShouldEqual(Terminal.Name);
        It should_set_creators_waiter = () => _result.Process.Created.Waiter.ShouldEqual(Waiter.Id);
        It should_set_creators_waiter_name = () => _result.Process.Created.WaiterName.ShouldEqual(Waiter.Name);

        static OpenProcesses _sut;
        static OpenProcess _result;
    }
    
    [Subject(typeof(OpenProcesses))]
    internal class When_closing_empty_checkout_with_openprocesses : OpenProcessesSpecsBase
    {
        Establish context = () =>
        {
            _sut = new OpenProcesses();
            _checkout = _sut.OpenCheckout(CheckoutId, Terminal, Waiter);
        };

        Because of = () => _sut.CloseCheckout(_checkout);

        It should_not_create_file = () => new DirectoryInfo(ProcessesDir).GetFiles().ShouldBeEmpty();

        static OpenProcesses _sut;
        static OpenProcess _checkout;
    }
    
    
    [Subject(typeof(OpenProcesses))]
    internal class When_closing_filled_checkout_with_openprocesses : OpenProcessesSpecsBase
    {
        Establish context = () =>
        {
            _sut = new OpenProcesses();
            _checkout = _sut.OpenCheckout(CheckoutId, Terminal, Waiter);
            _checkout.Process.Actions.Add(new OrderedAction(DateTime.Now));
            _process = _checkout.Process;
        };

        Because of = () => _sut.CloseCheckout(_checkout);

        It should_create_file = () => new DirectoryInfo(ProcessesDir).GetFiles().ShouldNotBeEmpty();
        It should_set_progressive_counter = () => _process.ProgressiveId.ShouldEqual("PE1");

        static OpenProcesses _sut;
        static OpenProcess _checkout;
        static Process _process;
    }

   
    #endregion Checkouts
    
    internal class OpenProcessesSpecsBase
    {
        protected static int TableId = 42;
        protected static int PartyId = 13;
        protected static int CheckoutId = 23;
        protected static Terminal Terminal = new () { Id=23, Name="Terminal 23" };
        protected static Waiter Waiter = new () { Id=27, Name="Waiter 27" };

        protected static string ProcessesDir
        {
            get
            {
                var di = new DirectoryInfo(Processes.ProcessesDir);
                return di.FullName;
            }
        }

        static void ClearDirectory()
        {
            if(Directory.Exists(ProcessesDir)) 
                Directory.Delete(ProcessesDir, true);   
        }

        Establish context = () => ClearDirectory();
        Cleanup teardown = () => ClearDirectory();
        
        protected static string GetTableFilename(int table, int party)
        {
            var fn = Path.Combine(ProcessesDir, $"T{table}.{party}");
            return fn;
        }
        
        protected static string GetCheckoutFilename(int id)
        {
            var fn = Path.Combine(ProcessesDir, $"C{id}");
            return fn;
        }
    }
}
#endif
