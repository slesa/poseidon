using Machine.Specifications;
using Moq;
using PosService.Core.Runtime;
using PosShare.CoreData;
using It = Machine.Specifications.It;
// ReSharper disable InconsistentNaming

namespace PosService.Core.Specs.Runtime
{
    /*
    [Subject(typeof(TerminalSession))]
    internal class When_creating_terminal_session
    {
        Establish context = () =>
        {
            _terminal = new Terminal() {Id = 1, Name = "Restaruant"};
            _sender = new Mock<IProvideSender>();
        };
        
        Because of = () => _sut = new TerminalSession(_terminal, _sender.Object);

        It should_contain_terminal_id = () => _sut.SessionId.ShouldNotBeNull();
        It should_set_empty_waiter = () => _sut.Waiter.ShouldEqual(TerminalSession.EmptyWaiter);
        It should_set_terminal = () => _sut.Terminal.ShouldEqual(_terminal);
        It should_set_connection = () => _sut.Connection.ShouldEqual(_sender.Object);
        
        static TerminalSession _sut;
        static Terminal _terminal;
        static Mock<IProvideSender> _sender;
    } */
}