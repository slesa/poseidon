﻿using System;
using ConClient.Config;

namespace ConClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("POS Client, V0.3b");
            Console.WriteLine();
            Console.WriteLine("You have entered a dimmed restaurant floor. Good luck!");

            var settingsReader = new Configure();
            var settings = settingsReader.ReadSettings();
            Console.WriteLine($"Connecting to host {settings.Host}:{settings.Port}");
            var environment = new Environment(settings);
            var processor = new Processor(settings, environment);
            processor.Run();
            
            // SettingsPersistence.Save(settings);
            Console.WriteLine("Bye");
        }
    }
}