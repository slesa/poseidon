namespace ConClient.Model
{
    public class KnownCommand
    {
        public string Command { get; set; }
        public string Description { get; set; }
        public string Parameters { get; set; }
    }
}