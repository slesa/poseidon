using System;
using System.Collections.Generic;
using System.Linq;
using PosShare.Payloads;

namespace ConClient.Input
{
    public class CommandParser
    {
        readonly Environment _environment;
        readonly List<IParseInput> _inputParsers;
        
        public CommandParser(Environment environment, IEnumerable<IParseInput> inputParsers)
        {
            _environment = environment;
            _inputParsers = inputParsers.ToList();
        }

        public void Parse(string command, IEnumerable<string> parameters)
        {
            foreach (var parser in _inputParsers)
            {
                if (parser.SuccessfullParsed(command, parameters)) break;
            }
        }

        public bool ParseInput(string input)
        {
            if (_environment.CurrentOrder == null) return false;
            var callback = _environment.CurrentOrder.AskItems[_environment.CurrentOrder.CurrentCallback];
            var line = _environment.CurrentOrder.OrderLines[callback.ForLine];
            switch (callback.Type)
            {
                case OrderAskType.Price:
                    if (!GetDecimalValue(input, callback.Label, out var price)) return false;
                    line = line.WithPrice(price);
                    break;
                case OrderAskType.Weight:
                    if (!GetDecimalValue(input, callback.Label, out var weight)) return false;
                    line = line.WithWeight(weight);
                    break;
                case OrderAskType.Length:
                    if (!GetDecimalValue(input, callback.Label, out var length)) return false;
                    line = line.WithLength(length);
                    break;
                case OrderAskType.Width:
                    if (!GetDecimalValue(input, callback.Label, out var width)) return false;
                    line = line.WithWidth(width);
                    break;
                case OrderAskType.Text:
                    line = line.WithArticle(input);
                    break;
                case OrderAskType.Constraint:
                {
                    var cb = callback as OrderAskConstraint;
                    if (!GetIntValue(input, cb.Label, out var choice)) return false;
                    // cb = callback.W
                    cb.Choice = choice;
                    // line = line.WithWidth(width);
                } break;
                default:
                    throw new NotImplementedException("Unknown callback");
            }
            
            _environment.CurrentOrder.OrderLines[callback.ForLine] = line;
            //sender.SendCommand(CommandPayload.CmdOrderProgress, _environment.CurrentOrder);
            _environment.CurrentOrder = null;
            return true;
        }

        static bool GetIntValue(string input, string label, out int value)
        {
            if (int.TryParse(input, out value)) return true;
            Console.WriteLine($"Unable to parse {input} as {label}");
            return false;
        }

        static bool GetDecimalValue(string input, string label, out decimal value)
        {
            if (decimal.TryParse(input, out value)) return true;
            Console.WriteLine($"Unable to parse {input} as {label}");
            return false;
        }
    }
}