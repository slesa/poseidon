using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConClient.Comm;
using ConClient.Model;
using PosShare.CoreData;

namespace ConClient.Input
{
    public class CoreDataInputParser: IParseInput
    {
        private readonly IHandleCoreDataRest _handler;
        readonly Environment _environment;
        
        public CoreDataInputParser(IHandleCoreDataRest handler, Environment environment)
        {
            _handler = handler;
            _environment = environment;
        }

        const string cmdGetArticles = "getarticles";
        const string cmdGetArticle = "getarticle";
        const string cmdGetCurrencies = "getcurrencies";
        const string cmdGetCurrency = "getcurrency";
        const string cmdGetFamilies = "getfamilies";
        const string cmdGetFamily = "getfamily";
        const string cmdGetFamilyGroups = "getfamilygroups";
        const string cmdGetFamilyGroup = "getfamilygroup";
        const string cmdGetPayforms = "getpayforms";
        const string cmdGetPayform = "getpayform";
        const string cmdGetTerminals = "getterminals";
        const string cmdGetTerminal = "getterminal";
        const string cmdGetVatRates = "getvatrates";
        const string cmdGetVatRate = "getvatrate";
        const string cmdGetWaiters = "getwaiters";
        const string cmdGetWaiter = "getwaiter";

        public IEnumerable<KnownCommand> KnownCommands
        {
            get
            {
                yield return new KnownCommand {Command = cmdGetArticles, Description = "Get all articles"};
                yield return new KnownCommand {Command = cmdGetArticle, Parameters = "<id>", Description = "Get article with this id"};
                yield return new KnownCommand {Command = cmdGetCurrencies, Description = "Get all currencles"};
                yield return new KnownCommand {Command = cmdGetCurrency, Parameters = "<id>", Description = "Get currency with this id"};
                yield return new KnownCommand {Command = cmdGetFamilies, Description = "Get all families"};
                yield return new KnownCommand {Command = cmdGetFamily, Parameters = "<id>", Description = "Get family with this is"};
                yield return new KnownCommand {Command = cmdGetFamilyGroups, Description = "Get all family groups"};
                yield return new KnownCommand {Command = cmdGetFamilyGroup, Parameters = "<id>", Description = "Get family group with this is"};
                yield return new KnownCommand {Command = cmdGetPayforms, Description = "Get all payforms"};
                yield return new KnownCommand {Command = cmdGetPayform, Parameters = "<id>", Description = "Get payform with this id"};
                yield return new KnownCommand {Command = cmdGetTerminals, Description = "Get all terminals"};
                yield return new KnownCommand {Command = cmdGetTerminal, Parameters = "<id>", Description = "Get terminal with this id"};
                yield return new KnownCommand {Command = cmdGetVatRates, Description = "Get all VAT rates"};
                yield return new KnownCommand {Command = cmdGetVatRate, Parameters = "<id>", Description = "Get VAT rate with this id"};
                yield return new KnownCommand {Command = cmdGetWaiters, Description = "Get all waiters"};
                yield return new KnownCommand {Command = cmdGetWaiter, Parameters = "<id>", Description = "Get waiter with this is"};
            }
        }

        public bool SuccessfullParsed(string command, IEnumerable<string> parameters)
        {
            Console.WriteLine("... working ...");
            switch (command)
            {
                case cmdGetArticles: 
                    GetArticles();
                    return true; 
                case cmdGetArticle: 
                    GetArticle(parameters.FirstOrDefault());
                    return true; 
                case cmdGetCurrencies: 
                    GetCurrencies();
                    return true; 
                case cmdGetCurrency: 
                    GetCurrency(parameters.FirstOrDefault());
                    return true; 
                case cmdGetFamilies: 
                    GetFamilies();
                    return true; 
                case cmdGetFamily: 
                    GetFamily(parameters.FirstOrDefault());
                    return true; 
                case cmdGetFamilyGroups: 
                    GetFamilyGroups();
                    return true; 
                case cmdGetFamilyGroup: 
                    GetFamilyGroup(parameters.FirstOrDefault());
                    return true; 
                case cmdGetPayforms: 
                    GetPayforms();
                    return true; 
                case cmdGetPayform: 
                    GetPayform(parameters.FirstOrDefault());
                    return true; 
                case cmdGetTerminals: 
                    GetTerminals();
                    return true; 
                case cmdGetTerminal: 
                    GetTerminal(parameters.FirstOrDefault());
                    return true; 
                case cmdGetVatRates: 
                    GetVatRates();
                    return true; 
                case cmdGetVatRate: 
                    GetVatRate(parameters.FirstOrDefault());
                    return true; 
                case cmdGetWaiters: 
                    GetWaiters();
                    return true; 
                case cmdGetWaiter: 
                    GetWaiter(parameters.FirstOrDefault());
                    return true; 
            }
            return false;
        }

        #region Articles
        void PrintArticle(Article article)
        {
            Console.WriteLine($"Plu {article.Plu} '{article.Name}' Family {article.Family}");
        }
        async Task GetArticles()
        {
            var result = await _handler.GetArticles();
            var articles = result.ToList();
            foreach (var article in articles)
                PrintArticle(article);
            Console.WriteLine($"{articles.Count} articles loaded");
        }
        async Task GetArticle(string param)
        {
            var id = GetId(param, "article");
            if(id==0) return;
            var article = await _handler.GetArticle(id);
            Console.Write($"Got article: ");
            PrintArticle(article);
        }
        #endregion Articles

        #region Currencies
        void PrintCurrency(Currency currency)
        {
            Console.WriteLine($"Currency {currency.Id} '{currency.Name}'");
        }
        async Task GetCurrencies()
        {
            var result = await _handler.GetCurrencies();
            var currencies = result.ToList();
            foreach (var currency in currencies)
                PrintCurrency(currency);
            Console.WriteLine($"{currencies.Count} currency loaded");
        }
        async Task GetCurrency(string param)
        {
            var id = GetId(param, "currency");
            if(id==0) return;
            var currency = await _handler.GetCurrency(id);
            Console.Write($"Got currency: ");
            PrintCurrency(currency);
        }
        #endregion Currencies

        #region Families
        void PrintFamily(Family family)
        {
            Console.WriteLine($"Family {family.Id} '{family.Name}' Family Group {family.FamilyGroup}");
        }
        async Task GetFamilies()
        {
            var result = await _handler.GetFamilies();
            var families = result.ToList();
            foreach (var family in families)
                PrintFamily(family);
            Console.WriteLine($"{families.Count} familles loaded");
        }
        async Task GetFamily(string param)
        {
            var id = GetId(param, "family");
            if(id==0) return;
            var family = await _handler.GetFamily(id);
            Console.Write($"Got family: ");
            PrintFamily(family);
        }
        #endregion Families

        #region Family Groups
        void PrintFamilyGroup(FamilyGroup group)
        {
            Console.WriteLine($"Group {group.Id} '{group.Name}'");
        }
        async Task GetFamilyGroups()
        {
            var result = await _handler.GetFamilyGroups();
            var groups = result.ToList();
            foreach (var group in groups)
                PrintFamilyGroup(group);
            Console.WriteLine($"{groups.Count} family groups loaded");
        }
        async Task GetFamilyGroup(string param)
        {
            var id = GetId(param, "family group");
            if(id==0) return;
            var group = await _handler.GetFamilyGroup(id);
            Console.Write($"Got family group: ");
            PrintFamilyGroup(group);
        }
        #endregion Family Groups

        #region Payforms
        void PrintPayform(Payform payform)
        {
            Console.WriteLine($"Payform {payform.Id} '{payform.Name}'");
        }
        async Task GetPayforms()
        {
            var result = await _handler.GetPayforms();
            var payforms = result.ToList();
            foreach (var payform in payforms)
                PrintPayform(payform);
            Console.WriteLine($"{payforms.Count} payforms loaded");
        }
        async Task GetPayform(string param)
        {
            var id = GetId(param, "payform");
            if(id==0) return;
            var payform = await _handler.GetPayform(id);
            Console.Write($"Got payform: ");
            PrintPayform(payform);
        }
        #endregion Payforms

        #region Terminals
        void PrintTerminal(Terminal terminal)
        {
            Console.WriteLine($"Terminal {terminal.Id} '{terminal.Name}'");
        }
        async Task GetTerminals()
        {
            var result = await _handler.GetTerminals();
            var terminals = result.ToList();
            foreach (var terminal in terminals)
                PrintTerminal(terminal);
            Console.WriteLine($"{terminals.Count} terminals loaded");
        }
        async Task GetTerminal(string param)
        {
            var id = GetId(param, "terminal");
            if(id==0) return;
            var terminal = await _handler.GetTerminal(id);
            Console.Write($"Got terminal: ");
            PrintTerminal(terminal);
        }
        #endregion Payforms

        #region VAT Rates
        void PrintVatRate(VatRate vatRate)
        {
            Console.WriteLine($"VAT rate {vatRate.Id} '{vatRate.Name}'");
        }
        async Task GetVatRates()
        {
            var result = await _handler.GetVatRates();
            var vatRates = result.ToList();
            foreach (var vatRate in vatRates)
                PrintVatRate(vatRate);
            Console.WriteLine($"{vatRates.Count} VAT rates loaded");
        }
        async Task GetVatRate(string param)
        {
            var id = GetId(param, "vatrate");
            if(id==0) return;
            var vatRate = await _handler.GetVatRate(id);
            Console.Write($"Got VAT rate: ");
            PrintVatRate(vatRate);
        }
        #endregion Payforms

        #region Waiters
        void PrintWaiter(Waiter waiter)
        {
            var rights = GetWaiterRightStr(waiter);
            Console.WriteLine($"Id {waiter.Id} '{waiter.Name}' Key {waiter.Keylock} Rights {rights}");
        }
        string GetWaiterRightStr(Waiter waiter)
        {
            var result = "";
            result += WaiterRightChar(waiter.CanCreateTable, 'C');
            result += WaiterRightChar(waiter.CanOrder, 'O');
            result += WaiterRightChar(waiter.CanVoid, 'V');
            result += WaiterRightChar(waiter.CanPay, 'P');
            result += WaiterRightChar(waiter.CanSplit, 'S');
            return result;
        }
        char WaiterRightChar(bool right, char ch)
        {
            return right ? ch : '-';
        }

        async Task GetWaiters()
        {
            var result = await _handler.GetWaiters();
            var waiters = result.ToList();
            foreach (var waiter in waiters)
                PrintWaiter(waiter);
            Console.WriteLine($"{waiters.Count} waiters loaded");
        }
        async Task GetWaiter(string param)
        {
            var id = GetId(param, "waiter");
            if(id==0) return;
            var waiter = await _handler.GetWaiter(id);
            Console.Write($"Got waiter: ");
            PrintWaiter(waiter);
        }
        #endregion Waiters

        int GetId(string param, string what)
        {
            if (!int.TryParse(param, out var id))
            {
                Console.WriteLine($"Unable to parse {param} as {what} id");
                return 0;
            }
            return id;
        }
    }
}