using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConClient.Comm;
using ConClient.Model;
using PosShare.Payloads;

namespace ConClient.Input
{
    public class SystemInputParser : IParseInput
    {
        private readonly IHandleSystemRest _systemHandler;
        private readonly ProcessesInputParser _processParser;
        readonly Environment _env;
        const string cmdTurnOn = "turnon";
        const string cmdTurnOff = "turnoff";
        const string cmdLogin = "login";
        const string cmdLogout = "logout";
        
        public SystemInputParser(IHandleSystemRest systemHandler, ProcessesInputParser processParser, Environment env)
        {
            _systemHandler = systemHandler;
            _processParser = processParser;
            _env = env;
        }

        public IEnumerable<KnownCommand> KnownCommands
        {
            get
            {
                yield return new KnownCommand { Command = cmdTurnOn, Parameters = "<terminal id>", Description = "Start new session for this terminal id"};
                yield return new KnownCommand { Command = cmdTurnOff, Description = "Stop current session"};
                yield return new KnownCommand { Command = cmdLogin, Parameters = "<waiter id>", Description = "Login waiter with id <waiter id> into current terminal"};
                yield return new KnownCommand {Command = cmdLogout, Description = "Logout current waiter from current terminal"};
            }
        }

        public bool SuccessfullParsed(string command, IEnumerable<string> parameters)
        {
            switch (command)
            {
                case cmdTurnOn: 
                    TurnOn(parameters.FirstOrDefault());
                    return true;
                case cmdTurnOff: 
                    TurnOff();
                    return true;
                case cmdLogin: 
                    Login(parameters.FirstOrDefault());
                    return true;
                case cmdLogout: 
                    Logout();
                    return true;
            }
            return false;
        }

        #region Waiter

        void Login(string value)
        {
            if (!int.TryParse(value, out var waiter))
            {
                Console.WriteLine($"Unable to convert input {value} to waiter");
                return;
            }
            Login(waiter);
        }

        void Login(int waiter, bool autoLogin=false)
        {
            if (waiter == 0)
            {
                Logout();
                return;
            }
            Console.WriteLine($"Logging in waiter {waiter}");
            var loggedIn = autoLogin 
                ?  _systemHandler.AutoLogin(_env.SessionId, waiter) 
                : _systemHandler.Login(_env.SessionId, waiter);
            if (loggedIn==null || loggedIn.Waiter == null)
            {
                while (loggedIn.Status == ErrorCode.Password)
                {
                    Console.Write($"Password required: ");
                    var pw = Console.ReadLine();
                    if (string.IsNullOrEmpty(pw)) break;
                    loggedIn = _systemHandler.Login(_env.SessionId, waiter, pw);
                }
                if (loggedIn.Waiter == null)
                {
                    Console.WriteLine($"Error: failed to login waiter {waiter}");
                    return;
                }
            }
            _env.CurrentWaiter = loggedIn.Waiter;
            Console.WriteLine($"Waiter {loggedIn.Waiter.Id} {loggedIn.Waiter.Name} logged in on terminal {_env.CurrentTerminal.Id}");

            if (_env.Settings.FixedTable != 0)
            {
                Console.WriteLine($"Fixed table configured");
                _processParser.OpenTable(_env.Settings.FixedTable, _env.Settings.FixedParty);
            }
        }

        void Logout()
        {
            Console.WriteLine($"Logging out waiter  {_env.CurrentWaiter.Id}");
            _systemHandler.Logout(_env.SessionId);
            _env.CurrentWaiter = Environment.EmptyWaiter;
            Console.WriteLine($"Waiter logged out from terminal {_env.CurrentTerminal.Id}");
        }
        
        #endregion Waiter

        #region Terminal

        void TurnOn(string value)
        {
            if (!int.TryParse(value, out var terminal))
            {
                Console.WriteLine($"Unable to convert input {value} to terminal");
                return;
            }
            TurnOn(terminal);
        }        
        
        public void TurnOn(int terminal)
        {
            if (terminal == 0)
            {
                TurnOff();
                return;
            }
            Console.WriteLine($"Turning on terminal {terminal}");
            var turnedOn = _systemHandler.TurnOn(terminal);
            if (turnedOn==null || turnedOn.SessionId == Guid.Empty)
            {
                Console.WriteLine($"Error: failed to register terminal {terminal}");
                return;
            }
            _env.SessionId = turnedOn.SessionId;
            _env.CurrentTerminal = turnedOn.Terminal;
            Console.WriteLine($"Terminal {terminal} claims session {turnedOn.SessionId}");
            
            if(_env.Settings.HasFixedWaiter())
                Login(_env.Settings.FixedWaiterId, true);
            // else if (_env.CurrentTerminal.HasDefaultWaiter())
                // Login(_env.CurrentTerminal.DefaultWaiter);
        }

        void TurnOff()
        {
            if (_env.HasWaiter())
                Logout();
            Console.WriteLine($"Turning off terminal {_env.CurrentTerminal.Id}");
            _systemHandler.TurnOff(_env.SessionId);
            _env.SessionId = Guid.Empty;
            _env.CurrentTerminal = Environment.EmptyTerminal;
            Console.WriteLine($"Terminal turned off");
        }

        #endregion Terminal 
    }
}