using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConClient.Comm;
using ConClient.Model;
using PosShare.Model;
using PosShare.Payloads;

namespace ConClient.Input
{
    public class ProcessesInputParser : IParseInput
    {
        private readonly IHandleProcessesRest _handler;
        readonly Environment _env;
        
        public ProcessesInputParser(IHandleProcessesRest handler, Environment env)
        {
            _handler = handler;
            _env = env;
        }

        const string cmdGetTables = "gettables";
        const string cmdOpenTable = "opentable";
        const string cmdGetCheckouts = "getcheckouts";
        const string cmdOpenCheckout = "opencheckout";
        const string cmdCloseProcess = "close";
        
        public IEnumerable<KnownCommand> KnownCommands
        {
            get
            {
                yield return new KnownCommand {Command = cmdGetTables, Description = "Get open tables"};
                yield return new KnownCommand {Command = cmdOpenTable, Parameters = "<table>[,<party>]", Description = "Open a table"};
                yield return new KnownCommand {Command = cmdGetCheckouts, Description = "Get open checkouts"};
                yield return new KnownCommand {Command = cmdOpenCheckout, Parameters = "[id]", Description = "Open a checkout"};
                yield return new KnownCommand {Command = cmdCloseProcess, Parameters = "guid", Description = "Close a process with process id guid"};
            }
        }

        public bool SuccessfullParsed(string command, IEnumerable<string> parameters)
        {
            switch (command)
            {
                case cmdGetTables: 
                    GetTables();
                    return true;
                case cmdCloseProcess: 
                    CloseProcess(parameters.FirstOrDefault());
                    return true;
                case cmdOpenTable: 
                    OpenTable(parameters.FirstOrDefault());
                    return true;
                case cmdGetCheckouts: 
                    GetCheckouts();
                    return true;
                case cmdOpenCheckout: 
                    OpenCheckout(parameters.FirstOrDefault());
                    return true;
            }
            return false;
        }

        #region Tables

        void GetTables()
        {
            var result = _handler.GetTables(_env.SessionId);
            if (result.Status != ErrorCode.None)
            {
                Console.WriteLine($"Could not get tables: {result.Status}");
                return;
            }
            PrintProcesses(result.Processes);
        }

        void OpenTable(string value)
        {
            var tableId = GetTable(value);
            var partyId = GetParty(value);
            OpenTable(tableId, partyId);
        }

        public void OpenTable(int tableId, int partyId)
        {
            var result = _handler.OpenTable(_env.SessionId, tableId, partyId);
            if (result.Status != ErrorCode.None)
            {
                Console.WriteLine($"Could not open table: {result.Status}");
                return;
            }
            _env.CurrentProcessId = result.ProcessId;
            foreach (var entry in result.Actions)
                PrintTableEntry(entry);
            Console.WriteLine();
            Console.WriteLine($"Process {result.ProcessId} opened");
        }

        int GetTable(string parameter, int table=0)
        {
            if (string.IsNullOrEmpty(parameter)) return table;
            var tuple = parameter.Split(",");
            if (!int.TryParse(tuple[0], out var tableId))
            {
                Console.WriteLine($"Could not parse {tuple[0]} as table");
                return table;
            }
            return tableId;
        }

        int GetParty(string parameter, int party=0)
        {
            if (string.IsNullOrEmpty(parameter)) return party;
            var tuple = parameter.Split(",");
            if (tuple.Length < 2) return 0;
            if (!int.TryParse(tuple[1], out var partyId))
            {
                Console.WriteLine($"Could not parse {tuple[0]} as party");
                return party;
            }
            return partyId;
        }

        #endregion Tables
        
        #region Checkouts

        void GetCheckouts()
        {
            var result = _handler.GetCheckouts(_env.SessionId);
            if (result.Status != ErrorCode.None)
            {
                Console.WriteLine($"Could not get checkouts: {result.Status}");
                return;
            }
            PrintProcesses(result.Processes);
        }
        
        void OpenCheckout(string value)
        {
            var checkoutId = GetId(value, 0);
            var result = _handler.OpenCheckout(_env.SessionId, checkoutId);
            if (result.Status != ErrorCode.None)
            {
                Console.WriteLine($"Could not open checkout: {result.Status}");
                return;
            }
            _env.CurrentProcessId = result.ProcessId;
            foreach (var entry in result.Actions)
                PrintTableEntry(entry);
            Console.WriteLine();
            Console.WriteLine($"Process {result.ProcessId} opened");
        }

        int GetId(string parameter, int id)
        {
            if (string.IsNullOrEmpty(parameter)) return id;
            if (!int.TryParse(parameter, out var checkoutId))
                return id;
            return checkoutId;
        }
        
        #endregion Checkouts
        
        #region Processes
        
        void CloseProcess(string param)
        {
            if (!Guid.TryParse(param, out var processId))
            {
                Console.WriteLine($"Could not parse {param} as Guid");
                return;
            }
            var result = _handler.CloseProcess(_env.SessionId, processId);
            if (result.Status != ErrorCode.None)
            {
                Console.WriteLine($"Could not close process: {result}");
                return;
            }
            Console.WriteLine($"Process {processId} closed");
        }
        

        static void PrintProcesses(List<ProcessEntry> processes)
        {
            foreach (var entry in processes)
            {
                // Console.Write($"On terminal {process.OnTerminal} from waiter {process.FromWaiter} ");
                // var process = entry.Process;
                if (entry.Type==ProcessType.Table)
                {
                    Console.Write($"table {entry.Table}/{entry.Party}"); // {process.Actions.Count} actions");
                }

                // var checkout = entry.Process as CheckoutProcess;
                if (entry.Type==ProcessType.Checkout)
                {
                    Console.Write($"checkout {entry.Checkout}"); // {process.Actions.Count} actions");
                }
                Console.WriteLine($": ({entry.Creator}) {entry.CreatorName} {entry.CreatedAt}");
            }
        }

        #endregion Processes

        internal static void PrintTableEntry(ProcessAction action)
        {
            switch (action.Type)
            {
                case ActionType.Creation:
                {
                    Console.WriteLine($"- Created from {action.Waiter}/{action.WaiterName} at {action.Timestamp}");
                    break;
                }
                case ActionType.Order:
                {
                    var order = action as OrderedAction;
                    var ordered = order.Payload;
                    Console.WriteLine($"- Order {action.ActionId} from {action.Waiter}/{action.WaiterName} at {action.Timestamp} for {ordered.ProcessId}");
                    foreach(var line in ordered.OrderLines)
                        Console.WriteLine($"  {line.Count} x {line.Plu} {line.Article} for {line.Price}");
                    break;
                }
            }
        }

    }
}