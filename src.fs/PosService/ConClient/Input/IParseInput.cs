using System.Collections.Generic;
using System.Threading.Tasks;
using ConClient.Model;

namespace ConClient.Input
{
    public interface IParseInput
    {
        IEnumerable<KnownCommand> KnownCommands { get; }
        bool SuccessfullParsed(string command, IEnumerable<string> parameters);
    }
}