using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConClient.Comm;
using ConClient.Model;
using PosShare.Payloads;

namespace ConClient.Input
{
    public class ActionsInputParser : IParseInput
    {
        private readonly IHandleActionsRest _actionsHandler;
        readonly Environment _env;

        public ActionsInputParser(IHandleActionsRest actionsHandler, Environment env)
        {
            _actionsHandler = actionsHandler;
            _env = env;
        }

        const string cmdOrder = "order";
        const string cmdVoid = "void";
        const string cmdSplit = "split";
        const string cmdCommit = "commit";

        public IEnumerable<KnownCommand> KnownCommands
        {
            get
            {
                yield return new KnownCommand { Command = cmdOrder, Parameters = "[<count>x]<plu> [article] [price]", Description = "Order an article"};
                yield return new KnownCommand { Command = cmdVoid, Parameters = "[<count>x]<plu>", Description = "Void article(s)"};
                yield return new KnownCommand { Command = cmdSplit, Parameters = "[<count>x]<plu>", Description = "Split article(s)"};
                yield return new KnownCommand { Command = cmdCommit, Description = "Commit current process"};
            }
        }

        public bool SuccessfullParsed(string command, IEnumerable<string> parameters)
        {
            switch (command)
            {
                case cmdOrder:
                    Order(parameters.FirstOrDefault(), parameters.Skip(1).ToList());
                    return true;
                case cmdCommit:
                    Commit();
                    return true;
            }
            return false;
        }

        void Order(string input, List<string> optional)
        {
            var plu = GetPlu(input);
            if (plu == 0)
            {
                Console.WriteLine($"Could not parse plu from {input}");
                return;
            }
            var count = GetCount(input);
            var article = GetArticle(plu, optional);
            var price = GetPrice(optional);
            var result = _actionsHandler.Order(_env.SessionId, _env.CurrentProcessId, count, plu, article, price);
            if (result is not { Status: ErrorCode.None })
            {
                Console.WriteLine($"Could not order plu: {result.Status}");
                return;
            }
        }

        void Commit()
        {
            var result = _actionsHandler.Commit(_env.SessionId, _env.CurrentProcessId);
            if (result.Status != ErrorCode.None)
            {
                Console.WriteLine($"Could commit actions: {result.Status}");
                return;
            }
        }
        
        decimal? GetPrice(List<string> optional)
        {
            foreach (var el in optional)
            {
                if (!el.Contains(".")) continue;
                if (decimal.TryParse(el, out var price))
                    return price;
            }
            return null;
        }

        string GetArticle(int plu, List<string> optional)
        {
            foreach (var el in optional)
            {
                if (el.Contains(".")) continue;
                return el;
            }
            return null;
        }

        int GetCount(string xinput)
        {
            var input = xinput.ToLower();
            if (!input.Contains('x'))
                return 1;
            input = input.Split("x")[0];
            return int.TryParse(input, out var count) 
                ? count
                : 1;
        }

        int GetPlu(string xinput)
        {
            var input = xinput.ToLower();
            if (input.Contains('x'))
                input = input.Split("x")[1];
            return int.TryParse(input, out var plu) 
                ? plu 
                : 0;
        }
    }
}