namespace ConClient.Config
{
    public class Settings
    {
        public string Host { get; set; }
        public int Port { get; set; }

        public bool HasFixedTerminal() { return FixedTerminalId!=0; }
        public int FixedTerminalId { get; set; }
        
        public bool HasFixedWaiter() { return FixedWaiterId!=0; }
        public int FixedWaiterId { get; set; }
        
        public int FixedTable { get; set; }
        public int FixedParty { get; set; }
    }
}