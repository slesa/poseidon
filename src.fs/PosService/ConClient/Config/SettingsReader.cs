using System.IO;
using Basics.Config;

namespace ConClient.Config
{
    public class Configure : ReadIniConfig
    {
        const string ConfigFile = "etc/settings.ini";
        const string HostSection = "Host";
        const string DefaultHost = "localhost";
        const int DefaultPort = 8284;
        const string StartupSection = "Startup";
        
        public Settings ReadSettings()
        {
            var result = new Settings();
            
            LoadIniFile(Content);
            result.Host = Read(HostSection, "RestHost", DefaultHost);
            result.Port = Read(HostSection, "RestPort", DefaultPort, ConfigConverters.ConvertToInt);
            
            result.FixedTerminalId = Read(StartupSection, "Terminal", 0, ConfigConverters.ConvertToInt);
            result.FixedWaiterId = Read(StartupSection, "Waiter", 0, ConfigConverters.ConvertToInt);
            result.FixedTable = Read(StartupSection, "Table", 0, ConfigConverters.ConvertToInt);
            result.FixedParty = Read(StartupSection, "Party", 0, ConfigConverters.ConvertToInt);

            return result;
        }

        string _content;
        string Content
        {
            get
            {
                if (string.IsNullOrEmpty(_content))
                {
                    _content = File.Exists(ConfigFile) ? File.ReadAllText(ConfigFile) : "";
                }
                return _content;
            }
        }
    }
}