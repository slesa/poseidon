#if never
using System;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using PosShare.Model;
using PosShare.Payloads;

namespace PosClient.Events
{
    public class ActionEventHandler : EventHandlerBase, IConnectToBus
    {
        readonly Environment _environment;
        
        public ActionEventHandler(Environment environment)
        {
            _environment = environment;
        }
        
        void OnOrdered(string msg)
        {
            var entry = JsonConvert.DeserializeObject<OrderedAction>(msg, new OrderAskItemConverter());
            ProcessEventHandler.PrintTableEntry(entry);
        }

        void OnCommitted(string msg)
        {
            Console.Write($"Committed: {msg}");
            // var entry = JsonConvert.DeserializeObject<CommittedAction>(msg);
            // ProcessEventHandler.PrintTableEntry(entry);
        }
        
        void OnOrderMissing(string msg)
        {
            var payload = JsonConvert.DeserializeObject<OrderPayload>(msg, new OrderAskItemConverter());
            _environment.CurrentOrder = payload;
            var callback = payload.AskItems[payload.CurrentCallback];
            switch (callback.Type)
            {
                case OrderAskType.Price: 
                    Console.Write($"Price for order: ");
                    break;
                case OrderAskType.Weight: 
                    Console.Write($"Weight of article: ");
                    break;
                case OrderAskType.Length: 
                    Console.Write($"Length of article: ");
                    break;
                case OrderAskType.Width: 
                    Console.Write($"Width of article: ");
                    break;
                case OrderAskType.Text: 
                    Console.Write($"Article text for order: ");
                    break;
                case OrderAskType.Constraint:
                    { Console.WriteLine($"Choose constraint article: ");
                      var cc = callback as OrderAskConstraint;
                      foreach(var choice in cc.Choices) 
                        Console.WriteLine($"* {choice.Plu} {choice.Article}");
                      Console.Write("-> ");
                    } break;
            }
        }
        
        void OnOrderError(string msg)
        {
            Console.Write($"Could not order: {msg}");
        }

        public void Connect(HubConnection connection)
        {
            TakeConnection(connection);
            
            connection.On<string>(EventPayload.EvtOrdered, OnOrdered);
            connection.On<string>(EventPayload.EvtOrderMissing, OnOrderMissing);
            connection.On<string>(EventPayload.EvtOrderError, OnOrderError);
            connection.On<string>(EventPayload.EvtCommitted, OnCommitted);
        }
        
    }
}
#endif
