#if never
*using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using PosShare.GuiBus;
using PosShare.Payloads;

namespace PosClient.Events
{
    public class DeviceEventHandler : EventHandlerBase, IConnectToBus
    {
        readonly Environment _environment;

        public DeviceEventHandler(Environment environment)
        {
            _environment = environment;
        }

        void OnHandleKeylock(string msg)
        {
            var payload = JsonConvert.DeserializeObject<KeylockPayload>(msg);
            /* if (payload.Terminal.Id != _environment.CurrentTerminal.Id)
            {
                System.Diagnostics.Debug.WriteLine("Keylock for another terminal");
                return;
            } */
            var cmd = new KeylockPayload(_environment.SessionId).WithKey(payload.Key);
            _sender.SendCommand(CommandPayload.CmdSetWaiterByKey, cmd);
        }

        public void Connect(HubConnection connection)
        {
            TakeConnection(connection);
            connection.On<string>(EventPayload.EvtDevKeylock, OnHandleKeylock);
        }
    }
}
#endif
