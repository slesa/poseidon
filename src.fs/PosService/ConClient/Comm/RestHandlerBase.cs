using ConClient.Config;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;

namespace ConClient.Comm
{
    public class RestHandlerBase
    {
        private readonly Settings _settings;

        public RestHandlerBase(Settings settings)
        {
            _settings = settings;
        }
        
        public RestClient Connection()
        {
            var connect = $"http://{_settings.Host}:{_settings.Port}";
            var client = new RestClient(connect);
            client.UseNewtonsoftJson();
            return client;
        }

        public RestRequest Request(string func, Method method=Method.GET)
        {
            var request = new RestRequest($"pos/{func}", method);
            request.AddHeader("Accept", "accplication/json");
            return request;
        }
    }
}