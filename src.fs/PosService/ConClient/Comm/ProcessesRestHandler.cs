using System;
using System.Net;
using ConClient.Config;
using PosShare;
using PosShare.Model;
using PosShare.Payloads;
using RestSharp;

namespace ConClient.Comm
{
    public class ProcessesRestHandler : RestHandlerBase, IHandleProcessesRest
    {
        public ProcessesRestHandler(Settings settings) : base(settings)
        {
        }

        public GotProcessesPayload GetTables(Guid sessionId)
        {
            var body = JSonShare.Serialize(new CommandPayload {SessionId = sessionId});
            var buffer = Execute(CommandPayload.CmdGetTables, body);
            var result = JSonShare.Deserialize<GotProcessesPayload>(buffer);
            return result;
        }

        public ProcessOpenedPayload OpenTable(Guid sessionId, int tableId, int partyId)
        {
            var body = JSonShare.Serialize(new OpenTablePayload {SessionId = sessionId, Table = tableId, Party = partyId});
            var buffer = Execute(CommandPayload.CmdOpenTable, body);
            var result = JSonShare.Deserialize<ProcessOpenedPayload>(buffer);
            return result;
        }

        public GotProcessesPayload GetCheckouts(Guid sessionId)
        {
            var body = JSonShare.Serialize(new CommandPayload {SessionId = sessionId});
            var buffer = Execute(CommandPayload.CmdGetCheckouts, body);
            var result = JSonShare.Deserialize<GotProcessesPayload>(buffer);
            return result;
        }

        public ProcessOpenedPayload OpenCheckout(Guid sessionId, int checkoutId)
        {
            var body = JSonShare.Serialize(new OpenCheckoutPayload {SessionId = sessionId, CheckoutId = checkoutId});
            var buffer = Execute(CommandPayload.CmdOpenCheckout, body);
            var result = JSonShare.Deserialize<ProcessOpenedPayload>(buffer);
            return result;
        }

        public EventPayload CloseProcess(Guid sessionId, Guid processId)
        {
            var body = JSonShare.Serialize(new ProcessPayload(sessionId, processId));
            var buffer = Execute(CommandPayload.CmdCloseProcess, body);
            var result = JSonShare.Deserialize<EventPayload>(buffer);
            return result;
        }

        string Execute(string target, string body)
        {
            var connection = Connection();
            var request = Request($"process/{target}");
            if (!string.IsNullOrEmpty(body))
                request.AddParameter("application/json", body, ParameterType.RequestBody);
            var response = connection.Execute(request, Method.POST);
            if( response.StatusCode!=HttpStatusCode.OK)
                Console.WriteLine($"Request to {target} failed: {response.StatusCode} {response.ErrorMessage}");
            return response.Content;
        }
    }
}