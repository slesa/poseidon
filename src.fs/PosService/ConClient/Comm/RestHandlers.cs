using ConClient.Config;

namespace ConClient.Comm
{
    public class RestHandlers
    {
        public RestHandlers(Settings settings)
        {
            CoreDataHandler = new CoreDataRestHandler(settings);
            SystemHandler = new SystemRestHandler(settings);
            ProcessesHandler = new ProcessesRestHandler(settings);
            ActionsHandler = new ActionsRestHandler(settings);
        }

        public IHandleSystemRest SystemHandler { get; }
        public IHandleCoreDataRest CoreDataHandler { get; } 
        public IHandleProcessesRest ProcessesHandler { get; }
        public IHandleActionsRest ActionsHandler { get; }
    }
}