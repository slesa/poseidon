using System;
using PosShare.Payloads;

namespace ConClient.Comm
{
    public interface IHandleSystemRest
    {
        TurnedOnPayload TurnOn(int terminal);
        void TurnOff(Guid sessionId);
        LoggedInPayload AutoLogin(Guid sessionId, int waiter);
        LoggedInPayload Login(Guid sessionId, int waiter, string password="");
        void Logout(Guid sessionId);
    }
}