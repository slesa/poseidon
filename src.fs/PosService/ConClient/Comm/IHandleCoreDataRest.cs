﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PosShare.CoreData;

namespace ConClient.Comm
{
    public interface IHandleCoreDataRest
    {
        Task<IEnumerable<Article>> GetArticles();
        Task<Article> GetArticle(int id);
        Task<IEnumerable<Currency>> GetCurrencies();
        Task<Currency> GetCurrency(int id);
        Task<IEnumerable<Family>> GetFamilies();
        Task<Family> GetFamily(int id);
        Task<IEnumerable<FamilyGroup>> GetFamilyGroups();
        Task<FamilyGroup> GetFamilyGroup(int id);
        Task<IEnumerable<Payform>> GetPayforms();
        Task<Payform> GetPayform(int id);
        Task<IEnumerable<Terminal>> GetTerminals();
        Task<Terminal> GetTerminal(int id);
        Task<IEnumerable<VatRate>> GetVatRates();
        Task<VatRate> GetVatRate(int id);
        Task<IEnumerable<Waiter>> GetWaiters();
        Task<Waiter> GetWaiter(int id);
    }
}