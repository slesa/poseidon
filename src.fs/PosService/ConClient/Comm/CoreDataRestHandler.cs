﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ConClient.Config;
using PosShare.CoreData;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;

namespace ConClient.Comm
{
    public class CoreDataRestHandler : RestHandlerBase, IHandleCoreDataRest
    {
        public CoreDataRestHandler(Settings settings) : base(settings)
        {
        }
        
        #region Articles
        public async Task<IEnumerable<Article>> GetArticles()
        {
            return await Execute<IEnumerable<Article>>("articles");
        }
        public async Task<Article> GetArticle(int id)
        {
            return await Execute<Article>("articles/{id}");
        }
        #endregion Articles

        #region Currencies
        public async Task<IEnumerable<Currency>> GetCurrencies()
        {
            return await Execute<IEnumerable<Currency>>("currencies");
        }
        public async Task<Currency> GetCurrency(int id)
        {
            return await Execute<Currency>("currencies/{id}");
        }
        #endregion Currencles
        
        #region Families
        public async Task<IEnumerable<Family>> GetFamilies()
        {
            return await Execute<IEnumerable<Family>>("families");
        }
        public async Task<Family> GetFamily(int id)
        {
            return await Execute<Family>("families/{id}");
        }
        #endregion Families

        #region Family Groups
        public async Task<IEnumerable<FamilyGroup>> GetFamilyGroups()
        {
            return await Execute<IEnumerable<FamilyGroup>>("famgroups");
        }

        public async Task<FamilyGroup> GetFamilyGroup(int id)
        {
            return await Execute<FamilyGroup>("famgroups/{id}");
        }
        #endregion Family Groups
        
        #region Payforms
        public async Task<IEnumerable<Payform>> GetPayforms()
        {
            return await Execute<IEnumerable<Payform>>("payforms");
        }
        public async Task<Payform> GetPayform(int id)
        {
            return await Execute<Payform>("payforms/{id}");
        }
        #endregion Payforms

        #region Terminals
        public async Task<IEnumerable<Terminal>> GetTerminals()
        {
            return await Execute<IEnumerable<Terminal>>("terminals");
        }
        public async Task<Terminal> GetTerminal(int id)
        {
            return await Execute<Terminal>("terminals/{id}");
        }
        #endregion Terminals

        #region VAT rates
        public async Task<IEnumerable<VatRate>> GetVatRates()
        {
            return await Execute<IEnumerable<VatRate>>("vatrates");
        }
        public async Task<VatRate> GetVatRate(int id)
        {
            return await Execute<VatRate>("vatrates/{id}");
        }
        #endregion VAT rates

        #region Waiters
        public async Task<IEnumerable<Waiter>> GetWaiters()
        {
            return await Execute<IEnumerable<Waiter>>("waiters");
        }

        public async Task<Waiter> GetWaiter(int id)
        {
            return await Execute<Waiter>("waiters/{id}");
        }
        #endregion Waiters


        async Task<T> Execute<T>(string target)
        {
            var connection = Connection();
            var request = Request($"data/{target}");
            var response = connection.Execute<T>(request);
            return response.Data;
        }
    }
}