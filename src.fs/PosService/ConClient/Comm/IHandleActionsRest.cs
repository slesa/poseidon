using System;
using PosShare.Payloads;

namespace ConClient.Comm
{
    public interface IHandleActionsRest
    {
        EventPayload Order(Guid sessionId, Guid processId, int count, int plu, string article=null, decimal? price=null);
        EventPayload Commit(Guid sessionId, Guid processId);
    }
}