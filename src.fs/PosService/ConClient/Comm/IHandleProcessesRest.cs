using System;
using PosShare.Payloads;

namespace ConClient.Comm
{
    public interface IHandleProcessesRest
    {
        GotProcessesPayload GetTables(Guid sessionId);
        ProcessOpenedPayload OpenTable(Guid envSessionId, int tableId, int partyId);
        GotProcessesPayload GetCheckouts(Guid sessionId);
        ProcessOpenedPayload OpenCheckout(Guid sessionId, int checkoutId);
        EventPayload CloseProcess(Guid sessionId, Guid processId);
    }
}