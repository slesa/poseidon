using System;
using System.Net;
using ConClient.Config;
using PosShare;
using PosShare.Payloads;
using RestSharp;

namespace ConClient.Comm
{
    public class ActionsRestHandler : RestHandlerBase, IHandleActionsRest
    {
        public ActionsRestHandler(Settings settings) : base(settings)
        {
        }

        public EventPayload Order(Guid sessionId, Guid processId, int count, int plu, string article=null, decimal? price=null)
        {
            var payload = new OrderCommandPayload(sessionId, processId)
                .WithCount(count).WithPlu(plu);
            if( !string.IsNullOrEmpty(article))
                payload.Article = article;
            if (price.HasValue)
                payload.Price = price;
            var body = JSonShare.Serialize(payload);
            var buffer = Execute(CommandPayload.CmdOrder, body);
            var result = JSonShare.Deserialize<EventPayload>(buffer);
            return result;
        }

        public EventPayload Commit(Guid sessionId, Guid processId)
        {
            var payload = new ProcessPayload(sessionId, processId);
            var body = JSonShare.Serialize(payload);
            var buffer = Execute(CommandPayload.CmdCommit, body);
            var result = JSonShare.Deserialize<EventPayload>(buffer);
            return result;
        }
        
        string Execute(string target, string body)
        {
            var connection = Connection();
            var request = Request($"actions/{target}");
            if (!string.IsNullOrEmpty(body))
                request.AddParameter("application/json", body, ParameterType.RequestBody);
            var response = connection.Execute(request, Method.POST);
            if( response.StatusCode!=HttpStatusCode.OK)
                Console.WriteLine($"Request to {target} failed: {response.StatusCode} {response.ErrorMessage}");
            return response.Content;
        }
    }
}