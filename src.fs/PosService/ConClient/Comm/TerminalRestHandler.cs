using ConClient.Config;

namespace ConClient.Comm
{
    public class TerminalRestHandler : RestHandlerBase, IHandleTerminalRest 
    {
        public TerminalRestHandler(Settings settings) : base(settings)
        {
        }
    }
}