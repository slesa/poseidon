using System;
using System.Net;
using ConClient.Config;
using PosShare;
using PosShare.Payloads;
using RestSharp;

namespace ConClient.Comm
{
    public class SystemRestHandler : RestHandlerBase, IHandleSystemRest
    {
        public SystemRestHandler(Settings settings) : base(settings)
        {
        }

        public TurnedOnPayload TurnOn(int terminal)
        {
            var body = JSonShare.Serialize(new TurnOnPayload {Terminal = terminal});
            var buffer = Execute(CommandPayload.CmdTurnOn, body);
            var result = JSonShare.Deserialize<TurnedOnPayload>(buffer);
            return result;
        }

        public void TurnOff(Guid sessionId)
        {
            var body = JSonShare.Serialize(new CommandPayload {SessionId = sessionId});
            Execute(CommandPayload.CmdTurnOff, body);
        }

        public LoggedInPayload AutoLogin(Guid sessionId, int waiter)
        {
            var body = JSonShare.Serialize(new LoginPayload {SessionId = sessionId, Waiter = waiter, Automatic = true});
            var buffer = Execute(CommandPayload.CmdSetWaiter, body);
            var result = JSonShare.Deserialize<LoggedInPayload>(buffer);
            return result;
        }

        public LoggedInPayload Login(Guid sessionId, int waiter, string password="")
        {
            var body = JSonShare.Serialize(new LoginPayload {SessionId = sessionId, Waiter = waiter, Password = password});
            var buffer = Execute(CommandPayload.CmdSetWaiter, body);
            var result = JSonShare.Deserialize<LoggedInPayload>(buffer);
            return result;
        }

        public void Logout(Guid sessionId)
        {
            var body = JSonShare.Serialize(new CommandPayload {SessionId = sessionId});
            Execute(CommandPayload.CmdClrWaiter, body);
        }

        string Execute(string target, string body)
        {
            var connection = Connection();
            var request = Request($"terminal/{target}");
            if (!string.IsNullOrEmpty(body))
                request.AddParameter("application/json", body, ParameterType.RequestBody);
            var response = connection.Execute(request, Method.POST);
            if( response.StatusCode!=HttpStatusCode.OK)
                Console.WriteLine($"Request to {target} failed: {response.StatusCode} {response.ErrorMessage}");
            return response.Content;
        }

    }
}