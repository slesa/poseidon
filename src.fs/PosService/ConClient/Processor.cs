using System;
using System.Collections.Generic;
using System.Linq;
using ConClient.Comm;
using ConClient.Config;
using ConClient.Input;
using ConClient.Model;

namespace ConClient
{
    public class Processor
    {
        const string cmdHelp = "help";
        const string cmdQuit = "quit";
        private readonly Settings _settings;
        Environment _env;

        public Processor(Settings settings, Environment env)
        {
            _settings = settings;
            _env = env;
        }

        public void Run()
        {
            var knownCommands = GetKnownCommands().ToList();
            var restHandlers = new RestHandlers(_settings);
                
            var parsers = CreateInputParsers(restHandlers, _env);
            var parser = new CommandParser(_env, parsers);
            foreach (var p in parsers) knownCommands.AddRange(p.KnownCommands);
            
//            var connectors = GetBusConnectors(_environment);
//            receiver.OpenConnection(connectors);

            
            if (_env.Settings.HasFixedTerminal())
            {
                Console.WriteLine($"Current terminal id has been set to {_env.Settings.FixedTerminalId}");
                var pparser = parsers.FirstOrDefault(x => x is SystemInputParser) as SystemInputParser;
                pparser.TurnOn(_env.Settings.FixedTerminalId);
            }

            EvalLoop(knownCommands, parser);
        }

        void EvalLoop(List<KnownCommand> knownCommands, CommandParser parser)
        {
            while (true)
            {
                Console.Write("Execute command: ");
                var input = Console.ReadLine();
                if (string.IsNullOrEmpty(input)) continue;
                if( parser.ParseInput(input)) continue;
                
                var values = input.Split(" ").ToList();
                var command = values.First().ToLower();
                if (command == cmdQuit) break;
                if (command == cmdHelp)
                {
                    ShowHelp(knownCommands);
                    continue;
                }

                if (!knownCommands.Any(x => x.Command == command))
                {
                    ShowHelp(knownCommands, command);
                    continue;
                }

                parser.Parse(command, values.Skip(1));
            }
        }

        void ShowHelp(IEnumerable<KnownCommand> knownCommands, string input=null)
        {
            if(!string.IsNullOrEmpty(input))
                Console.WriteLine($"Unknown command {input}");
            Console.WriteLine("Known commands:");
            foreach(var cmd in knownCommands)
                Console.WriteLine($"{cmd.Command:10}: {cmd.Parameters:10} | {cmd.Description}");
            Console.WriteLine("");
        }
        
/*        static IEnumerable<IConnectToBus> GetBusConnectors(Environment environment)
        {
            // yield return new DeviceEventHandler(environment);
        } */

        IEnumerable<IParseInput> CreateInputParsers(RestHandlers restHandlers, Environment environment)
        {
            yield return new CoreDataInputParser(restHandlers.CoreDataHandler, environment);
            yield return new ActionsInputParser(restHandlers.ActionsHandler, environment);
            var processes = new ProcessesInputParser(restHandlers.ProcessesHandler, environment);
            yield return processes;
            yield return new SystemInputParser(restHandlers.SystemHandler, processes, environment);
        }

        IEnumerable<KnownCommand> GetKnownCommands()
        {
            yield return new KnownCommand {Command = cmdQuit, Description = "Quit program"};
            yield return new KnownCommand {Command = cmdHelp, Description = "Show known commands"};
        }
    }
}