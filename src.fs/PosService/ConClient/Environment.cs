using System;
using ConClient.Config;
using PosShare.CoreData;
using PosShare.Payloads;

namespace ConClient
{
    public class Environment
    {
        public static readonly Terminal EmptyTerminal = new Terminal();
        public static readonly Waiter EmptyWaiter = new Waiter();

        public Environment(Settings settings)
        {
            Settings = settings;
            CurrentTerminal = EmptyTerminal;
            CurrentWaiter = EmptyWaiter;
        }
        
        public Settings Settings { get; }
        public Guid SessionId { get; set; }

        public bool HasTerminal() { return CurrentTerminal.Id!=0; }
        public Terminal CurrentTerminal { get; set; }
        
        public bool HasWaiter() { return CurrentWaiter.Id!=0; }
        public Waiter CurrentWaiter { get; set; }
        
        // public int CurrentTableId { get; set; }
        // public int CurrentPartyId { get; set; }
        // public int CurrentCheckoutId { get; set; }
        public Guid CurrentProcessId { get; set; }

        public OrderPayload CurrentOrder { get; set; }
    }
}