using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using log4net.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using PosService.Core;
using PosService.Core.Config;
using PosService.Core.Runtime;
using PosService.Core.Settings;
using PosShare;

namespace PosService
{
    public class Program
    {
        static ILog _log;

        public static void Main(string[] args)
        {
            var programPath = System.Reflection.Assembly.GetEntryAssembly().Location;
            var programInfo = new FileInfo(programPath);
            var workingDir = programInfo.DirectoryName;
            Directory.SetCurrentDirectory(workingDir);

            var log4netConfigFile = Path.Combine(workingDir, "etc", "log4net.xml");
            var fileInfo = new FileInfo(log4netConfigFile);
            ILoggerRepository logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, fileInfo);

            _log = LogManager.GetLogger(typeof(Program));

            var version = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();
            _log.Info($"[Start] PosService {version}");
            
            var configFile = Path.Combine(workingDir, Configure.ConfigFile);
            _log.Debug($"Read settimgs from {configFile}");
            RestServerSettings settings = null;
            var settingsReader = new Configure();

            try
            {
                settings = settingsReader.ReadRestSettings();
            }
            catch (Exception e)
            {
                _log.Debug($"Read Settings failed:\n{e}");
                throw;
            }

            var address = $"http://{settings.Host}:{settings.Port}";
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseUrls(address)
                .Build();
            host.Run();

//            CreateHostBuilder(args).Build().Run();
        }

        /* public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var settings = SettingsPersistence.Load<ServiceSettings>();
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls($"http://{settings.RestStdServer}:{settings.RestStdPort}");
                });
        } */
    }
}