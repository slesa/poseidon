using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Options;
using MQTTnet.Client.Subscribing;
using PosService.Core.NetBus;

namespace PosService.NetBus
{
    public class MqttConnector
    {
        readonly Guid _serviceId;
        readonly string _host;
        readonly int _port;
        readonly string _user;
        readonly string _pass;
        readonly IEnumerable<IHandleNetCommands> _handler;
        CancellationTokenSource _tokenSource;
        
        public MqttConnector(Guid serviceId, string host, int port, string user, string pass, IEnumerable<IHandleNetCommands> handler)
        {
            _serviceId = serviceId;
            _host = host;
            _port = port;
            _user = user;
            _pass = pass;
            _handler = handler;
            _tokenSource = new CancellationTokenSource();
        }
        /*public object SendCommand<T>(T cmd, string topic) where T: KafkaCommand
        {
            var msg = JsonConvert.SerializeObject(cmd, KafkaShare.GetJsonSettings());
            return Send(msg, topic);
        }
        
        public object SendEvent<T>(T evt, string topic) where T: KafkaEvent
        {
            var msg = JsonConvert.SerializeObject(evt, KafkaShare.GetJsonSettings());
            return Send(msg, topic);
        }*/
        
        public object Send(string message, string topic)
        {
            var appMsg = new MqttApplicationMessageBuilder()
                .WithTopic(topic).WithPayload(message).Build();
            try
            {
                if(Client.IsConnected)
                    Client.PublishAsync(appMsg, _tokenSource.Token).Wait();
                else
                    Console.WriteLine($"Cannot publish topic {topic}. Client not connected");
            }            
            catch (Exception e)
            {
                Console.WriteLine($"Oops, something went wrong: {e}");
            }
            return null;
        }

        MqttClientOptions _options;
        MqttClientOptions Options
        {
            get
            {
                if (_options == null)
                {
                    var builder =  new MqttClientOptionsBuilder()
                        .WithClientId(_serviceId.ToString())
                        .WithTcpServer(_host, _port);
                    if (!string.IsNullOrEmpty(_user))
                    {
                        builder = builder.WithCredentials(_user, _pass);
                    }
                    _options = (MqttClientOptions) builder.Build();
                }
                return _options;
            }
        }
        
        IMqttClient _client;
        internal IMqttClient Client
        {
            get
            {
                if (_client == null)
                {
                    Console.WriteLine($"Connecting to MQTT broker at {_host}:{_port}");
                    var mqttFactory = new MqttFactory();
                    _client = mqttFactory.CreateMqttClient();
                    _client.UseConnectedHandler(/*async*/ e =>
                    {
                        Console.WriteLine("### CONNECTED WITH BROKER ###");

                    });                    
                    try
                    {
                        var result = _client.ConnectAsync(Options, _tokenSource.Token).Result;
                        Console.WriteLine($"Connect mqtt: {result.ResultCode}");
                        _client.UseApplicationMessageReceivedHandler(ReceiveMessage);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error connect mqtt: {ex}");
                        _client = null;
                    }
                }
                return _client;
            }
//            set { _client = value; }
        }

        public void StartListening()
        {
            var options = new MqttClientSubscribeOptions();
            var topics = _handler.SelectMany(handler => handler.Topics);
            foreach (var topic in topics)
            {
                options.TopicFilters.Add(new MqttTopicFilter{ Topic = topic });   
            }
             
            Client.SubscribeAsync(options, _tokenSource.Token);
        }
        
        void ReceiveMessage(MqttApplicationMessageReceivedEventArgs args)
        {
            var topic = args.ApplicationMessage.Topic;
            var message =  Encoding.UTF8.GetString(args.ApplicationMessage.Payload);
            System.Console.WriteLine($"mqtt, in {topic} msg {message}");
            foreach (var handler in _handler.Where(handler => handler.CanHandle(topic)))
            {
                handler.Handle(topic, message);
                break;
            }
        }
    }
}