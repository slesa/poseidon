using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using PosService.Core.Commands;
using PosService.Core.Config;
using PosService.Core.CoreData;
using PosService.Core.CoreData.Classic;
using PosService.Core.NetBus;
using PosService.NetBus;
using PosService.Core.Devices;
using PosService.Core.Logic;
using PosService.Core.Runtime;
using PosService.Core.Settings;
using PosShare;
using Environment = PosService.Core.Environment;

namespace PosService
{
    public class Startup
    {
        readonly Environment _environment;
        //MqttConnector _mqttConnector;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _environment = new Environment();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<Environment>(_environment);
            services.AddSingleton<IProvideCoreData, CoreDataProvider>();
            services.AddSingleton<ICreateBons, BonCreator>();
            
            services.AddSingleton<IProvideSettings, SettingsProvider>();
            services.AddSingleton<IReadSettings, Configure>();
            
            services.AddSingleton<IHandleSystemCalls, SystemCallHandler>();
            services.AddSingleton<IHandleProcessesCalls, ProcessesCallHandler>();
            services.AddSingleton<IHandleActionsCalls, ActionsCallHandler>();
            
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "PosService", Version = "v1"});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "PosService v1"));
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseWebSockets();
            app.UseRouting();
            app.UseAuthorization();
            // app.UseDefaultFiles();
            // app.UseHttpsRedirection();

            /*app.UseCors(builder =>
                builder.WithOrigins($"http://{settings.RestStdServer}:{settings.RestStdPort}/pos")
                    // .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .SetIsOriginAllowed(_ => true)
                    .AllowCredentials()
            );*/
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                // endpoints.MapHub<PosController>("/pos");
            });
            

            /*
            var environment = app.ApplicationServices.GetService<Environment>();
            var dataProvider = app.ApplicationServices.GetService<IProvideCoreData>();

            var handlers = GetNetBusHandlers(dataProvider, environment); 
            _mqttConnector = new MqttConnector(settings.ServiceId.Value, settings.MqttServer, settings.MqttPort, settings.MqttUser, settings.MqttPass, handlers);
            if (!_mqttConnector.Client.IsConnected)
            {
                System.Diagnostics.Debug.WriteLine("Client not connected");
                return;
            }

            _mqttConnector.StartListening();*/
        }

        /* internal IEnumerable<IHandleNetCommands> GetNetBusHandlers(IProvideCoreData dataProvider, Environment environment)
        {
            yield return new KeylockHandler(dataProvider, environment);
        } */
    }
}