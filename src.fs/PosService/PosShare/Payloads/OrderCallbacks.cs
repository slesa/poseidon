using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PosShare.CoreData;

namespace PosShare.Payloads
{
    public enum OrderAskType
    {
//        Int, Double, String, Text, Currency, Constraint
        Price, Text, Weight, Length, Width, Constraint
    }

    public class OrderAskItem
    {
        protected OrderAskItem() {} // For serialization only
        protected OrderAskItem(OrderAskType type, int forLine, string label)
        {
            Type = type;
            Label = label;
            ForLine = forLine;
        }
        public OrderAskType Type { get; set; }
        public string Label { get; set; }
        public int ForLine { get; set; }
    }

    public class OrderAskItem<T> : OrderAskItem
    {
        internal OrderAskItem() {} // For serialization only
        public OrderAskItem(OrderAskType type, int forLine, string label, T @default) 
            : base(type, forLine, label)
        {
            Default = @default;
        }
        public T Default { get; set; }
    }

    public class OrderAskDouble : OrderAskItem<decimal>
    {
        internal OrderAskDouble() {} // For serialization only
        public OrderAskDouble(OrderAskType type, int forLine, string label, decimal @default, int precision=2) 
            : base(type, forLine, label, @default)
        {
            Precision = precision;
        }
        public int Precision { get; set; }
    }

    public class OrderAskString : OrderAskItem<string>
    {
        internal OrderAskString() {} // For serialization only
        public OrderAskString(OrderAskType type, int forLine, string label, string @default) 
            : base(type, forLine, label, @default)
        {
        }
    }

    public class OrderAskWeight : OrderAskDouble
    {
        internal OrderAskWeight() {} // For serialization only
        public OrderAskWeight(int forLine, decimal @default=0.0m) : base(OrderAskType.Weight, forLine, "Weight", @default, 3) { }
    }
    public class OrderAskLength : OrderAskDouble
    {
        internal OrderAskLength() {} // For serialization only
        public OrderAskLength(int forLine, decimal @default=0.0m) : base(OrderAskType.Length, forLine, "Length", @default) { }
    }
    public class OrderAskWidth : OrderAskDouble
    {
        internal OrderAskWidth() {} // For serialization only
        public OrderAskWidth(int forLine, decimal @default=0.0m) : base(OrderAskType.Width, forLine, "Width", @default) { }
    }
    public class OrderAskPrice : OrderAskDouble
    {
        internal OrderAskPrice() {} // For serialization only
        public OrderAskPrice(int forLine, decimal @default) : base(OrderAskType.Price, forLine, "Price", @default) { }
    }
    public class OrderAskArticleText : OrderAskString
    {
        internal OrderAskArticleText() {} // For serialization only
        public OrderAskArticleText(int forLine, string @default) : base(OrderAskType.Text, forLine, "Article text", @default) { }
    }

    public class ConstraintChoice
    {
        public int Plu { get; set; }
        public bool IsHint { get; set; }
        public bool UsePrice { get; set; }
        public decimal Price { get; set; }
        public string Article { get; set; }
    }
    
    public class OrderAskConstraint : OrderAskItem<ConstraintChoice>
    {
        internal OrderAskConstraint() {} // For serialization only
        public OrderAskConstraint(int forLine, Constraint constraint)
            : base(OrderAskType.Constraint, forLine, "Constraint", null)
        {
            Constraint = constraint;
        }
        public int Choice { get; set; }
        public OrderAskConstraint WithChoice(int choice) { Choice = choice; return this; }
        public Constraint Constraint { get; set; }
        public List<ConstraintChoice> Choices { get; } = new List<ConstraintChoice>();
    }
}