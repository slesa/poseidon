using System;

namespace PosShare.Payloads
{
    public class CommandPayload
    {
        public const string CmdTurnOn = "turnon";
        public const string CmdTurnOff = "turnoff";
        public const string CmdSetWaiter = "setwaiter";
        public const string CmdSetWaiterKey = "setwaiterkey";
        public const string CmdClrWaiter = "clrwaiter";
        
        public const string CmdGetTables = "gettables";
        public const string CmdOpenTable = "opentable";
        public const string CmdGetCheckouts = "getcheckouts";
        public const string CmdOpenCheckout = "opencheckout";
        public const string CmdCloseProcess = "closeprocess";
        
        public const string CmdOrder = "order";
        public const string CmdOrderProgress = "order_progress";
        public const string CmdCommit = "commit";
        public const string CmdVoid = "void";
        public const string CmdSplit = "split";
        
        public const string CmdGetArticles = "get_articles";
        public const string CmdGetArticle = "get_article";
        public const string CmdGetFamilies = "get_families";
        public const string CmdGetFamily = "get_family";
        public const string CmdGetFamilyGroups = "get_familygroups";
        public const string CmdGetFamilyGroup = "get_familygroup";
        public const string CmdGetWaiters = "get_waiters";
        public const string CmdGetWaiter = "get_waiter";
        
        public CommandPayload() {}
        public CommandPayload(Guid sessionId) { SessionId = sessionId; }
        public Guid SessionId { get; set; }
    }
}