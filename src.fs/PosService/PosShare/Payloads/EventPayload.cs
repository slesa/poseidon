using System;

namespace PosShare.Payloads
{
    // WARNING: Every property needs a getter because of JSon Converter!
    public class EventPayload
    {
        public const string EvtTerminalLoggedIn = "terminal_logged_in";
        public const string EvtTerminalLoggedOut = "terminal_logged_out";
        public const string EvtTerminalError = "terminal_error";
        public const string EvtWaiterLoggedIn = "waiter_logged_in";
        public const string EvtWaiterLoggedOut = "waiter_logged_out";
        public const string EvtWaiterError = "waiter_error";
        
        public const string EvtGotProcesses = "got_processes";
        public const string EvtProcessOpened = "process_opened";
        public const string EvtProcessClosed = "process_closed";
        public const string EvtProcessError = "process_error";
        public const string EvtGotTables = "got_tables";
        public const string EvtTableOpened = "table_opened";
        public const string EvtTableClosed = "table_closed";
        public const string EvtGotCheckouts = "got_checkouts";
        public const string EvtCheckoutOpened = "checkout_opened";
        public const string EvtCheckoutClosed = "checkout_closed";
        
        public const string EvtOrdered = "ordered";
        public const string EvtCommitted = "committed";
        
        public const string EvtOrderError = "order_error";
        public const string EvtOrderMissing = "order_missing";

        public const string EvtDevKeylock = "dev_keylock";
        
        public const string EvtDataError = "data_error";
        public const string EvtGotArticles = "got_articles";
        public const string EvtGotArticle = "got_article";
        public const string EvtGotFamilies = "got_families";
        public const string EvtGotFamily = "got_family";
        public const string EvtGotFamilyGroups = "got_familygroups";
        public const string EvtGotFamilyGroup = "got_familygroup";
        public const string EvtGotWaiters = "got_waiters";
        public const string EvtGotWaiter = "got_waiter";
        
        public EventPayload(){}
        public EventPayload(Guid sessionId, ErrorCode status){ SessionId=sessionId; Status = status; }
        public ErrorCode Status { get; set; }
        public Guid SessionId { get; set; }
        public EventPayload WithSessionId(Guid sessionId) { SessionId = sessionId; return this; }
    }
}