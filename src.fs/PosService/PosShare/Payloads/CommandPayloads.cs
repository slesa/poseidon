using System;

namespace PosShare.Payloads
{
    public class TurnOnPayload
    {
        public int Terminal { get; set; }
    }

    public class LoginPayload : CommandPayload
    {
        public int Waiter { get; set; }
        public string Password { get; set; }
        public bool Automatic { get; set; }
    }
    
    public class OpenTablePayload : CommandPayload
    {
        public int Table { get; set; }
        public int Party { get; set; }
    }
    
    public class OpenCheckoutPayload : CommandPayload
    {
        public int CheckoutId { get; set; }
    }
    
    public class ProcessPayload : CommandPayload
    {
        public ProcessPayload() {}
        public ProcessPayload(Guid sessionId, Guid processId) : base(sessionId) { ProcessId=processId; }
        public Guid ProcessId { get; set; }
    }
    
    
    public class KeylockPayload : CommandPayload 
    {
        public string Key { get; set; }
        public KeylockPayload WithKey(string key) { Key = key; return this; }
    }
    
    
}