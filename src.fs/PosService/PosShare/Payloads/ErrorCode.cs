namespace PosShare.Payloads
{
    
    public enum ErrorCode
    {
        None = 0, 
        NoSessionId = 1, 
        Password = 2, 
        Forbidden = 3,
        NotFound = 4 ,
        NoWaiter = 5,
        WrongWaiter = 6,
        NoTerminal = 7,
        WrongTerminal = 8,
        AlreadyOpen = 9,
        NoProcess = 10,
        InvalidValue = 11,
        MissingData = 12
    }
}