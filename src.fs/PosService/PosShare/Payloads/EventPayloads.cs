using System;
using System.Collections.Generic;
using PosShare.CoreData;
using PosShare.Model;

namespace PosShare.Payloads
{
    public class TurnedOnPayload : EventPayload
    {
        public Terminal Terminal { get; set; }
    }

    public class LoggedInPayload : EventPayload
    {
        public Waiter Waiter { get; set; }
    }

    public class GotProcessesPayload : EventPayload
    {
        public List<ProcessEntry> Processes { get; set; }
    }

    public class ProcessOpenedPayload : EventPayload
    {
        public Guid ProcessId { get; set; }
        public ProcessOpenedPayload WithProcessGuid(Guid id) { ProcessId = id; return this; }
        public int Table { get; set; }
        public ProcessOpenedPayload WithTable(int table)  { Table = table; return this; }
        public int Party { get; set; }
        public ProcessOpenedPayload WithParty(int party) { Party = party; return this; }
        public int CheckoutId { get; set; }
        public ProcessOpenedPayload WithCheckoutId(int id)  { CheckoutId = id; return this; }
        // public long ProgressiveId { get; set; }
        // public ProcessOpenedPayload WithProgressiveId(long id) { ProgressiveId = id; return this; }
        // public int Waiter { get; set; }
        // public ProcessOpenedPayload WithWaiter(int waiter) { Waiter = waiter; return this; }
        public List<ProcessAction> Actions { get; set; }
        public ProcessOpenedPayload WithActions(List<ProcessAction> actions)
        {
            Actions = actions;
            return this;
        }
    }

    
    
}