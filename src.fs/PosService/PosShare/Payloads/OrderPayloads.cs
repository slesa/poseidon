﻿using System;
using System.Collections.Generic;

namespace PosShare.Payloads
{
    public class OrderCommandPayload : ProcessPayload
    {
        public OrderCommandPayload() {}
        public OrderCommandPayload(Guid sessionId, Guid processId) : base(sessionId, processId) {}
        public int Count { get; set; }
        public OrderCommandPayload WithCount(int count) { Count = count; return this; }
        public int Plu { get; set; }
        public OrderCommandPayload WithPlu(int plu) { Plu = plu; return this; }
        public string Article { get; set; }
        public decimal? Price { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Length { get; set; }
        public decimal? Width { get; set; }
        
        public OrderPayload CurrentOrderPayload { get; set; }
    }
    
    public class OrderLine
    {
        public bool InProgress { get; set; }
        public OrderLine AsInProgress() { InProgress = true; return this; }

        public int Count { get; set; }
        public OrderLine WithCount(int count) { Count = count; return this; }
        public int Plu { get; set; }
        public OrderLine WithPlu(int plu) { Plu = plu; return this; }
        public string Article { get; set; }
        public OrderLine WithArticle(string article) { Article = article; return this; }
        public decimal? Price { get; set; }
        public OrderLine WithPrice(decimal? price) { Price = price; return this; }
        public decimal? Weight { get; set; }
        public OrderLine WithWeight(decimal? weight) { Weight = weight; return this; }
        public decimal? Length { get; set; }
        public OrderLine WithLength(decimal? length) { Length = length; return this; }
        public decimal? Width { get; set; }
        public OrderLine WithWidth(decimal? width) { Width = width; return this; }

        public bool IsFreePrice { get; set; }
        public OrderLine WithIsFreePrice(bool flag) { IsFreePrice = flag; return this; }
        public bool IsFreeText { get; set; }
        public OrderLine WithIsFreeText(bool flag) { IsFreeText = flag; return this; }
        public bool IsWeight { get; set; }
        public OrderLine WithIsWeight(bool flag) { IsWeight = flag; return this; }
        public bool IsCutGood { get; set; }
        public OrderLine WithIsCutGood(bool flag) { IsCutGood = flag; return this; }
        public bool IsAreaGood { get; set; }
        public OrderLine WithIsAreaGood(bool flag) { IsAreaGood = flag; return this; }

    }
    
    public class OrderPayload : ProcessPayload
    {
        public new OrderPayload WithProcessId(Guid id)  { ProcessId = id; return this; }

        List<OrderLine> _orderLines;
        public List<OrderLine> OrderLines => _orderLines ??= new List<OrderLine>();

        public int CurrentCallback { get; set; }
        public bool HasCallbacks => _askItems != null;
        List<OrderAskItem> _askItems;
        public List<OrderAskItem> AskItems => _askItems ??= new List<OrderAskItem>();
    }

    public class CommitPayload : ProcessPayload
    {
    }
}