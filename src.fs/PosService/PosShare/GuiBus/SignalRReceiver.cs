﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace PosShare.GuiBus
{
    public class SignalRReceiver
    {
        CancellationTokenSource _tokenSource;
        
        public SignalRReceiver(Settings settings)
        {
            var host = settings.RServer;
            var port = settings.RPort;
            _tokenSource = new CancellationTokenSource();
            
            Connection = new HubConnectionBuilder().WithUrl($"http://{host}:{port}/pos").WithAutomaticReconnect().Build();
        }

        public HubConnection Connection { get; set; }
        
        public async Task<bool> OpenConnection(IEnumerable<IConnectToBus> connectors)
        {
            foreach (var connector in connectors)
                connector.Connect(Connection);
            
            var connected = false;
            while (!connected)
            {
                try
                {
                    await Connection.StartAsync(_tokenSource.Token);
                    if (Connection.State == HubConnectionState.Connected)
                    {
                        return true;
                        // Connection.Closed += Connection_Closed;
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine($"SignalR connection error: {ex.Message}");
                }
                await Task.Delay(300);
            }
            return connected;
        }

        public void Stop()
        {
            Connection.StopAsync();
        }
    }
}