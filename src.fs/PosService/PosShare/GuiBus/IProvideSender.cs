﻿using PosShare.Payloads;

namespace PosShare.GuiBus
{
    public interface IProvideSender
    {
        object SendCommand(string command, CommandPayload payload);
        object SendEvent(string evtName, EventPayload payload);
        object SendEvent(string evtName, string payload);
        string IpAddress { get; }
    }
}