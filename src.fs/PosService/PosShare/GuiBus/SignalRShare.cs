using Newtonsoft.Json;

namespace PosShare.GuiBus
{
    public static class SignalRShare
    {
        public static JsonSerializerSettings GetJsonSettings()
        {
            var jsonSerializerSettings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.None,
                Formatting = Formatting.Indented,
            };
            return jsonSerializerSettings;
        }

        public static string Serialize<T>(T data)
        {
            var result = JsonConvert.SerializeObject(data, Formatting.Indented, GetJsonSettings());
            return result;
        }

        public static T Deserialize<T>(string buffer)
        {
            var result = JsonConvert.DeserializeObject<T>(buffer, GetJsonSettings());
            return result;
        }
    }
}