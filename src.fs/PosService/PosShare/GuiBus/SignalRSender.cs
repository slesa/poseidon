﻿using System.Threading;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using PosShare.Payloads;

namespace PosShare.GuiBus
{
    public abstract class SignalRSender : IProvideSender
    {
        CancellationTokenSource _tokenSource;

        public SignalRSender(string ipAddress)
        {
            _tokenSource = new CancellationTokenSource();
            IpAddress = ipAddress;
        }

        public string IpAddress { get; }

        public object SendCommand(string command, CommandPayload payload)
        {
            var msg = JsonConvert.SerializeObject(payload, SignalRShare.GetJsonSettings());
            return Send(command, msg, _tokenSource.Token);
        }

        public object SendEvent(string evtName, EventPayload payload)
        {
            var msg = JsonConvert.SerializeObject(payload, SignalRShare.GetJsonSettings());
            return Send(evtName, msg, _tokenSource.Token);
        }

        public object SendEvent(string evtName, string msg)
        {
            return Send(evtName, msg, _tokenSource.Token);
        }
        protected abstract object Send(string method, string msg, CancellationToken token);
    }

    public class SignalClientSender : SignalRSender
    {
        IClientProxy _client;

        public SignalClientSender(IClientProxy client, string ipAddress) : base(ipAddress)
        {
            _client = client;
        }
        
        protected override object Send(string method, string msg, CancellationToken token)
        {
            return _client.SendAsync(method, msg, token);
        }
    }
    
    public class SignalHubSender : SignalRSender
    {
        readonly HubConnection _connection;

        public SignalHubSender(HubConnection connection, string ipAddress) : base(ipAddress)
        {
            _connection = connection;
        }

        protected override object Send(string method, string msg, CancellationToken token)
        {
            return _connection.SendAsync(method, msg, token);
        }
    }
}