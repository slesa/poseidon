using Microsoft.AspNetCore.SignalR.Client;

namespace PosShare.GuiBus
{
    public interface IConnectToBus
    {
        void Connect(HubConnection connection);
    }
}