using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PosShare.Model;
using PosShare.Payloads;

namespace PosShare
{
    public abstract class CommonJsonConverter : JsonConverter
    {
        protected JsonReader CopyReaderForObject(JsonReader reader, JToken jToken)
        {
            var jTokenReader = jToken.CreateReader();
            jTokenReader.Culture = reader.Culture;
            jTokenReader.DateFormatString = reader.DateFormatString;
            jTokenReader.DateParseHandling = reader.DateParseHandling;
            jTokenReader.DateTimeZoneHandling = reader.DateTimeZoneHandling;
            jTokenReader.FloatParseHandling = reader.FloatParseHandling;
            jTokenReader.MaxDepth = reader.MaxDepth;
            jTokenReader.SupportMultipleContent = reader.SupportMultipleContent;
            return jTokenReader;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

    public class ProcessActionConverter : CommonJsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(ProcessAction).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType,
            object existingValue, JsonSerializer serializer)
        {
            var jObject = JObject.Load(reader);
            var type = (ActionType) (int) jObject["Type"];
            ProcessAction target = null;
            switch (type)
            {
                case ActionType.Order:
                    target = new OrderedAction();
                    break;
                default:
                    throw new NotImplementedException($"Process action, {type} not switched");
            }

            using (var pop = CopyReaderForObject(reader, jObject))
            {
                serializer.Populate(pop, target);
            }

            return target;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

    public class OrderAskItemConverter : CommonJsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(OrderAskItem).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType,
            object existingValue, JsonSerializer serializer)
        {
            var jObject = JObject.Load(reader);
            var type = (OrderAskType) (int) jObject["Type"];
            OrderAskItem target = null;
            switch (type)
            {
                case OrderAskType.Price:
                    target = new OrderAskPrice();
                    break;
                case OrderAskType.Text:
                    target = new OrderAskArticleText();
                    break;
                case OrderAskType.Weight:
                    target = new OrderAskWeight();
                    break;
                case OrderAskType.Length:
                    target = new OrderAskLength();
                    break;
                case OrderAskType.Width:
                    target = new OrderAskWidth();
                    break;
                case OrderAskType.Constraint:
                    target = new OrderAskConstraint();
                    break;
                default:
                    throw new NotImplementedException($"OrderAskItem, {type} not switched");
            }

            using (var pop = CopyReaderForObject(reader, jObject))
            {
                serializer.Populate(pop, target);
            }

            return target;
        }
    }

}