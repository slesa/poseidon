using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using PosShare.CoreData;

namespace PosShare.Model
{
    public enum ProcessType { Table, Checkout }
    
    public class ProcessEntry
    {
        public ProcessEntry(ProcessType type, Guid processId)
        {
            Type = type;
            ProcessId = processId;
        }
        public Guid ProcessId { get; set; }
        public int Table { get; set; }
        public ProcessEntry WithTable(int table) { Table = table; return this; }
        public int Party { get; set; }
        public ProcessEntry WithParty(int party) { Party = party; return this; }
        public int Checkout { get; set; }
        public ProcessEntry WithCheckout(int checkout) { Checkout = checkout; return this; }
        public DateTime CreatedAt { get; set;}
        public int Creator { get; set; }
        public string CreatorName { get; set; }
        public ProcessEntry WithCreated(CreatedAction created)
        {
            if (created != null)
            {
                CreatedAt = created.Timestamp;
                Creator = created.Waiter;
                CreatorName = created.WaiterName;
            }
            return this;
        }
        public ProcessType Type { get; set; } 
    }
    
    public class Processes
    {
        public const string ProcessesDir = "var/processes";

        public static IEnumerable<ProcessEntry> GetProcesses(Waiter waiter)
        {
            var files = Directory.GetFiles(GetProcessesDir());
            return LoadProcesses(files, waiter);
        }
        
        public static IEnumerable<ProcessEntry> GetTables(Waiter waiter)
        {
            var files = Directory.GetFiles(GetProcessesDir(), "T*.*");
            return LoadProcesses(files, waiter);
        }
        
        public static IEnumerable<ProcessEntry> GetCheckouts(Waiter waiter)
        {
            var files = Directory.GetFiles(GetProcessesDir(), "C*");
            return LoadProcesses(files, waiter);
        }

        static IEnumerable<ProcessEntry> LoadProcesses(string[] files, Waiter waiter)
        {
            foreach (var file in files)
            {
                var fn = new FileInfo(file).Name;
                var type = fn.StartsWith("T") ? ProcessType.Table : ProcessType.Checkout;
                fn = fn.Substring(1);
                var table = 0;
                var party = 0;
                var checkout = 0;
                if (type==ProcessType.Table)
                {
                    var parts = fn.Split(".");
                    table = int.Parse(parts[0]);
                    party = int.Parse(parts[1]);
                }
                if (type == ProcessType.Checkout)
                {
                    checkout = int.Parse(fn);
                }
                var content = File.ReadAllText(file);
                var process = JsonConvert.DeserializeObject<Process>(content);
                
                if (!waiter.CanAllTables && process.Created.Waiter != waiter.Id) continue;
                
                yield return new ProcessEntry(type, process.ProcessId)
                    .WithTable(table).WithParty(party).WithCheckout(checkout).WithCreated(process.Created);
            }
        }

        static string GetProcessesDir()
        {
            var di = new DirectoryInfo(ProcessesDir);
            if (!di.Exists)
                di.Create();
            return di.FullName;
        }
    }
}