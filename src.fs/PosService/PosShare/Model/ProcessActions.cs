using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PosShare.Payloads;

namespace PosShare.Model
{
    public enum ActionType
    {
        Creation, Order, Void, Split, Change, Commit
    }

    public class ProcessAction
    {
        internal ProcessAction() {} // For serlalization only
        protected ProcessAction(ActionType type, DateTime timestamp)
        {
            Type = type;
            Timestamp = timestamp;
        }
        public DateTime Timestamp { get; set; }
        public ActionType Type { get; set; }
        public bool Committed { get; set; }

        public Guid ActionId { get; set; }
        public ProcessAction WithActionId(Guid id) { ActionId = id; return this; }
        
        #region Terminal
        public int Terminal { get; set; }
        public string TerminalName { get; set; }
        public ProcessAction WithTerminal(int id, string name)
        {
            Terminal = id;
            TerminalName = name;
            return this;
        }
        #endregion Terminal

        #region Waiter
        public int Waiter { get; set; }
        public string WaiterName { get; set; }
        public ProcessAction WithWaiter(int id, string name)
        {
            Waiter = id;
            WaiterName = name;
            return this;
        }
        #endregion Waiter
    }
    
    public class CreatedAction
    {
        internal CreatedAction() {} // For serialization only
        public CreatedAction(DateTime timestamp, int terminal, string terminalName, int waiter, string waiterName)
        {
            Timestamp = timestamp;
            Terminal = terminal;
            TerminalName = terminalName;
            Waiter = waiter;
            WaiterName = waiterName;
        }
        public DateTime Timestamp { get; set; }
        public int Terminal { get; set; }
        public string TerminalName { get; set; }
        public int Waiter { get; set; }
        public string WaiterName { get; set; }
    }

    public class OrderedAction : ProcessAction
    {
        internal OrderedAction() {} // For serialization only
        public OrderedAction(DateTime timestamp) : base(ActionType.Order, timestamp) { }

        public OrderPayload Payload { get; set; }
        public OrderedAction WithPayload(OrderPayload payload)
        {
            Payload = payload;
            return this;
        }
    }

    public class CommittedAction : ProcessAction
    {
        internal CommittedAction() {} // For serialization only
        public CommittedAction(DateTime timestamp) : base(ActionType.Commit, timestamp) { }

        readonly List<ProcessAction> _actions = new();
        public List<ProcessAction> Actions => _actions;
    }
}