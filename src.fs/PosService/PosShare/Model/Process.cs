using System;
using System.Collections.Generic;

namespace PosShare.Model
{
    public class Process
    {
        public Guid ProcessId { get; set; }
        public string ProgressiveId { get; set; }
        public CreatedAction Created { get; set; }
        readonly List<ProcessAction> _actions = new();
        public List<ProcessAction> Actions => _actions;
    }

    public class TableProcess : Process
    {
        public TableProcess(int table, int party)
        {
            Table = table;
            Party = party;
        }
        public int Table { get; set; }
        public int Party { get; set; }
        public TableProcess WithProcessId(Guid id) { ProcessId = id; return this; }
    }

    public class CheckoutProcess : Process
    {
        public CheckoutProcess(int id)
        {
            Id = id;
        }
        public int Id { get; set; }
        public CheckoutProcess WithProcessId(Guid id) { ProcessId = id; return this; }
    }
}