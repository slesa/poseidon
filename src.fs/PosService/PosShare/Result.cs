namespace PosShare
{
    public class Error
    {
        public int Code { get; set; }
        public string Msg { get; set; }
    }
    
    public class Result<T>
    {
        public Result(T value)
        {
            IsError = false;
            Value = value;
        }

        public Result(Error value)
        {
            IsError = true;
            Error = value;
        }
        
        public bool IsError { get; }
        public T Value { get; }
        public Error Error { get; }
    }
}