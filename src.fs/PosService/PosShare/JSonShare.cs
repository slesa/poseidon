using Newtonsoft.Json;

namespace PosShare
{
    public class JSonShare
    {
        public static JsonSerializerSettings GetSettings()
        {
            var jsonSerializerSettings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.None,
                Formatting = Formatting.Indented,
            };
            return jsonSerializerSettings;
        }

        public static string Serialize<T>(T data)
        {
            var result = JsonConvert.SerializeObject(data, Formatting.Indented, GetSettings());
            return result;
        }

        public static T Deserialize<T>(string buffer)
        {
            var result = JsonConvert.DeserializeObject<T>(buffer, GetSettings());
            return result;
        }
    }
}