namespace PosShare.CoreData
{
    public class VatRate
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}