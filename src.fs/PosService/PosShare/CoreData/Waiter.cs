namespace PosShare.CoreData
{
    public class Waiter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Keylock { get; set; }
        
        public bool CanLogin { get; set; }
        public bool CanAllTables { get; set; }
        public bool CanCreateTable { get; set; }
        public bool CanOrder { get; set; }
        public bool CanVoid { get; set; }
        public bool CanPay { get; set; }
        public bool CanSplit { get; set; }
    }
}