namespace PosShare.CoreData
{
    public class Family
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int FamilyGroup { get; set; }
    }
}