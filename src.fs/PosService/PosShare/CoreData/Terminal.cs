namespace PosShare.CoreData
{
    public class Terminal
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}