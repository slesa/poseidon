namespace PosShare.CoreData
{
    public class FamilyGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}