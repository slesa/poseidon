using System.Collections.Generic;

namespace PosShare.CoreData
{
    public class ConstraintEntry
    {
        public int Plu { get; set; }
        public bool IsHint { get; set; }
        public bool UsePrice { get; set; }
        public decimal Price { get; set; }
    }
    
    public class ConstraintLevel
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public bool IsCloseable { get; set; }
        public bool IsRecursive { get; set; }
        public bool IsTakeCount { get; set; }

        List<ConstraintEntry> _entries;
        public List<ConstraintEntry> Entries
        {
            get { return _entries ??= new List<ConstraintEntry>();}
            set => _entries = value;
        }
    }
    
    public class Constraint
    {
        public int Plu { get; set; }
        
        public Constraint(int plu)
        {
            Plu = plu;
        }

        List<ConstraintLevel> _levels;
        public List<ConstraintLevel> Levels
        {
            get { return _levels ??= new List<ConstraintLevel>();}
            set => _levels = value;
        }
    }
}