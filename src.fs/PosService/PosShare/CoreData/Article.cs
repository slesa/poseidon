namespace PosShare.CoreData
{
    public class Article
    {
        public int Plu { get; set; }
        public string Name { get; set; }
        public int Family { get; set; }
        public decimal Price { get; set; }
        public bool IsHint { get; set; }
        public bool IsFreePrice { get; set; }
        public bool IsFreeText { get; set; }
        public bool IsWeight { get; set; }
        public bool IsCutGood { get; set; }
        public bool IsAreaGood { get; set; }
        public Constraint Constraint { get; set; }
    }
}