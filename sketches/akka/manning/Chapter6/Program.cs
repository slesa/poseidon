﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter6
{
    class GetData
    {
        private readonly string _key;
        public string Key { get { return _key; } }
        public GetData(string key)
        {
            _key = key;
        }
    }

    class SetData
    {
        private readonly string _data;
        private readonly string _key;

        public string Data { get {  return _data; } }
        public string Key { get { return _key; } }

        public SetData(string key, string data)
        {
            _data = data;
            _key = key;
        }
    }

    class DividerActor : UntypedActor
    {
        protected override void OnReceive(object message)
        {
            var denominator = (int)message;
            var result = 10 / denominator;
            Console.WriteLine("Result of dividing 10 by {0} is {1}", denominator, result);
        }
    }

    class DatabaseActor : UntypedActor
    {
        private Dictionary<string, string> _data = new Dictionary<string, string>();

        protected override void OnReceive(object message)
        {
            if (message is GetData)
            {
                var getDataMessage = (GetData)message;
                var keyLength = getDataMessage.Key.Length;
                Console.WriteLine("Received a GetData message with a key of length {0} characters", keyLength);
                var data = _data[getDataMessage.Key];
                Sender.Tell(data);
            }
            else if(message is SetData)
            {
                var setDataMessage = (SetData)message;
                _data[setDataMessage.Key] = setDataMessage.Data;
                Console.WriteLine("Updated the dictionary");
            }
        }
    }

    class AnomalyDetector : UntypedActor
    {
        protected override void OnReceive(object message)
        {
            if(message is Props)
            {
                var p = (Props)message;
                var actorRef = Context.ActorOf(p);
                Sender.Tell(actorRef);
            }
        }

        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(10, TimeSpan.FromMinutes(1.0), Decider.From(exception =>
            {
                if (exception is ArithmeticException)
                    return Directive.Restart;
                else if (exception is NullReferenceException)
                    return Directive.Resume;
                return Directive.Resume;
            }));
        }
    }

    class Program
    {
        static async Task TestDividerActor(IActorRef actor)
        {
            var child = await actor.Ask<IActorRef>(Props.Create<DividerActor>());
            child.Tell(1);
            child.Tell(10);
            child.Tell(0);
            child.Tell(5);
        }

        static async Task CallDb(IActorRef actor)
        {
            var child = await actor.Ask<IActorRef>(Props.Create<DatabaseActor>());
            
            //Set Key1 to TestValue
            child.Tell(new SetData("Key1", "TestValue"));

            //Get the just set key
            var validData = await child.Ask<string>(new GetData("Key1"));
            Console.WriteLine("The value of 'Key1' was '{0}'", validData);

            try
            {
                //Send a message which will cause an error
                var invalidData = await child.Ask<string>(new GetData(null), TimeSpan.FromMilliseconds(500.0));
                Console.WriteLine("The value of null was '{0}'", invalidData);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Never received a response for key 'null'");
            }

            //Get the original data again
            var repeatedValidData = await child.Ask<string>(new GetData("Key1"));
            Console.WriteLine("The value of 'Key1' was '{0}'", repeatedValidData);
        }

        static void Main(string[] args)
        {
            var actorSystem = ActorSystem.Create("MyActorSystem");
            var actorRef = actorSystem.ActorOf<AnomalyDetector>("actorA");

            TestDividerActor(actorRef).Wait();

            CallDb(actorRef).Wait();

            Console.ReadLine();
        }
    }
}
