﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter6
{
    public class ReaperActor : ReceiveActor
    {
        readonly HashSet<IActorRef> _watchedActors;

        public ReaperActor(HashSet<IActorRef> watchedActors)
        {
            _watchedActors = watchedActors;

            Receive<Terminated>(terminated =>
            {
                _watchedActors.Remove(terminated.ActorRef);
                if(_watchedActors.Count == 0)
                {
                    Context.System.Terminate();
                }
            });
        }

        protected override void PreStart()
        {
            foreach(var actor in _watchedActors)
            {
                Context.Watch(actor);
            }
        }
    }
}
