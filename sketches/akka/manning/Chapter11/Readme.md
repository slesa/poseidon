﻿# Chapter 11

This chapter relies on a SQL server express instance running locally. To set this up run the following commands at the command line if you don't already have a database server:

```
"C:\Program Files\Microsoft SQL Server\130\Tools\Binn\SqlLocalDB.exe" create MSSQLLocalDB

"C:\Program Files\Microsoft SQL Server\130\Tools\Binn\SqlLocalDB.exe" start MSSQLLocalDB
```

Once you have a server in place create a new database called AkkaPersistence and then run queries.sql