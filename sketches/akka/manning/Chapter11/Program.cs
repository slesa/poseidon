﻿using Akka;
using Akka.Actor;
using Akka.Configuration;
using Akka.Persistence;
using Akka.Persistence.Journal;
using Chapter11.Events.V1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter11
{
    namespace Events.V1
    {
        public class ItemAdded
        {
            public string ItemIdentifier { get; }
            public ItemAdded(string itemIdentifier)
            {
                ItemIdentifier = itemIdentifier;
            }
        }
    }

    namespace Events.V2
    {
        public class StockIdentifier
        {
            public string Identifier { get; }
            public StockIdentifier(string identifier)
            {
                Identifier = identifier;
            }
        }

        public class ItemAdded
        {
            public StockIdentifier ItemIdentifier { get; }

            public ItemAdded(StockIdentifier identifier)
            {
                ItemIdentifier = identifier;
            }
        }   
    }

    public class ShoppingCartActor : PersistentActor
    {
        public class AddItem
        {
            private readonly string _itemId;
            private readonly int _itemCount;

            public string ItemId { get { return _itemId; } }
            public int ItemCount { get { return _itemCount; } }

            public AddItem(string itemId, int itemCount)
            {
                _itemId = itemId;
                _itemCount = itemCount;
            }
        }

        public class GetCart { }

        public class Cart
        {
            private readonly Dictionary<string, int> _items;
            public Dictionary<string, int> Items { get { return _items; } }

            public Cart(Dictionary<string, int> items)
            {
                _items = items;
            }
        }

        public class ItemAdded
        {
            private readonly string _itemId;
            private readonly int _itemCount;

            public string ItemId { get { return _itemId; } }
            public int ItemCount { get { return _itemCount; } }

            public ItemAdded(string itemId, int itemCount)
            {
                _itemId = itemId;
                _itemCount = itemCount;
            }
        }

        private readonly Dictionary<string, int> _items = new Dictionary<string, int>();

        public override string PersistenceId => Self.Path.Name;

        protected override bool ReceiveCommand(object message)
        {
            if (message is AddItem)
            {
                var msg = (AddItem)message;
                var itemAddedEvent = new ItemAdded(msg.ItemId, msg.ItemCount);
                Persist(itemAddedEvent, HandleEvent);
                return true;
            }
            else if(message is GetCart)
            {
                Sender.Tell(new Cart(_items));
                return true;
            }
            return false;
        }

        private void HandleEvent(object @event)
        {
            if (@event is ItemAdded)
            {
                var evt = (ItemAdded)@event;
                if (_items.ContainsKey(evt.ItemId))
                {
                    var currentCount = _items[evt.ItemId];
                    var newCount = currentCount + evt.ItemCount;
                    _items[evt.ItemId] = newCount;
                }
                else
                {
                    _items[evt.ItemId] = evt.ItemCount;
                }
            }
        }

        protected override bool ReceiveRecover(object message)
        {
            HandleEvent(message);
            return true;
        }
    }

    public class ShoppingCartEventAdapter : IEventAdapter
    {
        public IEventSequence FromJournal(object evt, string manifest)
        {
            if(evt is Events.V1.ItemAdded)
            {
                var oldEvt = (Events.V1.ItemAdded)evt;
                var newEvt = new Events.V2.ItemAdded(new Events.V2.StockIdentifier(oldEvt.ItemIdentifier));
                return EventSequence.Single(newEvt);
            }
            return EventSequence.Single(evt);
        }

        public string Manifest(object evt)
        {
            return String.Empty;
        }

        public object ToJournal(object evt)
        {
            if(evt is Events.V2.ItemAdded)
            {
                var newEvt = (Events.V2.ItemAdded)evt;
                return new Events.V1.ItemAdded(newEvt.ItemIdentifier.Identifier);
            }
            return evt;
        }
    }
      
    class MessageEnvelope
    {
        public long DeliveryId { get; }
        public string Message { get; }
        public MessageEnvelope(long deliveryId, string message)
        {
            DeliveryId = deliveryId;
            Message = message;
        }
    }

    class MessageSent
    {
        public string Message { get; }
        public MessageSent(string message)
        {
            Message = message;
        }
    }

    class Confirm
    {
        public long DeliveryId { get; }
        public Confirm(long deliveryId)
        {
            DeliveryId = deliveryId;
        }
    }

    class MessageConfirmed
    {
        public long DeliveryId { get; }
        public MessageConfirmed(long deliveryId)
        {
            DeliveryId = deliveryId;
        }
    }

    class CustomDeliveryActor : AtLeastOnceDeliveryActor
    {
        private readonly IActorRef _destinationActor;

        public CustomDeliveryActor(IActorRef destinationActor)
        {
            _destinationActor = destinationActor;
        }

        public int WarnAfterNumberOfUnconfirmedAttempts { get { return 10000; } }

        public override string PersistenceId { get { return "GuaranteedSender"; } }

        private void Handler(MessageSent message)
        {
            Deliver(_destinationActor.Path, deliveryId => new MessageEnvelope(deliveryId, message.Message));
        }

        private void Handler(MessageConfirmed confirmed)
        {
            ConfirmDelivery(confirmed.DeliveryId);
        }

        protected override bool ReceiveCommand(object message)
        {
            if(message is string)
            {
                Persist(new MessageSent((string)message), Handler);
                return true;
            }
            else if(message is Confirm)
            {
                Persist(new MessageConfirmed(((Confirm)message).DeliveryId), Handler);
                return false;
            }
            return false;
        }

        protected override bool ReceiveRecover(object message)
        {
            if(message is MessageConfirmed)
            {
                Handler((MessageConfirmed)message);
                return true;
            }
            else if(message is MessageSent)
            {
                Handler((MessageSent)message);
                return true;
            }
            return false;
        }
    }

    class GuaranteedPrinterActor : ReceiveActor
    {
        public GuaranteedPrinterActor()
        {
            Receive<MessageEnvelope>(msg =>
            {
                Console.WriteLine("Received a message: {0}", msg.Message);
                Sender.Tell(new Confirm(msg.DeliveryId));
            });
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var configFile = File.ReadAllText("chapter11.conf");
            var config = ConfigurationFactory.ParseString(configFile);
            var persistenceSystem = ActorSystem.Create("persist", config);

            var shoppingCart = persistenceSystem.ActorOf<ShoppingCartActor>("shoppingcart1");
            shoppingCart.Tell(new ShoppingCartActor.AddItem("Item1", 12));
            shoppingCart.Tell(new ShoppingCartActor.AddItem("Item2", 5));

            var cart = shoppingCart.Ask<ShoppingCartActor.Cart>(new ShoppingCartActor.GetCart()).Result;

            Console.WriteLine("Shopping cart contains:");
            foreach(var item in cart.Items)
            {
                Console.WriteLine("    {0} x {1}", item.Key, item.Value);
            }

            Console.ReadLine();
        }
    }
}
