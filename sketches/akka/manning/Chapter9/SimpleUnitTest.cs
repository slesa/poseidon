﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Chapter9
{
    public class ShoppingCart
    {
        public List<Product> Products { get; private set; }

        public ShoppingCart()
        {
            Products = new List<Product>();
        }

        public void AddItemToCart(Product product)
        {
            Products.Add(product);
        }
    }

    public class SimpleUnitTest
    {
        [Fact]
        public void TheCartContainsAProductAfterAddingIt()
        {
            var expectedProduct = new Product { Category = "homeware", Price = 3.52M, SKU = "01222546" };

            var shoppingCart = new ShoppingCart();

            shoppingCart.AddItemToCart(expectedProduct);

            Assert.Contains(expectedProduct, shoppingCart.Products);
        }
    }

    public class ShoppingCartActorTests : TestKit
    {
        [Fact]
        public void AddingAProductToTheShoppingCart()
        {
            var testProduct = new Product { Category = "homeware", Price = 9.99M, SKU = "0122445588" };
            var actor = new TestActorRef<ShoppingCartActor>(Sys, Props.Create<ShoppingCartActor>());

            actor.Tell(new AddProductToCart(testProduct, 1));

            var underlyingActor = actor.UnderlyingActor;

            Assert.Contains(testProduct, underlyingActor.Products);
        }
    }

    public class TurnstileActorTests : TestKit
    {
        [Fact]
        public void ValidatingATicketTransitionsToTheUnlockedState()
        {
            var expectedState = Unlocked.Instance;

            var actor = new TestFSMRef<TurnstileActor, ITurnstileState, ITurnstileData>(Sys, Props.Create<TurnstileActor>().WithDispatcher(CallingThreadDispatcher.Id));

            actor.Tell(new TicketValidated());
            actor.Tell(new TicketValidated());

            Assert.Equal(expectedState, actor.StateName);
        }
    }

    interface ITurnstileState { }

    class Locked : ITurnstileState {
        public static readonly Locked Instance = new Locked();
    }

    class Unlocked : ITurnstileState {
        public static readonly Unlocked Instance = new Unlocked();
    }

    interface ITurnstileData { }

    class TicketValidated { }

    class BarrierPushed { }

    class TurnstileActor : FSM<ITurnstileState, ITurnstileData>
    {
        public TurnstileActor()
        {
            When(Locked.Instance, @event =>
            {
                if (@event.FsmEvent is TicketValidated)
                {
                    return GoTo(Unlocked.Instance);
                }
                return Stay();
            });

            When(Unlocked.Instance, @event =>
            {
                if (@event.FsmEvent is BarrierPushed)
                {
                    return GoTo(Locked.Instance);
                }
                return Stay();
            });

            StartWith(Locked.Instance, null);
            Initialize();
        }
    }

    public class ShoppingCartIntegrationTesting : TestKit
    {
        [Fact]
        public void PricingEngineComputesCorrectPriceForAShoppingCart()
        {
            var pricingActor = Sys.ActorOf<PricingActor>();
            var cartActor = Sys.ActorOf(Props.Create<ShoppingCartActor>(pricingActor));

            Within(Dilated(TimeSpan.FromSeconds(2.0)), () =>
            {
                var product = new Product { Category = "homeware", Price = 9.99M, SKU = "011444566" };
                cartActor.Tell(new AddProductToCart(product, 2));
                cartActor.Tell(new GetCartOverview());

                ExpectMsg<CartOverview>();
            });
        }

        [Fact]
        public void CartRequestsTotalFromPricingEngine()
        {
            var testProbe = CreateTestProbe();

            var testProbeReference = testProbe.Ref;
            var cartActor = Sys.ActorOf(Props.Create<ShoppingCartActor>(testProbeReference));

            var product = new Product { Category = "homeware", Price = 9.99M, SKU = "000444325" };
            cartActor.Tell(new AddProductToCart(product, 1));


            cartActor.Tell(new GetCartOverview());

            testProbe.ExpectMsg<ComputeCartTotal>();
        }
    }
}