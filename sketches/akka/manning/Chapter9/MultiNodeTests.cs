﻿using Akka.Actor;
using Akka.Remote.TestKit;
using Akka.Remote.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter9
{
    public class MultiNodeGuaranteedMessagingConfig : MultiNodeConfig
    {
        private readonly RoleName _first;
        private readonly RoleName _second;

        public RoleName First { get { return _first; } }
        public RoleName Second { get { return _second; } }

        public MultiNodeGuaranteedMessagingConfig()
        {
            TestTransport = true;

            _first = Role("first");
            _second = Role("second");

            CommonConfig = DebugConfig(true);
        }
    }

    public abstract class MultiNodeGuaranteedMessagingSpec : MultiNodeSpec
    {
        readonly MultiNodeGuaranteedMessagingConfig _config;

        public MultiNodeGuaranteedMessagingSpec()
            : this(new MultiNodeGuaranteedMessagingConfig()) { }

        public MultiNodeGuaranteedMessagingSpec(MultiNodeGuaranteedMessagingConfig config)
            : base(config, typeof(MultiNodeGuaranteedMessagingSpec))
        {
            _config = config;
        }

        protected override int InitialParticipantsValueFactory
        {
            get
            {
                return Roles.Count;
            }
        }

        [MultiNodeFact]
        public void MessagesShouldBeResentIfNoAcknowledgement()
        {
            RunOn(() =>
            {
                var pricingActor = Sys.ActorOf(Props.Create<PricingActor>());
            }, _config.First);

            TestConductor.Throttle(_config.First, _config.Second, ThrottleTransportAdapter.Direction.Both, 0.5f);

            TestConductor.Shutdown(_config.First);

            EnterBarrier("DeploymentComplete");
        }
    }

    public class MultiNodeGuaranteedMessagingSpec1 
        : MultiNodeGuaranteedMessagingSpec
    { }

     public class MultiNodeGuaranteedMessagingSpec2 
        : MultiNodeGuaranteedMessagingSpec
    { }
}
