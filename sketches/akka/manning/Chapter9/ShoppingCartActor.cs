﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter9
{
    public class AddProductToCart
    {
        private readonly Product _product;
        private readonly int _quantity;

        public Product Product { get { return _product; } }
        public int Quantity { get { return _quantity; } }

        public AddProductToCart(Product product, int quantity)
        {
            _product = product;
            _quantity = quantity;
        }
    }

    public class GetCartOverview
    { }

    public class CartOverview
    {
        private readonly int _itemsCount;
        private readonly decimal _totalPrice;

        public int ItemsCount { get { return _itemsCount; } }
        public decimal TotalPrice { get { return _totalPrice; } }

        public CartOverview(int itemsCount, decimal totalPrice)
        {
            _itemsCount = itemsCount;
            _totalPrice = totalPrice;
        }
    }

    public class ComputeCartTotal
    {
        private readonly IReadOnlyList<Product> _products;

        public IReadOnlyList<Product> Products { get { return _products; } }

        public ComputeCartTotal(IReadOnlyList<Product> products)
        {
            _products = products;
        }
    }

    public class Product
    {
        public string SKU { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof(Product))
                return false;

            var other = (Product)obj;
            return other.Category.Equals(Category) && other.Price.Equals(Price) && other.SKU.Equals(SKU);
        }

        public override int GetHashCode()
        {
            return SKU.GetHashCode() ^ Category.GetHashCode() ^ Price.GetHashCode();
        }
    }

    public class ShoppingCartActor : ReceiveActor
    {
        private readonly List<Product> _products = new List<Product>();

        public IReadOnlyList<Product> Products { get { return _products.AsReadOnly(); } }

        public ShoppingCartActor()
        {
            Receive<AddProductToCart>(msg =>
            {
                for (var i = 0; i < msg.Quantity; i++)
                    _products.Add(msg.Product);
            });
        }

        public ShoppingCartActor(IActorRef pricingActor)
        {
            Receive<AddProductToCart>(msg =>
            {
                for (var i = 0; i < msg.Quantity; i++)
                    _products.Add(msg.Product);
            });

            Receive<GetCartOverview>(msg =>
            {
                var message = new ComputeCartTotal(_products.AsReadOnly());
                pricingActor.Forward(message);
            });
        }
    }

    public class PricingActor : ReceiveActor
    {
        public PricingActor()
        {
            Receive<ComputeCartTotal>(msg =>
            {
                var cartTotal = msg.Products.Aggregate(0M, (totalPrice, product) =>
                {
                    return totalPrice + (product.Price);
                });

                Sender.Tell(new CartOverview(msg.Products.Distinct().Count(), cartTotal));
            });
        }
    }
}
