﻿using Akka.Actor;
using Akka.Cluster;
using Akka.Routing;

namespace Chapter12
{
    public class Worker : ReceiveActor
    {
        public Worker()
        {
            var cluster = Cluster.Get(Context.System);
            Receive<WorkerMessage>(msg =>
            {
                Context.System.Log.Info("Node {0} received a message: {1}", cluster.SelfAddress, msg.Msg);
            });
        }
    }

    public class WorkerMessage : IConsistentHashable
    {
        private readonly string _msg;
        public string Msg { get { return _msg; } }

        public WorkerMessage(string msg)
        {
            _msg = msg;
        }

        public object ConsistentHashKey => _msg.GetHashCode() % 2;
    }
}