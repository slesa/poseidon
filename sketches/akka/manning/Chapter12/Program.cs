﻿using Akka.Actor;
using Akka;
using Akka.Cluster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Cluster.Tools.Singleton;
using Akka.Cluster.Sharding;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Cluster.Tools.Client;
using System.Collections.Immutable;
using Akka.Configuration;
using System.IO;
using Akka.Cluster.Routing;
using Akka.Routing;

namespace Chapter12
{
    class ClusterListener : ReceiveActor
    {
        public ClusterListener()
        {
            var cluster = Cluster.Get(Context.System);
            cluster.Subscribe(Self, ClusterEvent.SubscriptionInitialStateMode.InitialStateAsEvents, typeof(ClusterEvent.UnreachableMember));

            Receive<ClusterEvent.UnreachableMember>(msg =>
            {
                Context.System.Log.Info("A node was detected as being unreachable: {0}", msg.Member.Address);
            });
        }
    }

    class CoordinatorActor : ReceiveActor
    {
        public CoordinatorActor()
        {
            var cluster = Cluster.Get(Context.System);
            Receive<string>(msg =>
            {
                Context.System.Log.Info("Node {0} received a message: {1}", cluster.SelfAddress, msg);
            });
        }
    }

    class AddItemToCart
    {
        public string ItemId { get; }
        public string ShoppingCartId { get; }
        public AddItemToCart(string itemId, string shoppingCartId)
        {
            ItemId = itemId;
            ShoppingCartId = shoppingCartId;
        }
    }

    public class ShoppingCartActor : ReceiveActor
    {
        private List<string> _items = new List<string>();

        public ShoppingCartActor()
        {
            SetReceiveTimeout(TimeSpan.FromHours(1.0));

            Receive<ReceiveTimeout>(msg =>
            {
                Context.Parent.Tell(new Passivate(PoisonPill.Instance));
            });

            Receive<AddItemToCart>(msg =>
            {
                _items.Add(msg.ItemId);
            });
        }
    }

    class ShoppingCartMessageExtractor : IMessageExtractor
    {
        //The EntityId method retrieves the identifier for the shopping cart to which
        //the message should be directed
        public string EntityId(object message)
        {
            return (message as AddItemToCart)?.ShoppingCartId;
        }

        //The EntityMessage retrieves the overall content of the message which should be sent
        //to the target actor
        public object EntityMessage(object message)
        {
            return (message as AddItemToCart);
        }

        //The ShardId method is used to work out which shard the message should be directed to
        public string ShardId(object message)
        {
            var hash = (message as AddItemToCart)?.ShoppingCartId.GetHashCode();
            var shardId = hash % 100;
            return shardId.ToString();
        }
    }

    class ChatMessage
    {
        public string Sender { get; }
        public string MessageContent { get; }

        public ChatMessage(string sender, string messageContent)
        {
            Sender = sender;
            MessageContent = messageContent;
        }
    }

    class JoinChatroom
    {
        public string ChatroomName { get; }
        public JoinChatroom(string chatroomName)
        {
            ChatroomName = chatroomName;
        }
    }

    class UserActor : ReceiveActor
    {
        public UserActor()
        {
            //Register the actor to receive any messages published on the path of the actor
            var mediator = DistributedPubSub.Get(Context.System).Mediator;
            mediator.Tell(new Subscribe("chatroom", Self));
            mediator.Tell(new Put(Self));

            Receive<SubscribeAck>(_ => Become(Subscribed));
        }

        public void Subscribed()
        {
            Receive<ChatMessage>(msg =>
            {
                Console.WriteLine("Received a message from {0}: {1}", msg.Sender, msg.MessageContent);
            });
        }
    }

    class EchoActor : UntypedActor
    {
        protected override void OnReceive(object message)
        {
            Sender.Tell(message);
        }
    }

    class Program
    {
        static async Task ClusterSingletonDemo(ActorSystem actorSystem, Cluster cluster)
        {
            //Create the Props for the actor which will be deployed on only one node in the cluster
            var coordinatorProps = Props.Create<CoordinatorActor>();

            //Create the settings for the cluster singleton manager, retrieving them from the actor system
            //configuration and then overriding the name of the singleton
            var settings =
                ClusterSingletonManagerSettings.Create(actorSystem)
                .WithSingletonName("coordinator");

            //Create the Props for the cluster singleton manager by providing the props of the singleton and the
            //settings to use for the singleton
            var clusterSingletonProps =
                ClusterSingletonManager.Props(coordinatorProps, settings);

            var coordinatorSingleton = actorSystem.ActorOf(clusterSingletonProps, "coordinatorManager");

            //Create the settings for the proxy using the configuration supplied in the actor system
            //configuration and override the name of the singleton to match the name provided above when
            //creating the actor for the cluster singleton manager
            var coordinatorProxySettings =
                ClusterSingletonProxySettings.Create(actorSystem)
                .WithSingletonName("coordinator");

            //Create the Props for the coordinator, ensuring the path matches that of the cluster singleton manager
            //created above when deploying the cluster singleton manager
            var coordinatorProxyProps =
                ClusterSingletonProxy.Props("/user/coordinatorManager", coordinatorProxySettings);

            //Deploy the coordinator proxy to the path /user/coordinatorProxy other actors will then be able to
            //send messages to this address and have them automatically forwarded onto the singleton wherever it is
            //deployed within the cluster
            var coordinatorProxy =
                actorSystem.ActorOf(coordinatorProxyProps, "coordinatorProxy");

            coordinatorProxy.Tell("Hello coordinator!");

        }

        static async Task ClusterGossipDemo(ActorSystem actorSystem)
        {
            var clusterListener =
                actorSystem.ActorOf<ClusterListener>("clusterListener");

            //If you now close one of the running processes, the ClusterListener will write out a message letting us know that
            // a node has become unreachable.
        }

        static async Task ClusterShardingDemo(ActorSystem actorSystem, Cluster cluster)
        {
            //Create the Props for the actor which resides within a shard
            var shoppingCartProps =
                Props.Create<ShoppingCartActor>();

            //Retrieve the shard extension from the actor system
            var shardingExtension =
                ClusterSharding.Get(actorSystem);

            //Create the settings for the cluster sharding, retrieving them from the
            //actor system config
            var clusterShardingSettings =
                ClusterShardingSettings.Create(actorSystem);

            //Build up a shard region which will be responsible for creating and shutting
            //down actors on each individual node within the cluster
            var region =
                shardingExtension.Start("shoppingCart",
                                        shoppingCartProps,
                                        clusterShardingSettings,
                                        new ShoppingCartMessageExtractor());

            region.Tell(new AddItemToCart("REF2201", "1"));
        }

        static async Task ClusterPubSubDemo(ActorSystem actorSystem, Cluster cluster)
        {
            var pubSub = DistributedPubSub.Get(actorSystem);

            //Spawn the actor at the address /user/anthony
            var user =
                actorSystem.ActorOf<UserActor>("anthony");

            await Task.Delay(1000);

            var mediator = DistributedPubSub.Get(actorSystem).Mediator;
            mediator.Tell(new Publish("chatroom", new ChatMessage("user1", "Hello from user1")));

            mediator.Tell(new Send("/user/anthony", new ChatMessage("user2", "Hello user1!"), localAffinity: true));

            mediator.Tell(new SendToAll("/user/anthony", new ChatMessage("user2", "Hello all user1!")));
        }

        static void Main(string[] args)
        {
            string configFile;
            if (args.Length > 0)
            {
                configFile = File.ReadAllText("seednode.conf");
            }
            else
            {
                configFile = File.ReadAllText("node.conf");
            }
            var config = ConfigurationFactory.ParseString(configFile);

            var actorSystem = ActorSystem.Create("Chapter12Cluster", config);
            Cluster cluster = Cluster.Get(actorSystem);

            ClusterSingletonDemo(actorSystem, cluster).Wait();

            ClusterGossipDemo(actorSystem).Wait();

            ClusterShardingDemo(actorSystem, cluster).Wait();

            ClusterPubSubDemo(actorSystem, cluster).Wait();

            ClusterWorkerDemo(actorSystem, cluster).Wait();

            Console.ReadLine();
        }

        static async Task ClusterWorkerDemo(ActorSystem actorSystem, Cluster cluster)
        {
            var clusterPoolSettings = new ClusterRouterPoolSettings(1000, 5, true);
            var clusterPoolProps =
                Props.Create<Worker>().WithRouter(new ClusterRouterPool(new RoundRobinPool(5), clusterPoolSettings));

            var router = actorSystem.ActorOf(clusterPoolProps);

            await Task.Delay(5000);

            for(int i = 0; i < 100; i++)
            {
                router.Tell(new WorkerMessage(String.Format("Download page {0}", i)));
            }
        }
    }
}
