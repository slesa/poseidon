﻿using Akka.Actor;
using Akka.Configuration;
using Akka.Event;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac.Builder;
using Autofac;
using Akka.DI.AutoFac;
using Akka.DI.Core;

namespace Chapter5
{
    class PersonActor : UntypedActor
    {
        private readonly string _response;
        private readonly int _age;
        private readonly ILoggingAdapter _log = Logging.GetLogger(Context);

        public PersonActor(string messageResponse)
        {
            _response = messageResponse;
            _age = Context.System.Settings.Config.GetInt("akka.team.avgAge");
        }

        protected override void OnReceive(object message)
        {
            _log.Info("Actor {0} just received a new message", Self.Path);
            Console.WriteLine("Person received a message and in response I say '{0}'. Also I'm {1} years old.", _response, _age);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var configFile = File.ReadAllText("MyActorSystem.conf");
            var config = ConfigurationFactory.ParseString(configFile);

            var teamMembers = config.GetStringList("akka.team.members");
            foreach(var teamMember in teamMembers)
            {
                Console.WriteLine("{0} is on the team", teamMember);
            }

            var containerBuilder = new Autofac.ContainerBuilder();
            containerBuilder.RegisterInstance<string>("Hello from DI");
            containerBuilder.RegisterType<PersonActor>();
            var container = containerBuilder.Build();

            var actorSystem = ActorSystem.Create("MyActorSystem", config);

            var propsResolver = new AutoFacDependencyResolver(container, actorSystem);

            var propsFromDI = actorSystem.DI().Props<PersonActor>();
            var personDI = actorSystem.ActorOf(propsFromDI);
            personDI.Tell("Hello!");

            var props = Props.Create(typeof(PersonActor), "Hello from the constructor");
            var personActor = actorSystem.ActorOf(props);
            personActor.Tell("Hello!");
            Console.ReadLine();
        }
    }
}
