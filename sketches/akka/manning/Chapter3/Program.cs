﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter3
{
    class Wave { }

    class VocalGreeting
    {
        private readonly string _greeting;
        public string Greeting {  get { return _greeting; } }
        public VocalGreeting(string greeting)
        {
            _greeting = greeting;
        }
    }
    class PersonActor : UntypedActor
    {
        protected override void OnReceive(object message)
        {
            if (message is VocalGreeting)
            {
                var msg = (VocalGreeting)message;
                Console.WriteLine("Hello there!");
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var actorSystem = ActorSystem.Create("MyActorSystem");
            var actorRef = actorSystem.ActorOf<PersonActor>("actorA");

            var actorSelection = actorSystem.ActorSelection("/user/actorA");

            actorRef.Tell(new VocalGreeting("Hello world!"));

            Console.ReadLine();

        }
    }
}
