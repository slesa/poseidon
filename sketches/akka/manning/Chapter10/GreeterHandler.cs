﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Net.Http;
using Akka.Actor;

namespace Chapter10
{
    public class GreeterController : ApiController
    {
        [HttpGet]
        [Route("hello/{name}")]
        public async Task<string> GetGreeting(string name)
        {
            var owinCtx = Request.GetOwinContext();
            var actorSystem = owinCtx.Get<ActorSystem>("akka.actorsystem");
            var greeter = actorSystem.ActorSelection("/user/greeter");
            var response = await greeter.Ask<string>(name);
            return response;
        }
    }
}
