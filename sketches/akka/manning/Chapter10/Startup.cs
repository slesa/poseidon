﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Akka.Actor;
using System.Web.Http;
using Microsoft.AspNet.SignalR;
using System.IO;

[assembly: OwinStartup(typeof(Chapter10.Startup))]

namespace Chapter10
{
    public class GreeterActor : ReceiveActor
    {
        public GreeterActor()
        {
            Receive<string>(name =>
            {
                Sender.Tell(String.Format("Hello, {0}!", name));
            });
        }
    }

    public class WebsocketActor : ReceiveActor
    {
        public WebsocketActor()
        {
            var connection = GlobalHost.ConnectionManager.GetConnectionContext<GraphingConnection>();
            Receive<ClientConnected>(msg =>
            {
                Console.WriteLine("A new client ({0}) connected to the websocket", msg.ConnectionId);
                Context.System.Scheduler.ScheduleTellRepeatedly(1000, 10000, Self, new SendMessage(msg.ConnectionId, "Hello world"), Self);
            });
            Receive<SendMessage>(msg =>
            {
                connection.Connection.Send(msg.ConnectionId, msg.Data);
            });
            Receive<MessageReceived>(msg =>
            {
                Console.WriteLine("Received a message from the client: {0}", msg.Data);
            });
            Receive<ClientDisconnected>(msg =>
            {
                Console.WriteLine("A client ({0}) disconnected", msg.ConnectionId);
            });
        }
    }

    public class GraphingConnection : PersistentConnection
    {
        protected override Task OnConnected(IRequest request, string connectionId)
        {
            var actorSystem = (ActorSystem)request.Environment["akka.actorsystem"];
            var persistentConnectionActor = actorSystem.ActorSelection("/user/messagingConnection");
            persistentConnectionActor.Tell(new ClientConnected(connectionId));
            return base.OnConnected(request, connectionId);
        }
        protected override Task OnReceived(IRequest request, string connectionId, string data)
        {
            var actorSystem = (ActorSystem)request.Environment["akka.actorsystem"];
            var persistentConnectionActor = actorSystem.ActorSelection("/user/messagingConnection");
            persistentConnectionActor.Tell(new MessageReceived(connectionId, data));
            return base.OnReceived(request, connectionId, data);
        }

        protected override Task OnDisconnected(IRequest request, string connectionId, bool stopCalled)
        {
            var actorSystem = (ActorSystem)request.Environment["akka.actorsystem"];
            var persistentConnectionActor = actorSystem.ActorSelection("/user/messagingConnection");
            persistentConnectionActor.Tell(new ClientDisconnected(connectionId));
            return base.OnDisconnected(request, connectionId, stopCalled);
        }
    }

    public class SendMessage
    {
        private readonly string _connectionId;
        private readonly string _data;

        public string ConnectionId { get { return _connectionId; } }
        public string Data { get { return _data; } }

        public SendMessage(string connectionId, string data)
        {
            _connectionId = connectionId;
            _data = data;
        }
    }

    public class ClientDisconnected
    {
        private readonly string _connectionId;

        public string ConnectionId { get { return _connectionId; } }

        public ClientDisconnected(string connectionId)
        {
            _connectionId = connectionId;
        }
    }

    public class ClientConnected
    {
        private readonly string _connectionId;

        public string ConnectionId { get { return _connectionId; } }

        public ClientConnected(string connectionId)
        {
            _connectionId = connectionId;
        }
    }

    public class MessageReceived
    {
        private readonly string _connectionId;
        private readonly string _data;

        public string ConnectionId { get { return _connectionId; } }
        public string Data { get { return _data; } }

        public MessageReceived(string connectionId, string data)
        {
            _connectionId = connectionId;
            _data = data;
        }
    }

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var actorSystem = ActorSystem.Create("signalr");
            var greeter = actorSystem.ActorOf<GreeterActor>("greeter");
            var messaging = actorSystem.ActorOf<WebsocketActor>("messagingConnection");

            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();

            app.Use((ctx, next) =>
            {
                ctx.Environment["akka.actorsystem"] = actorSystem;
                return next();
            }).Use(async (ctx, next) =>
            {
                if(ctx.Request.Path == new PathString("/"))
                {
                    var html = File.ReadAllText("index.html");
                    await ctx.Response.WriteAsync(html);
                    ctx.Response.ContentType = "text/html";
                    return;
                }
                else
                {
                    await next();
                }
            }).MapSignalR<GraphingConnection>("/graph")
              .UseWebApi(config);
        }
    }
}