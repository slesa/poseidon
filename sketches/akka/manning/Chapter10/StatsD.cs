﻿using Akka.Actor;
using Akka.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Chapter10
{
    public class StatsDServer : ReceiveActor
    {
        public StatsDServer()
        {
            var tcpManager = Context.System.Tcp();

            tcpManager.Tell(new Tcp.Bind(Self, new IPEndPoint(IPAddress.Any, 8080)));

            Receive<Tcp.Bound>(msg =>
            {
                Console.WriteLine("The connection was bound to port 8080");
            });

            Receive<Tcp.Connected>(msg =>
            {
                var connectionActor = Context.ActorOf(Props.Create(() => new StatsDServerChannel()));
                Sender.Tell(new Tcp.Register(connectionActor));
            });
        }
    }

    public class StatsDServerChannel : ReceiveActor
    {
        public StatsDServerChannel()
        {
            Receive<Tcp.Received>(msg =>
            {
                var statsDData = msg.Data.DecodeString();
                HandleStatsDPacket(statsDData);
            });
        }

        public void HandleStatsDPacket(string data)
        {
            Console.WriteLine("Received the following packet: {0}", data);
        }
    }

    public class StatsDClient : ReceiveActor
    {
        public StatsDClient()
        {
            var tcpManager = Context.System.Tcp();

            var serverEndpoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8080);
            tcpManager.Tell(new Tcp.Connect(serverEndpoint));

            Receive<Tcp.Connected>(msg =>
            {
                Sender.Tell(new Tcp.Register(Self));
                Become(Connected(Sender));
            });

            Receive<Tcp.CommandFailed>(msg =>
            {
                Console.WriteLine("Failed to connect to the remote endpoint");
            });
        }

        private Action Connected(IActorRef connection)
        {
            return () =>
            {
                Receive<string>(msg =>
                {
                    var write = Tcp.Write.Create(ByteString.FromString(msg));
                    connection.Tell(write);

                });
            };
        }
    }
}
