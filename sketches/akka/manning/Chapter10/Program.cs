﻿using Akka.Actor;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Chapter10
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:8085";
            using (var app = WebApp.Start<Startup>(url))
            {
                Console.WriteLine("Server is now listening on address {0}", url);
                Console.ReadLine();
            }

            var ioSystem = ActorSystem.Create("akkaio");

            var statsDServer = ioSystem.ActorOf<StatsDServer>("server");

            var statsDClient = ioSystem.ActorOf<StatsDClient>("client");

            Thread.Sleep(1000);

            statsDClient.Tell("Sent from the client over the network");

            Console.ReadLine();
        }
    }
}
