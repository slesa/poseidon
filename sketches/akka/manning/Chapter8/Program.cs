﻿using Akka.Actor;
using Akka.Configuration;
using Akka.Routing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter8
{
    class DeleteAccount : IPossiblyHarmful
    {
        private readonly string _accountId;
        public string AccountId { get { return _accountId; } }

        public DeleteAccount(string accountId)
        {
            _accountId = accountId;
        }
    }

    public class AuthenticatedMessageEnvelope
    {
        private readonly object _payload;
        private readonly IActorRef _target;
        private readonly string _authenticationToken;

        public object Payload { get { return _payload; } }
        public IActorRef Target { get { return _target; } }
        public string AuthenticationToken { get { return _authenticationToken; } }

        public AuthenticatedMessageEnvelope(object payload, IActorRef target, string authenticationToken)
        {
            _payload = payload;
            _target = target;
            _authenticationToken = authenticationToken;
        }
    }

    class SystemReceptionist : ReceiveActor
    {
        public SystemReceptionist()
        {
            Receive<AuthenticatedMessageEnvelope>(env =>
            {
                if(IsAuthorised(env.AuthenticationToken))
                {
                    env.Target.Forward(env.Payload);
                }
            });
        }

        private bool IsAuthorised(string authenticationToken)
        {
            if(authenticationToken == "test-auth-token")
            {
                Console.WriteLine("Successfully authenticated user");
                return true;
            }
            else
            {
                Console.WriteLine("Failed to authenticate user");
                return false;
            }
        }
    }


    class GreeterActor : ReceiveActor
    {
        public GreeterActor()
        {
            Receive<string>(name =>
            {
                Console.WriteLine("Hello {0}!", name);
                Sender.Tell(String.Format("Hello {0}", name));
            });
        }
    }

    class Cache : ReceiveActor
    {
        public Cache()
        {
            Receive<string>(msg =>
            {
                Console.WriteLine("Received a new message to store in the cache of {0}", Context.System.Name);
            });
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            if(args[0] == "node1")
            {
                var configFile = File.ReadAllText("node1.conf");
                var config = ConfigurationFactory.ParseString(configFile);
                var system = ActorSystem.Create("LocalSystem", config);

                var remoteGreeter = system.ActorSelection("akka.tcp://RemoteSystem@localhost:8081/user/greeter");

                var remoteMessage = remoteGreeter.Ask<string>("Anthony").Result;
                Console.WriteLine("Received a message from a remote actor: {0}", remoteMessage);

                var deploy =
                    Deploy.None
                        .WithScope(new RemoteScope(new Address("akka.tcp", "RemoteSystem", "localhost", 8081)));

                var remoteCacheProps =
                    Props.Create<Cache>().WithDeploy(deploy);

                var remoteCache = system.ActorOf(remoteCacheProps);

                remoteCache.Tell("Test string");

                var localGreeter = system.ActorOf<GreeterActor>("greeter");

                var greeterGroup = new RoundRobinGroup("akka.tcp://LocalSystem@localhost:8080/user/greeter", "akka.tcp://RemoteSystem@localhost:8081/user/greeter");

                var greeterDistributorProps = Props.Create<GreeterActor>().WithRouter(greeterGroup);

                var greeterDistributor = system.ActorOf(greeterDistributorProps);

                greeterDistributor.Tell("Tony");
                greeterDistributor.Tell("Steve");


                var receptionist = system.ActorSelection("akka.tcp://RemoteSystem@localhost:8081/user/receptionist");

                receptionist.Tell(new AuthenticatedMessageEnvelope("Via receptionist", remoteCache, "test-auth-token"));
                receptionist.Tell(new AuthenticatedMessageEnvelope("Via receptionist 2", remoteCache, "different-auth-token"));
            }
            if(args[0] == "node2")
            {
                var configFile = File.ReadAllText("node2.conf");
                var config = ConfigurationFactory.ParseString(configFile);
                var system = ActorSystem.Create("RemoteSystem", config);

                var greeterActor = system.ActorOf<GreeterActor>("greeter");

                var receptionist = system.ActorOf<SystemReceptionist>("receptionist");

                Console.WriteLine("Now running a remote actor system which is listening for connections");
            }

            Console.ReadLine();
        }
    }
}
