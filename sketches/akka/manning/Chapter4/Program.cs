﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter4.Turnstile
{
    public class TicketValidated { }
    public class BarrierPushed { }

    class TurnstileActor : ReceiveActor
    {
        public TurnstileActor()
        {
            Become(Locked);
        }

        public void Locked()
        {
            Receive<TicketValidated>(msg => Become(Unlocked));
            Receive<BarrierPushed>(msg => { });
        }

        public void Unlocked()
        {
            Receive<TicketValidated>(msg => { });
            Receive<BarrierPushed>(msg => Become(Locked));
        }
    }

    public interface ITurnstileState { }
    public class Locked : ITurnstileState
    {
        public static readonly Locked Instance = new Locked();
        private Locked() { }
    }

    public class Unlocked : ITurnstileState
    {
        public static readonly Unlocked Instance = new Unlocked();
        private Unlocked() { }
    }

    public interface ITurnstileData { }
    public class TurnstileData : ITurnstileData { }

    public class TurnstileFsm : FSM<ITurnstileState, ITurnstileData>
    {
        public TurnstileFsm()
        {
            When(Locked.Instance, @event =>
            {
                if (@event.FsmEvent is TicketValidated)
                {
                    Console.WriteLine("Ticket was validated");
                    return GoTo(Unlocked.Instance);
                }
                return Stay();
            });

            When(Unlocked.Instance, @event =>
            {
                if (@event.FsmEvent is BarrierPushed)
                {
                    Console.WriteLine("Barrier was pushed");
                    return GoTo(Locked.Instance);
                }
                return Stay();
            });

            StartWith(Locked.Instance, new TurnstileData());
            Initialize();
        }
    }
}

namespace Chapter4
{
    static class Database
    {
        public static string Get(string key)
        {
            return "Sample Data";
        }
    }
    class GetData
    {
        private readonly string _key;
        public string Key {  get { return _key; } }
        public GetData(string key)
        {
            _key = key;
        }
    }

    class GetDataSuccess
    {
        private readonly string _key;
        private readonly string _data;

        public string Key { get; set; }
        public string Data { get; set; }

        public GetDataSuccess()
        {
        }
    }

    class DatabaseUnavailable { }

    class DatabaseUnreachable { }

    class DatabaseAvailable { }

    class DatabaseActor : ReceiveActor
    {
        public DatabaseActor()
        {
            Become(Reachable);
        }
        public void Reachable()
        {
            this.Receive<GetData>(x =>
            {
                var data = Database.Get(x.Key);
                Sender.Tell(new GetDataSuccess
                    { Key = x.Key, Data = data });
            });
            this.Receive<DatabaseUnavailable>(x => Become(Unreachable));
        }
        public void Unreachable()
        {
            this.Receive<GetData>(x => Sender.Tell(
                            new DatabaseUnreachable()));
            this.Receive<DatabaseAvailable>(x => Become(Reachable));
        }
    }
    class Program
    {
        static async Task AskDatabaseForData(IActorRef actor)
        {
            Console.WriteLine("Asking database actor for key 'Key1'");
            var response = await actor.Ask(new GetData("Key1"));

            if (response is GetDataSuccess)
            {
                var data = (GetDataSuccess)response;
                Console.WriteLine("Successfully retrieved data: {0} = {1}", data.Key, data.Data);
            }
            else if (response is DatabaseUnreachable)
            {
                Console.WriteLine("Cannot currently access database");
            }
        }

        static async Task RunSequence(IActorRef actor)
        {
            await AskDatabaseForData(actor);
            actor.Tell(new DatabaseUnavailable());
            await AskDatabaseForData(actor);
            actor.Tell(new DatabaseAvailable());
            await AskDatabaseForData(actor);
        }

        static void Main(string[] args)
        {
            var actorSystem = ActorSystem.Create("MyActorSystem");
            var actorRef = actorSystem.ActorOf<DatabaseActor>("database");

            RunSequence(actorRef).Wait();

            Console.ReadLine();
        }
    }
}
