﻿namespace todomvc.core
{
    public class NotedEvent { }
    public class EditedEvent { }
    public class TickedOffEvent { }
    public class ResumedEvent { }
    public class DiscardedEvent { }
    public class ArchivedEvent { }
}
