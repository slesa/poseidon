﻿using System;

namespace todomvc.core
{
    public class Todo
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public bool IsTicked { get; set; }
        public bool IsArchived { get; set; }
    }
}
