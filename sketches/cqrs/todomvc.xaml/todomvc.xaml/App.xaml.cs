﻿using Avalonia;
using Avalonia.Markup.Xaml;

namespace todomvc.xaml
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
