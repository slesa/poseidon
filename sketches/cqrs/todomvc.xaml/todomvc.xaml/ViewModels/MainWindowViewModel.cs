﻿using System;
using System.Collections.ObjectModel;
using todomvc.core;

namespace todomvc.xaml.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel()
        {
            Todos = new ObservableCollection<Todo>();
            Todos.Add(new Todo(){Id=Guid.NewGuid(), Text = "Must be done"});
        }

        public string Greeting => "Hello World!";
        public string ItemsLeftText => "0 items left";

        public ObservableCollection<Todo> Todos { get;  }
    }
}
