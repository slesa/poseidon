import { Component, OnInit } from '@angular/core';
import {debounceTime, distinctUntilChanged, filter, Subject, switchMap, tap} from "rxjs";
import {map} from "rxjs/operators";
import {BookStoreService} from "../shared/book-store.service";
import {Book} from "../shared/book";

@Component({
  selector: 'bm-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  foundBooks: Book[] = [];
  keyUp$ = new Subject<any>(); // Todo: event.target.value in html
  isLoading = false

  constructor(private bs: BookStoreService) { }

  ngOnInit(): void {
    this.keyUp$
      .pipe(
      map(e => e.target.value),
      filter(term => term.length >= 3),
      debounceTime(500),
      distinctUntilChanged(),
      tap(() => this.isLoading = true),
      switchMap(searchTerm => this.bs.getAllSearch(searchTerm)),
      tap(() => this.isLoading = false),
    )
    .subscribe(books => this.foundBooks = books);
  }

}
