using System.Threading.Tasks;
using EventStore.ClientAPI;
using System;
using System.Text;

namespace EventStore.Csh
{
    public class Processor
    {
        const string streamName = "newstream";
        const string eventType  = "event-type";
        const string data       = "{ \"a\":\"2\"}";
        const string metadata   = "{}";

        public async Task Run()
        {
            var conn = await Connect();
            await Send(conn);
            await Replay(conn);
        }
        
        async Task<IEventStoreConnection> Connect()
        {
            var connection = EventStoreConnection.Create(
                new Uri("tcp://admin:changeit@localhost:1113"));
            await connection.ConnectAsync();
            return connection;
        }

        async Task Send(IEventStoreConnection conn)
        {
            var eventPayload = new EventData(
                eventId: Guid.NewGuid(),
                type: eventType,
                isJson: true,
                data: Encoding.UTF8.GetBytes(data),
                metadata: Encoding.UTF8.GetBytes(metadata)
            );
            var result = await conn.AppendToStreamAsync(streamName, ExpectedVersion.Any, eventPayload);
        }

        async Task Replay(IEventStoreConnection conn)
        {
            var readEvents = await conn.ReadStreamEventsForwardAsync(streamName, 0, 10, true);

            foreach (var evt in readEvents.Events)
            {
                Console.WriteLine(Encoding.UTF8.GetString(evt.Event.Data));
                Console.WriteLine(Encoding.UTF8.GetString(evt.Event.Metadata));
            }
        }
    }
}