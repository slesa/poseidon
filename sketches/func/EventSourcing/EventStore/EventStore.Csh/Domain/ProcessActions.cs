using System;
    
namespace EventStore.Csh.Domain
{
    public enum ActionType
    {
        Creation, Order, Void, Split, Change, Commit
    }
    
    public class ProcessAction
    {
        internal ProcessAction() {} // For serlalization only
        protected ProcessAction(ActionType type, DateTime timestamp)
        {
            Type = type;
            Timestamp = timestamp;
        }
        public DateTime Timestamp { get; set; }
        public ActionType Type { get; set; }
        public Guid ActionId { get; set; }
        public bool Committed { get; set; }
        
        public int Waiter { get; set; }
        public string WaiterName { get; set; }
        
        public ProcessAction WithActionId(Guid id) { ActionId = id; return this; }
        public ProcessAction WithWaiter(int id, string name)
        {
            Waiter = id;
            WaiterName = name;
            return this;
        }
    }
    
    public class CreatedAction 
    {
        internal CreatedAction() {} // For serialization only
        public CreatedAction(DateTime timestamp, int terminal, string terminalName, int waiter, string waiterName)
        {
            Timestamp = timestamp;
            Terminal = terminal;
            TerminalName = terminalName;
            Waiter = waiter;
            WaiterName = waiterName;
        }
        public DateTime Timestamp { get; set; }
        public int Terminal { get; set; }
        public string TerminalName { get; set; }
        public int Waiter { get; set; }
        public string WaiterName { get; set; }
    }
    
}