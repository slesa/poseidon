using System;

namespace EventStore.Csh
{
    public enum EventType
    {
        Creatied, Ordered
    }
    
    public class DomainEvent
    {
        public EventType Type { get; set; }
        DateTime Timestamp { get; set; }
        public int Terminal { get; set; }
        public string TerminalName { get; set; }
        public int Waiter { get; set; }
        public string WaiterName { get; set; }
        public string Payload { get; set; }
    }

    public class TableCreatedPayload
    {
        public int Table { get; set; }
        public int Party { get; set; }
        public Guid ProcessGuid { get; set; }
    }

    public class CheckoutCreatedPayload
    {
        public int Checkout { get; set; }
        public Guid ProcessGuid { get; set; }
    }
}