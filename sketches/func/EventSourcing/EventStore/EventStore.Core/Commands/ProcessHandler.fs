module Commands.ProcessHandler
open Commands.Command
open Newtonsoft.Json
open Newtonsoft.Json.FSharp
open EventStore.ClientAPI.Conn
open System

type CommandPayload = {
    SessionId: Guid 
}

let getProcesses payload =
    let args = JsonConvert.DeserializeObject<CommandPayload>(payload)
    ()

let getTables payload =
    ()

let getCheckouts payload =
    ()
    
let openTable payload =
    ()

let openCheckout payload =
    ()

let closeProcess payload =
    ()
    
let handleCommand (command: Command) =
    match command with
    | GetProcesses -> getProcesses
    | GetTables -> getTables
    | GetCheckoutss -> getCheckouts
    | OpenTable -> openTable
    | OpenCheckout -> openCheckout
    | CloseProcess -> closeProcess
    