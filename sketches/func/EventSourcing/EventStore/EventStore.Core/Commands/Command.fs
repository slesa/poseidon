module Commands.Command
open Domain.CoreData
open System

module CmdArgs =
    type OpenTable = {
        Center: int
        Waiter: int
        Table: int
        Party: int Option
    }
    type OpenCheckout = {
        Center: int
        Waiter: int
        Checkout: int
    }
    type CloseProcess = {
        Center: int
        Waiter: int
        Process: Guid
    }
    type Order = {
        Center: int
        Waiter: int
        Count: int
        Plu: int
    }
    type VoidPlu = {
        Center: int
        Waiter: int
        Count: int
        Plu: int
    }
    type VoidOrder = {
        Center: int
        Waiter: int
        Count: int
        Plu: int
    }
    type Pay = {
        Given: int
        Currency: Currency Option
        Payform: Payform
    }
    
type Command =
    | GetProcesses
    | GetTables
    | GetCheckouts
    | OpenTable of CmdArgs.OpenTable
    | OpenCheckout of CmdArgs.OpenCheckout
    | CloseProcess of CmdArgs.CloseProcess
    | Order of CmdArgs.Order
    | VoidPlu of CmdArgs.VoidPlu
    | VoidOrder of CmdArgs.VoidOrder
    | Pay of CmdArgs.Pay
    | Split
    | Change
    
module EventArgs =
    type TableCreated = {
        Id: Guid
        ProcessId: Guid
        Table: int
        Party: int Option
        Center: int
        CenterName: string
        Waiter: int
        WaiterName: string
    }
    type CheckoutCreated = {
        Id: Guid
        ProcessId: Guid
        Checkout: int
        Center: int
        CenterName: string
        Waiter: int
        WaiterName: string
    }
    type Ordered = {
        Id: Guid
        ProcessId: Guid
        Count: int
        Plu: int
        Article: string
        Center: int
        CenterName: string
        Waiter: int
        WaiterName: string
    }
    type Voided = {
        Id: Guid
        ProcessId: Guid
        OrderId: Guid
        Plu: int
        Center: int
        CenterName: string
        Waiter: int
        WaiterName: string
    }
    
type Events =
    | TableCreated of EventArgs.TableCreated
    | CheckoutCreated of EventArgs.CheckoutCreated
    | Ordered of EventArgs.Ordered
    | Voided of EventArgs.Voided
    
type State = {
    Articles: Article list
    Waiters: Waiter list
    Terminals: Terminal list
}

let execute state command =
    let event =
        match command with
        | OpenTable args ->
            failwith "Not implemented"
        | OpenCheckout args ->
            failwith "Not implemented"
        | CloseProcess args ->
            failwith "Not implemented"
        | Order args ->
            failwith "Not implemented"
        | VoidPlu args ->
            failwith "Not implemented"
        | VoidOrder args ->
            failwith "Not implemented"
        | Pay args ->
            failwith "Not implemented"
    event |> List.singleton