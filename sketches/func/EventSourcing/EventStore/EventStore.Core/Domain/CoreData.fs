module Domain.CoreData

type ConstraintEntry = {
    Plu: int
    IsHint: bool
    UsePrice: bool
    Price: decimal
}

type ConstraintLevel = {
    Id: int
    IsActive: bool
    IsCloseable: bool
    IsRecursive: bool
    IsTakeCount: bool
    Entries: ConstraintEntry list
}

type Constraint = {
    Plu: int
    Levels: ConstraintLevel list
}

type Article = {
    Plu: int
    Name: string
    IsHint: bool
    Family: int
    Price: decimal
    IsFreePrice: bool
    IsFreeText: bool
    IsWeight: bool
    IsCutGood: bool
    IsAreaGood: bool
    Constraint: Constraint
}

type Currency = {
    Id: int
    Name: string
    Short: string
}

type Payform = {
    Id: int
    Name: string
}

type Waiter = {
    Id: int
    Name: string
}

type Terminal = {
    Id: int
    Name: string
}