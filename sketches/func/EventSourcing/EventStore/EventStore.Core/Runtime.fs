module EventStore.Core.Runtime
open CoreData
open System

type TerminalSession = {
    SessionId: Guid
    Terminal: Terminal
    Waiter: Waiter
}