module Tests

open Expecto
open Events.Commands
open Events.Execute
open Events.Domain.Process

[<Tests>]
let tests =
  testList "When opening a non existing table" [
    let withOpenTableCommand f () =
      let state = State.Init
      let cmd = OpenTable { Table = 3; Party = 2; Waiter = 42; WaiterName = "Herr Schmeller" }
      let events = execute state cmd
      let state = applyAll state events
      f state
    
    yield! testFixture withOpenTableCommand [
      "it should create an open table", fun state -> Expect.equal 1 state.OpenTables.Length "check open tables"
      "it should set table no", fun state -> Expect.equal 3 state.OpenTables.[0].Table "check table id"
      "it should set party no", fun state -> Expect.equal 2 state.OpenTables.[0].Party "check party id"
      "it should set waiter", fun state -> Expect.equal 42 state.OpenTables.[0].Created.Value.Waiter "check waiter id"
      "it should set waiter name", fun state -> Expect.equal "Herr Schmeller" state.OpenTables.[0].Created.Value.WaiterName "check waiter name"
    ]
    (*
    testCase "" <| fun _ ->
      let subject = true
      Expect.isTrue subject "I compute, therefore I am."

    testCase "when true is not (should fail)" <| fun _ ->
      let subject = false
      Expect.isTrue subject "I should fail because the subject is false"

    testCase "I'm skipped (should skip)" <| fun _ ->
      Tests.skiptest "Yup, waiting for a sunny day..."

    testCase "I'm always fail (should fail)" <| fun _ ->
      Tests.failtest "This was expected..."

    testCase "contains things" <| fun _ ->
      Expect.containsAll [| 2; 3; 4 |] [| 2; 4 |]
                         "This is the case; {2,3,4} contains {2,4}"

    testCase "contains things (should fail)" <| fun _ ->
      Expect.containsAll [| 2; 3; 4 |] [| 2; 4; 1 |]
                         "Expecting we have one (1) in there"

    testCase "Sometimes I want to ༼ノಠل͟ಠ༽ノ ︵ ┻━┻" <| fun _ ->
      Expect.equal "abcdëf" "abcdef" "These should equal"

    test "I am (should fail)" {
      "╰〳 ಠ 益 ಠೃ 〵╯" |> Expect.equal true false
    }
    *)
  ]
