module Events.Domain.Process

type TableCreated = {
    Waiter: int
    WaiterName: string
}

type TableProcess = {
    Table: int
    Party: int
    Created: TableCreated option    
}

type State = {
    OpenTables: TableProcess list
}
with static  member Init = { OpenTables = [] }
