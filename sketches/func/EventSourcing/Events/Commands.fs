module Events.Commands

open System

type OpenTableCmd = {
    Table: int
    Party: int
    Waiter: int
    WaiterName: string
}
type OpenTableEvt = {
    Cmd:  OpenTableCmd
    TimeStamp: DateTime
}
type CloseTableCmd = {
    Table: int
    Party: int
}
type CloseTableEvt = {
    Cmd:  CloseTableCmd
    TimeStamp: DateTime
}

type Command =
    | OpenTable of OpenTableCmd 
    | CloseTable of CloseTableCmd

type Event =
    | TableOpened of OpenTableEvt
    | TableCreated of OpenTableEvt
    | TableClosed of CloseTableEvt
    