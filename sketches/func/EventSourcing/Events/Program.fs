﻿// Learn more about F# at http://docs.microsoft.com/dotnet/fsharp
// https://medium.com/@dzoukr/event-sourcing-step-by-step-in-f-be808aa0ca18

open System
open Events.Domain.Process
open Events.Execute

type Aggregate<'state, 'command, 'event> = {    
    Init : 'state    
    Apply: 'state -> 'event -> 'state    
    Execute: 'state -> 'command -> 'event list
}

let tablesAggregate = {
    Init = State.Init
    Execute = execute
    Apply = apply
}

// Define a function to construct a message to print
let from whom =
    sprintf "from %s" whom

[<EntryPoint>]
let main argv =
    let message = from "F#" // Call the function
    printfn "Hello world %s" message
    0 // return an integer exit code
