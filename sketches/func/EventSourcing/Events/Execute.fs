module Events.Execute
open Events.Domain.Process
open Events.Commands
open System

let execute (state: State) (command: Command): Event list =
    let events = seq {
        match command with
        | OpenTable args ->
            let isNew =
                let oldTable = state.OpenTables |> List.tryFind (fun x -> x.Table=args.Table && x.Party=args.Party)
                match oldTable with
                | None -> true
                | Some x ->
                    if (x.Created.IsSome && x.Created.Value.Waiter=args.Waiter)
                    then failwithf ""
                    else false
            yield TableOpened { Cmd = args; TimeStamp = DateTime.Now }
            if isNew then
                yield TableCreated { Cmd = args; TimeStamp = DateTime.Now }
//              (fun _ -> TableOpened { Cmd = args; TimeStamp = DateTime.Now } )
        | CloseTable args ->
            yield TableClosed { Cmd = args; TimeStamp = DateTime.Now }
//            (fun _ -> TableClosed { Cmd = args; TimeStamp = DateTime.Now } )
    }
    events |> List.ofSeq
    
let apply (state: State) (event: Event) =
    let inline replaceTable (tables: TableProcess list) (newTable: TableProcess) =
        tables |> Seq.map (fun x -> if x.Table = newTable.Table && x.Party =  newTable.Party then newTable else x)
    match event with
    | TableOpened args ->
        let newTable: TableProcess = { Table = args.Cmd.Table; Party = args.Cmd.Party; Created = None }
        { state with OpenTables = newTable :: state.OpenTables }
    | TableCreated args ->
        let oldTable = state.OpenTables |> List.tryFind (fun x -> x.Table=args.Cmd.Table && x.Party=args.Cmd.Party)
        if oldTable.Value.Created.IsNone then
            let newTable = { oldTable.Value with Created = Some { Waiter = args.Cmd.Waiter; WaiterName = args.Cmd.WaiterName } }
            let newList = replaceTable state.OpenTables newTable |> List.ofSeq
            { state with OpenTables = newList }
        else
            state
    | TableClosed args ->
        state
    
let rec applyAll (state: State) (events: Event list) =
    match events with
    | head :: tail ->
        applyAll (apply state head) tail 
//    | [event] ->
//        apply state event
    | [] -> state
    