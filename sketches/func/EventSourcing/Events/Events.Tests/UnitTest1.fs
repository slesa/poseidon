module Events.Tests

open NUnit.Framework
open Events.Commands
open Events.Execute
open Events.Domain.Process

[<SetUp>]
let Setup () =
    ()

[<Test>]
let Test1 () =
    let state = State.Init
    let cmd = OpenTable { Table = 1; Party = 1; Waiter = 42; WaiterName = "Herr Schmeller" }
    let events = execute state cmd
    let state = applyAll state events
    
    Assert.AreEqual(1, state.OpenTables.Length)