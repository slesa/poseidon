module Configs.Json
open Configs.Settings
open System.IO
open FsConfig
open Microsoft.Extensions.Configuration

let readConfig =
    let configRoot =
        ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("settings.json").Build()
    let appConfig = new AppConfig(configRoot)
    let config =
        match appConfig.Get<Settings>() with
        | Ok cfg -> cfg
        | Error error ->
            match error with
            | NotFound jsonVar ->
                failwithf $"Json var %s{jsonVar} not found"
            | BadValue (jsonVar, value) ->
                failwithf $"Json var %s{jsonVar} has bad value %s{value}"
            | NotSupported msg ->
                failwith msg
    config
 