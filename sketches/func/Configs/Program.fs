﻿open System
open FSharp.Configuration
open Configs.Settings
type IniFileType = IniFile<"settings.ini">

let printSettings title cfg =
//    let pup = (Environment.GetEnvironmentVariable "MYENV_TOWATCH")
//    printfn $"{pup}"
    printfn $"===[ {title}  ]============================"
    printfn "--- General -------------------------"
    printfn $"Terminal.......: {cfg.Terminal}"
    printfn $"To watch.......: {cfg.ToWatch}"
    printfn "--- Log -----------------------------"
    printfn $"Level..........: {cfg.Log.Level}"
    printfn $"File...........: {cfg.Log.File}"
    printfn "--- Db ------------------------------"
    printfn $"Type...........: {cfg.Db.DbType}"
    printfn $"Host...........: {cfg.Db.Host}"
    printfn $"Port...........: {cfg.Db.Port}"
    printfn $"User...........: {cfg.Db.User}"
    printfn $"Pass...........: {cfg.Db.Pass}"
    printfn "---------------------------- Done ---"


[<EntryPoint>]
let main argv =
    
    let envSettings = Configs.Env.readConfig
    printSettings "Environment" envSettings

    let jsonSettings = Configs.Json.readConfig
    printSettings "JSON" jsonSettings

    let xmlSettings = Configs.Xml.readConfig
    printSettings "XML" xmlSettings

    let iniSettings = Configs.Ini.readConfig
    printSettings "INI" iniSettings
    
    (*
    printfn "Terminal is %s" IniFileType.General.Terminal
    printfn "Host is %s" IniFileType.General.Host
    printfn "Port is %d" IniFileType.General.Port
    printfn "Loglevel is %s" IniFileType.General.LogLevel
    
    printfn "Database type is %s" IniFileType.Database.Type
    *)
    0 // return an integer exit code