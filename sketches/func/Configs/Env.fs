module Configs.Env
open Configs.Settings
open FsConfig

let readConfig =
    let config =
        match EnvConfig.Get<Settings>() with
        | Ok cfg -> cfg
        | Error error ->
            match error with
            | NotFound envVar ->
                failwithf $"Env var %s{envVar} not found"
            | BadValue (envVar, value) ->
                failwithf $"Env var %s{envVar} has bad value %s{value}"
            | NotSupported msg ->
                failwith msg
    config
 