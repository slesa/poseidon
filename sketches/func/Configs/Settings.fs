module Configs.Settings
open FsConfig

type DbType = MySQL | MSSql | SQLite | PostGres

type DbSettings = {
    [<DefaultValue("MSSql")>]
    DbType: DbType
    [<DefaultValue("localhost")>]
    Host: string
    [<DefaultValue("8080")>]
    Port: int
    [<DefaultValue("admin")>]
    User: string
    [<DefaultValue("")>]
    Pass: string
}

type LogLevel = LogNone | LogError | LogWarning | LogInfo | LogDebug

type LogSettings = {
    [<DefaultValue("LogInfo")>]
    Level: LogLevel
    [<DefaultValue("logfile.log")>]
    File: string
}

[<Convention("MYENV")>]
type Settings = {
    Terminal: int
    [<ListSeparator('|')>]
    ToWatch: int list
    Log: LogSettings
    Db: DbSettings
}

