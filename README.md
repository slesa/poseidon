# Poseidon

Generating a POS system, based on CQRS and Event Storing. A journey...


## Folders
- src.fs
- src.qt
- src.ts


## Services
- [ ] Shared
- [ ] CoreData
- [ ] BonMonitor
- [ ] PosKernel
- [ ] TextMaker
- [ ] DeviceKernel


## Service Modules
- [ ] Shared
- [ ] Settings
- [ ] Database
- [ ] Editor
- [ ] Function
- [ ] Rest
- [ ] WebSocket
- [ ] ESB


## Functionality F#
- ✅ Logging
- ✅ Konfiguration
      Env > Ini > Args
- [ ] Datenbank
- ✅ Daemon
- ✅ DirWatcher
- [ ] XML Reader
- [ ] AutoMapper
- [ ] Dat-Reader
- [ ] Service-Module



## BonMonitor

- BonKernel      F# Suave / Giraffe
  * [ ] /settings
  * [ ] /clients
  * [ ] /bons
  * [ ] /events
  * [ ] /courses
- (BonService     C# ASP.NET)
- ConClient      C# Console
- (WpfClient     C# WPF)
- FableClient    F#
- BonClient      C++
- AnguClient     TS


## POS
- PosKernel      F# Suave / Giraffe
- PosService     C# ASP.NET
- ConClient      C# Console
- (WpfClient     C# WPF)
- FableClient    F#
- PosClient      C++
- AnguClient     TS

### Funktionen

#### Turn on / turn off terminal
  - ✅ Service, Spec
  - ✅ Service, Rest-Controller 
  - ✅ Console, manual
  - ✅ Console, automatic
  - ✅ Client, manual
  - ✅ Client, automatic


#### Login / Logout waiter
  - ✅ Service, Spec
  - ✅ Service, Rest-Controller 
  - ✅ Console, manual
  - ✅ Console, automatic
  - ✅ Client, manual
  - ✅ Client, automatic

